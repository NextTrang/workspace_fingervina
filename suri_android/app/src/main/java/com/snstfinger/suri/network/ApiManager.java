package com.snstfinger.suri.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class ApiManager {
    private static String URL = "http://101.99.34.53:1114/repairsystem/";
//    private static String URL = "http://192.168.1.3:8081/";

    private static Retrofit retrofit;

//    public static Retrofit getConnection() {
//        retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
//        return retrofit;
//    }

    public static <T> T getProvider(Class<T> service) {
        // log
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logger).build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().client(client).baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }

        return retrofit.create(service);
    }
}
