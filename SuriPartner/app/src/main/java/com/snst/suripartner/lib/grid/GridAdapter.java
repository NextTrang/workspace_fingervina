package com.snst.suripartner.lib.grid;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.snst.suripartner.GalleryActivity;
import com.snst.suripartner.R;
import com.snst.suripartner.lib.Command;
import com.snst.suripartner.lib.gallery_cmd.CloseGallery;
import com.snst.suripartner.lib.gallery_cmd.Gallery;
import com.snst.suripartner.lib.gallery_cmd.GalleryInvoker;

import java.io.File;
import java.util.List;

public class GridAdapter extends BaseAdapter {
    private List<GridModel> gridModelList;
    private Context mContext;

    public GridAdapter(Context mContext, List<GridModel> gridModelList) {
        this.gridModelList = gridModelList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return gridModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return gridModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        ImageView gridImage = convertView.findViewById(R.id.gridImage);
        gridModelList.get(position).setImageView(gridImage);
        ImageView imageView = gridModelList.get(position).getImageView();
        final File photoFile = gridModelList.get(position).getImageFile();

        if (position == 0) {
            //load camera image
            Glide.with(mContext).asDrawable()
                    .load(R.drawable.ic_video_call_black_24dp)
                    .thumbnail(0.1f)
                    .centerCrop()
                    .into(imageView);
        } else {
            Glide.with(mContext)
                    .asBitmap()
                    .load(photoFile)
                    .thumbnail(0.1f)
                    .centerCrop()
                    .into(imageView);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gallery gallery = new Gallery()
                        .setPhotoFile(photoFile)
                        .setOnClickCamera(position == 0);
                Command close = new CloseGallery((GalleryActivity) mContext, gallery);
                GalleryInvoker invoker = new GalleryInvoker(null, close);
                invoker.doClose();
            }
        });

        return convertView;
    }
}
