package com.snst.suripartner.lib.quotation;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.pop_up.PopManagerActivity;
import com.snst.suripartner.databinding.QuotationBinding;
import com.snst.suripartner.labels.LbPop;

import java.util.List;

public class QuotationAdapter extends RecyclerView.Adapter<QuotationAdapter.MyViewHolder> implements QuotationNavigator {

    private Context mContext;
    private List<QuotationViewModel> mData;
    private LayoutInflater inflater;

    public QuotationAdapter(Context mContext, List<QuotationViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        QuotationBinding quotationBinding = QuotationBinding.inflate(inflater, parent, false);

        return new MyViewHolder(quotationBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        mData.get(position).setNavigator(this);
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    @Override
    public void onItemClick(QuotationViewModel viewModel) {
        Intent intent = new Intent(mContext, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.QUOTATION_DETAIL);
        intent.putExtra(LbPop.LbKey.QUOTATION, viewModel);
        ((AppCompatActivity) mContext).startActivityForResult(intent, LbPop.LbCode.QUOTATION_DETAIL);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private QuotationBinding quotationBinding;

        public MyViewHolder(QuotationBinding quotationBinding) {
            super(quotationBinding.getRoot());
            this.quotationBinding = quotationBinding;
        }

        public void bind(QuotationViewModel quotationViewModel) {
            quotationBinding.setQuotationViewModel(quotationViewModel);
        }
    }
}
