package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.data.model.ShopInfo;

import org.w3c.dom.Text;

public class ShopInfoAllReviewActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = ShopInfoAllReviewActivity.class.getName();

    private RequestNPredictionNStoreEntity mRequestEstimate;
//    private StoreNPredictionEntity mShopInfoNPrediction;
    private ShopInfo mShopInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_info_review);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
//        mShopInfoNPrediction = (StoreNPredictionEntity) estimateStatus.getSerializableExtra("shop_prediction_info");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        TextView point_store = findViewById(R.id.point_store);
        TextView point_quality = findViewById(R.id.point_quality);
        TextView point_kindness = findViewById(R.id.point_kindness);
        TextView point_accuracy = findViewById(R.id.point_accuracy);

        point_store.setText(mShopInfo.pointScore);
        point_quality.setText(mShopInfo.pointQuality);
        point_kindness.setText(mShopInfo.pointKindness);
        point_accuracy.setText(mShopInfo.pointAccuracy);

        Button doAction = findViewById(R.id.btn_action);
        doAction.setOnClickListener(this);

        actionBar.setTitle(R.string.title_activity_shop_info_rate);
        if (mRequestEstimate != null) {
            String status = mRequestEstimate.status;

            switch (status) {
                case "BOOKING":
                case "BOOKING_CONFIRM": {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    break;
                }

                case "REQUEST": {
//                    doAction.setText(R.string.btn_shop_book);
//                    doAction.setVisibility(View.VISIBLE);
                }

                case "SENDING_PAYMENT":
                case "REPAIR_STANDBY":
                case "REPAIR":
                case "SENDING_PAYMENT_UPDATE":
                case "COMPLETE":
                case "REVIEW":
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_action) {
            // desiredDate YN
            // no : warning popup
            // yes : go booking
            if(mShopInfo.desiredReservationDateYN.equalsIgnoreCase("y")) {

            } else {

            }
        }
    }
}
