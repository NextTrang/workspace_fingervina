package com.snst.suripartner.lib.gallery_cmd;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.lib.Command;

public class OpenGallery implements Command {

    private Gallery gallery;
    private AppCompatActivity context;

    public OpenGallery(AppCompatActivity context, Gallery gallery) {
        this.gallery = gallery;
        this.context = context;
    }

    @Override
    public void execute() {
        gallery.open(context);
    }
}
