package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.model.ShopInfo;

public class ShopInfoAllCaseActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = ShopInfoAllCaseActivity.class.getName();

    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_info_case);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        Button doAction = findViewById(R.id.btn_action);
        doAction.setOnClickListener(this);

        actionBar.setTitle(R.string.title_activity_shop_info_case);
        if (mRequestEstimate != null) {
            String status = mRequestEstimate.status;

            switch (status) {
                case "BOOKING":
                case "BOOKING_CONFIRM": {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    break;
                }

                case "REQUEST": {
//                    doAction.setText(R.string.btn_shop_book);
//                    doAction.setVisibility(View.VISIBLE);
                }

                case "SENDING_PAYMENT":
                case "REPAIR_STANDBY":
                case "REPAIR":
                case "SENDING_PAYMENT_UPDATE":
                case "COMPLETE":
                case "REVIEW":
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_action) {
            // desiredDate YN
            // no : warning popup
            // yes : go booking
            if(mShopInfo.desiredReservationDateYN.equalsIgnoreCase("y")) {

            } else {

            }
        }
    }
}
