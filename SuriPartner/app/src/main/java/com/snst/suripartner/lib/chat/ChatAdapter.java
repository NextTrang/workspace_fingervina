package com.snst.suripartner.lib.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.pop_up.PopChatActivity;
import com.snst.suripartner.databinding.ChatBinding;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> implements ChatNavigator {

    private Context mContext;
    private List<ChatViewModel> mData;
    private LayoutInflater inflater;

    public ChatAdapter(Context mContext, List<ChatViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        ChatBinding chatBinding = ChatBinding.inflate(inflater, parent, false);

        return new ChatAdapter.MyViewHolder(chatBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        mData.get(position).setNavigator(this);
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    @Override
    public void onItemClick(Chat chat) {
        Intent intent = new Intent(mContext, PopChatActivity.class);
        intent.putExtra("Chat", chat);
        mContext.startActivity(intent);
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ChatBinding chatBinding;

        public MyViewHolder(ChatBinding chatBinding) {
            super(chatBinding.getRoot());
            this.chatBinding = chatBinding;
        }

        public void bind(ChatViewModel chatViewModel) {
            chatBinding.setChatViewModel(chatViewModel);
            chatBinding.setChat(new Chat(chatViewModel));
        }
    }
}
