package com.snstfinger.suri.ui.home;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.repository.RequestEstimateRepository;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;

import java.util.List;

public class MySuriViewModel extends AndroidViewModel {
    public static final String TAG = MySuriViewModel.class.getName();

    private RequestEstimateRepository repository;
    private MutableLiveData<RequestNPredictionNStoreEntity> mainSuri = new MutableLiveData<>();

    public MySuriViewModel(@NonNull Application application) {
        super(application);
        repository = new RequestEstimateRepository();
    }

    @SuppressLint("NewApi")

    public void loadMySur(String userId) {
        MySuriInfoSharedPreferences pref = MySuriInfoSharedPreferences.getInstance(getApplication());
        LiveData<List<RequestNPredictionNStoreEntity>> mySuriList = repository.getAllRquestEstimate(userId);
        Log.d(TAG, "MySURI LIST::"+mySuriList.getValue());
//        mySuriList.getValue().stream().forEach(request -> {
//                    String menu_main = pref.getReqeustId();
//                    if (TextUtils.isEmpty(menu_main)) {
//                        mainSuri.setValue(request);
//                        return;
//
//                    } else {
//                        if (menu_main.equals(request.requestEstimateId)) {
//                            mainSuri.setValue(request);
//                            return;
//                        }
//                    }
//                }
//        );
    }

    @SuppressLint("NewApi")
    public LiveData<List<RequestNPredictionNStoreEntity>> getAllRquestEstimate(String userId) {
        return repository.getAllRquestEstimate(userId);
    }

    public LiveData<RequestNPredictionNStoreEntity> getMainSuri() {
        return mainSuri;
    }

    public void setMainSuri(RequestNPredictionNStoreEntity mySuri) {
        mainSuri.setValue(mySuri);
    }
}
