package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.suri.SuriViewModel;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestSendPayment implements ApiRequest {

    private static final String TAG = "RequestSendPayment";

    @Override
    public void execute(final Context context, String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Cookie", Sharing.Instance().getCookie());
        headerMap.put("Content-Type", "application/json");

        JsonParser parser = new JsonParser();
        final JsonObject jsInput = (JsonObject) parser.parse(inputs[0]);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendPayment(headerMap, jsInput);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        String idInput = jsInput.get(LbData.LbInfo.REQUEST_ESTIMATE_ID).getAsString();
                        for (SuriViewModel viewModel : Sharing.Instance().getSuriViewModels()) {
                            if (viewModel.id.equals(idInput)) {
                                viewModel.stateMask = LbState.LbMask.WAITING_CONFIRM;
                                break;
                            }
                        }

                        //refresh current tab
                        ((MainActivity) context).refresh();
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
