package com.snstfinger.suri.data.entity;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.BR;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestNPredictionNStoreEntity extends BaseObservable implements Serializable {
    public static final int TYPE_REQUEST = 1;
    public static final int TYPE_BOOKING = 2;
    public static final int TYPE_BOOKING_CONFIRM = 3;
    public static final int TYPE_SENDING_PAYMENT = 4;
    public static final int TYPE_REPAIR_STANDBY = 5;
    public static final int TYPE_REPAIR = 6;
    public static final int TYPE_SENDING_PAYMENT_UPDATE = 7;
    public static final int TYPE_COMPLETE = 8;
    public static final int TYPE_REVIEW = 9;

    @SerializedName("request_estimate_id")
    public String requestEstimateId;

    @SerializedName("object_id")
    public int objectId;

    @SerializedName("estimate_image1")
    public String estimateImage1;

    @SerializedName("estimate_image2")
    public String estimateImage2;

    @SerializedName("estimate_image3")
    public String estimateImage3;

    @SerializedName("estimate_image4")
    public String estimateImage4;

    @SerializedName("vehicle_number")
    public String vehicleNumber;

    @SerializedName("estimate_description")
    public String estimateDescription;

    @SerializedName("brand_name")
    public String brandName;

    @SerializedName("model_name")
    public String modelName;

    @SerializedName("estimate_repair_type_id")
    public String estimateRepairTypeId;

    @SerializedName("estimate_repair_option_id")
    public String estimateRepairOptionId;

    @SerializedName("estimate_address_id_1")
    public String estimateAddressId1;

    @SerializedName("estimate_address_id_2")
    public String estimateAddressId2;

    @SerializedName("estimate_promotion_code")
    public String estimatePromotionCode;

    @SerializedName("estimate_phonenumber")
    public String estimatePhonenumber;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date registerDate;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("prediction_estimate_id")
    public String predictionEstimateId;

    @SerializedName("status")
    public String status;

    @SerializedName("desired_reservation_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date desiredReservationDate;

    @SerializedName("object_name")
    public String objectName;

    @SerializedName("user_name")
    public String userName;

    @SerializedName("estimate_address_1")
    public String estimateAddress1;

    @SerializedName("estimate_address_2")
    public String estimateAddress2;

    @SerializedName("estimate_address_ll_1")
    public String estimateAddressGeoCode1;

    @SerializedName("estimate_address_ll_2")
    public String estimateAddressGeoCode2;

    @SerializedName("estimate_repair_type_ids")
    public String estimateRepairTypeIds;

    @SerializedName("estimate_repair_type_names")
    public String estimateRepairTypeNames;

    @SerializedName("estimate_repair_option_ids")
    public String estimateRepairOptionIds;

    @SerializedName("estimate_repair_option_names")
    public String estimateRepairOptionNames;

    @SerializedName("store_id")
    public String storeId;

    @SerializedName("prediction_price")
    public String predictionPrice;

    @SerializedName("prediction_price_comment")
    public String predictionPriceComment;

    @SerializedName("price")
    public String price;

    @SerializedName("prediction_estimate_time")
    public String predictionEstimateTime;

    @SerializedName("prediction_estimate_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionEstimateDate;

    @SerializedName("prediction_start_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionStartDate;

    @SerializedName("prediction_end_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionEndDate;

    @SerializedName("progress_rate")
    private String progressRate;

    @SerializedName("realtime_streaming")
    public String realtimeStreaming;

    @SerializedName("booking_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date bookingDate;

    @SerializedName("booker_name")
    public String bookingName;

    @SerializedName("booker_phonenumber")
    public String bookerPhonenumber;

    @SerializedName("prediction_image1")
    public String predictionImage1;

    @SerializedName("prediction_image2")
    public String predictionImage2;

    @SerializedName("manager_id")
    public String managerId;

    @SerializedName("desired_reservation_date_yn")
    public String desiredReservationDateYN;

    @SerializedName("repair_comment")
    public String repairComment;

    @SerializedName("client_read_yn")
    public String clientReadYN;

    @SerializedName("distance")
    public String distance;

    @SerializedName("store_name")
    public String storeName;

    @SerializedName("store_number")
    public String storeNumber;

    @SerializedName("store_manager_name")
    public String storeManagerName;

    @SerializedName("store_manager_id")
    public String storeManagerId;

    @SerializedName("store_image")
    public String storeImage;

    @SerializedName("store_description")
    public String storeDescription;

    @SerializedName("store_satisfaction")
    public String storeSatisfaction;

    @SerializedName("point_quality")
    public String pointQuality;

    @SerializedName("point_kindness")
    public String pointKindness;

    @SerializedName("point_accuracy")
    public String pointAccuracy;

    // for store rating
    @SerializedName("point_score")
    public String pointScore;

    @SerializedName("review_total")
    public String reviewTotal;

    @SerializedName("city")
    public String city;

    @SerializedName("district")
    public String district;

    @SerializedName("address")
    public String address;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

    @SerializedName("service_phone")
    public String servicePhone;

    @SerializedName("service_pc")
    public String servicePc;

    @SerializedName("service_bike")
    public String serviceBike;

    @SerializedName("service_car")
    public String serviceCar;

    @SerializedName("store_url")
    public String storeUrl;

    @SerializedName("service_pickup")
    public String servicePickup;

    @SerializedName("service_engineer_visiting")
    public String serviceEngineerVisiting;

    @SerializedName("store_business_number")
    public String storeBusinessNumber;

    @SerializedName("store_business_number_image")
    public String storeBusinessNumberImage;

    @SerializedName("store_engineer")
    public String storeEngineer;

    @SerializedName("score")
    public String score;

    public String numOfMatchStore;

    public String getRegisterDateFormatted() {
//        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
//        return registerDate == null ? "" : formatter.format(registerDate);

        return registerDate == null ? "" : Utils.getPastTimeFormatted(registerDate);
    }

    public String getDesiredReservationDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return desiredReservationDate == null ? "" : formatter.format(desiredReservationDate);
    }

    public String getPredictionEstimateDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionEstimateDate == null ? "" : formatter.format(predictionEstimateDate);
    }

    public String getPredictionStartDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionStartDate == null ? "" : formatter.format(predictionStartDate);
    }

    public String getPredictionEndDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionEndDate == null ? "" : formatter.format(predictionEndDate);
    }

    public String getBookingDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return bookingDate == null ? "" : formatter.format(bookingDate);
    }

    public String getPredictionPriceFormatted() {
        int status = getStatusCode();
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        if (status > TYPE_SENDING_PAYMENT) {
            return TextUtils.isEmpty(price) ? "0" : formatter.format(Float.parseFloat(price));
        }

        return TextUtils.isEmpty(predictionPrice) ? "0" : formatter.format(Float.parseFloat(predictionPrice));
    }

    public String getPriceFormatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(price) ? "0" : formatter.format(Float.parseFloat(price));
    }

    @Bindable
    private int statusCode = getStatusCode();

    public int getStatusCode() {
        if (status == null)
            return 0;
        else
            switch (status) {
                case "REQUEST":
                    return TYPE_REQUEST;

                case "BOOKING":
                    return TYPE_BOOKING;

                case "BOOKING_CONFIRM":
                    return TYPE_BOOKING_CONFIRM;

                case "SENDING_PAYMENT":
                    return TYPE_SENDING_PAYMENT;

                case "REPAIR_STANDBY":
                    return TYPE_REPAIR_STANDBY;

                case "REPAIR":
                    return TYPE_REPAIR;

                case "SENDING_PAYMENT_UPDATE":
                    return TYPE_SENDING_PAYMENT_UPDATE;

                case "COMPLETE":
                    return TYPE_COMPLETE;

                case "REVIEW":
                    return TYPE_REVIEW;

                default:
                    return 0;
            }
    }

    public boolean isShowInvoice() {
        int status = getStatusCode();
        boolean ret = (status == TYPE_SENDING_PAYMENT) || (status == TYPE_SENDING_PAYMENT_UPDATE);
        return ret;
    }

    static final String BASE_URL = "http://101.99.34.53:1114/repairsystem";

    @BindingAdapter(value = {"lazyImage", "defaultImage", "isCircle"})
    public static void loadImage(ImageView imageView, String imageURL, Drawable res, boolean isCircle) {
        Log.d("Lazy IMG","StoreIMG::"+imageURL);
        String resURL = "";
        if(imageURL != null) {
            String path[] = imageURL.split("&");
            resURL = BASE_URL+imageURL;
        }

        RequestManager loader = Glide.with(imageView.getContext());
        if(isCircle)
            loader.setDefaultRequestOptions(new RequestOptions().circleCrop());
        else
            loader.setDefaultRequestOptions(new RequestOptions().fitCenter());

        loader
                .load(resURL)
                .placeholder(res)
                .into(imageView);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("RequestEstimate Info::");
        sb.append(requestEstimateId);
        sb.append("::");
        sb.append(status);
        sb.append("::");
        sb.append(userId);

        return sb.toString();
    }

    public int getProgressRate() {
        int ret = 0;
        if(!TextUtils.isEmpty(progressRate)) {
            ret = (int) Float.parseFloat(progressRate);
        }

        return ret;
    }

    public void setProgressRate(String progressRate) {
        this.progressRate = progressRate;
    }

    public void setStatusCode(String status) {
        this.status = status;
        this.statusCode = getStatusCode();
        notifyPropertyChanged(BR.statusCode);
    }
}
