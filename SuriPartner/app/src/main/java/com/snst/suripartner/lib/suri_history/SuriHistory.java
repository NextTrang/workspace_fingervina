package com.snst.suripartner.lib.suri_history;

import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class SuriHistory extends BaseObservable implements Serializable {
    private static final String TAG = "SuriHistory";

    private String id;
    private String predictEstId;
    private String timeStamp = "";
    private String title = "";
    private String content = "";
    private String imageUrl;

    public SuriHistory() {
    }

    public SuriHistory(SuriHistoryViewModel viewModel) {
        this.id = viewModel.id;
        this.predictEstId = viewModel.predictEstId;
        this.timeStamp = viewModel.timeStamp;
        this.title = viewModel.title;
        this.content = viewModel.content;
        this.imageUrl = viewModel.imageUrl;
    }

    public SuriHistory(JSONObject jsInfo) {
        try {
            id = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            predictEstId = jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_ID);
            timeStamp = jsInfo.getString(LbData.LbInfo.REGISTER_DATE);
            title = jsInfo.getString(LbData.LbInfo.TITLE);
            content = jsInfo.getString(LbData.LbInfo.CONTENT);
            imageUrl = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.SURI_IMAGE1);

        } catch (JSONException e) {
            Log.e(TAG, "SuriHistory: " + e.getMessage());
        }
    }

    //Getter
    public String getTimeStamp() {
        return timeStamp;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    @Bindable
    public String getContent() {
        return content;
    }

    @Bindable
    public String getImageUrl() {
        return imageUrl;
    }

    @Bindable
    public String getId() {
        return id;
    }

    @Bindable
    public String getPredictEstId() {
        return predictEstId;
    }

    //Setter
    public SuriHistory setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(com.snst.suripartner.BR.title);
        return this;
    }

    public SuriHistory setContent(String content) {
        this.content = content;
        notifyPropertyChanged(com.snst.suripartner.BR.content);
        return this;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(com.snst.suripartner.BR.imageUrl);
    }

    public SuriHistory setId(String id) {
        this.id = id;
        return this;
    }

    public SuriHistory setPredictEstId(String predictEstId) {
        this.predictEstId = predictEstId;
        return this;
    }
}
