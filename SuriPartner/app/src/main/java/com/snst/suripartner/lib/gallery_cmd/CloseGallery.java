package com.snst.suripartner.lib.gallery_cmd;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.lib.Command;

public class CloseGallery implements Command {

    private AppCompatActivity context;
    private Gallery gallery;

    public CloseGallery(AppCompatActivity context, Gallery gallery) {
        this.context = context;
        this.gallery = gallery;
    }

    @Override
    public void execute() {
        gallery.close(context);
    }
}
