package com.snst.suripartner.lib.helper;

import java.util.Calendar;
import java.util.Date;

public class Checker {

    public static boolean isOutOfDate(Date deadline){
        if(deadline == null){
            return false;
        }

        Date currentTime = Calendar.getInstance().getTime();
        if(currentTime.after(deadline)){
            return true;
        }

        return false;
    }
}
