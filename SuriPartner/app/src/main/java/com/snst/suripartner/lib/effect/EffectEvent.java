package com.snst.suripartner.lib.effect;

import android.content.Context;
import android.view.View;

public class EffectEvent {
    Context context;

    public EffectEvent(Context context) {
        this.context = context;
    }

    public void visibleProgressBar(Effect effect){
        effect.setVisibleProgressBar(View.VISIBLE);
    }

    public void invisibleProgressBar(Effect effect){
        effect.setVisibleProgressBar(View.INVISIBLE);
    }
}
