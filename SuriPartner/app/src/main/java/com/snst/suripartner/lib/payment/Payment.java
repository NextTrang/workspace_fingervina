package com.snst.suripartner.lib.payment;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.BR;
import com.snst.suripartner.lib.content_price.ContentPrice;
import com.snst.suripartner.lib.helper.Conversion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Payment extends BaseObservable implements Serializable {
    //do not permit modify total prices from outside
    private String totalSuriPrice = "0";
    private String totalSuriPriceCurrency = "";
    private String totalPartPrice = "0";
    private String totalPartPriceCurrency = "";
    private String totalOptionPrice = "0";
    private String totalOptionPriceCurrency = "";
    private String totalPrice = "0";
    private String totalPriceCurrency = "";

    private String estCompletedDate;
    private String estCompletedTime;

    private List<ContentPrice> suriPrices = new ArrayList<>();
    private List<ContentPrice> partPrices = new ArrayList<>();
    private List<ContentPrice> optionPrices = new ArrayList<>();

    private String predictEstId;
    private String RequestEstId;

    //Getter
    public String getTotalSuriPrice() {
        return totalSuriPrice;
    }

    public String getTotalPartPrice() {
        return totalPartPrice;
    }

    public String getTotalOptionPrice() {
        return totalOptionPrice;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    @Bindable
    public String getTotalPriceCurrency() {
        return totalPriceCurrency;
    }

    @Bindable
    public String getTotalSuriPriceCurrency() {
        return totalSuriPriceCurrency;
    }

    @Bindable
    public String getTotalPartPriceCurrency() {
        return totalPartPriceCurrency;
    }

    @Bindable
    public String getTotalOptionPriceCurrency() {
        return totalOptionPriceCurrency;
    }

    public List<ContentPrice> getSuriPrices() {
        return suriPrices;
    }

    public List<ContentPrice> getPartPrices() {
        return partPrices;
    }

    public List<ContentPrice> getOptionPrices() {
        return optionPrices;
    }

    public String getPredictEstId() {
        return predictEstId;
    }

    public String getRequestEstId() {
        return RequestEstId;
    }

    public String getEstCompletedDate() {
        return estCompletedDate;
    }

    public String getEstCompletedTime() {
        return estCompletedTime;
    }

    //Setter
    public void updateTotalPrice() {
        int totalPrice = Integer.valueOf(totalSuriPrice) + Integer.valueOf(totalPartPrice) + Integer.valueOf(totalOptionPrice);
        this.totalPrice = String.valueOf(totalPrice);
        this.totalPriceCurrency = Conversion.number_2_currency(this.totalPrice);
        notifyPropertyChanged(BR.totalPriceCurrency);
    }

    public void setEstCompletedDate(String estCompletedDate) {
        this.estCompletedDate = estCompletedDate;
    }

    public void setEstCompletedTime(String estCompletedTime) {
        this.estCompletedTime = estCompletedTime;
    }

    public void setSuriPrices(List<ContentPrice> suriPrices) {
        this.suriPrices = suriPrices;
        int total = new PaymentEvent(null).getTotal(suriPrices);
        totalSuriPrice = String.valueOf(total);
        totalSuriPriceCurrency = Conversion.number_2_currency(totalSuriPrice);
        notifyPropertyChanged(BR.totalSuriPriceCurrency);
    }

    public void setPartPrices(List<ContentPrice> partPrices) {
        this.partPrices = partPrices;
        int total = new PaymentEvent(null).getTotal(partPrices);
        totalPartPrice = String.valueOf(total);
        totalPartPriceCurrency = Conversion.number_2_currency(totalPartPrice);
        notifyPropertyChanged(BR.totalPartPriceCurrency);
    }

    public void setOptionPrices(List<ContentPrice> optionPrices) {
        this.optionPrices = optionPrices;
        int total = new PaymentEvent(null).getTotal(optionPrices);
        totalOptionPrice = String.valueOf(total);
        totalOptionPriceCurrency = Conversion.number_2_currency(totalOptionPrice);
        notifyPropertyChanged(BR.totalOptionPriceCurrency);
    }

    public Payment setPredictEstId(String predictEstId) {
        this.predictEstId = predictEstId;
        return this;
    }

    public Payment setRequestEstId(String requestEstId) {
        RequestEstId = requestEstId;
        return this;
    }
}
