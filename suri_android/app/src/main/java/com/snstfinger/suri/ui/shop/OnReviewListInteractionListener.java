package com.snstfinger.suri.ui.shop;

import com.snstfinger.suri.data.entity.ReviewEntity;

public interface OnReviewListInteractionListener {
    void onReviewClick(ReviewEntity item);
}
