package com.snstfinger.suri.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snstfinger.suri.R;
import com.snstfinger.suri.ui.shop.ShopListActivity;

public class SearchShopFragment extends Fragment {

    public SearchShopFragment() {
    }

    public static SearchShopFragment newInstance(String param1, String param2) {
        SearchShopFragment fragment = new SearchShopFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search_shop, container, false);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Search Shop","Clicked card");
                Intent search = new Intent(getActivity(), ShopListActivity.class);
                startActivity(search);

            }
        });

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
