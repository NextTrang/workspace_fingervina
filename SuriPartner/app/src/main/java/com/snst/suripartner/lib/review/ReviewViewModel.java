package com.snst.suripartner.lib.review;

public class ReviewViewModel {
    public String requestEstId;
    public String comment;
    public String score;
    public String imageUrlFirst;
    public String imageUrlSecond;

    public ReviewViewModel (Review review){
        this.requestEstId = review.getRequestEstId();
        this.comment = review.getComment();
        this.score = review.getScore();
        this.imageUrlFirst = review.getImageUrlFirst();
        this.imageUrlSecond = review.getImageUrlSecond();
    }
}
