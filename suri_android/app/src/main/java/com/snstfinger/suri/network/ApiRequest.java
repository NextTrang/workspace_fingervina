package com.snstfinger.suri.network;

import android.content.Context;

public interface ApiRequest {
    void execute(final Context context, String... inputs);
}
