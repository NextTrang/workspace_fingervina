package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.snstfinger.suri.MyFragment;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.databinding.FragmentShopInfoCaseBinding;
import com.snstfinger.suri.labels.LbPop;
import com.snstfinger.suri.pop_up.PopManagerActivity;
import com.snstfinger.suri.ui.tools.ListVewDecorate;

/**
 * 업체 수리사례를 보여줌
 */
public class ShopInfoAllCaseFragment extends Fragment implements OnSuriCaseListInteractionListener, MyFragment {
    public static final String TAG = ShopInfoAllCaseFragment.class.getName();
    private static final String ARG_SHOP_INFO = "shop_info";

    private FragmentShopInfoCaseBinding mBinding;
    private PagedSuriCaseAdapter mAdapter;

    private ShopInfo mShopInfo;
    private SuriCaseViewModel mCaseViewModel;
    private String objectId = "";
    private String brandName = "";
    private String modelName = "";

    public ShopInfoAllCaseFragment() {
    }

    public static ShopInfoAllCaseFragment newInstance(ShopInfo shopInfo) {
        ShopInfoAllCaseFragment fragment = new ShopInfoAllCaseFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOP_INFO, shopInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mShopInfo = (ShopInfo) getArguments().getSerializable(ARG_SHOP_INFO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        RequestNPredictionNStoreEntity mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");
        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        mCaseViewModel = ViewModelProviders.of(this).get(SuriCaseViewModel.class);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_info_case, container, false);
        mAdapter = new PagedSuriCaseAdapter(this, false);
        mBinding.listviewCase.addItemDecoration(new ListVewDecorate(getActivity(), 22, 12));
        mBinding.listviewCase.setAdapter(mAdapter);

        if (mShopInfo == null) {
            getAllSuriCaseByStoreId(null, objectId, brandName, modelName);
        } else {
            getAllSuriCaseByStoreId(String.valueOf(mShopInfo.storeId), objectId, brandName, modelName);
        }

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getAllSuriCaseByStoreId(String storeId, String objectId, String brandName, String modelName) {
        mCaseViewModel.getSuriCasePagedList(storeId, objectId, brandName, modelName).observe(this, new Observer<PagedList<SuriCaseEntity>>() {
            @Override
            public void onChanged(PagedList<SuriCaseEntity> suriCaseEntities) {
                mAdapter.submitList(suriCaseEntities);

                mBinding.listviewCase.setVisibility(View.VISIBLE);
                mBinding.listviewCaseEmpty.setVisibility(View.GONE);

                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onCaseClick(SuriCaseEntity item) {
        Intent caseDetail = new Intent(getActivity(), SuriCaseDetailsActivity.class);
        caseDetail.putExtra("case", item);
        startActivity(caseDetail);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_suri_case, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                Intent intent = new Intent(getContext(), PopManagerActivity.class);
                intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.FILTER_SURI_CASE);
                getActivity().startActivityForResult(intent, LbPop.LbCode.FILTER_SURI_CASE);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    public void updateBrandModel(String objectId, String brandName, String modelName) {
        this.objectId = objectId;
        this.brandName = brandName;
        this.modelName = modelName;
        refresh();
    }

    @Override
    public void refresh() {
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(ShopInfoAllCaseFragment.this)
                    .attach(ShopInfoAllCaseFragment.this)
                    .commit();
        }
    }
}
