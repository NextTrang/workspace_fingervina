package com.snst.suripartner.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.R;

import java.util.ArrayList;
import java.util.Calendar;

public class TimeItemRecycleViewAdapter extends RecyclerView.Adapter<TimeItemRecycleViewAdapter.ViewHolder>{
    private ArrayList<String> mItemList;
    private Context mContext;
    private int checkedPosition = 0;
    private OnSelectTimeInteractionListener onClickListener;
    private boolean enable;

    public TimeItemRecycleViewAdapter(Context context, ArrayList<String> itemList, OnSelectTimeInteractionListener listener,boolean enable) {
        mContext = context;
        mItemList = itemList;
        onClickListener = listener;
        this.enable = enable;
    }

    public void setTimeTable(ArrayList<String> timeTable) {
        mItemList = new ArrayList<>();
        mItemList = timeTable;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_time, parent, false);
        return new ViewHolder(view,enable);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView textView;
        private boolean enable;

        public ViewHolder(View itemView, boolean enable) {
            super(itemView);
            mView = itemView;
            textView = itemView.findViewById(R.id.time);
            this.enable = enable;
        }

        public void bind(final String timeTag) {
            if (checkedPosition == -1) {
                mView.setSelected(false);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    mView.setSelected(true);
                } else {
                    mView.setSelected(false);
                }
            }

            textView.setText(timeTag);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (enable == false) {
                        return;
                    }

                    mView.setSelected(true);
                    if (checkedPosition != getAdapterPosition()) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = getAdapterPosition();
                        onClickListener.onSelectTimeInteraction(timeTag);
                    }
                }
            });
        }
    }

    public void setSelectTime(int position) {
        checkedPosition = position;
        notifyItemChanged(checkedPosition);
    }

    public String getSelected() {
        if (checkedPosition != -1) {
            return mItemList.get(checkedPosition);
        }

        return null;
    }

    public int getPositionFromDate(Calendar date) {
        int hour = date.get(Calendar.HOUR_OF_DAY);
        int min = date.get(Calendar.MINUTE);

        int idx = hour * 2;
        if (min >= 30) {
            idx += 1;
        }

        return idx;
    }

    public interface OnSelectTimeInteractionListener {
        void onSelectTimeInteraction(String item);
    }
}
