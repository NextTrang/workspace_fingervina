package com.snstfinger.suri.data.datasource;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.snstfinger.suri.data.entity.ReviewEntity;

public class ReviewListDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, ReviewEntity>> reviewLiveData = new MutableLiveData<>();
    private String stroeId;

    public ReviewListDataSourceFactory(String stroeId) {
        this.stroeId = stroeId;
    }

    @NonNull
    @Override
    public DataSource<Integer, ReviewEntity> create() {
        ReviewListDataSource reviewListDataSource = new ReviewListDataSource(stroeId);

        reviewLiveData.postValue(reviewListDataSource);

        return reviewListDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, ReviewEntity>> getReviewLiveData() {
        return reviewLiveData;
    }
}
