package com.snstfinger.suri.ui.estimate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.BrandEntity;
import com.snstfinger.suri.data.entity.ModelEntity;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.databinding.ActivitySearchBrandmodelBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.BrandModelAPI;
import com.snstfinger.suri.network.CommonResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchBrandModelActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    public static final String TAG = SearchBrandModelActivity.class.getName();

    private int mPage = 1;
    private boolean lastItemVisibleFlag = false;
    private boolean mLockListView = false;
    private int mRequestDelayTime = 1 * 1000;
    private Intent mRequestIntent;
    private RequestEstimate mRequestForm;
    private String searchType;

    ActivitySearchBrandmodelBinding binding;
    BaseAdapter adapter;
    Handler mHandler = new Handler();

    List arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_brandmodel);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(R.layout.app_bar_view_search);
        View view = actionBar.getCustomView();
        SearchView searchView = view.findViewById(R.id.search_item);


        mRequestIntent = getIntent();
        mRequestForm = (RequestEstimate) mRequestIntent.getSerializableExtra("request_form");
        searchType = mRequestIntent.getStringExtra("search_type");

        if (searchType.equals("brand")) {
            adapter = new SearchBrandListAdapter(arrayList);
            searchView.setQueryHint("Search a Brand");

        } else if (searchType.equals("model")) {
            adapter = new SearchModelListAdapter(arrayList);
            searchView.setQueryHint("Search a Model");
        }
        binding.list.setAdapter(adapter);
        binding.list.setOnItemClickListener(this);
        binding.list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && lastItemVisibleFlag && mLockListView == false) {
//                    progressBar.setVisibility(View.VISIBLE);

                    // next page
                    if (searchType.equals("brand")) {
                        getBrandListFromServer(searchView.getQuery().toString(), mPage++);

                    } else if (searchType.equals("model")) {
                        getModelListFromServer(searchView.getQuery().toString(), mPage++);

                    }
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        searchView.setActivated(true);
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 1) {
                    mHandler.removeCallbacksAndMessages(null);

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (searchType.equals("brand")) {
                                getBrandListFromServer(newText, mPage);

                            } else if (searchType.equals("model")) {
                                getModelListFromServer(newText, mPage);

                            }
                        }
                    }, mRequestDelayTime);
                }

                return true;
            }
        });

    }

    private void getBrandListFromServer(String name, int mPage) {
        mLockListView = true;
        BrandModelAPI brandModelAPI = ApiManager.getProvider(BrandModelAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = brandModelAPI.doGetBrandByNameWithPage(cookie, String.valueOf(mPage), name, mRequestForm.getObjectId().toString());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<BrandEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    ((SearchBrandListAdapter) adapter).mData.addAll(item);
                    adapter.notifyDataSetChanged();

                } else {
                    Log.e(TAG, response.toString());
                }
                mLockListView = false;
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetBrandByName onFailure");
                mLockListView = false;
            }
        });
    }

    private void getModelListFromServer(String name, int mPage) {
        mLockListView = true;
        BrandModelAPI brandModelAPI = ApiManager.getProvider(BrandModelAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = brandModelAPI.doGetModelByNameWithPage(cookie, String.valueOf(mPage), name, mRequestForm.getBrandId().toString());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<ModelEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    ((SearchModelListAdapter) adapter).mData.addAll(item);
                    adapter.notifyDataSetChanged();

                } else {
                    Log.e(TAG, response.toString());
                }
                mLockListView = false;
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetModelByName onFailure");
                mLockListView = false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object item = adapter.getItem(position);
        if (item instanceof BrandEntity) {
            Log.d(TAG, "Selected ITEM ::" + ((BrandEntity) item).brandName);
            mRequestForm.setBrandName(((BrandEntity) item).brandName);
            mRequestForm.setBrandId(((BrandEntity) item).brandId);

        } else if (item instanceof ModelEntity) {
            Log.d(TAG, "Selected ITEM ::" + ((ModelEntity) item).modelName);
            mRequestForm.setModelName(((ModelEntity) item).modelName);
            mRequestForm.setModelId(((ModelEntity) item).modelId);
        }

        setResult(RESULT_OK, mRequestIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            case R.id.action_close: {

                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
