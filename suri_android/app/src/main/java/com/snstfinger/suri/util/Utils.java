package com.snstfinger.suri.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class Utils {
    public static final String NUMBER_FORMAT = "###,###";
    public static final String FLOAT_FORMAT = "#,##0.##";
    public static final String DATE_FORMAT = "HH:mm dd/MM/yyyy";
    public static final String DATE_FORMAT_SERVER = "yyyy-MM-dd HH:mm:ss";

    public static String getCurrentTime() {
        Date date = Calendar.getInstance().getTime();
        String desiredDate = new SimpleDateFormat(DATE_FORMAT_SERVER).format(date);

        return desiredDate;
    }

    /**
     * 주어진 Date를 서버요청 타입으로 변경
     *
     * @param date
     * @return yyyy-MM-dd HH:mm:ss 포맷의 스트링타입
     */
    public static String getDateTime(Date date) {
        String desiredDate = new SimpleDateFormat(DATE_FORMAT_SERVER).format(date);
        return desiredDate;
    }

    public static Date dateFromUTC(Date date) {
        return new Date(date.getTime() + Calendar.getInstance().getTimeZone().getOffset(date.getTime()));
    }

    public static Date dateToUTC(Date date) {
        return new Date(date.getTime() - Calendar.getInstance().getTimeZone().getOffset(date.getTime()));
    }

    public static String localToUTC(String date) {
        Date dateTime = new Date();
        try {
            dateTime = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date utcDate = dateToUTC(dateTime);

        return getDateTime(utcDate);
    }

    public static String utcToLocal(String date) {
        Date dateTime = new Date();
        try {
            dateTime = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date utcDate = dateToUTC(dateTime);

        return getDateTime(utcDate);
    }


    public static String getPastTimeFormatted(Date date) {
        long registDate = date.getTime();
        long now = Calendar.getInstance().getTimeInMillis();

        long diffSec = (now - registDate) / 1000;
        if (diffSec > 86400) {
            // day
            long diffDay = diffSec / 86400;
            return diffDay + " days ago";

        } else if (diffSec > 3600) {
            // hour
            long diffHour = diffSec / 3600;
            return diffHour + " hours ago";

        } else if (diffSec > 60) {
            // min
            long diffMin = diffSec / 60;
            return diffMin + " min ago";
        }

        return diffSec + " sec ago";
    }

    public static String getMonthName(int month) {
        String monthSymbol = new DateFormatSymbols().getMonths()[month - 1];
        return monthSymbol;
    }

    public static String convertFloat2Int(String floatNumber) {
        if (!TextUtils.isEmpty(floatNumber)) {
            int dot = floatNumber.indexOf(".");
            if (dot != -1)
                floatNumber = floatNumber.substring(0, dot);
        }
        return floatNumber;
    }

    public static int dp2Px(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int sp2Px(Context context, float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static int getOrientationOfImage(String filepath) {
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }

        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

        if (orientation != -1) {
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
            }
        }

        return 0;
    }

    public static Bitmap getRotatedBitmap(Bitmap bitmap, int degrees) {
        if(bitmap == null) return null;
        if (degrees == 0) return bitmap;

        Matrix m = new Matrix();
        m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

        try {
            Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
            if(bitmap != converted) {
                bitmap.recycle();
                bitmap = converted;
            }
        } catch (Exception e) {
        }

        return bitmap;
    }

}
