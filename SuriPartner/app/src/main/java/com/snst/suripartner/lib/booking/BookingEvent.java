package com.snst.suripartner.lib.booking;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.api.RequestCancelBooking;
import com.snst.suripartner.pop_up.PopManagerActivity;
import com.snst.suripartner.lib.api.RequestConfirmBooking;

public class BookingEvent {

    Context context;

    public BookingEvent(Context context) {
        this.context = context;
    }

    public void onExpandCustomerInfos(BookingViewModel viewModel) {
        if (viewModel.visibleCustomerInfos == View.VISIBLE) {
            viewModel.setVisibleCustomerInfos(View.GONE);
        } else {
            viewModel.setVisibleCustomerInfos(View.VISIBLE);
        }
    }

    public void onConfirm(Booking booking) {
        String[] inputs = new String[2];
        inputs[0] = booking.getPredicEstId();
        inputs[1] = booking.getId();

        new RequestConfirmBooking().execute(context, inputs);
    }

    public void onTapTrash(Booking booking) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.CANCEL_BOOKING);
        intent.putExtra(LbPop.LbKey.BOOKING, booking);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.CANCEL_BOOKING);
    }

    public void onCancel(Booking booking, String reasonTypeId, String reasonComment) {

        String[] inputs = new String[5];
        inputs[0] = booking.getPredicEstId();
        inputs[1] = booking.getId();
        inputs[2] = reasonTypeId;
        inputs[3] = Sharing.Instance().getUser().getStoreId();
        inputs[4] = reasonComment;

        new RequestCancelBooking().execute(context, inputs);
    }
}
