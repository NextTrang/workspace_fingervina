package com.snst.suripartner.lib.review;

import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;

import org.json.JSONException;
import org.json.JSONObject;

public class Review {
    private String requestEstId;
    private String comment;
    private String score;
    private String imageUrlFirst;
    private String imageUrlSecond;

    public Review(ReviewViewModel viewModel) {
        this.requestEstId = viewModel.requestEstId;
        this.comment = viewModel.comment;
        this.score = viewModel.score;
        this.imageUrlFirst = viewModel.imageUrlFirst;
        this.imageUrlSecond = viewModel.imageUrlSecond;
    }

    public Review(JSONObject jsInfo) {
        try {
            requestEstId = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            comment = jsInfo.getString(LbData.LbInfo.REVIEW_COMMENT);
            score = jsInfo.getString(LbData.LbInfo.SCORE);
            imageUrlFirst = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.REVIEW_IMAGE1);
            imageUrlSecond = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.REVIEW_IMAGE2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getter

    public String getRequestEstId() {
        return requestEstId;
    }

    public String getComment() {
        return comment;
    }

    public String getScore() {
        return score;
    }

    public String getImageUrlFirst() {
        return imageUrlFirst;
    }

    public String getImageUrlSecond() {
        return imageUrlSecond;
    }
}
