package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.suri_history.SuriHistory;
import com.snst.suripartner.lib.suri_history.SuriHistoryViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestGetSuriHistory implements ApiRequest {
    private static final String TAG = "RequestGetSuriHistory";

    @Override
    public void execute(final Context context, final String... inputs) {

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSuriHistory(Sharing.Instance().getCookie(), inputs[0]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {

                        JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                        JSONArray jsInfos = jsData.getJSONArray(LbData.LbInfo.INFO);

                        Sharing.Instance().getSuriHistoryViewModels().clear();
                        for (int i = 0; i < jsInfos.length(); ++i) {
                            JSONObject jsInfo = jsInfos.getJSONObject(i);
                            Sharing.Instance().getSuriHistoryViewModels().add(new SuriHistoryViewModel(new SuriHistory(jsInfo)));
                        }
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
