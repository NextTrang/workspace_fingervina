package com.snstfinger.suri.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.databinding.ListItemMysuriBinding;

import java.util.ArrayList;
import java.util.List;

public class MySuriRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MySuriRecyclerViewAdapter.class.getName();
    private List mValues;
    private final MySuriListFragment.OnMySuriListInteractionListener mListener;

    public MySuriRecyclerViewAdapter(MySuriListFragment.OnMySuriListInteractionListener listener) {
        mValues = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemMysuriBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_mysuri, parent, false);

        return new QuotationShopViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        RequestNPredictionNStoreEntity item = (RequestNPredictionNStoreEntity) mValues.get(position);
        ((QuotationShopViewHolder) holder).binding.setRequestEstimate(item);
        ((QuotationShopViewHolder) holder).itemView.setOnClickListener(v -> mListener.onItemClick(item));

        ((QuotationShopViewHolder) holder).itemView.setOnLongClickListener(v -> {
            mListener.onItemLongClick(item);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    void setItemList(List items) {
        if (items == null) return;

        this.mValues = items;
        notifyDataSetChanged();
    }


    public class QuotationShopViewHolder extends RecyclerView.ViewHolder {
        ListItemMysuriBinding binding;

        public QuotationShopViewHolder(ListItemMysuriBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
