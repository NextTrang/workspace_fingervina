package com.snstfinger.suri.ui.tools

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
internal data class AlbumItem(val name: String, val isAll: Boolean,val bucketId: String) : Parcelable