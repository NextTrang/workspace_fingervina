package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SuriStoryEntity implements Serializable {

    @SerializedName("suri_history_id")
    private String suriHistoryId;

    @SerializedName("request_estimate_id")
    private String requestEstimateId;

    @SerializedName("prediction_estimate_id")
    private String predictionEstimateid;

    @SerializedName("suri_image1")
    private String suriImage1;

    @SerializedName("title")
    private String title;

    @SerializedName("contents")
    private String contents;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    private Date registerDate;

    public String getSuriHistoryId() {
        return suriHistoryId;
    }

    public void setSuriHistoryId(String suriHistoryId) {
        this.suriHistoryId = suriHistoryId;
    }

    public String getRequestEstimateId() {
        return requestEstimateId;
    }

    public void setRequestEstimateId(String requestEstimateId) {
        this.requestEstimateId = requestEstimateId;
    }

    public String getPredictionEstimateid() {
        return predictionEstimateid;
    }

    public void setPredictionEstimateid(String predictionEstimateid) {
        this.predictionEstimateid = predictionEstimateid;
    }

    public String getSuriImage1() {
        return suriImage1;
    }

    public void setSuriImage1(String suriImage1) {
        this.suriImage1 = suriImage1;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getRegisterDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return registerDate == null ? "" : formatter.format(registerDate);
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
}
