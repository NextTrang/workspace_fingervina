package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class RepairTypeEntity {
    @SerializedName("repair_type_id")
    public int typeId;

    @SerializedName("repair_type_name")
    public String typeName;

    @SerializedName("object_id")
    public int objectId;
}
