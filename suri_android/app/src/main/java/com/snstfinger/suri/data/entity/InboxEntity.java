package com.snstfinger.suri.data.entity;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import androidx.room.util.DBUtil;

import com.snstfinger.suri.BR;

import java.sql.Date;
import java.util.Calendar;

@Entity(tableName = "inbox")
public class InboxEntity extends BaseObservable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String contents;

    @TypeConverters(DateConverter.class)
    private Date regDate;
    private boolean readYN;

    @Ignore
    private boolean selected;

    @Ignore
    public InboxEntity(String title) {
        this.title = title;
        this.regDate = new Date(Calendar.getInstance().getTimeInMillis());
        this.readYN = false;
    }

    public InboxEntity(String title, String contents, boolean readYN) {
        this.title = title;
        this.regDate = new Date(Calendar.getInstance().getTimeInMillis());
        this.contents = contents;
        this.readYN = readYN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public boolean isReadYN() {
        return readYN;
    }

    public void setReadYN(boolean readYN) {
        this.readYN = readYN;
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyPropertyChanged(BR.selected);
    }

    public static class DateConverter {

        @TypeConverter
        public static Date toDate(Long timestamp) {
            return timestamp == null ? null : new Date(timestamp);
        }

        @TypeConverter
        public static Long toTimestamp(Date date) {
            return date == null ? null : date.getTime();
        }
    }
}
