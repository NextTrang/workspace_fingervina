package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.pop_up.PopSuriOngoingDetailActivity;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestUpdateRepair implements ApiRequest {
    private static final String TAG = "RequestUpdateRepair";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateRepair(Sharing.Instance().getCookie(), inputs[0],inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //update progress rate for UI
                        for (SuriViewModel viewModel : Sharing.Instance().getSuriViewModels()) {
                            if (viewModel.predictEstId.equals(inputs[0])) {
                                viewModel.progressRate = Integer.parseInt(inputs[1]);
                                break;
                            }
                        }

                        //refresh current tab
                        ((MainActivity) context).refresh();
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
