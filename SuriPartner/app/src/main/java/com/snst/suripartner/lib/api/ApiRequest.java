package com.snst.suripartner.lib.api;

import android.content.Context;

public interface ApiRequest {
    void execute(final Context context, String... inputs);
}
