package com.snstfinger.suri.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.snstfinger.suri.data.entity.InboxEntity;
import com.snstfinger.suri.data.repository.InboxRepository;

import java.util.ArrayList;
import java.util.List;

public class InboxViewModel extends AndroidViewModel {
    private InboxRepository mRepository;
    private LiveData<List<InboxEntity>> mAllNotice;
    private List<InboxEntity> seletedInbox = new ArrayList<>();

    public InboxViewModel(@NonNull Application application) {
        super(application);
        mRepository = new InboxRepository(application);
        mAllNotice = mRepository.getAll();
    }


    LiveData<List<InboxEntity>> getAll() {

        return mAllNotice;
    }

    public void insert(InboxEntity notice) {
        mRepository.insert(notice);
    }

    public void update(InboxEntity notice) {
        mRepository.update(notice);
    }

    public void delete(InboxEntity... notice) {
        mRepository.delete(notice);
    }

//    public LiveData<PagedList<SuriCaseEntity>> getSuriCasePagedList(@Nullable String storeId) {
//        SuriCaseListDataSourceFactory factory = new SuriCaseListDataSourceFactory(storeId);

    // LiveData<PageKeyedDataSource<Integer, SuriCaseEntity>> liveDataSource = factory.getSuriCasewLiveData();

//        PagedList.Config config = new PagedList.Config.Builder().setInitialLoadSizeHint(SuriCaseListDataSource.PAGE_SIZE).setPageSize(SuriCaseListDataSource.PAGE_SIZE).build();

//        suriCasePagedList = new LivePagedListBuilder(factory, config).build();

//        return suriCasePagedList;
//    }

}
