package com.snst.suripartner.lib.suri;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.databinding.SuriCompletedBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.api.RequestGetSuriHistory;
import com.snst.suripartner.pop_up.PopManagerActivity;

import java.util.List;

public class SuriCompletedAdapter extends RecyclerView.Adapter<SuriCompletedAdapter.MyViewHolder> implements SuriNavigator {

    private Context mContext;
    private List<SuriViewModel> mData;
    private LayoutInflater inflater;

    public SuriCompletedAdapter(Context mContext, List<SuriViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public SuriCompletedAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        SuriCompletedBinding binding = SuriCompletedBinding.inflate(inflater, parent, false);

        return new SuriCompletedAdapter.MyViewHolder(parent.getContext(), binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        mData.get(position).setNavigator(this);
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    @Override
    public void onItemClick(Suri suri) {
        //request suri history list
        new RequestGetSuriHistory().execute(mContext, suri.getId());

        Intent intent = new Intent(mContext, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SURI_COMPLETE_DETAIL);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) mContext).startActivityForResult(intent, LbPop.LbCode.SURI_COMPLETE_DETAIL);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private SuriCompletedBinding suriBinding;
        private Context context;

        public MyViewHolder(Context context, SuriCompletedBinding suriBinding) {
            super(suriBinding.getRoot());
            this.context = context;
            this.suriBinding = suriBinding;
        }

        public void bind(SuriViewModel viewModel) {
            suriBinding.setSuriViewModel(viewModel);
            suriBinding.setSuri(new Suri(viewModel));
            suriBinding.setHandler(new SuriEvent(context));
        }
    }
}
