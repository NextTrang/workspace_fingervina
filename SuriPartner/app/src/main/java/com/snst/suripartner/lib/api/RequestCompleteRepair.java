package com.snst.suripartner.lib.api;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.labels.LbRequest;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.pop_confirm.PopConfirm;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.pop_up.PopConfirmActivity;
import com.snst.suripartner.pop_up.PopSuriOngoingDetailActivity;

import org.json.JSONObject;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCompleteRepair implements ApiRequest {
    private static final String TAG = "RequestCompleteRepair";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());
        RetrofitClient.getInstance().getFlags().set(LbRequest.LbFlag.REQUEST_COMPLETE_REPAIR);

        File predictImage1 = new File(inputs[3]);
        File predictImage2 = new File(inputs[4]);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().completeRepair(
                Sharing.Instance().getCookie(),
                Conversion.string_2_requestBody(inputs[0]),
                Conversion.string_2_requestBody(inputs[1]),
                Conversion.string_2_requestBody(inputs[2]),
                Conversion.file_2_multiPartBodyPart(LbRequest.LbSuri.PREDICTION_IMAGE1,predictImage1),
                Conversion.file_2_multiPartBodyPart(LbRequest.LbSuri.PREDICTION_IMAGE2,predictImage2));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //update progress rate for UI
                        for (SuriViewModel viewModel : Sharing.Instance().getSuriViewModels()) {
                            if (viewModel.predictEstId.equals(inputs[0])) {
                                viewModel.state = viewModel.stateMask = LbState.WAITING;
                                break;
                            }
                        }

                        //refresh current tab
                        ((MainActivity) context).refresh();

                        PopConfirm popConfirm = new PopConfirm()
                                .setTitle(LbPop.LbTitle.SURI_IS_COMPLETED)
                                .setContent(LbPop.LbContent.SURI_IS_COMPLETED);
                        Intent intent = new Intent(context, PopConfirmActivity.class);
                        intent.putExtra(LbPop.LbKey.POP_CONFIRM,popConfirm);
                        context.startActivity(intent);
                    }

                    if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_COMPLETE_REPAIR)){
                        new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                        RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_COMPLETE_REPAIR);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "RequestCompleteRepair onFailure: " +  t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_COMPLETE_REPAIR)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_COMPLETE_REPAIR);
                }
            }
        });
    }
}
