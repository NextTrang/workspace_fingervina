package com.snstfinger.suri.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ResponseRequestNPredictionNStore;
import com.snstfinger.suri.data.entity.ResponseStoreNPrediction;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.data.model.HomeSuriStatus;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.databinding.FragmentRequestSuriBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.ui.estimate.RequestEstimateActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationGoingActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationShopActivity;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainSuriFragment extends Fragment {
    public static final String TAG = MainSuriFragment.class.getName();

    private static final String ARG_PARAM1 = "status";

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    // shared viewmodel 구현해보기
    // private HomeStatusViewModel mHomeSuriStatusViewModel;
    private HomeSuriStatus mHomeSuriStatus;
    private FragmentRequestSuriBinding mBinding;

    private MySuriViewModel mMySuriViewModel;

    public MainSuriFragment() {
    }

    public static MainSuriFragment newInstance(HomeSuriStatus param1) {
        MainSuriFragment fragment = new MainSuriFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mHomeSuriStatus = (HomeSuriStatus) getArguments().getSerializable(ARG_PARAM1);
        }


        mMySuriViewModel = ViewModelProviders.of(getActivity()).get(MySuriViewModel.class);
        mMySuriViewModel.getMainSuri().observe(this, new Observer<RequestNPredictionNStoreEntity>() {
            @Override
            public void onChanged(RequestNPredictionNStoreEntity requestNPredictionNStoreEntity) {
                mSelectedRequestEstimate = requestNPredictionNStoreEntity;
                updateHomeSuriStatus();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_request_suri, container, false);
        mBinding.setMySuri(mHomeSuriStatus);
        mBinding.layoutStatusCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mHomeSuriStatus.status = HomeSuriStatus.TYPE_SENDING_PAYMENT;
//                mHomeSuriStatus = null;

                if (mHomeSuriStatus == null) {
                    Intent request = new Intent(getActivity(), RequestEstimateActivity.class);
                    request.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_MOBILE));
                    startActivity(request);

                } else if (mHomeSuriStatus.status == HomeSuriStatus.TYPE_REQUEST) {
                    Intent statusDetail = new Intent(getActivity(), QuotationShopActivity.class);
                    statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
                    startActivity(statusDetail);

//                } else if (mHomeSuriStatus.status == HomeSuriStatus.TYPE_BOOKING || mHomeSuriStatus.status == HomeSuriStatus.TYPE_BOOKING_CONFIRM) {
//                    Intent statusDetail = new Intent(getActivity(), QuotationBookingActivity.class);
//                    statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
//                    startActivity(statusDetail);

                } else {
                    Intent statusDetail = new Intent(getActivity(), QuotationGoingActivity.class);
                    statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
                    startActivity(statusDetail);
                }


            }
        });

        refreshingData();

        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void refreshingData() {
        if (SuriLoginManager.getInstance(getActivity()).isLoggedIn()) {
            MySuriInfoSharedPreferences mMainSuriInfo = MySuriInfoSharedPreferences.getInstance(getActivity());
            String requestEstId = mMainSuriInfo.getReqeustId();
            if (TextUtils.isEmpty(requestEstId)) {
                UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getActivity());
                loadMyRequestEstimate(profile.getUserId());
            } else {

//                if (mSelectedRequestEstimate != null)
//                    updateMyRequestEstimate(mSelectedRequestEstimate.requestEstimateId);
                updateMyRequestEstimate(requestEstId);
            }

        }
    }

    // 초기 요청 조회
    private void loadMyRequestEstimate(String userId) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllRequestEstimateNPredictionNStore(cookie, userId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseRequestNPredictionNStore result = gson.fromJson(json, ResponseRequestNPredictionNStore.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    List item = result.getRequestEntities();
                    if (!item.isEmpty()) {
                        mHomeSuriStatus = new HomeSuriStatus();
                        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) item.get(0);
                        MySuriInfoSharedPreferences mMainSuriInfo = MySuriInfoSharedPreferences.getInstance(getActivity());
                        Log.d(TAG, "Latest Req::" + mSelectedRequestEstimate);
                        mMainSuriInfo.putReqeustId(mSelectedRequestEstimate.requestEstimateId);
                        getAllStoreByRequestId(mSelectedRequestEstimate.requestEstimateId);

                        // 홈상태 채우기
//                        updateHomeSuriStatus();

                        // viewmode 적용해보기
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().status = getStatusCode(mSelectedRequestEstimate.status);
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().setStoreName(mSelectedRequestEstimate.storeName);
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().setStoreAddress(mSelectedRequestEstimate.address);
//                        mHomeSuriStatus.setBookingDate(mSelectedRequestEstimate.getBookingDateFormatted());
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllRequestEstimateNPredictionNStore onFailure");
            }
        });
    }

    private void updateMyRequestEstimate(String requestEstimateId) {
        if (requestEstimateId == null)
            return;

        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetRequestEstimateNPredictionNStore(cookie, requestEstimateId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    Object info = ((LinkedTreeMap) data).get("Info");
                    if(info == null || info.equals("")) {
                        mSelectedRequestEstimate = null;
                        return;
                    }
                    JsonObject json = gson.toJsonTree(info).getAsJsonObject();
                    RequestNPredictionNStoreEntity item = gson.fromJson(json, RequestNPredictionNStoreEntity.class);

                    if (item != null) {
                        Log.d(TAG, "Update Req::" + item);
                        mSelectedRequestEstimate = item;

                        if(mMySuriViewModel!=null)
                            mMySuriViewModel.setMainSuri(mSelectedRequestEstimate);

                        getAllStoreByRequestId(mSelectedRequestEstimate.requestEstimateId);

                        // viewmode 적용해보기
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().status = getStatusCode(mSelectedRequestEstimate.status);
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().setStoreName(mSelectedRequestEstimate.storeName);
//                        mHomeSuriStatusViewModel.getSuriStatus().getValue().setStoreAddress(mSelectedRequestEstimate.address);
//                        mHomeSuriStatus.setBookingDate(mSelectedRequestEstimate.getBookingDateFormatted());
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetRequestEstimateNPredictionNStore onFailure");
            }
        });
    }

    private void getAllStoreByRequestId(String requestId) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllStoreNPrediction(cookie, requestId, null, "ISNULL(prediction_price) ASC");
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStoreNPrediction result = gson.fromJson(json, ResponseStoreNPrediction.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
//                        mHomeSuriStatus.setNumOfMatchStore(String.valueOf(page.count));
//                        mBinding.setMySuri(mHomeSuriStatus);
                        mSelectedRequestEstimate.numOfMatchStore = String.valueOf(page.count);
                    }

                    if(mMySuriViewModel!=null)
                        mMySuriViewModel.setMainSuri(mSelectedRequestEstimate);

                    List item = result.getStoreNPredictionEntities();
                    if (!item.isEmpty()) {
                        Log.d(TAG, "Latest Req::" + item.get(0));
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllStoreNPrediction onFailure");
            }
        });
    }

    private void updateHomeSuriStatus() {
        if (mHomeSuriStatus == null) {
            mHomeSuriStatus = new HomeSuriStatus();
        }

        mHomeSuriStatus.status = getStatusCode(mSelectedRequestEstimate.status);
        String productName = mSelectedRequestEstimate.brandName + "/" + mSelectedRequestEstimate.modelName;
        mHomeSuriStatus.setRequestName(productName);
        mHomeSuriStatus.setNumOfMatchStore(mSelectedRequestEstimate.numOfMatchStore);
        // booking
        mHomeSuriStatus.setStoreName(mSelectedRequestEstimate.storeName);
        mHomeSuriStatus.setStoreAddress(mSelectedRequestEstimate.address);
        mHomeSuriStatus.setBookingDate(mSelectedRequestEstimate.getBookingDateFormatted());
        // ongoing
        mHomeSuriStatus.setProgress(String.valueOf(mSelectedRequestEstimate.getProgressRate()));

        mHomeSuriStatus.setSuriRate(String.valueOf(mSelectedRequestEstimate.score));
        mBinding.setMySuri(mHomeSuriStatus);
    }

    private int getStatusCode(String status) {
        int statusCode = 0;

        switch (status) {
            case "REQUEST":
                statusCode = 1;
                break;
            case "BOOKING":
                statusCode = 2;
                break;
            case "BOOKING_CONFIRM":
                statusCode = 3;
                break;
            case "SENDING_PAYMENT":
                statusCode = 4;
                break;
            case "REPAIR_STANDBY":
                statusCode = 5;
                break;
            case "REPAIR":
                statusCode = 6;
                break;
            case "SENDING_PAYMENT_UPDATE":
                statusCode = 7;
                break;
            case "COMPLETE":
                statusCode = 8;
                break;
            case "REVIEW":
                statusCode = 9;
                break;
        }

        return statusCode;
    }

}
