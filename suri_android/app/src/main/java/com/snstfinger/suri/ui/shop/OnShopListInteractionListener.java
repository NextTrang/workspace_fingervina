package com.snstfinger.suri.ui.shop;

import com.snstfinger.suri.data.entity.StoreEntity;

public interface OnShopListInteractionListener {
    void onShopClick(StoreEntity item);
}
