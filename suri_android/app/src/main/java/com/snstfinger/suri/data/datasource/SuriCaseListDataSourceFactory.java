package com.snstfinger.suri.data.datasource;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.snstfinger.suri.data.entity.SuriCaseEntity;

public class SuriCaseListDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, SuriCaseEntity>> suriCasewLiveData = new MutableLiveData<>();
    private String stroeId;
    private String objectId="";
    private String brandName="";
    private String modelName="";

    public SuriCaseListDataSourceFactory(String stroeId) {
        this.stroeId = stroeId;
    }

    public SuriCaseListDataSourceFactory(String stroeId,String objectId, String brandName, String modelName) {
        this.stroeId = stroeId;
        this.objectId  = objectId;
        this.brandName = brandName;
        this.modelName = modelName;
    }

    @NonNull
    @Override
    public DataSource<Integer, SuriCaseEntity> create() {
        SuriCaseListDataSource suriCaseListDataSource = new SuriCaseListDataSource(stroeId,objectId,brandName,modelName);

        suriCasewLiveData.postValue(suriCaseListDataSource);

        return suriCaseListDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, SuriCaseEntity>> getSuriCasewLiveData() {
        return suriCasewLiveData;
    }
}
