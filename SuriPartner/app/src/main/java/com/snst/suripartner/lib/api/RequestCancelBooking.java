package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.booking.BookingViewModel;
import com.snst.suripartner.lib.effect.EffectEvent;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCancelBooking implements ApiRequest {

    private static final String TAG = "RequestCancelBooking";

    @Override
    public void execute(final Context context, final String... inputs) {

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().cancelBooking(Sharing.Instance().getCookie(), inputs[0], inputs[1], inputs[2], inputs[3], inputs[4]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //remove this booking from list
                        int index = -1;
                        for (BookingViewModel viewModel : Sharing.Instance().getBookingViewModels()) {
                            if (viewModel.id.equals(inputs[1])) {
                                index = Sharing.Instance().getBookingViewModels().indexOf(viewModel);
                                break;
                            }
                        }
                        Sharing.Instance().getBookingViewModels().remove(index);

                        //refresh current tab
                        ((MainActivity) context).refresh();
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
