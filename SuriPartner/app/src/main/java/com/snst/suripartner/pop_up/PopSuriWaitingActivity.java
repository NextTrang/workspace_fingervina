package com.snst.suripartner.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.SuriWaitingDetailBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriEvent;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.lib.suri_history.SuriHistoryAdapter;
import com.snst.suripartner.lib.suri_history.SuriHistoryViewModel;

import java.util.ArrayList;
import java.util.List;

public class PopSuriWaitingActivity extends AppCompatActivity {

    private Suri data;

    private RecyclerView rwSuriHistory;
    private List<SuriHistoryViewModel> suriHistories = new ArrayList<>();
    private SuriHistoryAdapter suriHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SuriWaitingDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_suri_waiting);

        data = (Suri) getIntent().getSerializableExtra(LbPop.LbKey.SURI);

        //binding data
        binding.setSuriViewModel(new SuriViewModel(data));
        binding.setSuri(data);
        binding.setHandler(new SuriEvent(this));
        binding.setUser(Sharing.Instance().getUser());

        suriHistories = Sharing.Instance().getSuriHistoryViewModels();
        setUI();
    }

    private void setUI() {
        rwSuriHistory = findViewById(R.id.suriHistory_recyclerview);
        suriHistoryAdapter = new SuriHistoryAdapter(suriHistories);
        rwSuriHistory.setLayoutManager(new LinearLayoutManager(this));
        rwSuriHistory.setAdapter(suriHistoryAdapter);
    }
}
