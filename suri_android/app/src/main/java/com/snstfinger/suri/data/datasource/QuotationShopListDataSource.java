package com.snstfinger.suri.data.datasource;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.data.entity.ResponseRequestNPredictionNStore;
import com.snstfinger.suri.data.entity.ResponseStoreNPrediction;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotationShopListDataSource extends PageKeyedDataSource<Integer, StoreNPredictionEntity> {
    public static final String TAG = QuotationShopListDataSource.class.getName();

    public static final int PAGE_SIZE = 10;

    private static final int FIRST_PAGE = 1;
    private String requestId, condition;

    public QuotationShopListDataSource(String requestId, String condition) {
        this.requestId = requestId;
        this.condition = condition;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        initGetAllStoreWithPrediction(requestId, condition, callback);

    }

    @Override
    public void loadBefore(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        loadPrePageGetAllStoreWithPrediction(params, requestId, condition, callback);

    }

    @Override
    public void loadAfter(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        loadNextPageGetAllStoreWithPrediction(params, requestId, condition, callback);

    }


    private void initGetAllStoreWithPrediction(String requestId, String condition, LoadInitialCallback callback) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        if (condition == null) {
            condition = "ISNULL(prediction_price) ASC, distance ASC";
        }

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllStoreNPrediction(cookie, requestId, String.valueOf(FIRST_PAGE), condition);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStoreNPrediction result = gson.fromJson(json, ResponseStoreNPrediction.class);

//                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//
//                        @Override
//                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
//                                throws JsonParseException {
//                            try {
//                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
//                                return m_ISO8601Local.parse(json.getAsString());
////                                return new SimpleDateFormat().parse(json.getAsString());
//                            } catch (ParseException e) {
//                                return null;
//                            }
//                        }
//                    }).create();
//                    Object paging = ((LinkedTreeMap) data).get("pageMap");
//                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = result.getPageMap(); //gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
//                        mHomeSuriStatus.setNumOfMatchStore(String.valueOf(page.count));
//                        mBinding.setMySuri(mHomeSuriStatus);

                    }

//                    Object info = ((LinkedTreeMap) data).get("Info");
//                    Type itemType = new TypeToken<List<StoreNPredictionEntity>>() {
//                    }.getType();
//                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List storeNPredictionEntities = result.getStoreNPredictionEntities(); //gson.fromJson(asJsonArray, itemType);

                    callback.onResult(storeNPredictionEntities, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllStoreNPrediction onFailure");
            }
        });
    }

    private void loadPrePageGetAllStoreWithPrediction(LoadParams params, String requestId, String condition, LoadCallback callback) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        if (condition == null) {
            condition = "ISNULL(prediction_price) ASC, distance ASC";
        }

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllStoreNPrediction(cookie, requestId, String.valueOf(params.key), condition);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStoreNPrediction result = gson.fromJson(json, ResponseStoreNPrediction.class);

//                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//
//                        @Override
//                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
//                                throws JsonParseException {
//                            try {
//                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
//                                return m_ISO8601Local.parse(json.getAsString());
////                                return new SimpleDateFormat().parse(json.getAsString());
//                            } catch (ParseException e) {
//                                return null;
//                            }
//                        }
//                    }).create();
//                    Object paging = ((LinkedTreeMap) data).get("pageMap");
//                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = result.getPageMap(); //gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
//                        mHomeSuriStatus.setNumOfMatchStore(String.valueOf(page.count));
//                        mBinding.setMySuri(mHomeSuriStatus);

                    }

//                    Object info = ((LinkedTreeMap) data).get("Info");
//                    Type itemType = new TypeToken<List<StoreNPredictionEntity>>() {
//                    }.getType();
//                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List storeNPredictionEntities = result.getStoreNPredictionEntities(); //gson.fromJson(asJsonArray, itemType);

                    callback.onResult(storeNPredictionEntities, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllStoreNPrediction onFailure");
            }
        });
    }

    private void loadNextPageGetAllStoreWithPrediction(LoadParams params, String requestId, String condition, LoadCallback callback) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        if (condition == null) {
            condition = "ISNULL(prediction_price) ASC, distance ASC";
        }

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllStoreNPrediction(cookie, requestId, String.valueOf(params.key), condition);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStoreNPrediction result = gson.fromJson(json, ResponseStoreNPrediction.class);

//                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//
//                        @Override
//                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
//                                throws JsonParseException {
//                            try {
//                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
//                                return m_ISO8601Local.parse(json.getAsString());
////                                return new SimpleDateFormat().parse(json.getAsString());
//                            } catch (ParseException e) {
//                                return null;
//                            }
//                        }
//                    }).create();
//                    Object paging = ((LinkedTreeMap) data).get("pageMap");
//                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = result.getPageMap(); // gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
//                        mHomeSuriStatus.setNumOfMatchStore(String.valueOf(page.count));
//                        mBinding.setMySuri(mHomeSuriStatus);

                    }

//                    Object info = ((LinkedTreeMap) data).get("Info");
//                    Type itemType = new TypeToken<List<StoreNPredictionEntity>>() {
//                    }.getType();
//                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List storeNPredictionEntities = result.getStoreNPredictionEntities(); //gson.fromJson(asJsonArray, itemType);

//                    if ((Integer) params.key <= page.pages)
                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(storeNPredictionEntities, page.page + 1);
                    else {
                        callback.onResult(storeNPredictionEntities, null);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllStoreNPrediction onFailure");
            }
        });
    }

}
