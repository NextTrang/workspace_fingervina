package com.snst.suripartner.lib.suri;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.databinding.SuriInvoiceBinding;

import java.util.List;

public class SuriInvoiceAdapter extends RecyclerView.Adapter<SuriInvoiceAdapter.MyViewHolder> {

    private List<SuriViewModel> mData;
    private LayoutInflater inflater;

    public SuriInvoiceAdapter(List<SuriViewModel> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        SuriInvoiceBinding binding = SuriInvoiceBinding.inflate(inflater, parent, false);

        return new SuriInvoiceAdapter.MyViewHolder(parent.getContext(), binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private SuriInvoiceBinding suriBinding;
        private Context context;

        public MyViewHolder(Context context, SuriInvoiceBinding suriBinding) {
            super(suriBinding.getRoot());
            this.context = context;
            this.suriBinding = suriBinding;
        }

        public void bind(SuriViewModel viewModel) {
            suriBinding.setSuriViewModel(viewModel);
            suriBinding.setSuri(new Suri(viewModel));
            suriBinding.setHandler(new SuriEvent(context));
        }
    }
}
