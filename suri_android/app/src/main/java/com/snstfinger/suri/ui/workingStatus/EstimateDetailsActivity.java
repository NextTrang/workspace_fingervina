package com.snstfinger.suri.ui.workingStatus;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.room.Room;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.databinding.ActivityEstimateDetailsBinding;
import com.snstfinger.suri.ui.estimate.ImageItemRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class EstimateDetailsActivity extends AppCompatActivity {
    private static final String TAG = EstimateDetailsActivity.class.getName();

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityEstimateDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_estimate_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + mSelectedRequestEstimate);

        actionBar.setTitle(mSelectedRequestEstimate.requestEstimateId);

        binding.setRequestEstimate(mSelectedRequestEstimate);

        List<String> items = new ArrayList<>();
        if (!TextUtils.isEmpty(mSelectedRequestEstimate.estimateImage1)) {
            items.add(mSelectedRequestEstimate.estimateImage1);
        }
        if (!TextUtils.isEmpty(mSelectedRequestEstimate.estimateImage2)) {
            items.add(mSelectedRequestEstimate.estimateImage2);
        }
        if (!TextUtils.isEmpty(mSelectedRequestEstimate.estimateImage3)) {
            items.add(mSelectedRequestEstimate.estimateImage3);
        }
        if (!TextUtils.isEmpty(mSelectedRequestEstimate.estimateImage4)) {
            items.add(mSelectedRequestEstimate.estimateImage4);
        }

        Log.d(TAG, "Number of selected Images: " + items.size());
        if (!items.isEmpty()) {
            ImageItemRecyclerViewAdapter adapter = new ImageItemRecyclerViewAdapter(items, false);
            binding.viewPager.setAdapter(adapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
