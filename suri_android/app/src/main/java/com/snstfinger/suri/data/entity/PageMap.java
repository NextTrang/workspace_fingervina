package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PageMap {

    @SerializedName("pages")
    public Integer pages;

    @SerializedName("prePage")
    public Integer prePage;

    @SerializedName("nextPage")
    public Integer nextPage;

    @SerializedName("count")
    public Integer count;

    @SerializedName("page")
    public Integer page;

    @SerializedName("pageList")
    public List<Integer> pageList;
}
