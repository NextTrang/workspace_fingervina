package com.snst.suripartner.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.snst.suripartner.R;
import com.snst.suripartner.labels.LbPop;

public class PopManagerActivity extends AppCompatActivity {

    private int popCode = -1;
    private Intent mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_manager);

        mData = getIntent();
        popCode = mData.getIntExtra(LbPop.LbKey.POP_CODE, -1);
        startPopup(popCode);
    }

    public void startPopup(int popCode) {
        switch (popCode) {
            case LbPop.LbCode.CANCEL_BOOKING:
                Intent iCancelBooking = new Intent(this, PopCancelBookingActivity.class);
                startActivityForResult(iCancelBooking, popCode);
                break;
            case LbPop.LbCode.CANCEL_REASON:
                Intent iCancelReason = new Intent(this, PopCancelReasonActivity.class);
                iCancelReason.putExtras(mData);
                startActivityForResult(iCancelReason, popCode);
                break;
            case LbPop.LbCode.SEND_INVOICE:
                Intent iSendInvoice = new Intent(this, PopSendInvoiceActivity.class);
                iSendInvoice.putExtras(mData);
                startActivityForResult(iSendInvoice, popCode);
                break;
            case LbPop.LbCode.QUOTATION_DETAIL:
                Intent iQuotationDetail = new Intent(this, PopQuotationDetailActivity.class);
                iQuotationDetail.putExtras(mData);
                startActivityForResult(iQuotationDetail, popCode);
                break;
            case LbPop.LbCode.SURI_ONGOING_DETAIL:
                Intent iSuriOngoingDetail = new Intent(this, PopSuriOngoingDetailActivity.class);
                iSuriOngoingDetail.putExtras(mData);
                startActivityForResult(iSuriOngoingDetail, popCode);
                break;
            case LbPop.LbCode.WRITE_SURI_HISTORY:
                Intent iSuriWriteHistory = new Intent(this, PopWriteSuriHistoryActivity.class);
                iSuriWriteHistory.putExtras(mData);
                startActivityForResult(iSuriWriteHistory, popCode);
                break;
            case LbPop.LbCode.REQUEST_COMPLETE_SURI:
                Intent iRequestCompleteSuri = new Intent(this, PopSuriRequestCompleteActivity.class);
                iRequestCompleteSuri.putExtras(mData);
                startActivityForResult(iRequestCompleteSuri, popCode);
                break;
            case LbPop.LbCode.SEE_MAP:
                Intent iSeeMap = new Intent(this, PopMapActivity.class);
                iSeeMap.putExtras(mData);
                startActivityForResult(iSeeMap, popCode);
                break;
            case LbPop.LbCode.NOTICE_LEFT_MENU:
                Intent iNoticeLeftMenu = new Intent(this, PopNoticeActivity.class);
                iNoticeLeftMenu.putExtras(mData);
                startActivityForResult(iNoticeLeftMenu, popCode);
                break;
            case LbPop.LbCode.SURI_WAITING_DETAIL:
                Intent iSuriWaitingDetail = new Intent(this, PopSuriWaitingActivity.class);
                iSuriWaitingDetail.putExtras(mData);
                startActivityForResult(iSuriWaitingDetail, popCode);
                break;
            case LbPop.LbCode.SURI_COMPLETE_DETAIL:
                Intent iSuriCompleteDetail = new Intent(this, PopSuriCompletedActivity.class);
                iSuriCompleteDetail.putExtras(mData);
                startActivityForResult(iSuriCompleteDetail, popCode);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Toast.makeText(this, "Result code: " + resultCode + " - RequestCode: " + requestCode, Toast.LENGTH_SHORT).show();

        if (data != null) {
            switch (requestCode) {
                case LbPop.LbCode.CANCEL_BOOKING:
                    //Back to Main Activity
                    mData.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.CANCEL_REASON);
                    mData.putExtra(LbPop.LbKey.POP_TITLE, LbPop.LbTitle.CANCEL_BOOKING);
                    setResult(RESULT_OK, mData);
                    break;
                case LbPop.LbCode.CANCEL_REASON:
                    String reasonTypeId = data.getStringExtra(LbPop.LbKey.REASON_TYPE_ID);
                    String reasonComment = data.getStringExtra(LbPop.LbKey.REASON_COMMENT);

                    mData.putExtra(LbPop.LbKey.REASON_TYPE_ID, reasonTypeId);
                    mData.putExtra(LbPop.LbKey.REASON_COMMENT, reasonComment);
                    setResult(RESULT_OK, mData);
                    break;
                default:
                    setResult(RESULT_OK, data);
            }
        }

        finish();
    }
}
