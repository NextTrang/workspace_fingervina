package com.snst.suripartner.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.PopCancelReasonBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.popCancelReason.PopCancelReason;
import com.snst.suripartner.lib.popCancelReason.PopCancelReasonEvent;

public class PopCancelReasonActivity extends AppCompatActivity {

    private Intent mData;
    private PopCancelReason popUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopCancelReasonBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_cancel_reason);

        mData = getIntent();

        popUp = new PopCancelReason().setTitle(mData.getStringExtra(LbPop.LbKey.POP_TITLE));
        binding.setPopCancelReason(popUp);

        PopCancelReasonEvent handler = new PopCancelReasonEvent(this);
        binding.setHandler(handler);

        setupPopup();
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .75));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }
}
