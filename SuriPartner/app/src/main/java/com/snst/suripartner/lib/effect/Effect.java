package com.snst.suripartner.lib.effect;

import android.view.View;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class Effect extends BaseObservable {
    private int visibleProgressBar = View.INVISIBLE;

    //Getter
    @Bindable
    public int getVisibleProgressBar() {
        return visibleProgressBar;
    }

    //Setter

    public void setVisibleProgressBar(int visibleProgressBar) {
        this.visibleProgressBar = visibleProgressBar;
        notifyPropertyChanged(com.snst.suripartner.BR.visibleProgressBar);
    }
}
