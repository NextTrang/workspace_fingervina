package com.snstfinger.suri.ui.shop;

import com.snstfinger.suri.data.entity.SuriCaseEntity;

public interface OnSuriCaseListInteractionListener {
    void onCaseClick(SuriCaseEntity item);
}
