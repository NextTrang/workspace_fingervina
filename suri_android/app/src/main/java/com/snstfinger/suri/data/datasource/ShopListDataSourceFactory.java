package com.snstfinger.suri.data.datasource;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.data.model.GeoCondition;

public class ShopListDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, StoreEntity>> shopInfoLiveData = new MutableLiveData<>();

    private GeoCondition geoCondition;

    public ShopListDataSourceFactory(GeoCondition condition) {
        this.geoCondition = condition;

    }

    @NonNull
    @Override
    public DataSource<Integer, StoreEntity> create() {
        ShopListDataSource shopInfoDataSource = new ShopListDataSource(geoCondition);

        shopInfoLiveData.postValue(shopInfoDataSource);

        return shopInfoDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, StoreEntity>> getShopInfoLiveData() {
        return shopInfoLiveData;
    }
}
