package com.snstfinger.suri.ui.workingStatus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.databinding.FragmentQuotationShopListBinding;
import com.snstfinger.suri.ui.dialog.FilterQuotationShopDialogFragment;

/**
 * 요청한 견적과 요청에 대한 견적제안한 상점리스트
 */
public class QuotationShopListFragment extends Fragment implements View.OnClickListener, FilterQuotationShopDialogFragment.OnClickDialogListener {
    private static final String TAG = QuotationShopListFragment.class.getName();

    private static final String PARAM = "param";
    private RequestNPredictionNStoreEntity mEstimateInfo;

    private QuotationShopViewModel mQuotationShopViewModel;
    private QuotationShopListAdapter mAdapter;
    private FragmentQuotationShopListBinding mBinding;
    private QuotationShopListFragment.OnListFragmentInteractionListener mListener;

    public QuotationShopListFragment() {
    }

    @SuppressWarnings("unused")
    public static QuotationShopListFragment newInstance(int option) {
        QuotationShopListFragment fragment = new QuotationShopListFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM, option);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            getArguments().getInt(PARAM);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        mEstimateInfo = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + mEstimateInfo);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quotation_shop_list, container, false);
        mBinding.setRequestEstimate(mEstimateInfo);

        mQuotationShopViewModel = ViewModelProviders.of(this).get(QuotationShopViewModel.class);
        mAdapter = new QuotationShopListAdapter(getActivity(), mListener);
        mBinding.listQuotationShop.setAdapter(mAdapter);
        mBinding.setHandler(this);

        doTaskGetStoresByCondition(null);

        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof QuotationShopListFragment.OnListFragmentInteractionListener) {
            mListener = (QuotationShopListFragment.OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.layout_request) {
            Intent detail = new Intent(getActivity(), EstimateDetailsActivity.class);
            detail.putExtra("request_estimate", mEstimateInfo);
            startActivity(detail);

        } else if (v.getId() == R.id.btn_delete) {
            ((QuotationShopActivity) getActivity()).showDeleteDialog();

        } else if (v.getId() == R.id.btn_filter) {
            Log.d(TAG, "filter Clicked");
            FilterQuotationShopDialogFragment filter = FilterQuotationShopDialogFragment.newInstance();
            filter.setTargetFragment(this, 1000);
            filter.show(getFragmentManager(), "filter");
        }

    }

    @Override
    public void onClickDialog(String condition) {
        doTaskGetStoresByCondition(condition);
    }

    private void doTaskGetStoresByCondition(String condition) {
        mQuotationShopViewModel.getQuotationShopPagedList(mEstimateInfo.requestEstimateId, condition).observe(this, new Observer<PagedList<StoreNPredictionEntity>>() {
            @Override
            public void onChanged(PagedList<StoreNPredictionEntity> storeNPredictionEntities) {
                mAdapter.submitList(storeNPredictionEntities);
            }
        });
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(StoreNPredictionEntity item);
    }
}
