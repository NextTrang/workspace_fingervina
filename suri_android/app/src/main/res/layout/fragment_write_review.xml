<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>

        <import type="android.view.View" />

        <import type="android.text.TextUtils" />

        <variable
            name="handler"
            type="com.snstfinger.suri.ui.workingStatus.WriteReviewFragment" />

        <variable
            name="reviewForm"
            type="com.snstfinger.suri.data.model.ReviewForm" />
    </data>

    <androidx.core.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <LinearLayout
                android:id="@+id/layout_total"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="@drawable/bg_app_bar"
                android:orientation="vertical"
                android:paddingStart="25dp"
                android:paddingTop="50dp"
                android:paddingEnd="25dp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent">

                <TextView
                    android:id="@+id/score_average"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text='@{String.format("%.1f", reviewForm.totalScore), default=4.5}'
                    android:textAppearance="@style/RateTotal" />

                <RatingBar
                    android:id="@+id/score_rate"
                    style="@style/ReviewRatingBar"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:isIndicator="true"
                    android:max="5"
                    android:numStars="5"
                    android:rating="@{reviewForm.totalScore,default=4.5}"
                    android:stepSize="0.5" />

            </LinearLayout>

            <TextView
                android:id="@+id/title_quality"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="41.6dp"
                android:paddingStart="25dp"
                android:text="@string/title_quality"
                android:textAppearance="@style/NavItemMenu"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/layout_total" />

            <TextView
                android:id="@+id/point_quality"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:paddingEnd="25dp"
                android:text="@{reviewForm.displayQuality}"
                android:textAppearance="@style/QuotationTitle"
                app:layout_constraintBaseline_toBaselineOf="@id/title_quality"
                app:layout_constraintEnd_toEndOf="parent" />

            <SeekBar
                android:id="@+id/seek_quality"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="14dp"
                android:max="100"
                android:paddingStart="30dp"
                android:paddingEnd="25dp"
                android:progress="@={reviewForm.quality}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/title_quality" />

            <TextView
                android:id="@+id/title_kindness"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="34dp"
                android:paddingStart="25dp"
                android:text="@string/title_kindness"
                android:textAppearance="@style/NavItemMenu"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/seek_quality" />

            <TextView
                android:id="@+id/point_kindness"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:paddingEnd="25dp"
                android:text="@{reviewForm.displayKindness}"
                android:textAppearance="@style/QuotationTitle"
                app:layout_constraintBaseline_toBaselineOf="@id/title_kindness"
                app:layout_constraintEnd_toEndOf="parent" />

            <SeekBar
                android:id="@+id/seek_kindness"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="14dp"
                android:max="100"
                android:paddingStart="30dp"
                android:paddingEnd="25dp"
                android:progress="@={reviewForm.kindness}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/title_kindness" />

            <TextView
                android:id="@+id/title_accuracy"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="34dp"
                android:paddingStart="25dp"
                android:text="@string/title_price_accuracy"
                android:textAppearance="@style/NavItemMenu"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/seek_kindness" />

            <TextView
                android:id="@+id/point_accuracy"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:paddingEnd="25dp"
                android:text="@{reviewForm.displayAccuracy}"
                android:textAppearance="@style/QuotationTitle"
                app:layout_constraintBaseline_toBaselineOf="@id/title_accuracy"
                app:layout_constraintEnd_toEndOf="parent" />

            <SeekBar
                android:id="@+id/seek_accuracy"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="14dp"
                android:enabled="false"
                android:max="100"
                android:paddingStart="30dp"
                android:paddingEnd="25dp"
                android:progress="@={reviewForm.accuracy}"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/title_accuracy" />

            <LinearLayout
                android:id="@+id/layout_review_contents"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="34dp"
                android:orientation="vertical"
                android:paddingStart="25dp"
                android:paddingEnd="25dp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toEndOf="parent"
                app:layout_constraintTop_toBottomOf="@id/seek_accuracy">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/title_write_review"
                    android:textAppearance="@style/QuotationTitle" />

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <ImageView
                        android:id="@+id/review_photo1"
                        android:layout_width="109.4dp"
                        android:layout_height="106.7dp"
                        android:layout_marginStart="7.7dp"
                        android:layout_marginTop="14dp"
                        android:visibility="@{TextUtils.isEmpty(reviewForm.reviewImage1)? View.GONE: View.VISIBLE}"
                        app:defaultImage="@{@drawable/default_gallery_pic}"
                        app:fileImage="@{reviewForm.reviewImage1}"
                        app:layout_constraintStart_toEndOf="@id/icon_add_photo"
                        app:layout_constraintTop_toTopOf="parent" />

                    <ImageView
                        android:id="@+id/review_photo2"
                        android:layout_width="109.4dp"
                        android:layout_height="106.7dp"
                        android:layout_marginStart="7.7dp"
                        android:layout_marginTop="14dp"
                        android:visibility="@{TextUtils.isEmpty(reviewForm.reviewImage2)? View.GONE: View.VISIBLE}"
                        app:defaultImage="@{@drawable/default_gallery_pic}"
                        app:fileImage="@{reviewForm.reviewImage2}"
                        app:layout_constraintStart_toEndOf="@id/review_photo1"
                        app:layout_constraintTop_toTopOf="parent" />
                </LinearLayout>

                <TextView
                    android:id="@+id/input_review"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="?attr/editTextBackground"
                    android:onClick="@{handler::onClick}"
                    android:text="@{reviewForm.reviewComment}" />

            </LinearLayout>

            <Button
                android:id="@+id/btn_write_review"
                android:layout_width="146dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="34dp"
                android:background="@drawable/bg_rounded_solid_blue"
                android:onClick="@{handler::onClick}"
                android:text="@string/btn_ok"
                android:textAppearance="@style/EstimateProgress"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/layout_review_contents" />

        </androidx.constraintlayout.widget.ConstraintLayout>
    </androidx.core.widget.NestedScrollView>
</layout>
