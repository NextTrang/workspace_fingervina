package com.snstfinger.suri.ui.workingStatus;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.ui.dialog.DeleteRequestDialogFragment;
import com.snstfinger.suri.ui.shop.ShopInfoActivity;

/**
 * 요청한 견적과 요청에 대한 견적제안한 상점리스트
 */
public class QuotationShopActivity extends AppCompatActivity implements QuotationShopListFragment.OnListFragmentInteractionListener {
    private static final String TAG = QuotationShopActivity.class.getName();

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation_shop);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        if(estimateStatus.getBooleanExtra("frompush",false)) {
            Log.d(TAG, "TEST::"+estimateStatus.getStringExtra("requestId"));
        }

        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + mSelectedRequestEstimate);

        if (mSelectedRequestEstimate != null) {
            // enum('REQUEST','BOOKING','BOOKING_CONFIRM','SENDING_PAYMENT','REPAIR_STANDBY','REPAIR','SENDING_PAYMENT_UPDATE','COMPLETE','REVIEW')
            switch (mSelectedRequestEstimate.getStatusCode()) {
                case RequestNPredictionNStoreEntity.TYPE_REQUEST: {
                    actionBar.setTitle(R.string.title_activity_quotation_waiting);
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_BOOKING:
                case RequestNPredictionNStoreEntity.TYPE_BOOKING_CONFIRM: {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT:
                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT_UPDATE: {
                    actionBar.setTitle(R.string.title_activity_quotation_invoice);
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_REVIEW: {
                    actionBar.setTitle(R.string.title_activity_quotation_complete);
                    break;
                }

                default: {
                    actionBar.setTitle(R.string.title_activity_quotation_ongoing);
                    break;
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(StoreNPredictionEntity item) {
        Log.d(TAG, "store clicked::" + item.storeId);
        Intent statusDetail = new Intent(this, ShopInfoActivity.class);
        statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
        statusDetail.putExtra("shop_info", StoreNPredictionEntity.convertToShopInfo(item));
        startActivity(statusDetail);
    }

    public void showDeleteDialog() {
        DeleteRequestDialogFragment dialog = DeleteRequestDialogFragment.newInstance(mSelectedRequestEstimate);
        dialog.show(getSupportFragmentManager(), "filter_dialog");
    }

}
