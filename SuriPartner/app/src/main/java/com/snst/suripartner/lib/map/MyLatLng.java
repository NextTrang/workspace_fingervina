package com.snst.suripartner.lib.map;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class MyLatLng implements Serializable {
    private double latitude;
    private double longitude;

    public MyLatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }
}
