package com.snst.suripartner.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;

import com.haibin.calendarview.CalendarView;
import com.snst.suripartner.R;
import com.snst.suripartner.databinding.PaymentBinding;
import com.snst.suripartner.lib.TimeItemRecycleViewAdapter;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.helper.SimpleWeekView;
import com.snst.suripartner.lib.payment.Payment;
import com.snst.suripartner.lib.payment.PaymentEvent;
import com.snst.suripartner.lib.content_price.ContentPrice;
import com.snst.suripartner.lib.content_price.ContentPriceAdapter;
import com.snst.suripartner.lib.suri.Suri;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class PopSendInvoiceActivity extends AppCompatActivity implements CalendarView.OnCalendarSelectListener, TimeItemRecycleViewAdapter.OnSelectTimeInteractionListener {

    private Suri data;
    private RecyclerView rwSuriPrices;
    private RecyclerView rwPartPrices;
    private RecyclerView rwOptionPrices;
    private List<ContentPrice> suriPrices = new ArrayList<>();
    private List<ContentPrice> partPrices = new ArrayList<>();
    private List<ContentPrice> optionPrices = new ArrayList<>();
    private ContentPriceAdapter suriPricesAdapter;
    private ContentPriceAdapter partPricesAdapter;
    private ContentPriceAdapter optionPricesAdapter;

    private Payment payment;
    private PaymentEvent handler;

    private com.haibin.calendarview.CalendarView mCalendarView;
    private TextView mTitleYearMonth;
    private RecyclerView mListViewTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PaymentBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_send_invoice);

        data = (Suri) getIntent().getSerializableExtra("Suri");

        handler = new PaymentEvent(this);
        binding.setHandler(handler);
        payment = new Payment()
                .setPredictEstId(data.getPredictEstId())
                .setRequestEstId(data.getId());
        binding.setPayment(payment);

        setupPopup();
        setupUI();

        mCalendarView = binding.customCalendarView;
        mTitleYearMonth = binding.titleYearMonth;
        mListViewTimes = binding.listViewTimes;

        Calendar mDesireDate = java.util.Calendar.getInstance();
        setupCalendarView(mDesireDate);
        setupListTimeView(mDesireDate);
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout(width, height);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }

    private void setupUI() {
        //add the first content-price of suri
        suriPrices.add(new ContentPrice());
        rwSuriPrices = findViewById(R.id.suriContentPrice_recyclerview);
        suriPricesAdapter = new ContentPriceAdapter(suriPrices);
        rwSuriPrices.setLayoutManager(new LinearLayoutManager(this));
        rwSuriPrices.setAdapter(suriPricesAdapter);

        //add the first content-price of part
        partPrices.add(new ContentPrice());
        rwPartPrices = findViewById(R.id.partContentPrice_recyclerview);
        partPricesAdapter = new ContentPriceAdapter(partPrices);
        rwPartPrices.setLayoutManager(new LinearLayoutManager(this));
        rwPartPrices.setAdapter(partPricesAdapter);

        //add the first content-price of option
        optionPrices.add(new ContentPrice());
        rwOptionPrices = findViewById(R.id.optionContentPrice_recyclerview);
        optionPricesAdapter = new ContentPriceAdapter(optionPrices);
        rwOptionPrices.setLayoutManager(new LinearLayoutManager(this));
        rwOptionPrices.setAdapter(optionPricesAdapter);
    }

    public void addPrice(int code) {
        //add new field of content price
        switch (code) {
            case 0://add for suri
                ContentPrice lastSuriContentPrice = suriPrices.get(suriPrices.size() - 1);
                if (!lastSuriContentPrice.getContent().isEmpty() && !lastSuriContentPrice.getPrice().isEmpty()) {
                    payment.setSuriPrices(suriPrices);
                    suriPrices.add(new ContentPrice());
                    suriPricesAdapter.notifyDataSetChanged();
                }
                break;
            case 1://add for part
                ContentPrice lastPartContentPrice = partPrices.get(partPrices.size() - 1);
                if (!lastPartContentPrice.getContent().isEmpty() && !lastPartContentPrice.getPrice().isEmpty()) {
                    payment.setPartPrices(partPrices);
                    partPrices.add(new ContentPrice());
                    partPricesAdapter.notifyDataSetChanged();
                }
                break;
            case 2://add for option
                ContentPrice lastOptionPrice = optionPrices.get(optionPrices.size() - 1);
                if (!lastOptionPrice.getContent().isEmpty() && !lastOptionPrice.getPrice().isEmpty()) {
                    payment.setOptionPrices(optionPrices);
                    optionPrices.add(new ContentPrice());
                    optionPricesAdapter.notifyDataSetChanged();
                }
                break;
        }

        payment.updateTotalPrice();
    }

    private void setupCalendarView(Calendar mDesireDate) {
        // set estimated complete time to calendar
        long estCompletedDate = data.getEstCompletedDate();
        if (estCompletedDate > 0) {
            mDesireDate.setTimeInMillis(estCompletedDate);
        }

        mCalendarView.scrollToCalendar(mDesireDate.get(java.util.Calendar.YEAR), mDesireDate.get(java.util.Calendar.MONTH) + 1, mDesireDate.get(java.util.Calendar.DATE));
        mCalendarView.setOnCalendarSelectListener(this);
        SimpleWeekView.enable = false;
        mTitleYearMonth.setText(SimpleWeekView.getMonthName(mDesireDate.get(java.util.Calendar.MONTH) + 1) + " " + mDesireDate.get(java.util.Calendar.YEAR));
    }

    private void setupListTimeView(Calendar mDesireDate) {
        // timetable
        String estCompletedTime = data.getEstCompletedTime();
        if (!TextUtils.isEmpty(estCompletedTime)) {
            String time[] = estCompletedTime.split(":");
            mDesireDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
            mDesireDate.set(java.util.Calendar.MINUTE, Integer.valueOf(time[1]));
            mDesireDate.set(java.util.Calendar.SECOND, 0);
        }

        ArrayList<String> mItems = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_table)));
        TimeItemRecycleViewAdapter mAdapter = new TimeItemRecycleViewAdapter(this, mItems, this,false);
        mListViewTimes.setAdapter(mAdapter);
        setTimeTable(mDesireDate);
    }

    private void setTimeTable(java.util.Calendar dateTime) {
        TimeItemRecycleViewAdapter adapter = (TimeItemRecycleViewAdapter) mListViewTimes.getAdapter();
        int position = adapter.getPositionFromDate(dateTime);
        adapter.setSelectTime(position);
        mListViewTimes.scrollToPosition(position);
    }

    @Override
    public void onCalendarOutOfRange(com.haibin.calendarview.Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(com.haibin.calendarview.Calendar calendar, boolean isClick) {
        mTitleYearMonth.setText(SimpleWeekView.getMonthName(calendar.getMonth()) + " " + calendar.getYear());

        if (isClick) {
            payment.setEstCompletedDate(Conversion.date_2_string(calendar.getTimeInMillis()));
        }
    }

    @Override
    public void onSelectTimeInteraction(String item) {
        if (!TextUtils.isEmpty(item)) {
            payment.setEstCompletedTime(item);
        }
    }
}
