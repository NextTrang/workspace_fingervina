package com.snst.suripartner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                //add close action here
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUI() {
        Toolbar quotationToolBar = findViewById(R.id.forgot_password_toolbar);
        setSupportActionBar(quotationToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
