package com.snst.suripartner.lib.repair_type;

import java.io.Serializable;

public class RepairType implements Serializable {
    private String name;

    public RepairType(String name) {
        this.name = name;
    }

    //Getter

    public String getName() {
        return name;
    }
}
