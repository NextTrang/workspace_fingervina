package com.snstfinger.suri.network;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

public class FacebookLoginCallback implements FacebookCallback<LoginResult> {
    @Override
    public void onSuccess(com.facebook.login.LoginResult loginResult) {
        // App code
        Log.d("FB_callback","success::"+loginResult);

        requestMe(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
        // App code
        Log.d("FB_callback","cancel::");
    }

    @Override
    public void onError(FacebookException exception) {
        // App code
        Log.d("FB_callback","error::"+exception);
    }

    public void requestMe(AccessToken token) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
                public void onCompleted(JSONObject object, GraphResponse response) {

                    Log.d("FB_callback","result::"+object==null?"null":toString());
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }
}
