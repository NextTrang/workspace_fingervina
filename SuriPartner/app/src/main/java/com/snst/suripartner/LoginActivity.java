package com.snst.suripartner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.snst.suripartner.databinding.LoginActivityBinding;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbRequest;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.api.RequestLogin;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private EditText txtID;
    private EditText txtPass;
    private Button btnLogin;
    private TextView tvForgot;
    private TextView tvSignup;

    private String cookie;
    private String mSuriToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginActivityBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        binding.setEffect(Sharing.Instance().getEffect());

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()) {
                    Log.w(TAG, "FCM Log::getInstance failed."+task.getException());

                    finish();
                }

                mSuriToken = task.getResult().getToken();
                Log.d(TAG,"FCM Log::Token."+mSuriToken);

            }
        });


        if(Sharing.Instance().getUser()!=null && !Sharing.Instance().getCookie().isEmpty()){
            Sharing.Instance().setLoadDataFinish(false);
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            return;
        }

        Sharing.Instance().reset();

        setupUI();
        setupButtonEvents();
    }

    private void setupUI() {
        txtID = findViewById(R.id.txtID);
        txtPass = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvForgot = findViewById(R.id.tvForgot);
        tvSignup = findViewById(R.id.tvSignup);
    }

    private void setupButtonEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(i);
            }
        });
    }

    private void doLogin() {
        if (validate()) {
            new RequestLogin().execute(this, txtID.getText().toString(), txtPass.getText().toString(), mSuriToken, LbRequest.LbUser.ACCOUNT);
        }
    }

    private boolean validate() {
        boolean result = false;

        String name = txtID.getText().toString();
        String password = txtPass.getText().toString();

        if (!name.isEmpty() && !password.isEmpty()) {
            result = true;
        }

        return result;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putString(LbData.LbInfo.USER_ID,txtID.getText().toString());
//        outState.putString(LbData.LbInfo.);
    }
}
