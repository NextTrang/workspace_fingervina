package com.snst.suripartner.labels;

public class LbData {
    public static final String STATUS = "status";
    public static final String MSG = "msg";
    public static final String DATA = "data";

    public static class LbPageMap {
        public static final String PAGE_MAP = "pageMap";
        public static final String NEXT_PAGE = "nextPage";
    }

    public static class LbInfo {
        public static final String INFO = "Info";
        public static final String STORE_REQUEST_ESTIMATE_ID = "store_request_estimate_id";
        public static final String ESTIMATE_IMAGE_1 = "estimate_image1";
        public static final String BRAND_NAME = "brand_name";
        public static final String REGISTER_DATE = "register_date";
        public static final String ESTIMATE_ADDRESS_1 = "estimate_address_1";
        public static final String ESTIMATE_ADDRESS_LL_1 = "estimate_address_ll_1";
        public static final String STATUS = "status";
        public static final String REQUEST = "REQUEST";
        public static final String BOOKING = "BOOKING";
        public static final String BOOKING_CONFIRM = "BOOKING_CONFIRM";
        public static final String SENDING_PAYMENT = "SENDING_PAYMENT";
        public static final String REPAIR_STANDBY = "REPAIR_STANDBY";
        public static final String REPAIR = "REPAIR";
        public static final String SENDING_PAYMENT_UPDATE = "SENDING_PAYMENT_UPDATE";
        public static final String COMPLETE = "COMPLETE";
        public static final String WRITE_YN = "write_yn";
        public static final String ESTIMATE_REPAIR_TYPE_NAMES = "estimate_repair_type_names";
        public static final String ESTIMATE_DESCRIPTION = "estimate_description";
        public static final String MODEL_NAME = "model_name";
        public static final String PREDICTION_ESTIMATE_DATE = "prediction_estimate_date";
        public static final String PREDICTION_END_DATE = "prediction_end_date";
        public static final String PREDICTION_PRICE = "prediction_price";
        public static final String REQUEST_ESTIMATE_ID = "request_estimate_id";
        public static final String PREDICTION_PRICE_COMMENT = "prediction_price_comment";
        public static final String USER_ID = "user_id";
        public static final String DESIRED_RESERVATION_DATE = "desired_reservation_date";
        public static final String DESIRED_RESERVATION_DATE_YN = "desired_reservation_date_yn";
        public static final String REPAIR_COMMENT = "repair_comment";
        public static final String PRICE = "price";
        public static final String USER_NAME = "user_name";
        public static final String BOOKER_PHONE_NUMBER = "booker_phonenumber";
        public static final String BOOKING_DATE = "booking_date";
        public static final String PREDICTION_ESTIMATE_ID = "prediction_estimate_id";
        public static final String BOOKER_NAME = "booker_name";
        public static final String PROGRESS_RATE = "progress_rate";
        public static final String TITLE = "title";
        public static final String CONTENT = "contents";
        public static final String SURI_IMAGE1 = "suri_image1";
        public static final String SCORE = "score";
        public static final String MANAGER_PICTURE = "manager_picture";
        public static final String PREDICTION_IMAGE_1 = "prediction_image1";
        public static final String PREDICTION_IMAGE_2 = "prediction_image2";
        public static final String REVIEW_COMMENT = "review_comment";
        public static final String REVIEW_IMAGE1 = "review_image1";
        public static final String REVIEW_IMAGE2 = "review_image2";
        public static final String STORE_BUSINESS_NUMBER = "store_business_number";
        public static final String SURI_TODO = "todo";
        public static final String SURI_COMPLETE = "complete";
    }

    public static class LbUserInfo {
        public static final String USER_INFO = "userInfo";
        public static final String STORE_ID = "store_id";
        public static final String USER_NAME = "user_name";
    }

}
