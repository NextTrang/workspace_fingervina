package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.databinding.ActivitySuricaseDetailsBinding;
import com.snstfinger.suri.ui.estimate.ImageItemRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class SuriCaseDetailsActivity extends AppCompatActivity {
    private static final String TAG = SuriCaseDetailsActivity.class.getName();

    private SuriCaseEntity mSuriCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySuricaseDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_suricase_details);
        getWindow().setBackgroundDrawable(new ColorDrawable(0xccffffff));

        Intent suriCase = getIntent();
        mSuriCase = (SuriCaseEntity) suriCase.getSerializableExtra("case");
        Log.d(TAG, "Selected Suri case::" + mSuriCase);

        binding.setSuriCase(mSuriCase);
        binding.btnClose.setOnClickListener(v -> finish());

        List<String> items = new ArrayList<>();
        if (!TextUtils.isEmpty(mSuriCase.getPredictionImage1())) {
            items.add(mSuriCase.getPredictionImage1());
        }
        if (!TextUtils.isEmpty(mSuriCase.getPredictionImage2())) {
            items.add(mSuriCase.getPredictionImage2());
        }

        Log.d(TAG, "Number of selected Images: " + items.size());
        if (!items.isEmpty()) {
            ImageItemRecyclerViewAdapter adapter = new ImageItemRecyclerViewAdapter(items, false);
            binding.viewPager.setAdapter(adapter);
        }
    }
}
