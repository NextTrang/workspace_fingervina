package com.snstfinger.suri.ui.shop;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.snstfinger.suri.data.datasource.ReviewListDataSource;
import com.snstfinger.suri.data.datasource.ReviewListDataSourceFactory;
import com.snstfinger.suri.data.entity.ReviewEntity;

public class ReviewViewModel extends ViewModel {
//    private ReviewRepository repository = new ReviewRepository();
//
//    public LiveData<List<ReviewEntity>> getAllReview(String storeId) {
//
//        return repository.getMutableLiveData(storeId);
//    }

    LiveData<PagedList<ReviewEntity>> reviewPagedList = new MutableLiveData<>();

    public LiveData<PagedList<ReviewEntity>> getReviewPagedList(@Nullable String storeId) {
        ReviewListDataSourceFactory factory = new ReviewListDataSourceFactory(storeId);

         LiveData<PageKeyedDataSource<Integer, ReviewEntity>> liveDataSource = factory.getReviewLiveData();

        PagedList.Config config = new PagedList.Config.Builder().setInitialLoadSizeHint(ReviewListDataSource.PAGE_SIZE).setPageSize(ReviewListDataSource.PAGE_SIZE).build();

        reviewPagedList = new LivePagedListBuilder(factory, config).setBoundaryCallback(new PagedList.BoundaryCallback<ReviewEntity>() {
            @Override
            public void onZeroItemsLoaded() {
                super.onZeroItemsLoaded();

                Log.d("MOON","Zeroooooooooooo!!");
            }

            @Override
            public void onItemAtFrontLoaded(@NonNull ReviewEntity itemAtFront) {
                super.onItemAtFrontLoaded(itemAtFront);
                Log.d("MOON","onItemAtFrontLoaded!!::"+itemAtFront.getReivewComment());
            }

            @Override
            public void onItemAtEndLoaded(@NonNull ReviewEntity itemAtEnd) {
                super.onItemAtEndLoaded(itemAtEnd);
                Log.d("MOON","onItemAtEndLoaded!!"+itemAtEnd.getReivewComment());
            }
        }).build();

        return reviewPagedList;
    }

}
