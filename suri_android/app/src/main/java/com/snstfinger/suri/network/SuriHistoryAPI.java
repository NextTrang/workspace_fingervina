package com.snstfinger.suri.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SuriHistoryAPI {
    @FormUrlEncoded
    @POST("surihistory/getAllSuriHistoryByRequestEstimateId")
    Call<CommonResponse> doPostGetAllSuriHistoryByRequestEstimateId(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId, @Field("page") String page);


}
