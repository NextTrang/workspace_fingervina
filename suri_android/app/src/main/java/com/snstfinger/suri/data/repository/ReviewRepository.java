package com.snstfinger.suri.data.repository;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.ReviewAPI;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewRepository {
    private static final String TAG = ReviewRepository.class.getName();

    private ArrayList<ReviewEntity> reviewEntities = new ArrayList<>();
    private MutableLiveData<List<ReviewEntity>> mutableLiveData = new MutableLiveData<>();

    public MutableLiveData<List<ReviewEntity>> getMutableLiveData(String storeId) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetReviewByStoreId(cookie, storeId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<ReviewEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    reviewEntities = gson.fromJson(asJsonArray, itemType);
                    if (!reviewEntities.isEmpty()) {
                        Log.d(TAG, "ReviewEntity size::" + reviewEntities.size());
                        mutableLiveData.setValue(reviewEntities);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });

        return mutableLiveData;
    }
}
