package com.snst.suripartner.lib.suri;

import android.view.View;

import androidx.databinding.BaseObservable;

import com.snst.suripartner.R;
import com.snst.suripartner.lib.helper.Checker;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.helper.Getter;
import com.snst.suripartner.lib.quotation.Quotation;
import com.snst.suripartner.lib.quotation.QuotationNavigator;

public class SuriViewModel extends BaseObservable {
    public String imageUrl;
    public String deviceName;
    public String predictPrice="";
    public String predictPriceCurrency="";
    public String price="";
    public String priceCurrency="";
    public String id;
    public String userName;
    public String phoneNumber;
    public String pickupAddress;
    public String predictionEstDate;
    public long estCompletedDate;
    public String estCompletedTime;
    public String bookingTime;
    public String state;
    public String timestamp;

    public int visibleCustomerInfos;

    //Using for send Invoice
    public String predictEstId;
    public String stateMask;

    //Using for Suri Ongoing Detail
    public int progressRate;
    public String bookerName;
    public String elapsedTime;

    //Using for Suri Complete tab
    public String score;
    public String predictionEndDate;
    public String completedTime="";

    public boolean outOfDate;

    private SuriNavigator navigator;

    //Using in Suri Waiting & Complete Detail
    public String predictImageUrl_1;
    public String predictImageUrl_2;
    public String repairComment;

    public SuriViewModel(Suri suri) {
        this.imageUrl = suri.getImageUrl();
        this.deviceName = suri.getDeviceName();
        setPredictPrice(suri.getPredictPrice());
        setPrice(suri.getPrice());
        this.id = suri.getId();
        setVisibleCustomerInfos(View.GONE);
        this.userName = suri.getUserName();
        this.phoneNumber = suri.getPhoneNumber();
        this.pickupAddress = suri.getPickupAddress();
        this.estCompletedDate = suri.getEstCompletedDate();
        this.estCompletedTime = suri.getEstCompletedTime();
        this.bookingTime = suri.getBookingTime();
        this.state = suri.getState();
        this.predictEstId = suri.getPredictEstId();
        this.stateMask = suri.getStateMask();
        this.timestamp = suri.getTimestamp();
        this.progressRate = suri.getProgressRate();
        this.bookerName = suri.getBookerName();
        this.score = suri.getScore();
        this.predictionEstDate = suri.getPredictionEstDate();
        this.predictImageUrl_1 = suri.getPredictImageUrl_1();
        this.predictImageUrl_2 = suri.getPredictImageUrl_2();
        this.repairComment = suri.getRepairComment();

        setOutOfDate(Checker.isOutOfDate(Conversion.string_2_date_time(predictionEstDate)));

        long days = Getter.getElapsedTime(bookingTime);
        if(days < 2){
            elapsedTime = days + " Day";
        }
        else {
            elapsedTime = days + " Days";
        }

        this.predictionEndDate = suri.getPredictionEndDate();
        if(!predictionEndDate.isEmpty()){
            String[] timeArr = Getter.getCompletedTime(bookingTime,predictionEndDate);
            completedTime = timeArr[0]+"d"+timeArr[1]+"h"+timeArr[2]+"m";
        }
    }

    public void onItemClick(Suri suri) {
        navigator.onItemClick(suri);
    }

    //Setter

    public void setVisibleCustomerInfos(int visibleCustomerInfos) {
        this.visibleCustomerInfos = visibleCustomerInfos;
        notifyChange();
    }

    public void setOutOfDate(boolean outOfDate) {
        this.outOfDate = outOfDate;
        notifyChange();
    }

    public void setNavigator(SuriNavigator navigator) {
        this.navigator = navigator;
    }

    public void setPrice(String price) {
        this.price = price;
        this.priceCurrency = Conversion.number_2_currency(price);
    }

    public void setPredictPrice(String predictPrice) {
        this.predictPrice = predictPrice;
        this.predictPriceCurrency = Conversion.number_2_currency(predictPrice);
    }
}
