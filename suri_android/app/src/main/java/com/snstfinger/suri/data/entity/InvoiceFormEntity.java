package com.snstfinger.suri.data.entity;

import android.text.TextUtils;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InvoiceFormEntity {
    @SerializedName("id")
    public String id;

    @SerializedName("prediction_estimate_id")
    public String predictionEstimateId;

    @SerializedName("request_estimate_id")
    public String requestEstimateId;

    @SerializedName("fix_1")
    public String fix1;

    @SerializedName("fix_1_price")
    public String fixPrice1;

    @SerializedName("fix_2")
    public String fix2;

    @SerializedName("fix_2_price")
    public String fixPrice2;

    @SerializedName("fix_3")
    public String fix3;

    @SerializedName("fix_3_price")
    public String fixPrice3;

    @SerializedName("fix_4")
    public String fix4;

    @SerializedName("fix_4_price")
    public String fixPrice4;

    @SerializedName("fix_5")
    public String fix5;

    @SerializedName("fix_5_price")
    public String fixPrice5;

    @SerializedName("part_1")
    public String part1;

    @SerializedName("part_1_price")
    public String partPrice1;

    @SerializedName("part_2")
    public String part2;

    @SerializedName("part_2_price")
    public String partPrice2;

    @SerializedName("part_3")
    public String part3;

    @SerializedName("part_3_price")
    public String partPrice3;

    @SerializedName("part_4")
    public String part4;

    @SerializedName("part_4_price")
    public String partPrice4;

    @SerializedName("part_5")
    public String part5;

    @SerializedName("part_5_price")
    public String partPrice5;

    @SerializedName("option_1")
    public String option1;

    @SerializedName("option_1_price")
    public String optionPrice1;

    @SerializedName("option_2")
    public String option2;

    @SerializedName("option_2_price")
    public String optionPrice2;

    @SerializedName("option_3")
    public String option3;

    @SerializedName("option_3_price")
    public String optionPrice3;

    @SerializedName("option_4")
    public String option4;

    @SerializedName("option_4_price")
    public String optionPrice4;

    @SerializedName("option_5")
    public String option5;

    @SerializedName("option_5_price")
    public String optionPrice5;

    @SerializedName("old_total_price")
    public String oldTotalPrice;

    @SerializedName("total_price")
    public String totalPrice;

    @SerializedName("count")
    public Integer count;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date registerDate;

    @SerializedName("price_comment")
    public String priceComment;

    public String getTotalFix() {
        int price1 = TextUtils.isEmpty(fixPrice1) ? 0 : (int) Float.parseFloat(fixPrice1);
        int price2 = TextUtils.isEmpty(fixPrice2) ? 0 : (int) Float.parseFloat(fixPrice2);
        int price3 = TextUtils.isEmpty(fixPrice3) ? 0 : (int) Float.parseFloat(fixPrice3);
        int price4 = TextUtils.isEmpty(fixPrice4) ? 0 : (int) Float.parseFloat(fixPrice4);
        int price5 = TextUtils.isEmpty(fixPrice5) ? 0 : (int) Float.parseFloat(fixPrice5);

        return getPriceFormatted(price1 + price2 + price3 + price4 + price5);
    }

    public String getTotalPart() {
        int price1 = TextUtils.isEmpty(partPrice1) ? 0 : (int) Float.parseFloat(partPrice1);
        int price2 = TextUtils.isEmpty(partPrice2) ? 0 : (int) Float.parseFloat(partPrice2);
        int price3 = TextUtils.isEmpty(partPrice3) ? 0 : (int) Float.parseFloat(partPrice3);
        int price4 = TextUtils.isEmpty(partPrice4) ? 0 : (int) Float.parseFloat(partPrice4);
        int price5 = TextUtils.isEmpty(partPrice5) ? 0 : (int) Float.parseFloat(partPrice5);

        return getPriceFormatted(price1 + price2 + price3 + price4 + price5);
    }

    public String getTotalOption() {
        int price1 = TextUtils.isEmpty(optionPrice1) ? 0 : (int) Float.parseFloat(optionPrice1);
        int price2 = TextUtils.isEmpty(optionPrice2) ? 0 : (int) Float.parseFloat(optionPrice2);
        int price3 = TextUtils.isEmpty(optionPrice3) ? 0 : (int) Float.parseFloat(optionPrice3);
        int price4 = TextUtils.isEmpty(optionPrice4) ? 0 : (int) Float.parseFloat(optionPrice4);
        int price5 = TextUtils.isEmpty(optionPrice5) ? 0 : (int) Float.parseFloat(optionPrice5);

        return getPriceFormatted(price1 + price2 + price3 + price4 + price5);
    }

    private String getPriceFormatted(int price) {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return formatter.format(price);
    }

    public String getRegisterDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return registerDate == null ? "" : formatter.format(registerDate);
    }

    public String getFixPrice1Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(fixPrice1) ? "" : formatter.format(Float.parseFloat(fixPrice1));
    }

    public String getFixPrice2Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(fixPrice2) ? "" : formatter.format(Float.parseFloat(fixPrice2));
    }

    public String getFixPrice3Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(fixPrice3) ? "" : formatter.format(Float.parseFloat(fixPrice3));
    }

    public String getFixPrice4Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(fixPrice4) ? "" : formatter.format(Float.parseFloat(fixPrice4));
    }

    public String getFixPrice5Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(fixPrice5) ? "" : formatter.format(Float.parseFloat(fixPrice5));
    }

    public String getPartPrice1Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(partPrice1) ? "" : formatter.format(Float.parseFloat(partPrice1));
    }

    public String getPartPrice2Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(partPrice2) ? "" : formatter.format(Float.parseFloat(partPrice2));
    }

    public String getPartPrice3Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(partPrice3) ? "" : formatter.format(Float.parseFloat(partPrice3));
    }

    public String getPartPrice4Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(partPrice4) ? "" : formatter.format(Float.parseFloat(partPrice4));
    }

    public String getPartPrice5Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(partPrice5) ? "" : formatter.format(Float.parseFloat(partPrice5));
    }

    public String getOptionPrice1Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(optionPrice1) ? "" : formatter.format(Float.parseFloat(optionPrice1));
    }

    public String getOPtionPrice2Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(optionPrice2) ? "" : formatter.format(Float.parseFloat(optionPrice2));
    }

    public String getOptionPrice3Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(optionPrice3) ? "" : formatter.format(Float.parseFloat(optionPrice3));
    }

    public String getOptionPrice4Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(optionPrice4) ? "" : formatter.format(Float.parseFloat(optionPrice4));
    }

    public String getOptionPrice5Formatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(optionPrice5) ? "" : formatter.format(Float.parseFloat(optionPrice5));
    }

    public String getOldTotalPrice() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return oldTotalPrice == null ? "0" : formatter.format(Float.parseFloat(oldTotalPrice));
    }

    public String getTotalPrice() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return totalPrice == null ? "0" : formatter.format(Float.parseFloat(totalPrice));
    }
}
