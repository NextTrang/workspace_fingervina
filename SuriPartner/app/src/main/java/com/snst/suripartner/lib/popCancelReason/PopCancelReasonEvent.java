package com.snst.suripartner.lib.popCancelReason;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.labels.LbPop;

public class PopCancelReasonEvent {

    Context context;

    public PopCancelReasonEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onCancel(String reasonId, String reasonComment) {
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.REASON_TYPE_ID, reasonId);
        i.putExtra(LbPop.LbKey.REASON_COMMENT, reasonComment);

        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();

    }
}
