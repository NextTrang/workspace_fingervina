package com.snst.suripartner.labels;

public class LbPop {

    public static class LbKey {
        public static final String POP_CODE = "PopCode";
        public static final String POP_TITLE = "PopTitle";
        public static final String BOOKING = "Booking";
        public static final String REASON_TYPE_ID = "ReasonTypeId";
        public static final String REASON_COMMENT = "ReasonComment";
        public static final String SURI = "Suri";
        public static final String SURI_HISTORY = "SuriHistory";
        public static final String SURI_COMPLETE = "SuriComplete";
        public static final String QUOTATION = "Quotation";
        public static final String PAYMENT = "Payment";
        public static final String PHOTO_URI = "PhotoUri";
        public static final String OPEN_CAMERA = "OpenCamera";
        public static final String PHOTO_FILE = "PhotoFile";
        public static final String POP_CONFIRM = "PopConfirm";
        public static final String MY_MAP = "MyMap";

    }

    public static class LbCode {
        public static final int CANCEL_BOOKING = 0;
        public static final int LEFT_MENU = 1;
        public static final int CANCEL_REASON = 2;
        public static final int SEND_INVOICE = 3;
        public static final int QUOTATION_DETAIL = 4;
        public static final int SURI_ONGOING_DETAIL = 5;
        public static final int WRITE_SURI_HISTORY = 6;
        public static final int CAMERA_REQUEST = 7;
        public static final int REQUEST_COMPLETE_SURI = 8;
        public static final int SEE_MAP = 9;
        public static final int NOTICE_LEFT_MENU = 10;
        public static final int SURI_WAITING_DETAIL = 11;
        public static final int SURI_COMPLETE_DETAIL = 12;
    }

    public static class LbTitle {
        public static final String CANCEL_BOOKING = "Cancel Booking";
        public static final String SURI_IS_COMPLETED = "SURI is completed";
        public static final String BOOKING_IS_CONFIRMED = "Booking is confirmed";
        public static final String PRICE_IS_CHANGED = "Price is changed";
        public static final String OFFER_COMPLETED = "Offer completed";
    }

    public static class LbContent{
        public static final String SURI_IS_COMPLETED = "Thank you for your efforts\nLet’s wait customer to pick up";
        public static final String BOOKING_IS_CONFIRMED = "—";
        public static final String PRICE_IS_CHANGED = "You can’t change\nprice anymore";
        public static final String OFFER_COMPLETED = "—";
    }
}
