package com.snstfinger.suri.data.model;

public enum ObjectId {
    PHONE(1), PC(2), BIKE(3), CAR(4);

    private final int value;

    private ObjectId(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
