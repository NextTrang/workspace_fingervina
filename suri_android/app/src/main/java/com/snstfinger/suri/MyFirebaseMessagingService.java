package com.snstfinger.suri;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.snstfinger.suri.data.datasource.SuriDatabase;
import com.snstfinger.suri.data.entity.InboxEntity;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.repository.InboxRepository;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.ui.tools.WebChatActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationShopActivity;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getName();

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Log.d(TAG, "FCM Log::Refresh token." + token);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "FCM Log::Received Notification." + remoteMessage.getNotification().getBody());
        String msgTitle = remoteMessage.getNotification().getTitle();
        String msgBody = remoteMessage.getNotification().getBody();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("title", msgTitle);
        intent.putExtra("body", msgBody);
        intent.putExtra("frompush", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        InboxEntity notice = new InboxEntity(msgTitle);
        notice.setContents(msgBody);
        insertNoti(notice);

        showNoti(intent);
    }

    // for test
    // parsing bodyData from push msg
    private void parseNoti(RemoteMessage remoteMessage) {
        String msgTitle = remoteMessage.getNotification().getTitle();
        String msgBody = remoteMessage.getNotification().getBody();

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "FCM Log::Received Notification. data payload::" + remoteMessage.getData());
        }

        JsonParser parser = new JsonParser();
        JsonObject json = (JsonObject) parser.parse(msgBody);
        String notiType = json.getAsJsonPrimitive("type").getAsString();

        Intent intent = new Intent();
        intent.putExtra("title", msgTitle);
        intent.putExtra("frompush", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (notiType.equalsIgnoreCase("notice")) {

        } else if (notiType.equalsIgnoreCase("inquire")) {
            intent.setClass(this, WebChatActivity.class);

            String requestId = json.getAsJsonPrimitive("requestId").getAsString();
            String targetId = json.getAsJsonPrimitive("targetId").getAsString();
            getRequestEstimate(requestId, intent);
            intent.putExtra("requestId", requestId);
            intent.putExtra("targetId", targetId);

        } else if (notiType.equalsIgnoreCase("quote")) {
            intent.setClass(this, QuotationShopActivity.class);

            String requestId = json.getAsJsonPrimitive("requestId").getAsString();
            getRequestEstimate(requestId, intent);
        }
    }

    private void showNoti(Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Channel ID";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(intent.getStringExtra("title"))
                .setContentText(intent.getStringExtra("body"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = "Channel Name";
            NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }


        notificationManager.notify(0, notificationBuilder.build());
    }

    private void insertNoti(InboxEntity notice) {
        SuriDatabase db = SuriDatabase.getSuriDatabase(this);

        new InboxRepository.InsertAsyncTask(db.getInboxDao()).execute(notice);
    }

    private void getRequestEstimate(String requestEstimateId, Intent intent) {
        if (requestEstimateId == null)
            return;

        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetRequestEstimateNPredictionNStore(cookie, requestEstimateId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object info = ((LinkedTreeMap) data).get("Info");
                    JsonObject json = gson.toJsonTree(info).getAsJsonObject();
                    RequestNPredictionNStoreEntity item = gson.fromJson(json, RequestNPredictionNStoreEntity.class);

                    if (item != null) {
                        Log.d(TAG, "Update Req::" + item);
                        intent.putExtra("request_estimate", item);

                        showNoti(intent);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetRequestEstimateNPredictionNStore onFailure");
            }
        });
    }
}
