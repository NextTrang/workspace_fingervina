package com.snstfinger.suri.network;

import android.content.Context;
import android.util.Log;
import android.webkit.CookieManager;
import android.widget.Toast;

import com.snstfinger.suri.lib.Brand;
import com.snstfinger.suri.pop_up.PopFilterSuriCaseActivity;
import com.snstfinger.suri.ui.login.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestBrandsByObjectId implements ApiRequest {

    private static final String TAG = "RequestFilterSuriCase";

    @Override
    public void execute(Context context, String... inputs) {
        if (context.getClass() != PopFilterSuriCaseActivity.class) {
            Log.e(TAG, "execute: This request only should be called in PopFilterSuriCaseActivity");
            return;
        }

        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<ResponseBody> call = storeAPI.getBrandsByObjectId(cookie, inputs[0]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString("msg");

                    if (msg.equals("OK")) {
                        JSONObject jsData = jsonObject.getJSONObject("data");
                        JSONArray jsInfos = jsData.getJSONArray("Info");

                        List<Brand> brands = new ArrayList<>();

                        for (int i = 0; i < jsInfos.length(); ++i) {
                            JSONObject jsInfo = jsInfos.getJSONObject(i);

                            Brand brand = new Brand(jsInfo);
                            brands.add(brand);
                        }

                        ((PopFilterSuriCaseActivity)context).updateBrand(brands);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
