package com.snstfinger.suri.data.model;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.BR;

import java.io.File;
import java.io.Serializable;

public class ReviewForm extends BaseObservable implements Serializable {
    @SerializedName("request_estimate_id")
    private String requestEstimateId;

    @SerializedName("prediction_estimate_id")
    private String predictionEstimateId;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("review_comment")
    private String reviewComment;

    @SerializedName("quality")
    private int quality;

    @SerializedName("kindness")
    private int kindness;

    private int accuracy;

    private int displayQuality;
    private int displayKindness;
    private int displayAccuracy;

    private int totalScore;

    @SerializedName("review_image1")
    @Bindable
    private String reviewImage1;

    @SerializedName("review_image2")
    @Bindable
    private String reviewImage2;

    public String getRequestEstimateId() {
        return requestEstimateId;
    }

    public void setRequestEstimateId(String requestEstimateId) {
        this.requestEstimateId = requestEstimateId;
    }

    public String getPredictionEstimateId() {
        int digit = (int) Float.parseFloat(predictionEstimateId);
        return String.format("%d", digit);
    }

    public void setPredictionEstimateId(String predictionEstimateId) {
        this.predictionEstimateId = predictionEstimateId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStoreId() {
        int digit = (int) Float.parseFloat(storeId);
        return String.format("%d", digit);
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(String reviewComment) {
        this.reviewComment = reviewComment;
    }

    public String getReviewImage1() {
        return reviewImage1;
    }

    public void setReviewImage1(String reviewImage1) {
        this.reviewImage1 = reviewImage1;
        notifyPropertyChanged(BR.reviewImage1);
    }

    public String getReviewImage2() {
        return reviewImage2;
    }

    public void setReviewImage2(String reviewImage2) {
        this.reviewImage2 = reviewImage2;
        notifyPropertyChanged(BR.reviewImage2);
    }

    @Bindable
    public int getQuality() {
        return quality;
    }

    public void setQuality(int progress) {
        this.quality = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.quality);
        setDisplayQuality(progress);
        updateTotalScore();
    }

    @Bindable
    public String getDisplayQuality() {
        return String.format("%.1f", (displayQuality / 20f));
    }

    public void setDisplayQuality(int progress) {
        this.displayQuality = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.displayQuality);
    }

    @Bindable
    public int getKindness() {
        return kindness;
    }

    public void setKindness(int progress) {
        this.kindness = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.kindness);
        setDisplayKindness(progress);
        updateTotalScore();
    }

    @Bindable
    public String getDisplayKindness() {
        return String.format("%.1f", (displayKindness / 20f));
    }

    public void setDisplayKindness(int progress) {
        this.displayKindness = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.displayKindness);
    }

    @Bindable
    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int progress) {
        this.accuracy = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.accuracy);
        setDisplayAccuracy(progress);
        updateTotalScore();
    }

    @Bindable
    public String getDisplayAccuracy() {
        return String.format("%.1f", (displayAccuracy / 20f));
    }

    public void setDisplayAccuracy(int progress) {
        this.displayAccuracy = progress;
        notifyPropertyChanged(com.snstfinger.suri.BR.displayAccuracy);
    }

    @Bindable
    public Float getTotalScore() {
        return (totalScore / 20f);
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
        notifyPropertyChanged(com.snstfinger.suri.BR.totalScore);
    }

    private void updateTotalScore() {
        int total = displayQuality + displayKindness + displayAccuracy;
        setTotalScore(total / 3);
    }

    @BindingAdapter(value = {"fileImage", "defaultImage"})
    public static void loadFileImage(ImageView imageView, String filePath, Drawable res) {
        Log.d("Lazy IMG", "StoreIMG::" + filePath);
        File file = null;
        if (filePath != null) {
            file = new File(filePath);
        }

        RequestManager loader = Glide.with(imageView.getContext());
        loader.load(file)
                .placeholder(res)
                .into(imageView);
    }
}
