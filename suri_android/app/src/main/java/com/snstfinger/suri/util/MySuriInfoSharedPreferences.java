package com.snstfinger.suri.util;

import android.content.Context;

public class MySuriInfoSharedPreferences extends SharedPrefsManager {
    public final String KEY_REQUEST_ID = "request_estimate_id";
    public final String KEY_OBJECT_ID = "object_id";
    public final String KEY_EST_IMG1 = "estimate_image1";
    public final String KEY_EST_IMG2= "estimate_image2";
    public final String KEY_EST_IMG3 = "estimate_image3";
    public final String KEY_EST_IMG4 = "estimate_image4";
    public final String KEY_VEHICLE_NUMBER = "vehicle_number";
    public final String KEY_EST_DESC = "estimate_description";
    public final String KEY_BRAND_NAME = "brand_name";
    public final String KEY_MODEL_NAME = "model_name";
    public final String KEY_EST_REPAIR_TYPE = "estimate_repair_type_selected";
    public final String KEY_EST_REPAIR_OPT = "estimate_repair_option_selected";
    public final String KEY_EST_ADDR1= "estimate_address_selected_1";
    public final String KEY_EST_ADDR2 = "estimate_address_selected_2";
    public final String KEY_EST_PROMO_CODE = "estimate_promotion_code";
    public final String KEY_EST_PHONE_NUMBER = "estimate_phonenumber";
    public final String KEY_USER_ID = "user_id";
    public final String KEY_DESIRED_DATE = "desired_reservation_date";

    private static MySuriInfoSharedPreferences instance;

    private MySuriInfoSharedPreferences(Context paramContext, int paramInt) {
        super(paramContext, "estimate_info", paramInt);
    }

    public static MySuriInfoSharedPreferences getInstance(Context context) {
        if(instance == null) {
            instance = new MySuriInfoSharedPreferences(context, Context.MODE_PRIVATE);
        }

        return instance;
    }

    public String getReqeustId() {
        return get(KEY_REQUEST_ID, "");
    }

    public String getObjectId() {
        return get(KEY_OBJECT_ID, "");
    }

    public String getEstimateImg1() {
        return get(KEY_EST_IMG1, "");
    }

    public String getEstimateImg2() {
        return get(KEY_EST_IMG2, "");
    }

    public String getEstimateImg3() {
        return get(KEY_EST_IMG3, "");
    }

    public String getEstimateImg4() {
        return get(KEY_EST_IMG4, "");
    }

    public String getVehicleNumber() {
        return get(KEY_VEHICLE_NUMBER, "");
    }

    public String getEstimateDescription() {
        return get(KEY_EST_DESC, "");
    }

    public String getBrandName() {
        return get(KEY_BRAND_NAME, "");
    }

    public String getModelName() {
        return get(KEY_MODEL_NAME, "");
    }

    public String getEstimateRepairType() {
        return get(KEY_EST_REPAIR_TYPE, "");
    }

    public String getEstimateRepairOption() {
        return get(KEY_EST_REPAIR_OPT, "");
    }

    public String getEstimateAddress1() {
        return get(KEY_EST_ADDR1, "");
    }

    public String getEstimateAddress2() {
        return get(KEY_EST_ADDR2, "");
    }

    public String getEstimatePromtionCode() {
        return get(KEY_EST_PROMO_CODE, "");
    }

    public String getEstimatePhoneNumber() {
        return get(KEY_EST_PHONE_NUMBER, "");
    }

    public String getUserId() {
        return get(KEY_USER_ID, "");
    }

    public String getDesiredDate() {
        return get(KEY_DESIRED_DATE, "");
    }

    public void putReqeustId(String paramString) {
        put(KEY_REQUEST_ID, paramString);
    }

    public void putObjectId(String paramString) {
        put(KEY_OBJECT_ID, paramString);
    }

    public void putEstimateImg1(String paramString) {
        put(KEY_EST_IMG1, paramString);
    }

    public void putEstimateImg2(String paramString) {
        put(KEY_EST_IMG2, paramString);
    }

    public void putEstimateImg3(String paramString) {
        put(KEY_EST_IMG3, paramString);
    }

    public void putEstimateImg4(String paramString) {
        put(KEY_EST_IMG4, paramString);
    }

    public void putVehicleNumber(String paramString) {
        put(KEY_VEHICLE_NUMBER, paramString);
    }

    public void putEstimateDescription(String paramString) {
        put(KEY_EST_DESC, paramString);
    }

    public void putBrandName(String paramString) {
        put(KEY_BRAND_NAME, paramString);
    }

    public void putModelName(String paramString) {
        put(KEY_MODEL_NAME, paramString);
    }

    public void putEstimateRepairType(String paramString) {
        put(KEY_EST_REPAIR_TYPE, paramString);
    }

    public void putEstimateRepairOption(String paramString) {
        put(KEY_EST_REPAIR_OPT, paramString);
    }

    public void putEstimateAddress1(String paramString) {
        put(KEY_EST_ADDR1, paramString);
    }

    public void putEstimateAddress2(String paramString) {
        put(KEY_EST_ADDR2, paramString);
    }

    public void putEstimatePromtionCode(String paramString) {
        put(KEY_EST_PROMO_CODE, paramString);
    }

    public void putEstimatePhoneNumber(String paramString) {
        put(KEY_EST_PHONE_NUMBER, paramString);
    }

    public void putUserId(String paramString) {
        put(KEY_USER_ID, paramString);
    }

    public void putDesiredDate(String paramString) {
        put(KEY_DESIRED_DATE, paramString);
    }
}
