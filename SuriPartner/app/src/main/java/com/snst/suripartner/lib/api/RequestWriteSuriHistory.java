package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbRequest;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.suri_history.SuriHistory;
import com.snst.suripartner.lib.suri_history.SuriHistoryViewModel;
import com.snst.suripartner.pop_up.PopSuriOngoingDetailActivity;

import org.json.JSONObject;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestWriteSuriHistory implements ApiRequest {
    private static final String TAG = "RequestWriteSuriHistory";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != PopSuriOngoingDetailActivity.class) {
            Log.e(TAG, "execute: This request only should be called in PopSuriOngoingDetailActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        File suriImage = new File(inputs[2]);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().writeSuriHistory(
                Sharing.Instance().getCookie(),
                Conversion.string_2_requestBody(inputs[0]),
                Conversion.string_2_requestBody(inputs[1]),
                Conversion.file_2_multiPartBodyPart(LbRequest.LbSuri.SURI_IMAGE1, suriImage),
                Conversion.string_2_requestBody(inputs[3]),
                Conversion.string_2_requestBody(inputs[4]));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                        SuriHistory suriHistory = new SuriHistory(jsData);
                        Sharing.Instance().getSuriHistoryViewModels().add(0, new SuriHistoryViewModel(suriHistory));

                        //refresh
                        ((PopSuriOngoingDetailActivity) context).refresh();
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
