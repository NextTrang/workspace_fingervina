package com.snstfinger.suri.data.model;

import androidx.databinding.BaseObservable;

import java.io.Serializable;

public class QuotationShopInfo extends BaseObservable implements Serializable {
    private String shopPicUrl;
    private String shopName;
    private String shopAddress;
    private float averageRating;
    private int numOfReviews;
    private float distance;
    private String repairManName;
    private int quotationPrice;
    private String duration;
    private int progress;
    private float satisfyRating;
    private boolean isPremium = false;

    public static QuotationShopInfo createQuotationShopInfo(String shopName, String shopSimpleLocation, float shopScore, int price, boolean isPremium) {
        QuotationShopInfo ret = new QuotationShopInfo();
        ret.shopName = shopName;
        ret.shopAddress = shopSimpleLocation;
        ret.averageRating = shopScore;
        ret.quotationPrice = price;
        ret.isPremium = isPremium;

        return ret;
    }


    public String getShopPicUrl() {
        return shopPicUrl;
    }

    public void setShopPicUrl(String shopPicUrl) {
        this.shopPicUrl = shopPicUrl;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    public String getRepairManName() {
        return repairManName;
    }

    public void setRepairManName(String repairManName) {
        this.repairManName = repairManName;
    }

    public int getQuotationPrice() {
        return quotationPrice;
    }

    public void setQuotationPrice(int quotationPrice) {
        this.quotationPrice = quotationPrice;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public float getSatisfyRating() {
        return satisfyRating;
    }

    public void setSatisfyRating(float satisfyRating) {
        this.satisfyRating = satisfyRating;
    }

    public int getNumOfReviews() {
        return numOfReviews;
    }

    public void setNumOfReviews(int numOfReviews) {
        this.numOfReviews = numOfReviews;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }
}
