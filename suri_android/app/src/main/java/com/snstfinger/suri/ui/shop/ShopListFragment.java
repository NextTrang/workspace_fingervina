package com.snstfinger.suri.ui.shop;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.data.model.GeoCondition;
import com.snstfinger.suri.ui.tools.ListVewDecorate;

/**
 * 등록된 모든 상점리스트
 */
public class ShopListFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = ShopListFragment.class.getName();

    private static final String PARAM = "param";

    private ShopListViewModel mViewModel;
    private ShopListAdapter mAdapter;
    private RecyclerView mListview;
    private OnShopListInteractionListener mListener;

    public ShopListFragment() {
    }

    @SuppressWarnings("unused")
    public static ShopListFragment newInstance(int option) {
        ShopListFragment fragment = new ShopListFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM, option);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            getArguments().getInt(PARAM);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_list, container, false);

        mListview = view.findViewById(R.id.listview_shopinfo);

        mViewModel = ViewModelProviders.of(getActivity()).get(ShopListViewModel.class);
        mAdapter = new ShopListAdapter(getActivity(), mListener);
        mListview.setAdapter(mAdapter);

        mListview.addItemDecoration(new ListVewDecorate(getActivity(), 24, 6));

        Location loc = ((ShopListActivity) getActivity()).getLocation();
//        doTaskGetStoresByLocation(loc.getLatitude(), loc.getLongitude(), 15);
        mViewModel.shopPagedList.observe(this, new Observer<PagedList<StoreEntity>>() {
            @Override
            public void onChanged(PagedList<StoreEntity> storeEntities) {
                mAdapter.submitList(storeEntities);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShopListInteractionListener) {
            mListener = (OnShopListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShopListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_filter) {
            Log.d(TAG, "filter Clicked");
            String condition = "ISNULL(prediction_price) ASC, point_score DESC";
        }
    }

    private void doTaskGetStoresByLocation(double latitude, double longitude, int range) {
        mViewModel.setGeoCondition(new GeoCondition(latitude, longitude, range, null, null));
//        mViewModel.getShopPagedList(latitude, longitude, range).observe(this, new Observer<PagedList<StoreEntity>>() {
//            @Override
//            public void onChanged(PagedList<StoreEntity> storeEntities) {
//                mAdapter.submitList(storeEntities);
//            }
//        });
    }
}
