package com.snst.suripartner.lib.helper;

import android.widget.EditText;

public class EditTextHandler {
    static boolean ignoreChanged = false;
    public static void formatCurrency(EditText editText){
        if (ignoreChanged) {
            ignoreChanged = false;
            return;
        }

        String currentText = editText.getText().toString();
        boolean hasDot = currentText.contains(".");
        String[] dotSplit = currentText.split("\\.");

        String beforeDotCurrency = "";
        if (dotSplit.length > 0) {
            String beforeDotNumber = Conversion.currency_2_number(dotSplit[0]);
            beforeDotCurrency = Conversion.number_2_currency(beforeDotNumber);
        }
        beforeDotCurrency = hasDot ? beforeDotCurrency + "." : beforeDotCurrency;

        ignoreChanged = true;
        int pos = editText.getSelectionEnd();
        if (dotSplit.length > 1) {
            editText.setText(beforeDotCurrency + dotSplit[1]);
        } else {

            editText.setText(beforeDotCurrency);
        }

        if (pos > editText.getText().length()) {
            pos = editText.getText().length();
        } else {
            pos = pos + (editText.getText().length() - currentText.length());
            pos = pos < 0 ? 0 : pos;
        }

        editText.setSelection(pos);
    }
}
