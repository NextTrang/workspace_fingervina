package com.snstfinger.suri.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.ProgressBar;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.snstfinger.suri.R;
import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.data.model.AccountType;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.network.UserAPI;
import com.snstfinger.suri.util.SettingSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;
import com.zing.zalo.zalosdk.oauth.LoginVia;
import com.zing.zalo.zalosdk.oauth.OAuthCompleteListener;
import com.zing.zalo.zalosdk.oauth.OauthResponse;
import com.zing.zalo.zalosdk.oauth.ZaloOpenAPICallback;
import com.zing.zalo.zalosdk.oauth.ZaloSDK;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SnsAuthenticActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = SnsAuthenticActivity.class.getName();
    private static final int RC_SIGN_IN = 100;

    private CallbackManager mFBCallbackManager;
    //    private FirebaseAuth mAuth;
    private ProfileTracker mProfileTracker;
    private Profile mProfile;
    private boolean mIsLoggedin;

    UserInfoSharedPreferences profile;
    Button loginWithFB;
    Button loginWithZa;
    Button loginWithGoogle;

    private ProgressBar progressBar;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFBCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        ZaloSDK.Instance.onActivityResult(this, requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, String.format("reqCode::%s, resCode::%s, data::%s", requestCode, resultCode, data == null ? "null" : data.getExtras()));

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snslogin);

        profile = UserInfoSharedPreferences.getInstance(getApplicationContext());
        mFBCallbackManager = CallbackManager.Factory.create();

        //region 페이스북빌트인 로그인버튼 테스트
        LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setPermissions("email", "public_profile", "photo");
        // Callback registration
        loginButton.registerCallback(mFBCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "error::" + error);
            }
        });
        //endregion


        loginWithFB = findViewById(R.id.btn_login_facebook);
        loginWithZa = findViewById(R.id.btn_login_zalo);
        loginWithGoogle = findViewById(R.id.btn_login_google);
        loginWithFB.setOnClickListener(this);
        loginWithZa.setOnClickListener(this);
        loginWithGoogle.setOnClickListener(this);

        progressBar = findViewById(R.id.pbLoading);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateUI();
    }

    @Override
    public void onClick(View v) {
        SettingSharedPreferences settings = SettingSharedPreferences.getInstance(getApplicationContext());
        if (v.getId() == R.id.btn_login_facebook) {
            // facebook
            settings.putAccountType(AccountType.FACEBOOK.name());
            if (mIsLoggedin) {
                logoutfb();
            } else {
                loginFacebook();
            }

        } else if (v.getId() == R.id.btn_login_zalo) {
            // zalo
            settings.putAccountType(AccountType.ZALO.name());
            if (mIsLoggedin) {
                logoutZalo();
            } else {
                loginZalo();
            }
        } else if (v.getId() == R.id.btn_login_google) {
            // google
            settings.putAccountType(AccountType.GOOGLE.name());
            if (mIsLoggedin) {
                logoutGoogle();
            } else {
                loginGoogle();
            }
        }

    }

    public static String getApplicationHashKey(Context ctx) throws Exception {
        PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);
        for (Signature signature : info.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            String sig = Base64.encodeToString(md.digest(), Base64.DEFAULT).trim();
            if (sig.trim().length() > 0) {
                return sig;
            }
        }

        return "";
    }

    //region facebook login
    private void loginFacebook() {
        final LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithReadPermissions(SnsAuthenticActivity.this, Arrays.asList("email", "public_profile"));
        loginManager.registerCallback(mFBCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                requestMe(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "error::" + error);
            }
        });
    }

    private void logoutfb() {
        LoginManager.getInstance().logOut();
        updateUI();
    }

    // getProfile from facebook
    private void requestMe(AccessToken token) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d(TAG, "FB profile ::" + object == null ? "null" : toString());

                Profile profile = Profile.getCurrentProfile();
                if (profile != null) {

                }
                String dispName = profile.getName();
                String pictureUrl = String.valueOf(Profile.getCurrentProfile().getProfilePictureUri(200, 200));

                if (object != null) {
                    try {
                        String email = object.getString("email");

                        Log.d(TAG, "email::" + email + " name::" + dispName);

                        createProfile(email, dispName, pictureUrl, AccountType.FACEBOOK);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                logInSuri();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    // endregion


    // region zalo login
    private void loginZalo() {
        progressBar.setVisibility(View.VISIBLE);
        ZaloSDK.Instance.authenticate(this, LoginVia.APP_OR_WEB, new OAuthCompleteListener() {
            @Override
            public void onGetOAuthComplete(OauthResponse response) {
                if (TextUtils.isEmpty(response.getOauthCode())) {
                    Log.e(TAG, String.format("ERROR LOGIN(code::%d, msg:%s)", response.getErrorCode(), response.getErrorMessage()));

                } else {
                    Log.d(TAG, "SUCCESS LOGIN with Zalo");
                    requestMeZalo();
                }
            }
        });
    }

    private void logoutZalo() {
        ZaloSDK.Instance.unauthenticate();
        updateUI();
    }

    ZaloOpenAPICallback callback = new ZaloOpenAPICallback() {
        @Override
        public void onResult(JSONObject jSONObject) {
            try {
                Log.d("zalo", "onResult: " + jSONObject);

                String name = jSONObject.getString("name");
                String userId = jSONObject.getString("id");
                JSONObject data = jSONObject.getJSONObject("picture").getJSONObject("data");
                String imgUrl = data.getString("url");

                createProfile(userId, name, imgUrl, AccountType.ZALO);
                logInSuri();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };

    String[] reqField = {"id", "name", "picture"};

    private void requestMeZalo() {
        ZaloSDK.Instance.getProfile(this, callback, reqField);
    }

    // endregion


    // region google login
    private void loginGoogle() {
        GoogleSignInClient client = SuriLoginManager.getInstance(this).getGoogleClient();
        Intent signIntent = client.getSignInIntent();
        startActivityForResult(signIntent, RC_SIGN_IN);

    }

    private void logoutGoogle() {
        GoogleSignInClient client = SuriLoginManager.getInstance(this).getGoogleClient();
        client.signOut();
        updateUI();
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
//        showProgressDialog();
        if (acct != null) {
            String dispName = acct.getDisplayName();
            String email = acct.getEmail();
            String id = acct.getId();
            Uri photo = acct.getPhotoUrl();

            Log.d(TAG, "name::" + dispName + " email::" + email + " id::" + id);
            createProfile(email, dispName, null, AccountType.GOOGLE);

            logInSuri();
        }
    }
    // endregion

    private void createProfile(String userId, String name, String pictureUrl, AccountType snsType) {
        if (profile == null) {
            profile = UserInfoSharedPreferences.getInstance(getApplicationContext());
        }
        profile.putUserId(userId);
        profile.putUserEmail(userId);
        profile.putUserPw(userId);
        profile.putUserName(name);
        profile.putProfileUrl(pictureUrl);
        profile.putAccountType(snsType.name());

    }

    private void updateUI() {
        mIsLoggedin = SuriLoginManager.getInstance(this).isLoggedIn();

        if (mIsLoggedin) {
            AccountType accountType = SuriLoginManager.getInstance(getApplicationContext()).getAccountType();

            if (AccountType.FACEBOOK == accountType) {
                loginWithFB.setText(R.string.btn_logout_with_sns);
            } else if (AccountType.GOOGLE == accountType) {
                loginWithGoogle.setText(R.string.btn_logout_with_sns);
            } else if (AccountType.ZALO == accountType) {
                loginWithZa.setText(R.string.btn_logout_with_sns);
            }

        } else {
            loginWithFB.setText(R.string.btn_login_with_facebook);
            loginWithZa.setText(R.string.btn_login_with_zalo);
            loginWithGoogle.setText(R.string.btn_login_with_google);

        }
    }

    private void logInSuri() {
        progressBar.setVisibility(View.INVISIBLE);
        setResult(RESULT_OK);
        finish();
    }

    private boolean isSuriMember() {

        return false;
    }

    private void registerSuriAccount() {
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), profile.getUserName(), profile.getUserPhone(), "SNS");
        Call<CommonResponse> call = userAPI.doCreateUser(user);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Log.d(TAG, res.toString());

                    List<String> cookies = response.raw().headers("Set-Cookie");
                    if (cookies != null) {
                        for (String cookie : cookies) {
                            Log.d(TAG, "cookies::" + cookie);
                            String sessionid = cookie.split(";\\s*")[0];
                            if (sessionid.startsWith("starrkCookie")) {
                                CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
                            }
                        }

                        String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                        Log.d(TAG, "check SessionId=" + cook);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "API onFailure");
            }
        });

    }

    private void loginWithSNS() {
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), "SNS");

        Call<CommonResponse> call = userAPI.doLogin(user.getUserId(), user.getUserPw(), user.getLoginType(), SuriApplication.mSuriToken);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Log.d(TAG, res.toString());

                    List<String> cookies = response.raw().headers("Set-Cookie");
                    if (cookies != null) {
                        for (String cookie : cookies) {
                            Log.d(TAG, "cookies::" + cookie);
                            String sessionid = cookie.split(";\\s*")[0];
                            if (sessionid.startsWith("starrkCookie")) {
                                CookieManager.getInstance().setCookie(SuriApplication.APP_URL, sessionid);
                            }
                        }

                        String cook = CookieManager.getInstance().getCookie(SuriApplication.APP_URL);
                        Log.d(TAG, "check SessionId=" + cook);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "API onFailure");
            }
        });

    }

}
