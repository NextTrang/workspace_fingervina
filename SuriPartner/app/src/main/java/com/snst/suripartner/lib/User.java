package com.snst.suripartner.lib;

import com.snst.suripartner.labels.LbData;

import org.json.JSONException;
import org.json.JSONObject;

public class User {
    private String storeId;
    private String userId;
    private String userName;
    private String managerPicture;

    public User(JSONObject jsUserInfo) {
        try {
            storeId = jsUserInfo.getString(LbData.LbUserInfo.STORE_ID);
            userId = jsUserInfo.getString(LbData.LbInfo.USER_ID);
            userName = jsUserInfo.getString(LbData.LbUserInfo.USER_NAME);
            managerPicture = jsUserInfo.getString(LbData.LbInfo.MANAGER_PICTURE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getStoreId() {
        return storeId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getManagerPicture() {
        return managerPicture;
    }
}
