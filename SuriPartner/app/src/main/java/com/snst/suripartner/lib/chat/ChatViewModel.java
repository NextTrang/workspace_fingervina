package com.snst.suripartner.lib.chat;

import androidx.lifecycle.ViewModel;

public class ChatViewModel extends ViewModel {
    public String imageUrl;
    public String id;
    public String deviceName;
    public String message;
    public String day;
    public String userId;

    private ChatNavigator navigator;

    public ChatViewModel(Chat chat) {
        this.imageUrl = chat.getImageUrl();
        this.id = chat.getId();
        this.deviceName = chat.getDeviceName();
        this.message = chat.getMessage();
        this.day = chat.getDay();
        this.userId = chat.getUserId();
    }

    public void onItemClick(Chat chat) {
        navigator.onItemClick(chat);
    }

    //Getter

    public String getImageUrl() {
        return imageUrl;
    }

    //Setter

    public void setNavigator(ChatNavigator navigator) {
        this.navigator = navigator;
    }
}
