package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class UserEntity {
    @SerializedName("role_id")
    private Integer roleId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_pw")
    private String userPw;
    @SerializedName("user_name")
    private String displayName;
    @SerializedName("user_email")
    private String userEmail;

    @SerializedName("greeting_comment")
    private String greeting_comment;

    @SerializedName("manager_picture")
    private String manager_picture;

    @SerializedName("phone_kind")
    private String phone_kind;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("user_phonenumber")
    private String userPhone;

    @SerializedName("fcm_token")
    private String fcmToken;


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String userPw) {
        this.userPw = userPw;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
