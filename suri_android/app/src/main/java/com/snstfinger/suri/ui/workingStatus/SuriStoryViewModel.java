package com.snstfinger.suri.ui.workingStatus;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.snstfinger.suri.data.entity.SuriStoryEntity;
import com.snstfinger.suri.data.repository.SuriStoryRepository;

import java.util.List;

public class SuriStoryViewModel extends ViewModel {
    private SuriStoryRepository repository = new SuriStoryRepository();

    public LiveData<List<SuriStoryEntity>> getAllStories(String requestId) {

        return repository.getMutableLiveData(requestId);
    }
}
