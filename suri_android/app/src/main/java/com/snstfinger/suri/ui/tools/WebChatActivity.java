package com.snstfinger.suri.ui.tools;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.databinding.AppBarViewChatBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.util.UserInfoSharedPreferences;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebChatActivity extends AppCompatActivity {
    public static final String TAG = WebChatActivity.class.getName();

    private WebView mWebView;
    private String serverUrl = "http://101.99.34.53:1114/repairsystem/chat/index?userId=%s&toUserId=%s&isStore=false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        AppBarViewChatBinding chatHeader = DataBindingUtil.inflate(getLayoutInflater(), R.layout.app_bar_view_chat, toolbar, false);
        actionBar.setCustomView(chatHeader.getRoot());

        UserInfoSharedPreferences user = UserInfoSharedPreferences.getInstance(getApplicationContext());
        Intent connectionInfo = getIntent();
        String host = user.getUserId();
        boolean fromPush = connectionInfo.getBooleanExtra("frompush", false);
        String target = connectionInfo.getStringExtra("target");

        if(fromPush) {
            String requestId = connectionInfo.getStringExtra("requestId");
            getRequestEstimate(requestId, connectionInfo);

        } else {
            RequestNPredictionNStoreEntity mRequestEstimate = (RequestNPredictionNStoreEntity) connectionInfo.getSerializableExtra("request_estimate");
            if (mRequestEstimate != null) {
                RequestNPredictionNStoreEntity.loadImage(chatHeader.chatImage, mRequestEstimate.estimateImage1, getResources().getDrawable(R.drawable.default_chat), false);
                chatHeader.chatId.setText(mRequestEstimate.requestEstimateId);
                chatHeader.chatName.setText(mRequestEstimate.brandName + "/" + mRequestEstimate.modelName);
            } else {
                ReviewEntity.loadImage(chatHeader.chatImage, user.getProfileUrl(), getResources().getDrawable(R.drawable.default_profile));
                chatHeader.chatId.setText(user.getUserId());
                chatHeader.chatName.setVisibility(View.GONE);
            }
        }


        mWebView = findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.loadUrl(String.format(serverUrl,host, target));
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClientClass());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void getRequestEstimate(String requestEstimateId, Intent intent) {
        if (requestEstimateId == null)
            return;

        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetRequestEstimateNPredictionNStore(cookie, requestEstimateId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object info = ((LinkedTreeMap) data).get("Info");
                    JsonObject json = gson.toJsonTree(info).getAsJsonObject();
                    RequestNPredictionNStoreEntity item = gson.fromJson(json, RequestNPredictionNStoreEntity.class);

                    if (item != null) {
                        Log.d(TAG, "Update Req::" + item);
                        intent.putExtra("request_estimate", item);

                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetRequestEstimateNPredictionNStore onFailure");
            }
        });
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, url);
            view.loadUrl(url);
            return true;
        }
    }
}
