package com.snstfinger.suri.data.datasource;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.snstfinger.suri.data.entity.StoreNPredictionEntity;

public class QuotationShopListDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, StoreNPredictionEntity>> shopInfoLiveData = new MutableLiveData<>();

    private String requestId;
    private String condition;

    public QuotationShopListDataSourceFactory(String requestId, String condition) {
        this.requestId = requestId;
        this.condition = condition;
    }

    @NonNull
    @Override
    public DataSource<Integer, StoreNPredictionEntity> create() {
        QuotationShopListDataSource quotationShopDataSource = new QuotationShopListDataSource(requestId, condition);

        shopInfoLiveData.postValue(quotationShopDataSource);

        return quotationShopDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, StoreNPredictionEntity>> getQuotationShopLiveData() {
        return shopInfoLiveData;
    }
}
