package com.snstfinger.suri.network;

import android.content.Context;
import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.snstfinger.suri.R;
import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.data.entity.ResponseReviews;
import com.snstfinger.suri.data.entity.ResponseUser;
import com.snstfinger.suri.data.model.AccountType;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.data.model.Result;
import com.snstfinger.suri.ui.login.LoggedInUserView;
import com.snstfinger.suri.ui.login.LoginResult;
import com.snstfinger.suri.util.SettingSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;
import com.zing.zalo.zalosdk.oauth.ValidateOAuthCodeCallback;
import com.zing.zalo.zalosdk.oauth.ZaloSDK;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SuriLoginManager extends LoginProvider {
    public static final String TAG = SuriLoginManager.class.getName();

    public static final int LOGIN_SUCCESS = 11;
    public static final int ACCOUNT_DUPLICATE = 12;
    public static final int LOGIN_FAILED = 13;
    public static final int LOGOUT_SUCCESS = 14;
    public static final int LOGOUT_FAILED = 15;

    private static SuriLoginManager mLoginManager;
    private static Context mContext;
    private boolean mIsLoggedin = false;

    // google
    private static GoogleSignInClient mGoogleSignInClient;
    // end google

    public static SuriLoginManager getInstance(Context context) {
        if (mLoginManager == null) {
            mLoginManager = new SuriLoginManager();
            mContext = context;
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestProfile().requestEmail().build();
            mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
        }

        return mLoginManager;
    }

    public GoogleSignInClient getGoogleClient() {
        return mGoogleSignInClient;
    }

    @Override
    public void requestLogin(OnResultLoginListener onResultLoginListener) {
        loginWithSNS(onResultLoginListener);
    }

    @Override
    public void requestUser(OnReceiveUserListener onReceiveUserListener) {

    }

    @Override
    public boolean isLoggedIn() {
        AccountType accountType = getAccountType();
        if (AccountType.FACEBOOK == accountType) {
            checkFacebook();

        } else if (AccountType.GOOGLE == accountType) {
            checkGoogle();

        } else if (AccountType.ZALO == accountType) {
            checkZalo();

        }

        return mIsLoggedin;
    }

    @Override
    public void logout(final OnResultLogoutListener onResultLogoutListener) {
        AccountType accountType = getAccountType();
        if (AccountType.FACEBOOK == accountType) {
            logoutFacebook(onResultLogoutListener);

        } else if (AccountType.GOOGLE == accountType) {
            logoutGoogle(onResultLogoutListener);

        } else if (AccountType.ZALO == accountType) {
            logoutZalo(onResultLogoutListener);
        }
    }

    public AccountType getAccountType() {
        AccountType accountType = null;
        SettingSharedPreferences settings = SettingSharedPreferences.getInstance(mContext);
        String type = settings.getAccountType();

        if (!type.isEmpty()) {
            accountType = AccountType.valueOf(type);
        }

        return accountType;
    }

    public String getSuriCookie() {
        String cookie = CookieManager.getInstance().getCookie(SuriApplication.APP_URL);
        Log.d(TAG, "check SessionId=" + cookie);

        return cookie;
    }


    private void checkFacebook() {
        // check facebook
        if (AccessToken.getCurrentAccessToken() == null) {
            mIsLoggedin = false;
            Log.d(TAG, "NOT LoggedIn facebook");
        } else {
            mIsLoggedin = true;
            Log.d(TAG, "LoggedIn facebook");
        }
    }

    private void checkZalo() {
        // check zalo
        ZaloSDK.Instance.isAuthenticate(new ValidateOAuthCodeCallback() {
            @Override
            public void onValidateComplete(boolean validated, int errorCode, long userId, String oauthCode) {
                if (validated) {
                    mIsLoggedin = true;
                    Log.d(TAG, "LoggedIn zalo");
                } else {
                    mIsLoggedin = false;
                    Log.d(TAG, "NOT LoggedIn zalo");
                }
            }
        });
    }

    private void checkGoogle() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mContext);
        if (account != null) {
            mIsLoggedin = true;
            Log.d(TAG, "LoggedIn google");
        } else {
            mIsLoggedin = false;
            Log.d(TAG, "NOT LoggedIn google");
        }
    }

    private void logoutGoogle(final OnResultLogoutListener onResultLogoutListener) {
        mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                onResultLogoutListener.onResult(LOGOUT_SUCCESS);
            }
        });
    }

    private void logoutFacebook(final OnResultLogoutListener onResultLogoutListener) {
        LoginManager.getInstance().logOut();
        onResultLogoutListener.onResult(LOGOUT_SUCCESS);
    }

    private void logoutZalo(final OnResultLogoutListener onResultLogoutListener) {

    }

    // region suri login
    public void loginWithSNS(final OnResultLoginListener onResultLoginListener) {
        Log.d(TAG, "loginWithSNS::Token::" + SuriApplication.mSuriToken);

        UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(mContext);
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        final LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), profile.getUserName(), profile.getUserPhone(), "SNS");

        Call<CommonResponse> call = userAPI.doLogin(user.getUserId(), user.getUserPw(), user.getLoginType(), SuriApplication.mSuriToken);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Log.d(TAG, res.toString());
                    if (res.getStatus() != 200) {
                        return;
                    }

                    List<String> cookies = response.raw().headers("Set-Cookie");
                    if (cookies != null) {
                        for (String cookie : cookies) {
                            Log.d(TAG, "cookies::" + cookie);
                            String sessionid = cookie.split(";\\s*")[0];
                            if (sessionid.startsWith("starrkCookie")) {
                                CookieManager.getInstance().setCookie(SuriApplication.APP_URL, sessionid);
                            }
                        }

                        String cook = CookieManager.getInstance().getCookie(SuriApplication.APP_URL);
                        Log.d(TAG, "check SessionId=" + cook);

                        Gson gson = new Gson();
                        JsonObject json = gson.toJsonTree(res.getData()).getAsJsonObject();
                        ResponseUser user2 = gson.fromJson(json, ResponseUser.class);

                        onResultLoginListener.onResult(LOGIN_SUCCESS, user);
                    }
                } else {
                    Log.e(TAG, response.toString());
                    onResultLoginListener.onResult(LOGIN_FAILED, null);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "API onFailure");
                onResultLoginListener.onResult(LOGIN_FAILED, null);
            }
        });
    }

    // endregion

    // region create SURI account
    public void registerSuriAccount(final OnResultLoginListener onResultLoginListener) {
        UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(mContext);
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), profile.getUserName(), profile.getUserPhone(), "SNS");
        Call<CommonResponse> call = userAPI.doCreateUser(user);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Log.d(TAG, res.toString());

                    List<String> cookies = response.raw().headers("Set-Cookie");
                    if (cookies != null) {
                        for (String cookie : cookies) {
                            Log.d(TAG, "cookies::" + cookie);
                            String sessionid = cookie.split(";\\s*")[0];
                            if (sessionid.startsWith("starrkCookie")) {
                                CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
                            }
                        }

                        String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                        Log.d(TAG, "check SessionId=" + cook);

                        onResultLoginListener.onResult(LOGIN_SUCCESS, user);
                    }

                } else {
                    Log.e(TAG, response.toString());
                    if(response.code() == 500) {
                        Gson gson = new Gson();
                        ErrorResponse error = null;
                        try {
                            error = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                            if(error.getMessage().contains("Duplicate")) {
                                onResultLoginListener.onResult(ACCOUNT_DUPLICATE, user);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.e(TAG, error.toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "API onFailure");
            }
        });

    }
    // endregion
}
