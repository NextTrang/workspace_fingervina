package com.snstfinger.suri;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.labels.LbPop;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.ui.estimate.RequestEstimateActivity;
import com.snstfinger.suri.ui.dialog.ChangeMySuriDialogFragment;
import com.snstfinger.suri.ui.home.InboxActivity;
import com.snstfinger.suri.ui.home.MySuriListFragment;
import com.snstfinger.suri.ui.home.MySuriViewModel;
import com.snstfinger.suri.ui.login.LoginActivity;
import com.snstfinger.suri.ui.login.SnsAuthenticActivity;
import com.snstfinger.suri.ui.login.SettingsActivity;
import com.snstfinger.suri.ui.shop.OnReviewListInteractionListener;
import com.snstfinger.suri.ui.shop.ShopInfoAllCaseFragment;
import com.snstfinger.suri.ui.tools.WebChatActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationGoingActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationShopActivity;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;
import com.snstfinger.suri.util.SettingSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends BaseActivity implements View.OnClickListener, OnReviewListInteractionListener, MySuriListFragment.OnMySuriListInteractionListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivity.class.getName();

    public static final int REQ_SNS_AUTH = 100;
    public static final int REQ_LOGIN = 101;

    private AppBarConfiguration mAppBarConfiguration;
    private UserInfoSharedPreferences mProfileInfo;
    private MySuriInfoSharedPreferences mMainSuriInfo;
    private Button profileEdit, btnReqPhone, btnReqLaptop, btnReqBike, btnReqCar;
    private ImageView profilePic;
    private TextView profileName;
    private TextView profileEmail;

    private MySuriViewModel mMySuriViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setLogo(R.drawable.logo);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View navHeader = navigationView.getHeaderView(0);

        profileEdit = navHeader.findViewById(R.id.btn_edit);
        profilePic = navHeader.findViewById(R.id.profilePic);
        profileEdit.setOnClickListener(this);
        profileName = navHeader.findViewById(R.id.profileName);
        profileEmail = navHeader.findViewById(R.id.profileEmail);
        btnReqPhone = navHeader.findViewById(R.id.btn_phone);
        btnReqLaptop = navHeader.findViewById(R.id.btn_laptop);
        btnReqBike = navHeader.findViewById(R.id.btn_bike);
        btnReqCar = navHeader.findViewById(R.id.btn_car);
        btnReqPhone.setOnClickListener(this);
        btnReqLaptop.setOnClickListener(this);
        btnReqBike.setOnClickListener(this);
        btnReqCar.setOnClickListener(this);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.setNavigationItemSelectedListener(this);


        // 안됨
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        toggle.setHomeAsUpIndicator(R.drawable.home_menu);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();

        SettingSharedPreferences settings = SettingSharedPreferences.getInstance(getApplicationContext());
        String accountType = settings.getAccountType();

        mProfileInfo = UserInfoSharedPreferences.getInstance(getApplicationContext());
        mMainSuriInfo = MySuriInfoSharedPreferences.getInstance(getApplicationContext());

        if (accountType.isEmpty()) {
            // 첫실행
            Intent loginIntent = new Intent(MainActivity.this, SnsAuthenticActivity.class);
            startActivityForResult(loginIntent, REQ_SNS_AUTH);
        } else {
            // SNS 접속이력이 있음
            // 접속여부 확인 진행
            if (!SuriLoginManager.getInstance(getApplicationContext()).isLoggedIn()) {
                Intent loginIntent = new Intent(MainActivity.this, SnsAuthenticActivity.class);
                startActivityForResult(loginIntent, REQ_SNS_AUTH);
            } else {
                // SNS 로그인 상태니까
                // 수리 로그인으로 세션가져오기(따로 업데이트기능이 없음)
                SuriLoginManager.getInstance(getApplicationContext()).requestLogin(this);
            }
        }

        mMySuriViewModel = ViewModelProviders.of(this).get(MySuriViewModel.class);
//        mMySuriViewModel = vita.with(VitaOwner.None).getViewModel<MySuriViewModel>();

        mMySuriViewModel.getMainSuri().observe(this, new Observer<RequestNPredictionNStoreEntity>() {
            @Override
            public void onChanged(RequestNPredictionNStoreEntity requestNPredictionNStoreEntity) {
                MySuriInfoSharedPreferences pref = MySuriInfoSharedPreferences.getInstance(getApplication());
                pref.putReqeustId(requestNPredictionNStoreEntity.requestEstimateId);
            }
        });

        checkPermissionAll();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification: {
                Intent inbox = new Intent(this, InboxActivity.class);
                startActivity(inbox);

                return true;
            }

            case R.id.action_request: {
                Intent request = new Intent(this, RequestEstimateActivity.class);
                request.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_MOBILE));
                startActivity(request);

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onResult(int result, LoggedInUser user) {
        Log.d(TAG, "result::" + result);
        if (result == SuriLoginManager.LOGIN_SUCCESS) {
            updateProfie();
        }
    }

    @Override
    public void onClick(View v) {
        closeDrawer();

        Intent intent;
        if (v.getId() == R.id.btn_edit) {
            // activity or fragment?
            if (SuriLoginManager.getInstance(this).isLoggedIn()) {
                intent = new Intent(this, SettingsActivity.class);
            } else {
                intent = new Intent(this, SnsAuthenticActivity.class);
            }
            startActivity(intent);

        } else if (v.getId() == R.id.btn_phone) {
            intent = new Intent(this, RequestEstimateActivity.class);
            intent.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_MOBILE));
            startActivity(intent);
        } else if (v.getId() == R.id.btn_laptop) {
            intent = new Intent(this, RequestEstimateActivity.class);
            intent.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_PC));
            startActivity(intent);
        } else if (v.getId() == R.id.btn_bike) {
            intent = new Intent(this, RequestEstimateActivity.class);
            intent.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_BIKE));
            startActivity(intent);
        } else if (v.getId() == R.id.btn_car) {
            intent = new Intent(this, RequestEstimateActivity.class);
            intent.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_CAR));
            startActivity(intent);
        } else if (v.getId() == R.id.layout_header) {
            Intent estimateStatus;
            RequestNPredictionNStoreEntity estimateInfo = mMySuriViewModel.getMainSuri().getValue();
            if (estimateInfo.getStatusCode() == RequestNPredictionNStoreEntity.TYPE_REQUEST) {
                estimateStatus = new Intent(this, QuotationShopActivity.class);
            } else {
                estimateStatus = new Intent(this, QuotationGoingActivity.class);
            }

            estimateStatus.putExtra("request_estimate", estimateInfo);
            startActivity(estimateStatus);
        }
    }

    @Override
    public void onItemClick(RequestNPredictionNStoreEntity item) {
        Log.d(TAG, "clicked suri history::" + item.requestEstimateId);

        Intent statusDetail;
        if (item.getStatusCode() == RequestNPredictionNStoreEntity.TYPE_REQUEST) {
            statusDetail = new Intent(this, QuotationShopActivity.class);
        } else {
            statusDetail = new Intent(this, QuotationGoingActivity.class);
        }

        statusDetail.putExtra("request_estimate", item);
        startActivity(statusDetail);
    }

    @Override
    public void onItemLongClick(RequestNPredictionNStoreEntity item) {
        Log.d(TAG, "LONG clicked suri history::" + item.requestEstimateId);
        ChangeMySuriDialogFragment dialog = ChangeMySuriDialogFragment.newInstance(item);
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean fromPush = getIntent().getBooleanExtra("frompush", false);
        if (fromPush) {
            String requestId = getIntent().getStringExtra("requestId");
            Log.d(TAG, "TEST::" + requestId);
        }
    }

    public void changeMyMainSuri(RequestNPredictionNStoreEntity item) {
        mMySuriViewModel.setMainSuri(item);
    }

    private void closeDrawer() {
        DrawerLayout drawer = mAppBarConfiguration.getDrawerLayout();
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
    }

    private void updateProfie() {
        String userId = mProfileInfo.getUserId();
        String name = mProfileInfo.getUserName();
        String email = mProfileInfo.getUserEmail();
        String uri = mProfileInfo.getProfileUrl();

        profileName.setText(name);
        profileEmail.setText(email);
        Glide.with(this).setDefaultRequestOptions(new RequestOptions().circleCrop()).load(uri).into(profilePic);
    }

    private void makeChat() {
        Intent goChat = new Intent(this, WebChatActivity.class);
        goChat.putExtra("target", "INFO_CENTER");
        startActivity(goChat);
    }

    @Override
    public void onReviewClick(ReviewEntity item) {
        Log.d(TAG, "clicked review::" + item.getScore());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.nav_history: {
//                startActivity(new Intent(this, MySuriListActivity.class));
                NavController navController = findNavController(this, R.id.nav_host_fragment);
                navController.navigate(R.id.nav_history);

                break;
            }

            case R.id.nav_guarantee: {
//                startActivity(new Intent(this, WarrantyActivity.class));

                NavController navController = findNavController(this, R.id.nav_host_fragment);
                navController.navigate(R.id.nav_guarantee);
                break;
            }

            case R.id.nav_customer: {
                makeChat();

                break;
            }
        }
        closeDrawer();

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_SNS_AUTH) {
                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivityForResult(loginIntent, REQ_LOGIN);
            } else if (requestCode == REQ_LOGIN) {
                updateProfie();
            } else if (requestCode == LbPop.LbCode.FILTER_SURI_CASE && data != null) {
                int id = data.getIntExtra("object_id",0);
                String objectId = "";
                if(id != 0){
                    objectId = String.valueOf(id);
                }

                String brandName = data.getStringExtra("brand_name");
                if(brandName.equals("All")){
                    brandName = "";
                }

                String modelName = data.getStringExtra("model_name");
                FragmentManager mFragmentManager = getSupportFragmentManager();

                ((ShopInfoAllCaseFragment) mFragmentManager.getFragments().get(0)
                        .getChildFragmentManager()
                        .getFragments().get(0))
                        .updateBrandModel(objectId, brandName, modelName);
            }

        }
    }
}
