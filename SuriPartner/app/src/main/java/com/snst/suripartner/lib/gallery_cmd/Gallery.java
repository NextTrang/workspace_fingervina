package com.snst.suripartner.lib.gallery_cmd;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.GalleryActivity;
import com.snst.suripartner.labels.LbPop;

import java.io.File;

public class Gallery {
    private int requestCode = -1;
    private Intent data;
    private File photoFile;
    private boolean onClickCamera;

    public Gallery setRequestCode(int requestCode) {
        this.requestCode = requestCode;
        return this;
    }

    public Gallery setData(Intent data) {
        this.data = data;
        return this;
    }

    public Gallery setPhotoFile(File photoFile) {
        this.photoFile = photoFile;
        return this;
    }

    public Gallery setOnClickCamera(boolean onClickCamera) {
        this.onClickCamera = onClickCamera;
        return this;
    }

    public void open(AppCompatActivity context) {
        if (requestCode <= 0) {
            Log.e("Gallery", "open: requestCode is invalid. Please set requestCode again");
        }

        Intent intent = new Intent(context, GalleryActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    public void close(AppCompatActivity context) {
        if (requestCode <= 0) {
            Log.e("Gallery", "open: requestCode is invalid. Please set requestCode again");
        } else if (data == null) {
            Log.e("Gallery", "close: data can not null. Please set data");
        }

        if (requestCode == LbPop.LbCode.CAMERA_REQUEST) {
            //return from camera
            Intent intent = new Intent();
            Uri photoUri = data.getData();
            intent.putExtra(LbPop.LbKey.PHOTO_URI, photoUri);
            context.setResult(context.RESULT_OK, intent);
            context.finish();
        } else {
            //finish after clicking an image
            GalleryActivity galleryActivity = (GalleryActivity) context;
            Intent intent = new Intent();
            if (onClickCamera) {
                intent.putExtra(LbPop.LbKey.OPEN_CAMERA, true);
            } else {
                intent.putExtra(LbPop.LbKey.PHOTO_FILE, photoFile);
            }
            galleryActivity.setResult(Activity.RESULT_OK, intent);
            galleryActivity.finish();
        }
    }
}
