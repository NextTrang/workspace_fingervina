package com.snstfinger.suri.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.MainActivity;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ResponseReviews;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.data.model.HomeSuriStatus;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.ReviewAPI;
import com.snstfinger.suri.network.StoreAPI;
import com.snstfinger.suri.ui.estimate.RequestEstimateActivity;
import com.snstfinger.suri.ui.shop.OnSuriCaseListInteractionListener;
import com.snstfinger.suri.ui.shop.SuriCaseDetailsActivity;
import com.snstfinger.suri.ui.tools.HorizonListVewDecorate;
import com.snstfinger.suri.ui.tools.ListVewDecorate;
import com.snstfinger.suri.ui.tools.ReviewRecyclerViewAdapter;
import com.snstfinger.suri.ui.tools.SuriCaseRecyclerViewAdapter;
import com.snstfinger.suri.ui.tools.SuriIndicator;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.navigation.Navigation.findNavController;

public class HomeFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, OnSuriCaseListInteractionListener {
    public static final String TAG = HomeFragment.class.getName();

    private Button btnAllCase, btnAllReview;
    private RecyclerView mListViewCase, mListViewReview;
    private TextView mEmptyCase, mEmptyReview;


    SwipeRefreshLayout mSwipeLayout;
    private ViewPager2 mPager;
    private SuriIndicator mIndicator;
    private FragmentStateAdapter pagerAdapter;
    private SuriCaseRecyclerViewAdapter mCaseAdapter;
    private ReviewRecyclerViewAdapter mReviewAdapter;

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    private StoreNPredictionEntity mSelectedStore;
    private HomeSuriStatus mHomeSuriStatus = new HomeSuriStatus();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        // set indicator in fragment. not Activity
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setLogo(R.drawable.logo);
        actionBar.setHomeAsUpIndicator(R.drawable.home_menu);

        mSwipeLayout = root.findViewById(R.id.swiperefresh);
        mSwipeLayout.setOnRefreshListener(this);

        mPager = root.findViewById(R.id.view_pager);
        pagerAdapter = new ViewPagerFragmentAdapter(this);
//        MainSuriFragment suri = MainSuriFragment.newInstance(mHomeSuriStatus);
        ((ViewPagerFragmentAdapter) pagerAdapter).pagerAdapter(new MainSuriFragment());
        ((ViewPagerFragmentAdapter) pagerAdapter).pagerAdapter(new SearchShopFragment());

        mPager.setAdapter(pagerAdapter);
        mPager.setClipToPadding(false);
        mPager.setPageTransformer(new MarginPageTransformer(Utils.dp2Px(getActivity(), 23)));
        mPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.d(TAG, "Selected::" + position);

                mIndicator.selectIndicator(position);
//                mIndicator.postInvalidate();

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        mIndicator = root.findViewById(R.id.indicator);
        mIndicator.createIndicatorPanel(2, R.drawable.indicator_off, R.drawable.indicator_on, 0);

        // recently suri-case
        mListViewCase = root.findViewById(R.id.listview_case);
        mEmptyCase = root.findViewById(R.id.listview_case_empty);
        mCaseAdapter = new SuriCaseRecyclerViewAdapter(this, true);
        mListViewCase.setAdapter(mCaseAdapter);
        mListViewCase.addItemDecoration(new HorizonListVewDecorate(getContext(), 19, 19));
        getRecentSuriCase();

        // recently reviews
        mListViewReview = root.findViewById(R.id.listview_review);
        mEmptyReview = root.findViewById(R.id.listview_review_empty);
        mReviewAdapter = new ReviewRecyclerViewAdapter(null, true);
        mListViewReview.setAdapter(mReviewAdapter);
        mListViewReview.addItemDecoration(new ListVewDecorate(getContext(), 19, 0));
        getRecentReview();

        btnAllReview = root.findViewById(R.id.btn_all_reviews);
        btnAllReview.setOnClickListener(this);
        btnAllCase = root.findViewById(R.id.btn_all_case);
        btnAllCase.setOnClickListener(this);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public void onClick(View v) {
//        Toast.makeText(getActivity(), "clicked::"+v.getId(), Toast.LENGTH_SHORT).show();

        switch (v.getId()) {
            case R.id.btn_request_estimate: {
                Intent reqeust = new Intent(getActivity(), RequestEstimateActivity.class);
                reqeust.putExtra("request_form", new RequestEstimate(RequestEstimate.TYPE_MOBILE));
                startActivity(reqeust);

                break;
            }

            case R.id.btn_search_shop: {
                break;
            }

            case R.id.btn_all_case: {
                NavController navController = findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_all_case);

                break;
            }

            case R.id.btn_all_reviews: {
                NavController navController = findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_all_review);
                break;
            }
        }
    }

    @Override
    public void onCaseClick(SuriCaseEntity item) {
        Intent caseDetail = new Intent(getActivity(), SuriCaseDetailsActivity.class);
        caseDetail.putExtra("case", item);
        startActivity(caseDetail);
    }

    @Override
    public void onRefresh() {
        mSwipeLayout.setRefreshing(true);
        ((MainSuriFragment) ((ViewPagerFragmentAdapter) pagerAdapter).createFragment(0)).refreshingData();
//        //3초후에 해당 adapter를 갱신하고 동글뱅이를 닫아준다.setRefreshing(false);
//        //핸들러를 사용하는 이유는 일반쓰레드는 메인쓰레드가 가진 UI에 접근할 수 없기 때문에 핸들러를 이용해서
//        //메시지큐에 메시지를 전달하고 루퍼를 이용하여 순서대로 UI에 접근한다.
//
//        //반대로 메인쓰레드에서 일반 쓰레드에 접근하기 위해서는 루퍼를 만들어야 한다.
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //해당 어댑터를 서버와 통신한 값이 나오면 됨
//                String afterItems[] = insertData();
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, afterItems);
//                listview.setAdapter(adapter);
//                mSwipeLayout.setRefreshing(false);
//            }
//        },3000);

        mSwipeLayout.setRefreshing(false);
    }

    private void getRecentSuriCase() {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
//        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllRecentSuriCases(null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<SuriCaseEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List itemList = gson.fromJson(asJsonArray, itemType);
                    if (!itemList.isEmpty()) {
                        Log.d(TAG, "SuriCaseEntity size::" + itemList.size());
                        mEmptyCase.setVisibility(View.GONE);
                        mListViewCase.setVisibility(View.VISIBLE);
                        mCaseAdapter.setItemList(itemList);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void getRecentReview() {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
//        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetAllRecentReviews(null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List itemList = result.getReviewEntities();
                    if (!itemList.isEmpty()) {
                        Log.d(TAG, "ReviewEntity size::" + itemList.size());
                        mEmptyReview.setVisibility(View.GONE);
                        mListViewReview.setVisibility(View.VISIBLE);
                        mReviewAdapter.setItemList(itemList);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

}