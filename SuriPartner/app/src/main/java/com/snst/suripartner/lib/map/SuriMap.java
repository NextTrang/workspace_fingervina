package com.snst.suripartner.lib.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.model.DirectionsResult;

public interface SuriMap {
    void moveTo(LatLng location);
    void calculateDirections(LatLng departure, LatLng destination);
    void addPolylinesToMap(final DirectionsResult result);
}
