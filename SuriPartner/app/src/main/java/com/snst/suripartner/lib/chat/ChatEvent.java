package com.snst.suripartner.lib.chat;

import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class ChatEvent {

    Context context;

    public ChatEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }
}
