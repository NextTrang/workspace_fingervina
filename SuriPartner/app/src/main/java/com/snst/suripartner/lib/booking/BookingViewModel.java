package com.snst.suripartner.lib.booking;

import android.view.View;

import androidx.databinding.BaseObservable;

import com.snst.suripartner.lib.helper.Conversion;

public class BookingViewModel extends BaseObservable {
    public String imageUrl;
    public String deviceName;
    public String predictPrice="";
    public String predictPriceCurrency="";
    public String price;
    public String id;
    public String userName;
    public String phoneNumber;
    public String pickupAddress;
    public String predictionEstDate;
    public String bookingTime;

    public int visibleCustomerInfos;

    //Using for send confirm
    public String predicEstId;

    public BookingViewModel(Booking booking) {
        this.imageUrl = booking.getImageUrl();
        this.deviceName = booking.getDeviceName();
        setPredictPrice(booking.getPredictPrice());
        this.price = booking.getPrice();
        this.id = booking.getId();
        this.userName = booking.getUserName();
        this.phoneNumber = booking.getPhoneNumber();
        this.pickupAddress = booking.getPickupAddress();
        this.predictionEstDate = booking.getPredictionEstDate();
        this.bookingTime = booking.getBookingTime();
        setVisibleCustomerInfos(View.GONE);
        this.predicEstId = booking.getPredicEstId();
    }

    public void setVisibleCustomerInfos(int visibleCustomerInfos) {
        this.visibleCustomerInfos = visibleCustomerInfos;
        notifyChange();
    }

    public void setPredictPrice(String predictPrice) {
        this.predictPrice = predictPrice;
        this.predictPriceCurrency = Conversion.number_2_currency(predictPrice);
    }
}
