package com.snst.suripartner.labels;

public class LbConst {
    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9002;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9003;

    public static final String BASE_LINK = "http://101.99.34.53:1114/repairsystem";
}
