package com.snstfinger.suri.ui.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.snstfinger.suri.R;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

public class LoginActivity extends FragmentActivity {
    private static final String TAG = LoginActivity.class.getName();
    private LoginViewModel loginViewModel;

    private EditText userPhoneEditText;
    private ProgressBar loadingProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getApplicationContext());

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText userEmailEditText = findViewById(R.id.user_email);
        userPhoneEditText = findViewById(R.id.user_phone);
        final Button loginButton = findViewById(R.id.login);
        loadingProgressBar = findViewById(R.id.loading);

        usernameEditText.setText(profile.getUserName());
        userEmailEditText.setText(profile.getUserEmail());
        userPhoneEditText.setText(getPhoneNumber());

//        loginViewModel = new LoginViewModel(getApplication());
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory(getApplication())).get(LoginViewModel.class);
        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getUserPhoneError() != null) {
                    userPhoneEditText.setError(getString(loginFormState.getUserPhoneError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    if (loginResult.getError() == R.string.account_duplicate) {
                        loadingProgressBar.setVisibility(View.VISIBLE);
                        loginViewModel.login();
                        return;

                    } else {
                        showLoginFailed(loginResult.getError());
                    }
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(userEmailEditText.getText().toString(), usernameEditText.getText().toString(),
                        userPhoneEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        userPhoneEditText.addTextChangedListener(afterTextChangedListener);
        userPhoneEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    profile.putUserName(usernameEditText.getText().toString());
//                    profile.putUserPhone(userPhoneEditText.getText().toString());
//
////                    SuriLoginManager.getInstance(getApplicationContext()).requestLogin(LoginActivity.this);
//                    loginViewModel.login(usernameEditText.getText().toString(), userEmailEditText.getText().toString(), userPhoneEditText.getText().toString());
                    tryRegisterLogin(usernameEditText.getText().toString(), userEmailEditText.getText().toString(), userPhoneEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryRegisterLogin(usernameEditText.getText().toString(), userEmailEditText.getText().toString(), userPhoneEditText.getText().toString());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    userPhoneEditText.setText(getPhoneNumber());
                } else {

                }

                return;
            }
        }

    }

    private void tryRegisterLogin(String name, String email, String phone) {
        final UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getApplicationContext());
        loadingProgressBar.setVisibility(View.VISIBLE);
        profile.putUserName(name);
        profile.putUserPhone(phone);
        loginViewModel.register();
    }


    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private String getPhoneNumber() {
        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        String phoneNumber = null;

        if (telephony != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)) {

                } else {
                    ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                }
            } else {
                if (telephony.getLine1Number() != null) {
                    phoneNumber = telephony.getLine1Number();
                } else {
                    if (telephony.getSimSerialNumber() != null) {
                        phoneNumber = telephony.getSimSerialNumber();
                    }
                }
            }
        }

        return phoneNumber;
    }

}
