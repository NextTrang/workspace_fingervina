package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseRequestNPredictionNStore {
    @SerializedName("pageMap")
    @Expose
    private PageMap pageMap;

    @SerializedName("Info")
    @Expose
    private List<RequestNPredictionNStoreEntity> requestEntities = null;

    public PageMap getPageMap() {
        return pageMap;
    }

    public List<RequestNPredictionNStoreEntity> getRequestEntities() {
        return requestEntities;
    }
}
