package com.snst.suripartner.lib.api;

import java.util.BitSet;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String BASE_URL = "http://101.99.34.53:1114/repairsystem/";
    private static RetrofitClient mInstance;
    private Retrofit retrofit;
    private BitSet flags = new BitSet(16);

    private RetrofitClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }

    //Getter

    public BitSet getFlags() {
        return flags;
    }
}
