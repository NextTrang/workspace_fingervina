package com.snstfinger.suri.ui.workingStatus;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.model.ReviewForm;

public class ReviewFormViewModel extends ViewModel {
    private final MutableLiveData<ReviewForm> revureForm = new MutableLiveData<ReviewForm>();

    public LiveData<ReviewForm> getReviewForm() {
        return revureForm;
    }

    public void initReviewForm(RequestNPredictionNStoreEntity mySuriProduct) {
        revureForm.setValue(makeReviewForm(mySuriProduct));
    }

    private ReviewForm makeReviewForm(RequestNPredictionNStoreEntity mySuriProduct) {
        ReviewForm reviewForm = new ReviewForm();
        reviewForm.setRequestEstimateId(mySuriProduct.requestEstimateId);
        reviewForm.setPredictionEstimateId(mySuriProduct.predictionEstimateId);
        reviewForm.setUserId(mySuriProduct.userId);
        reviewForm.setStoreId(mySuriProduct.storeId);

        // display default score of store.
        int qualityPoint = mySuriProduct.pointQuality.isEmpty()?60:(int) (Float.parseFloat(mySuriProduct.pointQuality) * 20);
        int kindnessPoint = mySuriProduct.pointKindness.isEmpty()?60:(int) (Float.parseFloat(mySuriProduct.pointKindness) * 20);
        int accuracyPoint = (int)(Float.parseFloat(mySuriProduct.predictionPrice)/Float.parseFloat(mySuriProduct.price)*100);

        reviewForm.setQuality(qualityPoint);
        reviewForm.setKindness(kindnessPoint);
        reviewForm.setAccuracy(accuracyPoint);

        return reviewForm;
    }
}
