package com.snstfinger.suri.network;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class DateTypeAdapter implements JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            DateFormat utc = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
            utc.setTimeZone(TimeZone.getTimeZone("UTC"));
            return utc.parse(json.getAsString());
        } catch (ParseException e) {
            return null;
        }
    }
}
