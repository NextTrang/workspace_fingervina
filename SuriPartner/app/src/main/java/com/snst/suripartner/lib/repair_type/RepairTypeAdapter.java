package com.snst.suripartner.lib.repair_type;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.snst.suripartner.databinding.RepairTypeBinding;
import java.util.List;

public class RepairTypeAdapter extends RecyclerView.Adapter<RepairTypeAdapter.MyViewHolder> {

    private Context mContext;
    private List<RepairTypeViewModel> mData;
    private LayoutInflater inflater;

    public RepairTypeAdapter(Context mContext, List<RepairTypeViewModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RepairTypeBinding binding = RepairTypeBinding.inflate(inflater, parent, false);

        return new RepairTypeAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private RepairTypeBinding binding;

        public MyViewHolder(RepairTypeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(RepairTypeViewModel viewModel) {
            binding.setRepairTypeViewModel(viewModel);
        }
    }
}
