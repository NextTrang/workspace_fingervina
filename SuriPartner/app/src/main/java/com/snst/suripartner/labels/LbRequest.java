package com.snst.suripartner.labels;

public class LbRequest {
    public static class LbUser {
        public static final String USER_ID = "user_id";
        public static final String USER_PW = "user_pw";
        public static final String LOGIN_KIND = "login_kind";
        public static final String ACCOUNT = "ACCOUNT";
        public static final String FCM_TOKEN = "fcm_token";
    }

    public static class LbRegPredictEst {
        public static final String PREDICTION_PRICE = "prediction_price";
        public static final String PREDICTION_PRICE_COMMENT = "prediction_price_comment";
        public static final String PREDICTION_ESTIMATE_DATE = "prediction_estimate_date";
        public static final String ENGINEER_ID = "engineer_id";
        public static final String DESIRED_RESERVATION_DATE_YN = "desired_reservation_date_yn";
    }

    public static class LbCommon {
        public static final String STORE_ID = "store_id";
        public static final String PAGE = "page";
        public static final String REQUEST_ESTIMATE_ID = "request_estimate_id";
        public static final String PREDICTION_ESTIMATE_ID = "prediction_estimate_id";
    }

    public static class LbCancelBooking {
        public static final String PARTNER_BOOKING_CANCEL_TYPE_ID = "partner_booking_cancel_type_id";
        public static final String COMMENT = "comment";
    }

    public static class LbSendPayment {
        public static final String PAYMENT_STATEMENT = "paymentStatement";
    }

    public static class LbSuri{
        public static final String SURI_IMAGE1 = "suri_image1";
        public static final String TITLE = "title";
        public static final String CONTENTS = "contents";
        public static final String PROGRESS_RATE = "progress_rate";
        public static final String REPAIR_COMMENT = "repair_comment";
        public static final String PREDICTION_IMAGE1 = "prediction_image1";
        public static final String PREDICTION_IMAGE2 = "prediction_image2";
    }

    public static class LbFlag{
        public static final int REQUEST_DATA = 1;
        public static final int REQUEST_UPDATE_BEFORE_COMPLETE_REPAIR = 2;
        public static final int REQUEST_COMPLETE_REPAIR = 3;
    }
}
