package com.snst.suripartner.lib.quotation;

import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.helper.Getter;
import com.snst.suripartner.lib.repair_type.RepairType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Quotation {
    private static final String TAG = "Quotation";
    private String imageUrl;
    private String name;
    private String timestamp;
    private String address;
    private String addressLatLng;
    private String id;
    private String state;
    private String damageDescription;
    private String brandName;
    private String modelName;
    private long estCompletedDate;
    private String estCompletedTime;
    private String estPrice;
    private String requestEstId;
    private String predictionPriceComment;
    private String userId;
    private String desiredReservationDate;
    private Boolean desiredReservationDateYn = false;

    private List<RepairType> estimateRepairTypes = new ArrayList<>();

    public Quotation(QuotationViewModel viewModel) {
        this.imageUrl = viewModel.imageUrl;
        this.name = viewModel.name;
        this.timestamp = viewModel.timestamp;
        this.address = viewModel.address;
        this.addressLatLng = viewModel.addressLatLng;
        this.id = viewModel.id;
        this.state = viewModel.state;
        this.estimateRepairTypes = viewModel.estimateRepairTypes;
        this.damageDescription = viewModel.damageDescription;
        this.brandName = viewModel.brandName;
        this.modelName = viewModel.modelName;
        this.estCompletedDate = viewModel.estCompletedDate;
        this.estCompletedTime = viewModel.estCompletedTime;
        this.estPrice = viewModel.estPrice;
        this.requestEstId = viewModel.requestEstId;
        this.predictionPriceComment = viewModel.predictionPriceComment;
        this.userId = viewModel.userId;
        this.desiredReservationDate = viewModel.desiredReservationDate;
        this.desiredReservationDateYn = viewModel.desiredReservationDateYn;
    }

    public Quotation(JSONObject jsInfo) {
        try {
            String write_yn = jsInfo.getString(LbData.LbInfo.WRITE_YN);
            switch (write_yn) {
                case "y":
                    state = LbState.OFFERED;
                    break;
                case "n":
                    state = LbState.ONGOING;
                    break;
            }

            imageUrl = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.ESTIMATE_IMAGE_1);
            name = jsInfo.getString(LbData.LbInfo.BRAND_NAME);
            timestamp = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.REGISTER_DATE));
            address = jsInfo.getString(LbData.LbInfo.ESTIMATE_ADDRESS_1);
            addressLatLng = jsInfo.getString(LbData.LbInfo.ESTIMATE_ADDRESS_LL_1);
            id = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            damageDescription = jsInfo.getString(LbData.LbInfo.ESTIMATE_DESCRIPTION);
            brandName = jsInfo.getString(LbData.LbInfo.BRAND_NAME);
            modelName = jsInfo.getString(LbData.LbInfo.MODEL_NAME);

            String predictionEstDate = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_DATE));
            if (!predictionEstDate.isEmpty()) {
                String[] estDate = predictionEstDate.split(" ");
                estCompletedDate = Conversion.string_2_date(estDate[Getter.posLocalDate]);

                String[] hr_min_sec = estDate[Getter.posLocalTime].split(":");
                estCompletedTime = hr_min_sec[0] + ":" + hr_min_sec[1];
            }

            estPrice = jsInfo.getString(LbData.LbInfo.PREDICTION_PRICE);

            String estRepairTypesNames = jsInfo.getString(LbData.LbInfo.ESTIMATE_REPAIR_TYPE_NAMES);
            String[] parsedNames = estRepairTypesNames.split("\\|");
            for (String name : parsedNames) {
                estimateRepairTypes.add(new RepairType(name));
            }

            //below infos are used for offer quotation
            requestEstId = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            predictionPriceComment = jsInfo.getString(LbData.LbInfo.PREDICTION_PRICE_COMMENT);
            userId = jsInfo.getString(LbData.LbInfo.USER_ID);

            desiredReservationDate = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.DESIRED_RESERVATION_DATE));
            String bData = jsInfo.getString(LbData.LbInfo.DESIRED_RESERVATION_DATE_YN);
            if (bData.equals("y")) {
                desiredReservationDateYn = true;
            } else {
                desiredReservationDateYn = false;
            }

        } catch (JSONException e) {
            Log.e(TAG, "onPostExecute: " + e.getMessage());
        } catch (ParseException e) {
            Log.e(TAG, "Quotation: " + e.getMessage());
        }
    }

    //Getter
    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getDesiredReservationDate() {
        return desiredReservationDate;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public long getEstCompletedDate() {
        return estCompletedDate;
    }

    public String getEstCompletedTime() {
        return estCompletedTime;
    }

    public List<RepairType> getEstimateRepairTypes() {
        return estimateRepairTypes;
    }

    public String getEstPrice() {
        return estPrice;
    }

    public String getRequestEstId() {
        return requestEstId;
    }

    public String getPredictionPriceComment() {
        return predictionPriceComment;
    }

    public String getUserId() {
        return userId;
    }

    public Boolean getDesiredReservationDateYn() {
        return desiredReservationDateYn;
    }

    public String getAddressLatLng() {
        return addressLatLng;
    }

    //Setter

    public void setEstPrice(String estPrice) {
        this.estPrice = estPrice;
    }

    public void setPredictionPriceComment(String predictionPriceComment) {
        this.predictionPriceComment = predictionPriceComment;
    }

    public void setDesiredReservationDateYn(Boolean desiredReservationDateYn) {
        this.desiredReservationDateYn = desiredReservationDateYn;
    }

    public void setEstCompletedDate(long estCompletedDate) {
        this.estCompletedDate = estCompletedDate;
    }

    public void setEstCompletedTime(String estCompletedTime) {
        this.estCompletedTime = estCompletedTime;
    }
}
