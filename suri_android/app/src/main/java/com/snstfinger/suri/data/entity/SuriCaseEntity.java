package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.network.EmptyStringAsNullTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SuriCaseEntity implements Serializable {

    @SerializedName("prediction_estimate_id")
    private int predictionEstimateid;

    @SerializedName("price")
    @JsonAdapter(EmptyStringAsNullTypeAdapter.class)
    private String price;

    @SerializedName("repair_comment")
    private String repairComment;

    @SerializedName("prediction_image1")
    private String predictionImage1;

    @SerializedName("prediction_image2")
    private String predictionImage2;

    @SerializedName("brand_name")
    private String brandName;

    @SerializedName("model_name")
    private String modelName;

    @SerializedName("score")
    @JsonAdapter(EmptyStringAsNullTypeAdapter.class)
    private Float score;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    private Date registerDate;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("store_name")
    private String storeName;

    public int getPredictionEstimateid() {
        return predictionEstimateid;
    }

    public void setPredictionEstimateid(int predictionEstimateid) {
        this.predictionEstimateid = predictionEstimateid;
    }

    public String getPrice() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return price == null ? "0" : formatter.format(Float.parseFloat(price));
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRepairComment() {
        return repairComment;
    }

    public void setRepairComment(String repairComment) {
        this.repairComment = repairComment;
    }

    public String getPredictionImage1() {
        return predictionImage1;
    }

    public void setPredictionImage1(String predictionImage1) {
        this.predictionImage1 = predictionImage1;
    }

    public String getPredictionImage2() {
        return predictionImage2;
    }

    public void setPredictionImage2(String predictionImage2) {
        this.predictionImage2 = predictionImage2;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public String getRegisterDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return registerDate == null ? "" : formatter.format(registerDate);
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
