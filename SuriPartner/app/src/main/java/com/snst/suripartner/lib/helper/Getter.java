package com.snst.suripartner.lib.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Getter {

    public static String getRealPathFromURI(Context context, Uri uri) {
        if (uri.getScheme().equals("file")) {
            return uri.getPath();
        }

        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    public static int posLocalTime = 0;
    public static int posLocalDate = 1;

    public static String getDateTimeFromServer(String serverDate) {
        if (serverDate == null || serverDate.isEmpty()) {
            return "";
        }

        try {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date inputDate = serverFormat.parse(serverDate);

            SimpleDateFormat localFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            localFormat.setTimeZone(TimeZone.getDefault());
            String outputDate = localFormat.format(inputDate);
            return outputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static long getElapsedTime(String startDate) {
        Date current = Calendar.getInstance().getTime();

        long diff = current.getTime() - Conversion.string_2_date_time(startDate).getTime();
        return TimeUnit.MILLISECONDS.toDays(diff);
    }

    public static String[] getCompletedTime(String startDate, String endDate) {
        Date dateStart = Conversion.string_2_date_time(startDate);
        Date dateEnd = Conversion.string_2_date_time(endDate);

        long diff = dateEnd.getTime()-dateStart.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(diff);
        long hours = TimeUnit.MILLISECONDS.toHours(diff - TimeUnit.DAYS.toMillis(days));
        long mins = TimeUnit.MILLISECONDS.toMinutes(diff - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours));

        String[] result = {String.valueOf(days),String.valueOf(hours),String.valueOf(mins)};

        return result;
    }
}
