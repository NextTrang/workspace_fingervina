package com.snstfinger.suri.ui.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.data.model.SelectSuriObject;
import com.snstfinger.suri.databinding.DialogSelectTypeBinding;

public class SelectSuriDialogFragment extends DialogFragment {
    private SelectSuriObject suri;
    private DialogSelectTypeBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle options = getArguments();
        suri = ((SelectSuriObject) options.getSerializable("object_suri"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_type, container, false);
        binding.setHandler(this);
        binding.setSuri(suri);

        return binding.getRoot();
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public void onStart() {
//        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
//        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);
//        getDialog().getWindow().setLayout(width, height);
        super.onStart();
    }

    public void onDismiss(View view) {
        getDialog().dismiss();

    }

    public void selectObject(View view) {
        suri.setContent(getString(R.string.msg_select_obj));
        suri.setStep(2);

    }

    public void submit(View view) {
        int checkedRadioButtonId = binding.selectSuri.getCheckedRadioButtonId();

        switch (checkedRadioButtonId) {
            case R.id.phone:
                suri.getRequestForm().setObjectId(RequestEstimate.TYPE_MOBILE);
                break;

            case R.id.pc:
                suri.getRequestForm().setObjectId(RequestEstimate.TYPE_PC);
                break;

            case R.id.bike:
                suri.getRequestForm().setObjectId(RequestEstimate.TYPE_BIKE);
                break;

            case R.id.car:
                suri.getRequestForm().setObjectId(RequestEstimate.TYPE_CAR);
                break;
        }

        getDialog().dismiss();
    }
}
