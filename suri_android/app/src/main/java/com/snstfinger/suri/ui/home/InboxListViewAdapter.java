package com.snstfinger.suri.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.InboxEntity;
import com.snstfinger.suri.databinding.ListItemInboxBinding;

import java.util.ArrayList;
import java.util.List;

public class InboxListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = InboxListViewAdapter.class.getName();

    private List<InboxEntity> mValues;

    public InboxListViewAdapter() {
        mValues = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemInboxBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_inbox, parent, false);

        return new InboxViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (mValues != null) {
            InboxEntity item = (InboxEntity) mValues.get(position);
            ((InboxViewHolder) holder).binding.setNotice(item);
        }
    }

    @Override
    public int getItemCount() {
        if (mValues != null)
            return mValues.size();
        else return 0;
    }

    public List<InboxEntity> getSelectedItem() {
        List<InboxEntity> selected = new ArrayList<>();
        for(InboxEntity item : mValues) {
            if(item.isSelected()) {
                selected.add(item);
            }
        }
        return selected;
    }

    void setItemList(List<InboxEntity> items) {
        this.mValues = items;
        notifyDataSetChanged();
    }


    public class InboxViewHolder extends RecyclerView.ViewHolder {
        ListItemInboxBinding binding;

        public InboxViewHolder(ListItemInboxBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
