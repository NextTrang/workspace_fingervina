package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.snstfinger.suri.BaseActivity;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.ui.tools.MyLocationTracker;

public class ShopListActivity extends BaseActivity implements OnShopListInteractionListener, View.OnClickListener {
    private static final String TAG = ShopListActivity.class.getName();

    private FragmentManager mFragmentManager;
    private MyLocationTracker myLocationTracker;
    private ShopListViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shop_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        myLocationTracker =new MyLocationTracker(this);

        Button btnMap = findViewById(R.id.btn_list_map);
        Button btnFilter = findViewById(R.id.btn_filter);
        btnMap.setOnClickListener(this);
        btnFilter.setOnClickListener(this);

        mViewModel = ViewModelProviders.of(this).get(ShopListViewModel.class);

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new ShopListOnMapFragment()).commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        myLocationTracker.stopUsingGPS();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onShopClick(StoreEntity item) {
        Intent shopDetail = new Intent(this, ShopInfoActivity.class);
        shopDetail.putExtra("shop_info", StoreEntity.convertToShopInfo(item));
        startActivity(shopDetail);
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "Clicked::"+v.getId());

        if(v.getId() == R.id.btn_list_map)  {
            if(((CheckBox)v).isChecked()) {
                mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new ShopListOnMapFragment()).commit();

            } else {
                mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new ShopListFragment()).commit();

            }
        }
    }

    public Location getLocation() {
        return myLocationTracker.getLocation();
    }
}
