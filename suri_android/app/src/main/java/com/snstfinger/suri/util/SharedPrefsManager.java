package com.snstfinger.suri.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsManager {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    protected SharedPrefsManager(Context context, String paramString, int mode) {
        this.mContext = context;
        this.mSharedPreferences = this.mContext.getSharedPreferences(paramString, mode);
        this.mEditor = this.mSharedPreferences.edit();
    }

    public void clear() {
        this.mEditor.clear();
        this.mEditor.commit();
    }

    protected float get(String paramString, float paramFloat) { return this.mSharedPreferences.getFloat(paramString, paramFloat); }

    protected int get(String paramString, int paramInt) { return this.mSharedPreferences.getInt(paramString, paramInt); }

    protected long get(String paramString, long paramLong) { return this.mSharedPreferences.getLong(paramString, paramLong); }

    protected String get(String paramString1, String paramString2) { return this.mSharedPreferences.getString(paramString1, paramString2); }

    protected boolean get(String paramString, boolean paramBoolean) { return this.mSharedPreferences.getBoolean(paramString, paramBoolean); }

    protected void put(String paramString, float paramFloat) {
        this.mEditor.putFloat(paramString, paramFloat);
        this.mEditor.commit();
    }

    protected void put(String paramString, int paramInt) {
        this.mEditor.putInt(paramString, paramInt);
        this.mEditor.commit();
    }

    protected void put(String paramString, long paramLong) {
        this.mEditor.putLong(paramString, paramLong);
        this.mEditor.commit();
    }

    protected void put(String paramString1, String paramString2) {
        this.mEditor.putString(paramString1, paramString2);
        this.mEditor.commit();
    }

    protected void put(String paramString, boolean paramBoolean) {
        this.mEditor.putBoolean(paramString, paramBoolean);
        this.mEditor.commit();
    }
}
