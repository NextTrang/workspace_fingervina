package com.snst.suripartner.lib.helper;

import android.text.format.DateFormat;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Conversion {

    public static String date_2_string(long date) {
        return DateFormat.format("dd/MM/yyyy", new Date(date)).toString();
    }

    public static long string_2_date(String strDate) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(strDate).getTime();
    }

    public static Date string_2_date_time(String strDateTime){
        try {
            SimpleDateFormat localFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            Date date = localFormat.parse(strDateTime);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static RequestBody string_2_requestBody(String string) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), string);
    }

    public static MultipartBody.Part file_2_multiPartBodyPart(String fieldName, File file) {
        return MultipartBody.Part.createFormData(fieldName, file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
    }

    public static String string_2_serverDate(String localDate){
        try {
            SimpleDateFormat localFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
            localFormat.setTimeZone(TimeZone.getDefault());
            Date inputDate = localFormat.parse(localDate);

            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String outputDate = serverFormat.format(inputDate);
            return outputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String number_2_currency(String number){
        if(number.isEmpty()){
            return number;
        }

        DecimalFormat formatter = new DecimalFormat("###,###");
        return formatter.format(Double.parseDouble(number));
    }

    public static String currency_2_number(String currency){
        return currency.replaceAll(",","");
    }
}
