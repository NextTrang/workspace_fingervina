package com.snstfinger.suri.ui.shop;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.databinding.ListItemCaseBinding;
import com.snstfinger.suri.databinding.ListItemSimpleCaseBinding;

import java.util.ArrayList;
import java.util.List;

public class PagedSuriCaseAdapter extends PagedListAdapter<SuriCaseEntity, RecyclerView.ViewHolder> {

    private final OnSuriCaseListInteractionListener mListener;
    private boolean mIsSimple;

    public PagedSuriCaseAdapter(OnSuriCaseListInteractionListener listener, boolean isSimple) {
        super(DIFF_CALLBACK);
        mListener = listener;
        mIsSimple = isSimple;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mIsSimple) {
            ListItemSimpleCaseBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_simple_case, parent, false);
            return new SimpleCaseViewHolder(binding);

        } else {
            ListItemCaseBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_case, parent, false);
            return new CaseViewHolder(binding);

        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SuriCaseEntity item = getItem(position);
        if (mIsSimple)
            ((SimpleCaseViewHolder) holder).bind(item);
        else
            ((CaseViewHolder) holder).bind(item);

    }

    private static DiffUtil.ItemCallback<SuriCaseEntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<SuriCaseEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull SuriCaseEntity oldItem, @NonNull SuriCaseEntity newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull SuriCaseEntity oldItem, @NonNull SuriCaseEntity newItem) {
            return oldItem.getPredictionEstimateid() == newItem.getPredictionEstimateid();
        }
    };

    public class CaseViewHolder extends RecyclerView.ViewHolder {
        ListItemCaseBinding binding;

        public CaseViewHolder(ListItemCaseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(SuriCaseEntity item) {
            binding.setSuriCase(item);
            itemView.setOnClickListener(v -> mListener.onCaseClick(item));
        }
    }

    public class SimpleCaseViewHolder extends RecyclerView.ViewHolder {
        ListItemSimpleCaseBinding binding;

        public SimpleCaseViewHolder(ListItemSimpleCaseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(SuriCaseEntity item) {
            binding.setSuriCase(item);
            itemView.setOnClickListener(v -> mListener.onCaseClick(item));
        }
    }
}
