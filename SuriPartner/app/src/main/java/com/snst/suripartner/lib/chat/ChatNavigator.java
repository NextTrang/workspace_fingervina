package com.snst.suripartner.lib.chat;

public interface ChatNavigator {
    void onItemClick(Chat chat);
}
