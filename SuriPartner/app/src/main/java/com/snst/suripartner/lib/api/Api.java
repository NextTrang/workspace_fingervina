package com.snst.suripartner.lib.api;

import com.google.gson.JsonObject;
import com.snst.suripartner.labels.LbRequest;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {

    @FormUrlEncoded
    @POST("user/login")
    Call<ResponseBody> userLogin(
            @Field(LbRequest.LbUser.USER_ID) String userId,
            @Field(LbRequest.LbUser.USER_PW) String password,
            @Field(LbRequest.LbUser.FCM_TOKEN) String token,
            @Field(LbRequest.LbUser.LOGIN_KIND) String account
    );

    @FormUrlEncoded
    @POST("store/getAllRequestEstimateListByStoreId")
    Call<ResponseBody> storeAllEstimateList(
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCommon.PAGE) String page
    );

    @FormUrlEncoded
    @POST("store/getRequestEstimatesQUOTATION")
    Call<ResponseBody> getQuotations(
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCommon.PAGE) String page
    );

    @FormUrlEncoded
    @POST("store/getRequestEstimatesBOOKING")
    Call<ResponseBody> getBookings(
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCommon.PAGE) String page
    );

    @FormUrlEncoded
    @POST("store/getRequestEstimatesSURI")
    Call<ResponseBody> getSURI(
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCommon.PAGE) String page
    );

    @FormUrlEncoded
    @POST("predictionestimate/registerPredictionEstimate")
    Call<ResponseBody> registerPredictEstimate(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) String requestEstimateId,
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbRegPredictEst.PREDICTION_PRICE) String predictPrice,
            @Field(LbRequest.LbRegPredictEst.PREDICTION_PRICE_COMMENT) String predictPriceComment,
            @Field(LbRequest.LbRegPredictEst.PREDICTION_ESTIMATE_DATE) String predictEstimateDate,
            @Field(LbRequest.LbRegPredictEst.ENGINEER_ID) String engineerId,
            @Field(LbRequest.LbRegPredictEst.DESIRED_RESERVATION_DATE_YN) String desireReserveDataYn
    );

    @FormUrlEncoded
    @POST("predictionestimate/confirmBookingPredictionEstimate")
    Call<ResponseBody> confirmBooking(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) String predictEstId,
            @Field(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) String requestEstId
    );

    @FormUrlEncoded
    @POST("predictionestimate/cancelBookingPredictionEstimate")
    Call<ResponseBody> cancelBooking(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) String predictEstId,
            @Field(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) String requestEstId,
            @Field(LbRequest.LbCancelBooking.PARTNER_BOOKING_CANCEL_TYPE_ID) String cancelTypeId,
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCancelBooking.COMMENT) String comment
    );

    @POST("predictionestimate/sendPaymentStatement")
    Call<ResponseBody> sendPayment(
            @HeaderMap Map<String, String> header,
            @Body JsonObject body
    );

    @FormUrlEncoded
    @POST("predictionestimate/updateStartRepair")
    Call<ResponseBody> startSuri(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) String predictEstId
    );

    @FormUrlEncoded
    @POST("surihistory/getAllSuriHistoryByRequestEstimateId")
    Call<ResponseBody> getSuriHistory(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) String requestEstId
    );

    @Multipart
    @POST("surihistory/saveSuriHistory")
    Call<ResponseBody> writeSuriHistory(
            @Header("Cookie") String cookie,
            @Part(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) RequestBody requestEstId,
            @Part(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) RequestBody predictEstId,
            @Part MultipartBody.Part suriImage1,
            @Part(LbRequest.LbSuri.TITLE) RequestBody title,
            @Part(LbRequest.LbSuri.CONTENTS) RequestBody contents
    );

    @FormUrlEncoded
    @POST("predictionestimate/updateRepair")
    Call<ResponseBody> updateRepair(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) String predictEstId,
            @Field(LbRequest.LbSuri.PROGRESS_RATE) String progressRate
    );

    @Multipart
    @POST("predictionestimate/requestRepairComplete")
    Call<ResponseBody> completeRepair(
            @Header("Cookie") String cookie,
            @Part(LbRequest.LbCommon.PREDICTION_ESTIMATE_ID) RequestBody predictEstId,
            @Part(LbRequest.LbCommon.REQUEST_ESTIMATE_ID) RequestBody requestEstId,
            @Part(LbRequest.LbSuri.REPAIR_COMMENT) RequestBody repairComment,
            @Part MultipartBody.Part predictImage1,
            @Part MultipartBody.Part predictImage2

    );

    @POST("predictionestimate/resendPaymentStatement")
    Call<ResponseBody> resendPayment(
            @HeaderMap Map<String, String> header,
            @Body JsonObject body
    );

    @FormUrlEncoded
    @POST("review/getReviewByStoreId")
    Call<ResponseBody> getReviews(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.STORE_ID) String storeId,
            @Field(LbRequest.LbCommon.PAGE) String page
    );

    @FormUrlEncoded
    @POST("store/getStoreInfoByStoreID")
    Call<ResponseBody> getStoreInfo(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.STORE_ID) String storeId
    );

    @FormUrlEncoded
    @POST("store/countTotalSuriCompleteAndTodo")
    Call<ResponseBody> getCountSuri(
            @Header("Cookie") String cookie,
            @Field(LbRequest.LbCommon.STORE_ID) String storeId
    );
}
