package com.snstfinger.suri.data.entity;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.network.DateTypeAdapter;

import java.io.Serializable;
import java.util.Date;

public class ReviewEntity implements Serializable {

    @SerializedName("review_id")
    private String reviewId;

    @SerializedName("request_estimate_id")
    private String requestEstimateId;

    @SerializedName("prediction_estimate_id")
    private String predictionEstimateid;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("review_comment")
    private String reivewComment;

    @SerializedName("quality")
    private Float quality;

    @SerializedName("kindness")
    private Float kindness;

    @SerializedName("accuracy")
    private Float accuracy;

    @SerializedName("score")
    private Float score;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    private Date registerDate;

    @SerializedName("satisfaction")
    private String satisfaction;

    @SerializedName("review_image1")
    private String review_image1;

    @SerializedName("review_image2")
    private String review_image2;

    @SerializedName("user_name")
    private String userName;

    @SerializedName("manager_picture")
    private String managerPicUrl;

    public ReviewEntity() {
    }

    public ReviewEntity(String url, String name, float score, String comment) {
        this.managerPicUrl = url;
        this.userName = name;
        this.score = score;
        this.reivewComment = comment;
    }

    public String getManagerPicUrl() {
        return managerPicUrl;
    }

    public void setManagerPicUrl(String managerPicUrl) {
        this.managerPicUrl = managerPicUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getReivewComment() {
        return reivewComment;
    }

    public void setReivewComment(String reivewComment) {
        this.reivewComment = reivewComment;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getRequestEstimateId() {
        return requestEstimateId;
    }

    public void setRequestEstimateId(String requestEstimateId) {
        this.requestEstimateId = requestEstimateId;
    }

    public String getPredictionEstimateid() {
        return predictionEstimateid;
    }

    public void setPredictionEstimateid(String predictionEstimateid) {
        this.predictionEstimateid = predictionEstimateid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Float getQuality() {
        return quality;
    }

    public void setQuality(Float quality) {
        this.quality = quality;
    }

    public Float getKindness() {
        return kindness;
    }

    public void setKindness(Float kindness) {
        this.kindness = kindness;
    }

    public Float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Float accuracy) {
        this.accuracy = accuracy;
    }

    public String getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(String satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getReview_image1() {
        return review_image1;
    }

    public void setReview_image1(String review_image1) {
        this.review_image1 = review_image1;
    }

    public String getReview_image2() {
        return review_image2;
    }

    public void setReview_image2(String review_image2) {
        this.review_image2 = review_image2;
    }

    public int getSeekBarQuality() {
        if (quality != null) {
            return (int) (quality * 20);
        }
        return 0;
    }

    public int getSeekBarKindness() {
        if (kindness != null) {
            return (int) (kindness * 20);
        }
        return 0;
    }

    public int getSeekBarAccuracy() {
        if (accuracy != null) {
            return (int) (accuracy * 20);
        }
        return 0;
    }

    @BindingAdapter("bind:disabled")
    public static void setDisabled(SeekBar view, boolean enabled) {
        view.setOnTouchListener((v, event) -> true);
    }

    @BindingAdapter(value = {"profile", "defaultImage"})
    public static void loadImage(ImageView imageView, String imageURL, Drawable res) {
        Log.d("Profile IMG", "StoreIMG::" + imageURL);

        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(new RequestOptions().circleCrop())
                .load(imageURL)
                .placeholder(res)
                .into(imageView);
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
}
