package com.snstfinger.suri.ui.tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class SuriCameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private final String TAG = SuriCameraPreview.class.getName();

    private SurfaceHolder mHolder;

    private  int mCameraId;

    private Camera mCamera;
    private Camera.CameraInfo mCameraInfo;

    private int mDisplayOrientation;

    public SuriCameraPreview(Context context, int cameraId) {
        super(context);

        Log.d(TAG, "SuriCameraPreview id::"+cameraId);

        // 0 -> back
        // 1 -> front
        mCameraId = cameraId;

        try {
            mCamera = Camera.open(mCameraId);

        } catch (Exception e) {
            Log.d(TAG, "Camera is not available");

        }

        mHolder = getHolder();
        mHolder.addCallback(this);

        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mDisplayOrientation = ((Activity)context).getWindowManager().getDefaultDisplay().getRotation();

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraId, cameraInfo);

        mCameraInfo = cameraInfo;

        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();

//            mCamera.autoFocus(new Camera.AutoFocusCallback() {
//                @Override
//                public void onAutoFocus(boolean success, Camera camera) {
//                    if(success) {
//
//                    }
//                }
//            });

        } catch (IOException e) {
            Log.e(TAG, "Error setting camera preview: " + e.getMessage());
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged");

        if(mHolder.getSurface() == null) {
            Log.e(TAG, "preview surface is null");

            return;
        }

        try {
            mCamera.stopPreview();
            Log.d(TAG, "Preview stopped.");

        } catch (Exception e) {
            Log.e(TAG, "Error starting camera preview: " + e.getMessage());
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        int orientation = calculatePreviewOrientation(mCameraInfo, mDisplayOrientation);
        mCamera.setDisplayOrientation(orientation);
        Camera.Parameters parameters = mCamera.getParameters();
        List<String> focusMode = parameters.getSupportedFocusModes();
        if(focusMode.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        mCamera.setParameters(parameters);

        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
            Log.d(TAG, "Camera preview started.");
        } catch (Exception e) {
            Log.e(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed");
        if(mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;

            holder.removeCallback(this);
        }
    }

    public int calculatePreviewOrientation(Camera.CameraInfo info, int rotation) {
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    public void takePicture() {
        mCamera.takePicture(shutterCallback, rawCallback, jpgCallback);
    }

    private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {

        }
    };

    private Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

        }
    };

    private Camera.PictureCallback jpgCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            int width = camera.getParameters().getPictureSize().width;
            int height = camera.getParameters().getPictureSize().height;
            int orientation = calculatePreviewOrientation(mCameraInfo, mDisplayOrientation);

            //byte array 를 bitmap 으로 변환
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeByteArray( data, 0, data.length, options);

            //이미지를 디바이스 방향으로 회전
            Matrix matrix = new Matrix();

            /**
             * 셀카 모드에는 저장 시 좌우 반전을 해줘야 한다.
             */
            if(mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                //Log.d("MyTag","180도 추가 회전 후 좌우반전을 해줍니다.");
                //orientation += 180;
                matrix.setScale(-1,1);
            }

            matrix.postRotate(orientation);
            bitmap =  Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);

            //bitmap 을  byte array 로 변환
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] currentData = stream.toByteArray();

            //파일로 저장
            new SuriCameraPreview.SaveImageTask().execute(currentData);
        }
    };

    private class SaveImageTask extends AsyncTask<byte[], Void, File> {

        @Override
        protected void onPostExecute(File mediaFile) {
            super.onPostExecute(mediaFile);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();
                Log.d(TAG, "Camera preview started.");
            } catch (Exception e) {
                Log.d(TAG, "Error starting camera preview: " + e.getMessage());
            }

            // 갤러리에 반영
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(mediaFile));
            getContext().sendBroadcast(mediaScanIntent);
        }

        @Override
        protected File doInBackground(byte[]... data) {
            FileOutputStream outStream = null;
            File mediaFile = null;
            try {

                File mediaStorageDir = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"SURI");
                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs();
                }

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

//                if (type == MEDIA_TYPE_IMAGE){
                    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                            "SURI_"+ timeStamp + ".jpg");
//                } else if(type == MEDIA_TYPE_VIDEO) {
//                    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
//                            "VID_"+ timeStamp + ".mp4");
//                } else {
//                    return null;
//                }

                outStream = new FileOutputStream(mediaFile);
                outStream.write(data[0]);
                outStream.flush();
                outStream.close();

                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to "
                        + mediaFile.getAbsolutePath());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return mediaFile;
        }
    }
}
