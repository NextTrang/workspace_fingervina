package com.snst.suripartner.lib.map;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.io.Serializable;

public class MyMap extends BaseObservable implements Serializable {

    private MyLatLng destination;
    private MyLatLng departure;
    private MyLatLng myLocation;
    private String desAddress;

    //Getter
    public MyLatLng getDestination() {
        return destination;
    }

    public MyLatLng getDeparture() {
        return departure;
    }

    @Bindable
    public MyLatLng getMyLocation() {
        return myLocation;
    }

    public String getDesAddress() {
        return desAddress;
    }

    //setter
    public MyMap setDestination(MyLatLng destination) {
        this.destination = destination;
        return this;
    }

    public MyMap setDeparture(MyLatLng departure) {
        this.departure = departure;
        return this;
    }

    public MyMap setMyLocation(MyLatLng myLocation) {
        this.myLocation = myLocation;
        notifyPropertyChanged(com.snst.suripartner.BR.myLocation);
        return this;
    }

    public MyMap setDesAddress(String desAddress) {
        this.desAddress = desAddress;
        return this;
    }
}
