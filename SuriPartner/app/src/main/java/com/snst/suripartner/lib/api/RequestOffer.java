package com.snst.suripartner.lib.api;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.helper.Getter;
import com.snst.suripartner.lib.pop_confirm.PopConfirm;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.pop_up.PopConfirmActivity;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestOffer implements ApiRequest {

    private static final String TAG = "RequestOffer";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().registerPredictEstimate(Sharing.Instance().getCookie(),
                inputs[0], inputs[1], inputs[2], inputs[3], inputs[4], inputs[5], inputs[6]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //move this quotation to offered tab
                        for (QuotationViewModel viewModel : Sharing.Instance().getQuotationViewModels()) {
                            if (viewModel.id.equals(inputs[0])) {
                                viewModel.state = LbState.OFFERED;
                                viewModel.estPrice = inputs[2];
                                viewModel.predictionPriceComment = inputs[3];

                                String[] estDate = Getter.getDateTimeFromServer(inputs[4]).split(" ");
                                viewModel.estCompletedDate  = Conversion.string_2_date(estDate[Getter.posLocalDate]);
                                String[] hr_min_sec = estDate[Getter.posLocalTime].split(":");
                                viewModel.estCompletedTime = hr_min_sec[0] + ":" + hr_min_sec[1];

                                viewModel.userId = inputs[5];

                                if (inputs[6].equals("y")) {
                                    viewModel.desiredReservationDateYn = true;
                                } else {
                                    viewModel.desiredReservationDateYn = false;
                                }
                                break;
                            }
                        }

                        //refresh Quotation
                        ((MainActivity) context).refresh();

                        PopConfirm popConfirm = new PopConfirm()
                                .setTitle(LbPop.LbTitle.OFFER_COMPLETED)
                                .setContent(LbPop.LbContent.OFFER_COMPLETED);
                        Intent intent = new Intent(context, PopConfirmActivity.class);
                        intent.putExtra(LbPop.LbKey.POP_CONFIRM,popConfirm);
                        context.startActivity(intent);
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
