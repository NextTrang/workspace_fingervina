package com.snst.suripartner.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.RequestCompleteSuriBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.helper.Devicer;
import com.snst.suripartner.lib.helper.Getter;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri_complete.SuriComplete;
import com.snst.suripartner.lib.suri_complete.SuriCompleteEvent;

import java.io.File;

public class PopSuriRequestCompleteActivity extends AppCompatActivity {

    private Suri suri;
    private SuriComplete suriComplete = new SuriComplete();
    private SuriCompleteEvent handler = new SuriCompleteEvent(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RequestCompleteSuriBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_suri_request_complete);

        suri = (Suri) getIntent().getSerializableExtra("Suri");

        //binding data
        binding.setHandler(handler);
        binding.setSuriComplete(suriComplete
                .setPredictEstId(suri.getPredictEstId())
                .setReqEstId(suri.getId())
                .setProgressRate(suri.getProgressRate()));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            //get file returned from Gallery
            File photoFile = (File) data.getSerializableExtra(LbPop.LbKey.PHOTO_FILE);
            boolean openCamera = data.getBooleanExtra(LbPop.LbKey.OPEN_CAMERA, false);

            if (openCamera) {
                Devicer.openCamera(this);
            } else if (photoFile != null) {
                handler.onAssignImage(suriComplete, photoFile);
            }
        } else if (requestCode == LbPop.LbCode.CAMERA_REQUEST) {
            handler.onAssignImage(suriComplete, Devicer.photoFile);
            Devicer.photoFile = null;
        }
    }
}
