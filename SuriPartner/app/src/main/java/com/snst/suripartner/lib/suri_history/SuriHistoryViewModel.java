package com.snst.suripartner.lib.suri_history;

public class SuriHistoryViewModel {

    public String id;
    public String predictEstId;
    public String timeStamp;
    public String title;
    public String content;
    public String imageUrl;

    public SuriHistoryViewModel(SuriHistory suriHistory) {
        this.id = suriHistory.getId();
        this.predictEstId = suriHistory.getPredictEstId();
        this.timeStamp = suriHistory.getTimeStamp();
        this.title = suriHistory.getTitle();
        this.content = suriHistory.getContent();
        this.imageUrl = suriHistory.getImageUrl();
    }
}
