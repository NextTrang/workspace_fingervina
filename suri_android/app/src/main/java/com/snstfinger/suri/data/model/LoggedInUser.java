package com.snstfinger.suri.data.model;

import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.network.ApiManager;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    @SerializedName("role_id")
    private Integer roleId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("user_pw")
    private String userPw;
    @SerializedName("user_name")
    private String displayName;
    @SerializedName("user_email")
    private String userEmail;

    @SerializedName("user_phonenumber")
    private String userPhone;

    @SerializedName("fcm_token")
    private String fcmToken;

    @SerializedName("login_kind")
    private String loginType;

    public LoggedInUser(String userId, String userEmail, String userPw, String displayName, String userPhone, String loginType) {
        this.roleId = 0;
        this.userId = userId;
        this.userEmail = userEmail;
        this.userPw = userPw;
        this.displayName = displayName;
        this.userPhone = userPhone;
        this.loginType = loginType;
    }

    public LoggedInUser(String userId, String userEmail, String userPw, String loginType) {
        this.roleId = 0;
        this.userId = userId;
        this.userEmail = userEmail;
        this.userPw = userPw;
        this.loginType = loginType;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String userPw) {
        this.userPw = userPw;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
