package com.snstfinger.suri.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.InboxEntity;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;
import com.snstfinger.suri.util.SettingSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * 알림내역 리스트
 */
public class InboxActivity extends AppCompatActivity implements View.OnClickListener {
    public static String TAG = com.snstfinger.suri.ui.home.InboxActivity.class.getName();

    private InboxViewModel mInboxViewModel;
    final InboxListViewAdapter mAdapter = new InboxListViewAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);


        RecyclerView listView = findViewById(R.id.listview_inbox);
//        final InboxListViewAdapter adapter = new InboxListViewAdapter();
        listView.setAdapter(mAdapter);

        mInboxViewModel = ViewModelProviders.of(this).get(InboxViewModel.class);
        mInboxViewModel.getAll().observe(this, new Observer<List<InboxEntity>>() {
            @Override
            public void onChanged(List<InboxEntity> inboxEntities) {
                mAdapter.setItemList(inboxEntities);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_delete: {
                List<InboxEntity> list = mAdapter.getSelectedItem();
                InboxEntity[] array = list.toArray(new InboxEntity[list.size()]);
                Log.d(TAG, "size="+array.length);
                mInboxViewModel.delete(array);

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_payment:

                break;

            case R.id.btn_share:

                break;

            case R.id.btn_terms_condition:

                break;

            case R.id.btn_policy:

                break;

            case R.id.btn_delete_me:

                break;

        }
    }

    private void clearMyData() {
        SettingSharedPreferences settings = SettingSharedPreferences.getInstance(getApplicationContext());
        settings.clear();
        MySuriInfoSharedPreferences mysuri = MySuriInfoSharedPreferences.getInstance(getApplicationContext());
        mysuri.clear();
        UserInfoSharedPreferences user = UserInfoSharedPreferences.getInstance(getApplicationContext());
        user.clear();
    }

}
