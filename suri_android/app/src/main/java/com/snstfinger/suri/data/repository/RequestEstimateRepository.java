package com.snstfinger.suri.data.repository;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ResponseRequestNPredictionNStore;
import com.snstfinger.suri.data.entity.ResponseStores;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestEstimateRepository {
    private static final String TAG = RequestEstimateRepository.class.getName();

    private ArrayList<StoreNPredictionEntity> storeNPredictionEntities = new ArrayList<>();
    private MutableLiveData<List<StoreNPredictionEntity>> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<List<RequestNPredictionNStoreEntity>> mutableLiveData4RequestEstimate = new MutableLiveData<>();

    public MutableLiveData<List<StoreNPredictionEntity>> getMutableLiveData(String requestId, String condition, String page) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        if(condition == null) {
            condition = "ISNULL(prediction_price) ASC, distance ASC";
        }

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllStoreNPrediction(cookie, requestId, null, condition);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
//                                return new SimpleDateFormat().parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
//                        mHomeSuriStatus.setNumOfMatchStore(String.valueOf(page.count));
//                        mBinding.setMySuri(mHomeSuriStatus);

                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<StoreNPredictionEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    storeNPredictionEntities = gson.fromJson(asJsonArray, itemType);
                    if (!storeNPredictionEntities.isEmpty()) {
                        Log.d(TAG, "Be Requested::" + storeNPredictionEntities.size());
                        mutableLiveData.setValue(storeNPredictionEntities);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllStoreNPrediction onFailure");
            }
        });

        return mutableLiveData;
    }

    public MutableLiveData<List<RequestNPredictionNStoreEntity>> getAllRquestEstimate(String userId) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = requestEstimateAPI.doPostGetAllRequestEstimateNPredictionNStore(cookie, userId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseRequestNPredictionNStore result = gson.fromJson(json, ResponseRequestNPredictionNStore.class);

//                   Gson gson = new Gson();
//                    Object paging = ((LinkedTreeMap) data).get("pageMap");
//                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = result.getPageMap(); //gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

//                    Object info = ((LinkedTreeMap) data).get("Info");
//                    Type itemType = new TypeToken<List<RequestNPredictionNStoreEntity>>() {
//                    }.getType();
//                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = result.getRequestEntities();//gson.fromJson(asJsonArray, itemType);
                    if (!item.isEmpty()) {
                        Log.d(TAG, "My Req count::" + item.size());
                        mutableLiveData4RequestEstimate.setValue(item);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllRequestEstimateNPredictionNStore onFailure");
            }
        });

        return mutableLiveData4RequestEstimate;
    }

}
