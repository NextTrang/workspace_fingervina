package com.snst.suripartner.lib.suri_history;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Command;
import com.snst.suripartner.lib.api.RequestWriteSuriHistory;
import com.snst.suripartner.lib.gallery_cmd.Gallery;
import com.snst.suripartner.lib.gallery_cmd.GalleryInvoker;
import com.snst.suripartner.lib.gallery_cmd.OpenGallery;

public class SuriHistoryEvent {

    Context context;

    public SuriHistoryEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onOpenGallery(View view) {
        Gallery gallery = new Gallery().setRequestCode(LbPop.LbCode.WRITE_SURI_HISTORY);
        Command open = new OpenGallery((AppCompatActivity) context, gallery);
        GalleryInvoker invoker = new GalleryInvoker(open, null);
        invoker.doOpen();
    }

    public void onWriteSuriHistory(SuriHistory suriHistory) {
        String[] inputs = new String[5];
        inputs[0] = suriHistory.getId();
        inputs[1] = suriHistory.getPredictEstId();
        inputs[2] = suriHistory.getImageUrl();
        inputs[3] = suriHistory.getTitle();
        inputs[4] = suriHistory.getContent();

        new RequestWriteSuriHistory().execute(context, inputs);
    }

    public void onTransferData(SuriHistory suriHistory) {
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.SURI_HISTORY, suriHistory);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }
}
