package com.snstfinger.suri.ui.shop;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.data.model.GeoCondition;
import com.snstfinger.suri.ui.tools.HorizonListVewDecorate;

import java.util.List;

/**
 * 등록된 모든 상점리스트 지도
 */
public class ShopListOnMapFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, View.OnFocusChangeListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnMarkerClickListener {
    private static final String TAG = ShopListOnMapFragment.class.getName();

    private static final String PARAM = "param";
    private static final int ZOOM_LEVEL = 12;
    private Location mSelectedPosition;
    private Marker mOldMarker;

    private OnShopListInteractionListener mListener;
    private RecyclerView mListview;
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    private ShopListViewModel mViewModel;
    private ShopListAdapter mAdapter;

    public ShopListOnMapFragment() {
    }

    @SuppressWarnings("unused")
    public static ShopListOnMapFragment newInstance(int option) {
        ShopListOnMapFragment fragment = new ShopListOnMapFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM, option);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            getArguments().getInt(PARAM);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_list_map, container, false);

        mListview = view.findViewById(R.id.listview_shopinfo);
        mViewModel = ViewModelProviders.of(getActivity()).get(ShopListViewModel.class);
        mAdapter = new ShopListAdapter(getContext(), mListener);
        mListview.setOnFocusChangeListener(this);
        mListview.setAdapter(mAdapter);

        mListview.addItemDecoration(new HorizonListVewDecorate(getContext(), 24, 6));

        mMapView = view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMapView.getMapAsync(this);

        // Get the button view
        View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 250);

        mViewModel.shopPagedList.observe(this, new Observer<PagedList<StoreEntity>>() {
            @Override
            public void onChanged(PagedList<StoreEntity> storeEntities) {
                mAdapter.submitList(storeEntities);

                showStoreOnMap(storeEntities);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShopListInteractionListener) {
            mListener = (OnShopListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShopListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_filter) {
            Log.d(TAG, "filter Clicked");
            String condition = "ISNULL(prediction_price) ASC, point_score DESC";
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;


        //런타임 퍼미션 처리
        // 1. 위치 퍼미션 체크
        if (((ShopListActivity) getActivity()).hasPermissions(ShopListActivity.PERMISSIONS_LOCATION)) {
            // 2. 이미 퍼미션을 가지고 있다면
            // 안드로이드 6.0 이하 버전은 런타임 퍼미션이 필요없기 때문에 이미 허용된 걸로 인식
            updateStoreList();

        } else {  //2. 퍼미션 요청을 허용한 적이 없다면 퍼미션 요청. 2가지 경우(3-1, 4-1)

            // 3-1. 사용자가 퍼미션 거부를 한 적이 있는 경우
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ShopListActivity.PERMISSIONS_LOCATION[0])) {

                // 3-2. 요청을 진행하기 전에 사용자가에게 퍼미션이 필요한 이유를 설명
                Snackbar.make(mListview, R.string.msg_permission_denied,
                        Snackbar.LENGTH_INDEFINITE).setAction(R.string.btn_ok, new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        // 3-3. 사용자게에 퍼미션 요청. 요청 결과는 onRequestPermissionResult
                        requestPermissions(ShopListActivity.PERMISSIONS_LOCATION, ShopListActivity.REQUEST_LOCATION);
                    }
                }).show();


            } else {
                // 4-1. 사용자가 퍼미션 거부를 한 적이 없는 경우에는 퍼미션 요청
                // 요청 결과는 onRequestPermissionResult
                requestPermissions(ShopListActivity.PERMISSIONS_LOCATION, ShopListActivity.REQUEST_LOCATION);
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void updateStoreList() {
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        mGoogleMap.setOnCameraIdleListener(this);
        mGoogleMap.setOnMarkerClickListener(this);

        Location loc = ((ShopListActivity) getActivity()).getLocation();
        mSelectedPosition = loc;

        doTaskGetStoresByLocation(loc.getLatitude(), loc.getLongitude(), 15);

        LatLng currLocation = new LatLng(loc.getLatitude(), loc.getLongitude());
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(currLocation));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));
    }


    private void doTaskGetStoresByLocation(double latitude, double longitude, int range) {
        mViewModel.setGeoCondition(new GeoCondition(latitude, longitude, range, null, null));
//        mViewModel.getShopPagedList(latitude, longitude, range).observe(this, new Observer<PagedList<StoreEntity>>() {
//            @Override
//            public void onChanged(PagedList<StoreEntity> storeEntities) {
//                mAdapter.submitList(storeEntities);
//            }
//        });

    }

    void showStoreOnMap(PagedList<StoreEntity> storeEntities) {
        if (mGoogleMap == null) return;

        mOldMarker = null;
        mGoogleMap.clear();

        List<StoreEntity> list = mAdapter.getStoreItems();

        if (list == null || list.isEmpty()) return;

        for (StoreEntity store : list) {
            String lat = store.latitude;
            String lon = store.longitude;

            LatLng location = new LatLng(Double.valueOf(lat), Double.valueOf(lon));

            MarkerOptions markerOptions = new MarkerOptions();

            markerOptions.position(location);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_normal_shop));
            markerOptions.title(store.storeName);
            markerOptions.snippet(store.address);

            mGoogleMap.addMarker(markerOptions).setTag(store);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Log.d(TAG, "view::" + v);
    }

    @Override
    public void onCameraIdle() {
        if (mGoogleMap == null) return;

        LatLng newCenter = mGoogleMap.getProjection().getVisibleRegion().latLngBounds.getCenter();
        // mGoogleMap.addMarker(new MarkerOptions().position(newCenter));

        Location candidateLocation = new Location(LocationManager.GPS_PROVIDER);
        candidateLocation.setLatitude(newCenter.latitude);
        candidateLocation.setLongitude(newCenter.longitude);

        if (mSelectedPosition.distanceTo(candidateLocation) > (7 * 1000)) {
            mSelectedPosition = candidateLocation;
            doTaskGetStoresByLocation(mSelectedPosition.getLatitude(), mSelectedPosition.getLongitude(), 15);

        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mOldMarker != null && mOldMarker.isVisible()) {
            mOldMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.icon_normal_shop));
        }

        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.icon_normal_shop_s));
        mOldMarker = marker;

        StoreEntity selected = (StoreEntity) marker.getTag();
        PagedList<StoreEntity> list = mAdapter.getStoreItems();
        for (int idx = 0; idx < list.size(); idx++) {
            if (selected.storeId == list.get(idx).storeId) {
                mListview.scrollToPosition(idx);
                break;
            }
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grandResults) {

        if (permsRequestCode == ShopListActivity.REQUEST_LOCATION) {
            if (ShopListActivity.verifyPermissions(grandResults)) {
                // 퍼미션을 허용했다면 위치 업데이트를 시작합니다.
                updateStoreList();

            } else {
                // 거부한 퍼미션이 있다면 앱을 사용할 수 없는 이유를 설명해주고 앱을 종료합니다.2 가지 경우가 있습니다.
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ShopListActivity.PERMISSIONS_LOCATION[0])
                        || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ShopListActivity.PERMISSIONS_LOCATION[1])) {

                    // 사용자가 거부만 선택한 경우에는 앱을 다시 실행하여 허용을 선택하면 앱을 사용할 수 있습니다.
                    Snackbar.make(mListview, R.string.msg_permission_denied,
                            Snackbar.LENGTH_INDEFINITE).setAction(R.string.btn_ok, new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            getActivity().finish();
                        }
                    }).show();

                } else {
                    // "다시 묻지 않음"을 사용자가 체크하고 거부를 선택한 경우에는 설정(앱 정보)에서 퍼미션을 허용해야 앱을 사용할 수 있습니다.
                    Snackbar.make(mListview, R.string.msg_permission_setting,
                            Snackbar.LENGTH_INDEFINITE).setAction(R.string.btn_ok, new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            getActivity().finish();
                        }
                    }).show();
                }
            }

        }
    }
}
