package com.snstfinger.suri.data.repository;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.SuriStoryEntity;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.SuriHistoryAPI;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuriStoryRepository {
    private static final String TAG = SuriStoryRepository.class.getName();

    private ArrayList<SuriStoryEntity> storyEntities = new ArrayList<>();
    private MutableLiveData<List<SuriStoryEntity>> mutableLiveData = new MutableLiveData<>();

    public MutableLiveData<List<SuriStoryEntity>> getMutableLiveData(String requestId) {
        SuriHistoryAPI storyAPI = ApiManager.getProvider(SuriHistoryAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storyAPI.doPostGetAllSuriHistoryByRequestEstimateId(cookie, requestId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<SuriStoryEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    storyEntities = gson.fromJson(asJsonArray, itemType);
                    if (!storyEntities.isEmpty()) {
                        Log.d(TAG, "StoryEntity size::" + storyEntities.size());
                        mutableLiveData.setValue(storyEntities);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriHistoryByRequestEstimateId onFailure");
            }
        });

        return mutableLiveData;
    }
}
