package com.snst.suripartner.lib.chat;

import android.util.Log;

import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Chat implements Serializable {
    private static final String TAG = "Chat";
    private String imageUrl;
    private String id;
    private String deviceName;
    private String message;
    private String day;
    private String userId;

    public Chat(ChatViewModel viewModel) {
        this.imageUrl = viewModel.imageUrl;
        this.id = viewModel.id;
        this.deviceName = viewModel.deviceName;
        this.message = viewModel.message;
        this.day = viewModel.day;
        this.userId = viewModel.userId;
    }

    public Chat(JSONObject jsInfo) {
        try {
            imageUrl = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.ESTIMATE_IMAGE_1);
            id = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            deviceName = jsInfo.getString(LbData.LbInfo.BRAND_NAME);
            message = jsInfo.getString(LbData.LbInfo.REPAIR_COMMENT);
            userId = jsInfo.getString(LbData.LbInfo.USER_ID);
        } catch (JSONException e) {
            Log.e(TAG, "Chat: " + e.getMessage());
        }
    }

    //Getter

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getMessage() {
        return message;
    }

    public String getDay() {
        return day;
    }

    public String getUserId() {
        return userId;
    }
}
