package com.snst.suripartner.lib.quotation;

public interface QuotationNavigator {
    void onItemClick(QuotationViewModel viewModel);
}
