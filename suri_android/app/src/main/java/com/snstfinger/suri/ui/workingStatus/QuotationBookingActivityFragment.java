package com.snstfinger.suri.ui.workingStatus;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.databinding.FragmentQuotationBookingBinding;
import com.snstfinger.suri.ui.shop.ShopLocationActivity;

/**
 * 견적제안한 상점의 부킹상세화면
 */
public class QuotationBookingActivityFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {
    public static final String TAG = QuotationBookingActivityFragment.class.getName();


    private RequestNPredictionNStoreEntity mQuotationShop;
    private FragmentQuotationBookingBinding mBinding;

    //    private MapView mMapView;
    private GoogleMap mMap;

    public QuotationBookingActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        mQuotationShop = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");

        Log.d(TAG, "Request Estimate::" + mQuotationShop.requestEstimateId);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quotation_booking, container, false);
        mBinding.setShopInfo(mQuotationShop);
        mBinding.shopLocation.onCreate(savedInstanceState);
        mBinding.shopLocation.getMapAsync(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(this);

        if (!mQuotationShop.latitude.isEmpty() && !mQuotationShop.longitude.isEmpty()) {
            LatLng location = new LatLng(Double.valueOf(mQuotationShop.latitude), Double.valueOf(mQuotationShop.longitude));
            mMap.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_normal_shop)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Intent shopMap = new Intent(getActivity(), ShopLocationActivity.class);
        shopMap.putExtra("name", mQuotationShop.storeName);
        shopMap.putExtra("location", latLng);
        startActivity(shopMap);
    }
}
