package com.snst.suripartner.lib.gallery_cmd;

import com.snst.suripartner.lib.Command;

public class GalleryInvoker {

    private Command open;
    private Command close;

    public GalleryInvoker(Command open, Command close) {
        this.open = open;
        this.close = close;
    }

    public void doOpen() {
        open.execute();
    }

    public void doClose() {
        close.execute();
    }
}
