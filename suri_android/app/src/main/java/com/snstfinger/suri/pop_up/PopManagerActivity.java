package com.snstfinger.suri.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.snstfinger.suri.R;
import com.snstfinger.suri.labels.LbPop;

public class PopManagerActivity extends AppCompatActivity {

    private int popCode = -1;
    private Intent mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_manager);

        mData = getIntent();
        popCode = mData.getIntExtra(LbPop.LbKey.POP_CODE, -1);
        startPopup(popCode);
    }

    public void startPopup(int popCode) {
        switch (popCode) {
            case LbPop.LbCode.FILTER_SURI_CASE:
                Intent iFilterSuriCase = new Intent(this, PopFilterSuriCaseActivity.class);
                startActivityForResult(iFilterSuriCase, popCode);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                default:
                    setResult(RESULT_OK, data);
            }
        }

        finish();
    }
}