package com.snst.suripartner.lib.suri;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.api.RequestGetSuriHistory;
import com.snst.suripartner.lib.api.RequestStartSuri;
import com.snst.suripartner.lib.api.RequestUpdateRepair;
import com.snst.suripartner.pop_up.PopManagerActivity;

import java.util.ArrayList;
import java.util.List;

public class SuriEvent {

    Context context;

    public SuriEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onExpandCustomerInfos(SuriViewModel viewModel) {
        if (viewModel.visibleCustomerInfos == View.VISIBLE) {
            viewModel.setVisibleCustomerInfos(View.GONE);
        } else {
            viewModel.setVisibleCustomerInfos(View.VISIBLE);
        }
    }

    public void onPopupSendInvoice(Suri suri) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SEND_INVOICE);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.SEND_INVOICE);
    }

    public void onStartSuri(Suri suri) {
        if (suri.getStateMask().equals(LbState.LbMask.START_SURI)) {
            new RequestStartSuri().execute(context, suri.getPredictEstId());
        }

        showSuriDetail(suri);
    }

    public void onPopWriteSuriHistory(Suri suri) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.WRITE_SURI_HISTORY);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.WRITE_SURI_HISTORY);
    }

    public void onPopCompleteSuri(Suri suri) {
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.REQUEST_COMPLETE_SURI);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.REQUEST_COMPLETE_SURI);
    }

    public void onUpdateRepair(Suri suri) {
        String[] inputs = new String[2];
        inputs[0] = suri.getPredictEstId();
        inputs[1] = String.valueOf(suri.getProgressRate());

        new RequestUpdateRepair().execute(context, inputs);
    }

    public void onTransferData(Suri suri) {
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.SURI, suri);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }

    public void onPreviousSuri(Suri suri, String state) {
        List<SuriViewModel> data = filterState(state);

        int index = -1;
        for (SuriViewModel viewModel : data) {
            if (suri.getId().equals(viewModel.id)) {
                index = data.indexOf(viewModel);
            }
        }

        int preIndex = index < 1 ? 0 : (index - 1);

        if (preIndex != index) {
            suri = new Suri(data.get(preIndex));

            ((AppCompatActivity) context).finish();

            switch (state) {
                case LbState.LbMask.REPAIR:
                    showSuriDetail(suri);
                    break;
                case LbState.WAITING:
                    showSuriWaitingDetail(suri);
                    break;
                case LbState.COMPLETED:
                    showSuriCompletedDetail(suri);
                    break;
            }
        }
    }

    public void onNextSuri(Suri suri, String state) {
        List<SuriViewModel> data = filterState(state);

        int index = -1;
        for (SuriViewModel viewModel : data) {
            if (suri.getId().equals(viewModel.id)) {
                index = data.indexOf(viewModel);
            }
        }

        int nextIndex = index < (data.size() - 1) ? (index + 1) : (data.size() - 1);

        if (nextIndex != index) {
            suri = new Suri(data.get(nextIndex));

            ((AppCompatActivity) context).finish();

            switch (state) {
                case LbState.LbMask.REPAIR:
                    showSuriDetail(suri);
                    break;
                case LbState.WAITING:
                    showSuriWaitingDetail(suri);
                    break;
                case LbState.COMPLETED:
                    showSuriCompletedDetail(suri);
                    break;
            }
        }
    }

    private List<SuriViewModel> filterState(String state) {
        List<SuriViewModel> result = new ArrayList<>();

        for (SuriViewModel viewModel : Sharing.Instance().getSuriViewModels()) {
            if (viewModel.stateMask.equals(state)) {
                result.add(viewModel);
            }
        }

        return result;
    }

    private void showSuriDetail(Suri suri) {
        //request suri history list
        new RequestGetSuriHistory().execute(context, suri.getId());

        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SURI_ONGOING_DETAIL);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.SURI_ONGOING_DETAIL);
    }

    private void showSuriWaitingDetail(Suri suri) {
        //request suri history list
        new RequestGetSuriHistory().execute(context, suri.getId());

        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SURI_WAITING_DETAIL);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.SURI_WAITING_DETAIL);
    }

    private void showSuriCompletedDetail(Suri suri) {
        //request suri history list
        new RequestGetSuriHistory().execute(context, suri.getId());

        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SURI_COMPLETE_DETAIL);
        intent.putExtra(LbPop.LbKey.SURI, suri);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.SURI_COMPLETE_DETAIL);
    }
}
