package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseUser {
    @SerializedName("userInfo")
    @Expose
    private UserEntity user;

    public UserEntity getUser() {
        return user;
    }
}
