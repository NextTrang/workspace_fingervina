package com.snstfinger.suri.pop_up;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.snstfinger.suri.R;
import com.snstfinger.suri.lib.Brand;
import com.snstfinger.suri.network.RequestBrandsByObjectId;
import com.snstfinger.suri.network.RequestModelsByBrandId;

import java.util.ArrayList;
import java.util.List;

public class PopFilterSuriCaseActivity extends AppCompatActivity {

    private Spinner brandDropList;
    private Spinner modelDropList;
    private Spinner objectDropList;
    private Button btnOK;
    private ImageButton btnClose;

    private List<Brand> brands = new ArrayList<>();
    private List<String> models = new ArrayList<>();

    private ArrayAdapter<String> brandAdapter;
    private ArrayAdapter<String> modelAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_filter_suri_case);

        setupPopup();

        binding();
        setEvents();
    }

    private void binding() {
        brandDropList = findViewById(R.id.spnFilterBrand);
        modelDropList = findViewById(R.id.spnFilterModel);
        objectDropList = findViewById(R.id.spnFilterObject);
        btnOK = findViewById(R.id.btnOk);
        btnClose = findViewById(R.id.ibtnClose);
    }

    private void setEvents() {
        objectDropList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String objectId = "";
                if (position != 0) {
                    objectId = String.valueOf(position);
                    new RequestBrandsByObjectId().execute(PopFilterSuriCaseActivity.this, objectId);
                } else {
                    updateBrand(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        brandDropList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //get brand id
                String brandId = "";
                if (position != 0) {
                    brandId = brands.get(position - 1).getId();
                    //request Api
                    new RequestModelsByBrandId().execute(PopFilterSuriCaseActivity.this, brandId);
                }else {
                    updateModel(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        modelDropList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = getIntent();
                data.putExtra("object_id",objectDropList.getSelectedItemPosition());
                data.putExtra("brand_name", brandDropList.getSelectedItem().toString());
                data.putExtra("model_name", modelDropList.getSelectedItem().toString());
                setResult(RESULT_OK, data);
                finish();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupPopup() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .8));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 0;
        getWindow().setAttributes(params);
    }

    public void updateBrand(List<Brand> brands) {
        this.brands = brands;

        List<String> items = new ArrayList<>();

        if (this.brands == null) {
            items.add("");
        } else {
            items.add("All");
            for (Brand brand : this.brands) {
                items.add(brand.getName());
            }
        }

        brandAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        brandDropList.setAdapter(brandAdapter);
        brandAdapter.notifyDataSetChanged();
    }

    public void updateModel(List<String> models) {
        if(models == null){
            this.models.clear();
            this.models.add("");
        }else {
            this.models = models;
        }

        modelAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, this.models);
        modelDropList.setAdapter(modelAdapter);
        modelAdapter.notifyDataSetChanged();
    }
}