package com.snstfinger.suri;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.LoginProvider;
import com.snstfinger.suri.network.NotificationAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity implements LoginProvider.OnResultLoginListener, LoginProvider.OnResultLogoutListener {
    private static final String TAG = BaseActivity.class.getName();

    public static final int REQUEST_CAMERA = 0;
    public static final int REQUEST_STORAGE = 1;
    public static final int REQUEST_LOCATION = 2;
    public static final int REQUEST_PHONE = 3;
    public static final int REQUEST_ALL = 4;

    public static String[] PERMISSIONS_CAMERA = {Manifest.permission.CAMERA};
    public static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static String[] PERMISSIONS_PHONE = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};

    public static String[] PERMISSIONS_ALL = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "CAMERA permission granted");
                } else {
                    Log.d(TAG, "CAMERA_PERMISSION NOT Granted");
                }

                break;
            }

            case REQUEST_STORAGE: {
                if (verifyPermissions(grantResults)) {
                    Log.d(TAG, "All required permission granted");
                } else {
                    Log.d(TAG, "STORAGE_PERMISSION NOT granted");
                }
                break;
            }

            case REQUEST_LOCATION: {
                if (verifyPermissions(grantResults)) {
                    Log.d(TAG, "All required permission granted");
                } else {
                    Log.d(TAG, "LOCATION_PERMISSION NOT granted");
                }
                break;
            }

            case REQUEST_PHONE: {
                if (verifyPermissions(grantResults)) {
                    Log.d(TAG, "All required permission granted");
                } else {
                    Log.d(TAG, "PHONE_PERMISSION NOT granted");
                }
                break;
            }

            case REQUEST_ALL: {
                if (verifyPermissions(grantResults)) {
                    Log.d(TAG, "All required permission granted");
                } else {
                    Log.d(TAG, "ALL permission NOT granted");
                }
                break;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    public static boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if (grantResults.length < 1) {
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public boolean hasPermissions(String... permissions) {
        int sdkVersion = Build.VERSION.SDK_INT;

        if (sdkVersion >= Build.VERSION_CODES.M) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "NOT Granted PERMISSION::" + permission);
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Request all required permissions.
     */
    public void checkPermissionAll() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            for (String permission : PERMISSIONS_ALL) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//                        // 거부했을때 권한사용 설명
//                        Log.d(TAG, "NOT Granted::" + permission);
//
//                    } else {
                    ActivityCompat.requestPermissions(this, PERMISSIONS_ALL, REQUEST_ALL);
                    Log.d(TAG, "REQUEST PERMISSION::" + permission);
//                    }

                } else {
                    Log.d(TAG, "Granted::" + permission);

                }
            }
        }
    }

    public void checkPermissionCamera() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            for (String permission : PERMISSIONS_CAMERA) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
//                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//                        // 거부했을때 권한사용 설명
//                        Log.d(TAG, "NOT Granted::"+permission);
//                    } else {
                    ActivityCompat.requestPermissions(this, PERMISSIONS_CAMERA, REQUEST_CAMERA);
                    Log.d(TAG, "REQUEST PERMISSION::" + permission);
//                    }

                } else {
                    Log.d(TAG, "Granted::" + permission);

                }
            }
        }
    }

    public void checkPermissionStorage() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            for (String permission : PERMISSIONS_STORAGE) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        // 거부했을때 권한사용 설명
                        Log.d(TAG, "NOT Granted::" + permission);
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_STORAGE);
                        Log.d(TAG, "REQUEST PERMISSION::" + permission);
                    }

                } else {
                    Log.d(TAG, "Granted::" + permission);

                }
            }
        }
    }

    public void checkPermissionLocation() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            for (String permission : PERMISSIONS_LOCATION) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        // 거부했을때 권한사용 설명
                        Log.d(TAG, "NOT Granted::" + permission);
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
                        Log.d(TAG, "REQUEST PERMISSION::" + permission);
                    }

                } else {
                    Log.d(TAG, "Granted::" + permission);

                }
            }
        }
    }

    public void checkPermissionPhone() {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            for (String permission : PERMISSIONS_PHONE) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        // 거부했을때 권한사용 설명
                        Log.d(TAG, "NOT Granted::" + permission);
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS_PHONE, REQUEST_PHONE);
                        Log.d(TAG, "REQUEST PERMISSION::" + permission);
                    }

                } else {
                    Log.d(TAG, "Granted::" + permission);

                }
            }
        }
    }

    public void sendNoticeByUserId(String userId, String title) {
        NotificationAPI noticeAPI = ApiManager.getProvider(NotificationAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");

        String body = "TEST from Device";

        Call<CommonResponse> call = noticeAPI.doPostSendNotificationByUserId(cookie, userId, title, body);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Log.d(TAG, res.toString());
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostRequestEst onFailure");
            }
        });
    }

    @Override
    public void onResult(int result, LoggedInUser user) {

    }

    @Override
    public void onResult(int result) {

    }
}
