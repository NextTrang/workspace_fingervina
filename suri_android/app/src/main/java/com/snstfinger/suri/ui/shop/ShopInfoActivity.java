package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.ui.dialog.ShopBookingDialogFragment;

public class ShopInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = ShopInfoActivity.class.getName();

    public static final int REQ_BOOKING_CODE = 100;
    public final int REQ_QUOTATION_CODE = 110;

    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;
    private FragmentManager mFragmentManager;

    private Button mDoAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_info);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new ShopInfoActivityFragment()).commit();

        mDoAction = findViewById(R.id.btn_action);
        mDoAction.setOnClickListener(this);

        actionBar.setTitle(R.string.title_activity_shop_info);
        if (mRequestEstimate != null) {
            String status = mRequestEstimate.status;

            switch (status) {
                case "BOOKING":
                case "BOOKING_CONFIRM": {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    break;
                }

                case "REQUEST": {
                    isAvailableBooking();
                    break;
                }

                case "SENDING_PAYMENT":
                case "REPAIR_STANDBY":
                case "REPAIR":
                case "SENDING_PAYMENT_UPDATE":
                case "COMPLETE":
                case "REVIEW":
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_action: {
                // desiredDate YN
                // no : warning popup
                // yes : go booking
                if (mShopInfo.desiredReservationDateYN.equalsIgnoreCase("y")) {
                    showBookingForm();

                } else {
                    showWarningDialogForBooking();
                }

                break;
            }
        }
    }

    void isAvailableBooking() {
        // no quotation
        if (TextUtils.isEmpty(mShopInfo.predictionEstimateId)) {
            mDoAction.setVisibility(View.GONE);

        } else {
            mDoAction.setText(R.string.btn_shop_book);
            mDoAction.setVisibility(View.VISIBLE);

        }

    }

    void showBookingForm() {
        Intent bookingForm = new Intent(this, ShopBookingActivity.class);
        bookingForm.putExtra("request_estimate", mRequestEstimate);
        bookingForm.putExtra("shop_info", mShopInfo);
        startActivityForResult(bookingForm, REQ_BOOKING_CODE);
    }

    void showWarningDialogForBooking() {
        ShopBookingDialogFragment dialog = new ShopBookingDialogFragment();
        Bundle args = new Bundle();

        args.putSerializable("request_estimate", mRequestEstimate);
        args.putSerializable("shop_info", mShopInfo);

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "booking");
    }

    public void showShopProfile() {
        ShopInfoProfileFragment fragment = new ShopInfoProfileFragment();
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, fragment).addToBackStack(null).commit();
    }

    public void showAllReviews() {
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new ShopInfoAllReviewFragment()).addToBackStack(null).commit();
    }

    public void showAllCase() {
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, ShopInfoAllCaseFragment.newInstance(mShopInfo)).addToBackStack(null).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_BOOKING_CODE) {
            if(resultCode == RESULT_OK) {
                mDoAction.setVisibility(View.GONE);
            } else {
                mDoAction.setText(R.string.btn_shop_book);
                mDoAction.setVisibility(View.VISIBLE);
            }
        }
    }
}
