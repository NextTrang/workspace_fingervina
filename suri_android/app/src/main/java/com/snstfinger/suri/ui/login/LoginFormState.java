package com.snstfinger.suri.ui.login;

import androidx.annotation.Nullable;

/**
 * Data validation state of the login form.
 */
class LoginFormState {
    @Nullable
    private Integer usernameError;
    @Nullable
    private Integer userPhoneError;
    private boolean isDataValid;

    LoginFormState(@Nullable Integer usernameError, @Nullable Integer userPhoneError) {
        this.usernameError = usernameError;
        this.userPhoneError = userPhoneError;
        this.isDataValid = false;
    }

    LoginFormState(boolean isDataValid) {
        this.usernameError = null;
        this.userPhoneError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getUsernameError() {
        return usernameError;
    }

    @Nullable
    Integer getUserPhoneError() {
        return userPhoneError;
    }

    boolean isDataValid() {
        return isDataValid;
    }
}
