package com.snstfinger.suri.util;

import android.content.Context;

public class UserInfoSharedPreferences extends SharedPrefsManager {
    public final String KEY_USER_ID = "key_user_id";
    public final String KEY_USER_PW = "key_user_pw";
    public final String KEY_USER_NAME = "key_user_name";
    public final String KEY_USER_EMAIL = "key_user_email";
    public final String KEY_USER_PHONE= "key_mobile_no";
    public final String KEY_USER_ADDR = "key_user_addr";
    public final String KEY_LOGIN_TYPE = "key_login_type";

    // sns kind
    public final String KEY_ACCOUNT_TYPE = "key_account_type";
    public final String KEY_PROFILE_URL = "key_profile_url";

    private static UserInfoSharedPreferences instance;

    private UserInfoSharedPreferences(Context paramContext, int mode) {
        super(paramContext, "user_info", mode);
    }

    public static UserInfoSharedPreferences getInstance(Context context) {
        if(instance == null) {
            instance = new UserInfoSharedPreferences(context, Context.MODE_PRIVATE);
        }

        return instance;
    }

    public String getUserId() {
        return get(KEY_USER_ID, "");
    }

    public String getUserPw() {
        return get(KEY_USER_PW, "");
    }

    public String getUserName() {
        return get(KEY_USER_NAME, "");
    }

    public String getUserEmail() {
        return get(KEY_USER_EMAIL, "");
    }

    public String getUserAddr() {
        return get(KEY_USER_ADDR, "");
    }

    public String getUserPhone() {
        return get(KEY_USER_PHONE, "");
    }

    public String getLoginType() {
        return get(KEY_LOGIN_TYPE, "");
    }

    public void putUserId(String paramString) {
        put(KEY_USER_ID, paramString);
    }

    public void putUserPw(String paramString) {
        put(KEY_USER_PW, paramString);
    }

    public void putUserName(String paramString) {
        put(KEY_USER_NAME, paramString);
    }

    public void putUserEmail(String paramString) {
        put(KEY_USER_EMAIL, paramString);
    }

    public void putUserAddr(String paramString) {
        put(KEY_USER_ADDR, paramString);
    }

    public void putUserPhone(String paramString) {
        put(KEY_USER_PHONE, paramString);
    }

    public void putLoginType(String paramString) {
        put(KEY_LOGIN_TYPE, paramString);
    }

    public String getAccountType() {
        return get(KEY_ACCOUNT_TYPE,"");
    }

    // sns kind or email(local)
    public void putAccountType(String paramString) {
        put(KEY_ACCOUNT_TYPE, paramString);
    }

    public String getProfileUrl() {
        return get(KEY_PROFILE_URL,"");
    }
    public void putProfileUrl(String paramString) {
        put(KEY_PROFILE_URL, paramString);
    }
}
