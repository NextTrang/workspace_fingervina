package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.internal.LinkedTreeMap;
import com.haibin.calendarview.CalendarLayout;
import com.haibin.calendarview.CalendarView;
import com.snstfinger.suri.BaseActivity;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.ui.tools.TimeItemRecycleViewAdapter;
import com.snstfinger.suri.util.Utils;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopBookingActivity extends BaseActivity implements View.OnClickListener, TimeItemRecycleViewAdapter.OnSelectTimeInteractionListener, CalendarView.OnCalendarSelectListener {
    public static final String TAG = ShopBookingActivity.class.getName();

    private String mRequestEstimateId;
    private String mPredictionEstimateId;
    private String mBookingDate;

    private Calendar mDesireDate;

    private TextInputEditText mBookerName;
    private TextInputEditText mBookerPhoneNumber;
    private com.haibin.calendarview.CalendarView mCalendarView;
    private TextView mTitleYearMonth;
    private RecyclerView mListViewTimes;
    private Button mBtnBooking;
    private Button mBtnCancel;

    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_booking);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        mBookerName = findViewById(R.id.input_contact_name);
        mBookerPhoneNumber = findViewById(R.id.input_contact_phone);
        mCalendarView = findViewById(R.id.calendarView);
        mTitleYearMonth = findViewById(R.id.title_year_month);
        mListViewTimes = findViewById(R.id.listView_times);
        mBtnBooking = findViewById(R.id.btn_book);
        mBtnCancel = findViewById(R.id.btn_cancel);


        mBtnBooking.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);

        // set calendar
        setupDateInCalendar();
        // timetable
        setupTimeInList();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            case R.id.action_close: {

                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_book) {
            sendBookingInfo();

        } else if (v.getId() == R.id.btn_now) {
            setCurrentCalendar();

        } else {
            finish();

        }
    }

    @Override
    public void onCalendarOutOfRange(com.haibin.calendarview.Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(com.haibin.calendarview.Calendar calendar, boolean isClick) {
        mTitleYearMonth.setText(Utils.getMonthName(calendar.getMonth()) + " " + calendar.getYear());

        if (isClick) {
            mDesireDate.set(java.util.Calendar.YEAR, calendar.getYear());
            mDesireDate.set(java.util.Calendar.MONTH, calendar.getMonth() - 1);
            mDesireDate.set(java.util.Calendar.DAY_OF_MONTH, calendar.getDay());

//            String dateTime = Utils.getDateTime(mDesireDate.getTime());
        }
    }

    @Override
    public void onSelectTimeInteraction(String item) {
        Log.d(TAG, "clicked::" + item);
        if (!TextUtils.isEmpty(item)) {
            String time[] = item.split(":");
            mDesireDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
            mDesireDate.set(java.util.Calendar.MINUTE, Integer.valueOf(time[1]));
            mDesireDate.set(java.util.Calendar.SECOND, 0);

//            String dateTime = Utils.getDateTime(mDesireDate.getTime());
        }
    }

    private void setupDateInCalendar() {
        String desiredDate = mRequestEstimate.getDesiredReservationDateFormatted();
        if (TextUtils.isEmpty(desiredDate)) {
            // set current dateTime
            long selectedDate = mCalendarView.getSelectedCalendar().getTimeInMillis();
            mDesireDate = java.util.Calendar.getInstance();
            mDesireDate.setTimeInMillis(selectedDate);

            Log.d(TAG, "today::" + mDesireDate);
            Log.d(TAG, "year::" + mDesireDate.get(java.util.Calendar.YEAR));
            Log.d(TAG, "month::" + mDesireDate.get(java.util.Calendar.MONTH));
            Log.d(TAG, "day::" + mDesireDate.get(java.util.Calendar.DATE));

        } else {
            // desired dateTime
            Date dateTime = new Date();
            try {
                dateTime = new SimpleDateFormat(Utils.DATE_FORMAT).parse(desiredDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mDesireDate = java.util.Calendar.getInstance();
            mDesireDate.setTime(dateTime);
            Log.d(TAG, "today::" + mDesireDate);
            Log.d(TAG, "year::" + mDesireDate.get(java.util.Calendar.YEAR));
            Log.d(TAG, "month::" + mDesireDate.get(java.util.Calendar.MONTH));
            Log.d(TAG, "day::" + mDesireDate.get(java.util.Calendar.DATE));

            mCalendarView.scrollToCalendar(mDesireDate.get(java.util.Calendar.YEAR), mDesireDate.get(java.util.Calendar.MONTH) + 1, mDesireDate.get(java.util.Calendar.DATE));
        }

        mCalendarView.setOnCalendarSelectListener(this);
        mTitleYearMonth.setText(Utils.getMonthName(mDesireDate.get(java.util.Calendar.MONTH) + 1) + " " + mDesireDate.get(java.util.Calendar.YEAR));
    }

    private void setupTimeInList() {
//        if (mShopInfo.isDesiredDate()) {
//            LinearLayoutManager disabler = new LinearLayoutManager(getApplicationContext()) {
//                @Override
//                public boolean canScrollHorizontally() {
//                    return false;
//                }
//
//                @Override
//                public boolean canScrollVertically() {
//                    return false;
//                }
//            };
//            disabler.setOrientation(RecyclerView.HORIZONTAL);
//            mListViewTimes.setLayoutManager(disabler);
//        }

        ArrayList<String> mItems = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_table)));
        TimeItemRecycleViewAdapter mAdapter = new TimeItemRecycleViewAdapter(getApplication(), mItems, this);
        mListViewTimes.setAdapter(mAdapter);
        setTimeTable(mDesireDate);
    }

    private void setCurrentCalendar() {
        mDesireDate = java.util.Calendar.getInstance();
        mCalendarView.scrollToCurrent(true);
        setTimeTable(mDesireDate);
    }

    private void setTimeTable(java.util.Calendar dateTime) {
        TimeItemRecycleViewAdapter adapter = (TimeItemRecycleViewAdapter) mListViewTimes.getAdapter();
        int position = adapter.getPositionFromDate(dateTime);
        adapter.setSelectTime(position);
        mListViewTimes.scrollToPosition(position);
    }

    private int isValidateForm() {
        String reqEstID = mRequestEstimate.requestEstimateId;
        String preEstID = mShopInfo.predictionEstimateId;
        String bookerName = mBookerName.getText().toString();
        String bookerPhone = mBookerPhoneNumber.getText().toString();
        String bookingDate = Utils.getDateTime(mDesireDate.getTime());

        if (TextUtils.isEmpty(reqEstID) || TextUtils.isEmpty(preEstID)) {
            return R.string.error_no_form_id;
        }

        if (TextUtils.isEmpty(bookerName)) {
            return R.string.error_no_name;
        }

        if (TextUtils.isEmpty(bookerPhone)) {
            return R.string.error_no_number;
        }

        if (TextUtils.isEmpty(bookingDate)) {
            return R.string.error_no_desired_date;
        }

        return 0;
    }

    private void sendBookingInfo() {
        int errorCode = isValidateForm();
        if (errorCode != 0) {
            Snackbar.make(mBtnBooking, errorCode, Snackbar.LENGTH_LONG).setAction(android.R.string.ok, v -> {
                // nothing
            }).show();

            return;
        }

        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");

        String reqEstID = mRequestEstimate.requestEstimateId;
        String preEstID = mShopInfo.predictionEstimateId;
        String bookerName = mBookerName.getText().toString();
        String bookerPhone = mBookerPhoneNumber.getText().toString();
        String bookingDate = Utils.getDateTime(mDesireDate.getTime());
        String utcDate = Utils.localToUTC(bookingDate);

        Call<CommonResponse> call = requestEstimateAPI.doPostBookingRequestEstimate(cookie, reqEstID, preEstID, utcDate, bookerName, bookerPhone);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
//                    String requestId = ((LinkedTreeMap) data).get("reqeust_estimate_id").toString();
                    Log.d(TAG, res.toString());
                    if (!TextUtils.isEmpty(mShopInfo.storeManagerId)) {
                        sendNoticeByUserId(mShopInfo.storeManagerId, "BOOKING");
                    }
                    Snackbar.make(mBtnBooking, "BOOKING SUCCESS", Snackbar.LENGTH_LONG).show();
                    setResult(RESULT_OK);

                } else {
                    Log.e(TAG, response.toString());
                    Snackbar.make(mBtnBooking, "BOOKING FAIL", Snackbar.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED);
                }
                finish();
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostRequestEst onFailure");

                Snackbar.make(mBtnBooking, "BOOKING FAIL", Snackbar.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}
