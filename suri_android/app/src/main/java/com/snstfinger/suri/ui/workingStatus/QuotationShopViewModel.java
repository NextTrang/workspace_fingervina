package com.snstfinger.suri.ui.workingStatus;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.snstfinger.suri.data.datasource.QuotationShopListDataSource;
import com.snstfinger.suri.data.datasource.QuotationShopListDataSourceFactory;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;

public class QuotationShopViewModel extends AndroidViewModel {
    private LiveData<PagedList<StoreNPredictionEntity>> quotationShopPagedList;
    private LiveData<PageKeyedDataSource<Integer, StoreNPredictionEntity>> liveDataSource;

    public QuotationShopViewModel(@NonNull Application application) {
        super(application);
    }

    public  LiveData<PagedList<StoreNPredictionEntity>> getQuotationShopPagedList(String requestId, String condition) {
        QuotationShopListDataSourceFactory factory = new QuotationShopListDataSourceFactory(requestId, condition);
        liveDataSource = factory.getQuotationShopLiveData();
        PagedList.Config pagedListConfig = new PagedList.Config.Builder().setEnablePlaceholders(true).setInitialLoadSizeHint(QuotationShopListDataSource.PAGE_SIZE).setPageSize(QuotationShopListDataSource.PAGE_SIZE).build();

        quotationShopPagedList = new LivePagedListBuilder(factory, pagedListConfig).build();

        return quotationShopPagedList;
    }

}
