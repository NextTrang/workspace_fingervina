package com.snstfinger.suri.data.datasource;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.data.entity.ResponseReviews;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.ReviewAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewListDataSource extends PageKeyedDataSource<Integer, ReviewEntity> {
    public static final String TAG = ReviewListDataSource.class.getName();

    public static final int PAGE_SIZE = 10;
    private static final int FIRST_PAGE = 1;

    private String storeId;

    public ReviewListDataSource(String storeId) {
        this.storeId = storeId;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        if(storeId == null) {
            initGetReviewRecently(callback);
        } else {
            initGetReviewByStoreId(callback);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if(storeId == null) {
            loadPrePageGetReviewRecently(params, callback);
        } else {
            loadPrePageGetReviewByStoreId(params, callback);
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if(storeId == null) {
            loadNextPageGetReviewRecently(params, callback);
        } else {
            loadNextPageGetReviewByStoreId(params, callback);
        }
    }

    private void initGetReviewRecently(LoadInitialCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        Call<CommonResponse> call = reivewAPI.doPostGetAllRecentReviews(String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    private void loadPrePageGetReviewRecently(LoadParams params, LoadCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        Call<CommonResponse> call = reivewAPI.doPostGetAllRecentReviews(String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    private void loadNextPageGetReviewRecently(LoadParams params, LoadCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        Call<CommonResponse> call = reivewAPI.doPostGetAllRecentReviews(String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    private void initGetReviewByStoreId(LoadInitialCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetReviewByStoreId(cookie, storeId, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    private void loadPrePageGetReviewByStoreId(LoadParams params, LoadCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetReviewByStoreId(cookie, storeId, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    callback.onResult(item, page.prePage);


                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    private void loadNextPageGetReviewByStoreId(LoadParams params, LoadCallback callback) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetReviewByStoreId(cookie, storeId, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getReviewEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }
}
