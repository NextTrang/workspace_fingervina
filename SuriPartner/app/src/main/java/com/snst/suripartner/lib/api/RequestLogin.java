package com.snst.suripartner.lib.api;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.LoginActivity;
import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.User;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.store_info.StoreInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestLogin implements ApiRequest {

    private static final String TAG = "RequestLogin";

    public void execute(final Context context, String... inputs) {
        if (context.getClass() != LoginActivity.class) {
            Log.e(TAG, "execute: This request only should be called in LoginActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().userLogin(inputs[0], inputs[1], inputs[2], inputs[3]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);

                    if (msg.equals("OK")) {

                        Sharing.Instance().reset();

                        JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                        JSONObject jsUserInfo = jsData.getJSONObject(LbData.LbUserInfo.USER_INFO);

                        User user = new User(jsUserInfo);
                        //store new user info in Sharing
                        Sharing.Instance().setUser(context, user);
                        String cookie = response.headers().value(0).split(";")[0];
                        Sharing.Instance().setCookie(context, cookie);

                        requestStoreInfo(context, Sharing.Instance().getUser().getStoreId());
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }

    private void requestStoreInfo(final Context context, String... inputs) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getStoreInfo(Sharing.Instance().getCookie(), inputs[0]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                        JSONObject jsInfo = jsData.getJSONObject(LbData.LbInfo.INFO);
                        Sharing.Instance().setStoreInfo(new StoreInfo(jsInfo));

                        requestCountSuri(context,Sharing.Instance().getUser().getStoreId());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestCountSuri(final Context context, String... inputs) {
        {
            Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getCountSuri(Sharing.Instance().getCookie(), inputs[0]);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.getString(LbData.MSG);
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        if (msg.equals("OK")) {
                            JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                            JSONObject jsInfo = jsData.getJSONObject(LbData.LbInfo.INFO);
                            int todo = jsInfo.getInt(LbData.LbInfo.SURI_TODO);
                            int complete = jsInfo.getInt(LbData.LbInfo.SURI_COMPLETE);

                            Sharing.Instance().getStoreInfo()
                                    .setSuriTodo(String.valueOf(todo))
                                    .setSuriComplete(String.valueOf(complete));

                            Intent i = new Intent(context, MainActivity.class);
                            context.startActivity(i);
                        }

                        new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
