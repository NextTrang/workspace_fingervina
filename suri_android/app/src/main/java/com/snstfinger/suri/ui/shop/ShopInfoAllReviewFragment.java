package com.snstfinger.suri.ui.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.databinding.FragmentShopInfoReviewBinding;

/**
 * 업체 리뷰정보를 보여줌
 */
public class ShopInfoAllReviewFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = ShopInfoAllReviewFragment.class.getName();

    private ShopInfo mShopInfo;
    private ReviewViewModel mReivewViewModel;

    private FragmentShopInfoReviewBinding mBinding;
    private PagedReviewAdapter mAdapter;

    public ShopInfoAllReviewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        RequestNPredictionNStoreEntity mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");
        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        mReivewViewModel = ViewModelProviders.of(this).get(ReviewViewModel.class);

        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_shop_info_review, container, false);
        mBinding.setShopInfo(mShopInfo);
        mAdapter = new PagedReviewAdapter(null, false);
        mBinding.listReviews.setAdapter(mAdapter);

        getAllReviewByStoreId(String.valueOf(mShopInfo.storeId));

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    private void getAllReviewByStoreId(String storeId) {
//        mReivewViewModel.getAllReview(storeId).observe(this, new Observer<List<ReviewEntity>>() {
//            @Override
//            public void onChanged(List<ReviewEntity> reviewEntities) {
//                mAdapter.setItemList(reviewEntities);
//            }
//        });
        mReivewViewModel.getReviewPagedList(storeId).observe(this, new Observer<PagedList<ReviewEntity>>() {
            @Override
            public void onChanged(PagedList<ReviewEntity> reviewEntities) {
                mAdapter.submitList(reviewEntities);
            }
        });

    }
}
