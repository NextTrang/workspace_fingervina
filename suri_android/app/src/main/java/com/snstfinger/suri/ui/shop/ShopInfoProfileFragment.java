package com.snstfinger.suri.ui.shop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.databinding.FragmentShopInfoCaseBinding;
import com.snstfinger.suri.databinding.FragmentShopInfoProfileBinding;
import com.snstfinger.suri.ui.tools.ListVewDecorate;
import com.snstfinger.suri.ui.tools.WebChatActivity;

/**
 * 업체 소개화면
 */
public class ShopInfoProfileFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = ShopInfoProfileFragment.class.getName();
    private static final String ARG_SHOP_INFO = "shop_info";

    private FragmentShopInfoProfileBinding mBinding;

    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;

    public ShopInfoProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mShopInfo = (ShopInfo) getArguments().getSerializable(ARG_SHOP_INFO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
//        mQuotationShop = (StoreNPredictionEntity) estimateStatus.getSerializableExtra("shop_prediction_info");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_info_profile, container, false);
        mBinding.setShopInfo(mShopInfo);
        mBinding.setHandler(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_chat: {
                makeChat();
                break;
            }

            case R.id.btn_call: {
                makeCall();
                break;
            }
        }
    }

    @SuppressLint("NewApi")
    private void makeCall() {
        String tel = "tel:" + mShopInfo.storeNumber;
        if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(tel)));
    }

    private void makeChat() {
        Intent goChat = new Intent(getActivity(), WebChatActivity.class);
        goChat.putExtra("request_estimate", mRequestEstimate);
        goChat.putExtra("target", mShopInfo.storeManagerId);
        startActivity(goChat);
    }
}
