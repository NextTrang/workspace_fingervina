package com.snstfinger.suri.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.ui.dialog.InvoiceDialogFragment;
import com.snstfinger.suri.ui.shop.ShopInfoActivity;
import com.snstfinger.suri.ui.tools.WarrantyActivity;
import com.snstfinger.suri.ui.workingStatus.QuotationBookingActivityFragment;
import com.snstfinger.suri.ui.workingStatus.QuotationGoingActivityFragment;
import com.snstfinger.suri.ui.workingStatus.QuotationShopListFragment;

/**
 * 메인 표시내역과 이용내역 리스트
 * 프래그먼트에서 액티비티로 변경하려는데 viewmodel 공유방법을 찾아야함.
 * - drawer 툴바가 커스텀어려움, 각기 다른 툴바를 가질 수 있는지 모름...
 * 현재 액티비티비 안쓰고 그냥 프래그먼트를 보여주고 있음.
 */
public class MySuriListActivity extends AppCompatActivity implements QuotationShopListFragment.OnListFragmentInteractionListener, View.OnClickListener {
    private static final String TAG = MySuriListActivity.class.getName();

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation_going);
        View container = findViewById(R.id.layout_main_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mFragmentManager = getSupportFragmentManager();


        ImageButton warranty = findViewById(R.id.icon_warranty);
        warranty.setOnClickListener(this);
        Intent estimateStatus = getIntent();
        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + mSelectedRequestEstimate);

        if (mSelectedRequestEstimate != null) {
            // enum('REQUEST','BOOKING','BOOKING_CONFIRM','SENDING_PAYMENT','REPAIR_STANDBY','REPAIR','SENDING_PAYMENT_UPDATE','COMPLETE','REVIEW')
            switch (mSelectedRequestEstimate.getStatusCode()) {
                case RequestNPredictionNStoreEntity.TYPE_REQUEST: {
                    actionBar.setTitle(R.string.title_activity_quotation_waiting);
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_BOOKING:
                case RequestNPredictionNStoreEntity.TYPE_BOOKING_CONFIRM: {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationBookingActivityFragment()).commit();
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT:
                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT_UPDATE: {
                    actionBar.setTitle(R.string.title_activity_quotation_invoice);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_REVIEW: {
                    actionBar.setTitle(R.string.title_activity_quotation_complete);
                    warranty.setVisibility(View.VISIBLE);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }

                default: {
                    actionBar.setTitle(R.string.title_activity_quotation_ongoing);
                    container.setBackgroundResource(R.color.light_blue);

                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(StoreNPredictionEntity item) {
        Log.d(TAG, "store clicked::" + item.storeId);
        Intent statusDetail = new Intent(this, ShopInfoActivity.class);
        statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
        statusDetail.putExtra("shop_info", item);
        startActivity(statusDetail);
    }

    void showDialogByRequestEstimateId(boolean takAction) {
        InvoiceDialogFragment dialog = new InvoiceDialogFragment();
        Bundle args = new Bundle();
        args.putString("request_estimate_id", mSelectedRequestEstimate.requestEstimateId);
        args.putBoolean("take_action", takAction);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "invoice");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.icon_warranty) {
            Intent warranty = new Intent(this, WarrantyActivity.class);
            startActivity(warranty);
        } else {
            Log.d(TAG, "View Clicked::" + v);
        }
    }
}
