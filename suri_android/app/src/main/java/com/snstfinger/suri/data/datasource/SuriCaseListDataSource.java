package com.snstfinger.suri.data.datasource;

import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.entity.ResponseSuriCase;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.StoreAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuriCaseListDataSource extends PageKeyedDataSource<Integer, SuriCaseEntity> {
    public static final String TAG = SuriCaseListDataSource.class.getName();

    public static final int PAGE_SIZE = 10;
    private static final int FIRST_PAGE = 1;

    private String storeId;
    private String objectId="";
    private String brandName = "";
    private String modelName = "";

    public SuriCaseListDataSource(String storeId,String objectId, String brandName, String modelName) {
        this.storeId = storeId;
        this.objectId  = objectId;
        this.brandName = brandName;
        this.modelName = modelName;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        if (storeId == null) {
            if (objectId.isEmpty()) {
                initGetSuriCaseRecently(callback);
            } else {
                initGetFilterSuriCase(callback);
            }
        } else {
            initGetSuriCaseByStoreId(callback);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if (storeId == null) {
            if (objectId.isEmpty()) {
                loadPrePageGetSuriCaseRecently(params, callback);
            }else {
                loadPrePageGetFilterSuriCase(params ,callback);
            }
        } else {
            loadPrePageGetSuriCaseByStoreId(params, callback);
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if (storeId == null) {
            if (objectId.isEmpty()) {
                loadNextPageGetSuriCaseRecently(params, callback);
            }else {
                loadNextPageGetFilterSuriCase(params,callback);
            }
        } else {
            loadNextPageGetSuriCaseByStoreId(params, callback);
        }
    }

    private void initGetSuriCaseRecently(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        Call<CommonResponse> call = storeAPI.doPostGetAllRecentSuriCases(String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadPrePageGetSuriCaseRecently(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        Call<CommonResponse> call = storeAPI.doPostGetAllRecentSuriCases(String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);
                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadNextPageGetSuriCaseRecently(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllRecentSuriCases(String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void initGetSuriCaseByStoreId(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllSuriCase(cookie, storeId, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadPrePageGetSuriCaseByStoreId(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllSuriCase(cookie, storeId, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadNextPageGetSuriCaseByStoreId(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllSuriCase(cookie, storeId, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void initGetFilterSuriCase(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.filterSuriCase(cookie, String.valueOf(FIRST_PAGE), objectId, brandName, modelName);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadPrePageGetFilterSuriCase(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.filterSuriCase(cookie, String.valueOf(params.key), objectId, brandName, modelName);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void loadNextPageGetFilterSuriCase(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.filterSuriCase(cookie, String.valueOf(params.key), objectId, brandName, modelName);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseSuriCase result = gson.fromJson(json, ResponseSuriCase.class);
                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getSuriCaseEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }
}
