package com.snstfinger.suri.data.model;

import android.text.TextUtils;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import com.snstfinger.suri.ui.tools.ArcProgress;

import java.io.Serializable;
import java.text.DecimalFormat;

public class HomeSuriStatus extends BaseObservable implements Serializable {
    public static final int TYPE_REQUEST = 1;
    public static final int TYPE_BOOKING = 2;
    public static final int TYPE_BOOKING_CONFIRM = 3;
    public static final int TYPE_SENDING_PAYMENT = 4;
    public static final int TYPE_REPAIR_STANDBY = 5;
    public static final int TYPE_REPAIR = 6;
    public static final int TYPE_SENDING_PAYMENT_UPDATE = 7;
    public static final int TYPE_COMPLETE = 8;
    public static final int TYPE_REVIEW = 9;

    private String requestName;
    private String numOfMatchStore;
    private String storeName;
    private String storeAddress;
    private String bookingTime;
    private String bookingDate;
    private String progress;
    private String suriRate;
    public int status = TYPE_REPAIR;

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getNumOfMatchStore() {
        return numOfMatchStore;
    }

    public void setNumOfMatchStore(String numOfMatchStore) {
        this.numOfMatchStore = numOfMatchStore;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getProgress() {
        if (TextUtils.isEmpty(progress)) {
            return "0";
        } else {
            if (status >= TYPE_COMPLETE)
                return "100";
            else return progress;
        }
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getSuriRate() {
        if (TextUtils.isEmpty(suriRate)) {
            return 0;
        } else {
            int star = (int) (Float.parseFloat(suriRate) * 2);
            return star;
        }
    }

    public void setSuriRate(String suriRate) {
        this.suriRate = suriRate;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    @BindingAdapter("arc_progress")
    public static void setAcrProgress(ArcProgress acrProgress, int value) {
        acrProgress.setProgress(value);
    }

//    public static HomeSuriStatus createEstimateStatus(String estimateTitle, String estimateNumber, int status) {
//        HomeSuriStatus ret = new HomeSuriStatus();
//        ret.status = status;
//        ret.bookingTime = estimateTitle;
//        ret.estimateNumber = estimateNumber;
//        return ret;
//    }

}
