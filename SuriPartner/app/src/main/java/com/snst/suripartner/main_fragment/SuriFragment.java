package com.snst.suripartner.main_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.snst.suripartner.R;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.suri.SuriCompletedAdapter;
import com.snst.suripartner.lib.suri.SuriInvoiceAdapter;
import com.snst.suripartner.lib.suri.SuriOngoingAdapter;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.lib.suri.SuriWaitingAdapter;

import java.util.ArrayList;
import java.util.List;

public class SuriFragment extends Fragment implements MyFragment {
    View view;
    private RecyclerView recyclerView;
    private List<SuriViewModel> mData = new ArrayList<>();
    private TabLayout tabLayout;

    private static int tabPosition = 0;

    public SuriFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.suri_fragment, container, false);
        recyclerView = view.findViewById(R.id.suri_recyclerview);

        RecyclerView.Adapter adapter;
        switch (tabPosition) {
            case 0:
                mData = filter(0);
                adapter = new SuriInvoiceAdapter(mData);
                break;
            case 1:
                mData = filter(1);
                adapter = new SuriOngoingAdapter(mData);
                break;
            case 2:
                mData = filter(2);
                adapter = new SuriWaitingAdapter(this.getContext(), mData);
                break;
            case 3:
                mData = filter(3);
                adapter = new SuriCompletedAdapter(this.getContext(), mData);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + tabPosition);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        tabLayout = view.findViewById(R.id.tablayout_suri);
        tabLayout.addTab(tabLayout.newTab().setText(LbState.INVOICE));
        tabLayout.addTab(tabLayout.newTab().setText(LbState.ONGOING));
        tabLayout.addTab(tabLayout.newTab().setText(LbState.WAITING));
        tabLayout.addTab(tabLayout.newTab().setText(LbState.COMPLETED));
        tabLayout.getTabAt(tabPosition).select();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getSuriViewModels();
    }

    @Override
    public void onStart() {
        super.onStart();
        setOnClickTabs();
    }

    private void setOnClickTabs() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                refresh();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private List<SuriViewModel> filter(int filterCode) {
        List<SuriViewModel> result = new ArrayList<>();

        switch (filterCode) {
            case 0://filter Ongoing
                for (SuriViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.INVOICE)) {
                        result.add(viewModel);
                    }
                }
                break;
            case 1://filter Ongoing
                for (SuriViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.ONGOING)) {
                        result.add(viewModel);
                    }
                }
                break;
            case 2://filter Waiting
                for (SuriViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.WAITING)) {
                        result.add(viewModel);
                    }
                }
                break;
            case 3://filter Completed
                for (SuriViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.COMPLETED)) {
                        result.add(viewModel);
                    }
                }
                break;
        }

        return result;
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getSuriViewModels();
        mData = filter(tabPosition);
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(SuriFragment.this)
                    .attach(SuriFragment.this)
                    .commit();
        }
    }
}
