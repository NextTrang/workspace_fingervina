package com.snstfinger.suri.ui.estimate;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.snstfinger.suri.BaseActivity;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.data.model.SelectSuriObject;
import com.snstfinger.suri.ui.dialog.LoadingDialogFragment;
import com.snstfinger.suri.ui.dialog.SelectSuriDialogFragment;

public class RequestEstimateActivity extends BaseActivity implements LoadingDialogFragment.OnLoadingResultListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_estimate);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        checkPermissionAll();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            case R.id.action_close: {

                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return super.onCreateOptionsMenu(menu);
    }

    void showDialogForObjectId(SelectSuriObject object) {
        SelectSuriDialogFragment dialog = new SelectSuriDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable("object_suri", object);

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "selectType");
    }

    void showDialogSending(RequestEstimate requestForm) {
        LoadingDialogFragment dialog = new LoadingDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable("request_form", requestForm);

        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "sending");
    }

    @Override
    public void onLoadingResult(boolean isSuccess) {
        Log.d("TEST", "success::"+isSuccess);
        if(isSuccess) {
            finish();
        }
    }
}
