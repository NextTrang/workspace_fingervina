package com.snstfinger.suri.ui.tools;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.databinding.ListItemCaseBinding;
import com.snstfinger.suri.databinding.ListItemSimpleCaseBinding;
import com.snstfinger.suri.ui.shop.OnSuriCaseListInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class SuriCaseRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SuriCaseEntity> mValues;
    private final OnSuriCaseListInteractionListener mListener;
    private boolean mIsSimple;

    public SuriCaseRecyclerViewAdapter(OnSuriCaseListInteractionListener listener, boolean isSimple) {
        mValues = new ArrayList<>();
        mListener = listener;
        mIsSimple = isSimple;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mIsSimple) {
            ListItemSimpleCaseBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_simple_case, parent, false);
            return new SimpleCaseViewHolder(binding);

        } else {
            ListItemCaseBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_case, parent, false);
            return new CaseViewHolder(binding);

        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SuriCaseEntity item = mValues.get(position);
        if (mIsSimple)
            ((SimpleCaseViewHolder) holder).bind(item);
        else
            ((CaseViewHolder) holder).bind(item);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setItemList(List<SuriCaseEntity> item) {
        if (item != null) {
            this.mValues = item;
            notifyDataSetChanged();
        }
    }

    public class CaseViewHolder extends RecyclerView.ViewHolder {
        ListItemCaseBinding binding;

        public CaseViewHolder(ListItemCaseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(SuriCaseEntity item) {
            binding.setSuriCase(item);
            itemView.setOnClickListener(v -> mListener.onCaseClick(item));
        }
    }

    public class SimpleCaseViewHolder extends RecyclerView.ViewHolder {
        ListItemSimpleCaseBinding binding;

        public SimpleCaseViewHolder(ListItemSimpleCaseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(SuriCaseEntity item) {
            binding.setSuriCase(item);
            itemView.setOnClickListener(v -> mListener.onCaseClick(item));
        }
    }
}
