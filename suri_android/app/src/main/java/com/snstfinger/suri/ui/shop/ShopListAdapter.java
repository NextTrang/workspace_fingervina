package com.snstfinger.suri.ui.shop;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedList;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.databinding.ListItemShopBinding;

public class ShopListAdapter extends PagedListAdapter<StoreEntity, ShopListAdapter.ItemViewHolder> {
    private static final String TAG = ShopListAdapter.class.getName();

    private Context mContext;
    private OnShopListInteractionListener mListener;
    private PagedList<StoreEntity> mCurrentList;

    ShopListAdapter(Context context, OnShopListInteractionListener listener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemShopBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_shop, parent, false);

        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        StoreEntity item = getItem(position);

        if(item != null) {
            holder.binding.setShopInfo(item);
            holder.itemView.setOnClickListener(v -> mListener.onShopClick(item));

        } else {
            Log.d(TAG, "item is null");
        }
    }

    public PagedList<StoreEntity> getStoreItems() {
        mCurrentList = getCurrentList();

        return mCurrentList;
    }

    private static DiffUtil.ItemCallback<StoreEntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<StoreEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull StoreEntity oldItem, @NonNull StoreEntity newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull StoreEntity oldItem, @NonNull StoreEntity newItem) {
            return oldItem.storeId == newItem.storeId;
        }
    };

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ListItemShopBinding binding;

        public ItemViewHolder(ListItemShopBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
