package com.snst.suripartner.lib.api;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.booking.BookingViewModel;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.pop_confirm.PopConfirm;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.pop_up.PopConfirmActivity;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestConfirmBooking implements ApiRequest {

    private static final String TAG = "RequestConfirmBooking";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().confirmBooking(Sharing.Instance().getCookie(), inputs[0], inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //remove this booking from list
                        int index = -1;
                        for (BookingViewModel viewModel : Sharing.Instance().getBookingViewModels()) {
                            if (viewModel.id.equals(inputs[1])) {
                                index = Sharing.Instance().getBookingViewModels().indexOf(viewModel);
                                break;
                            }
                        }
                        Sharing.Instance().getBookingViewModels().remove(index);

                        //Add to Send Invoice of Suri tab
                        JSONObject jsData = jsonObject.getJSONObject(LbData.DATA);
                        JSONObject jsInfo = jsData.getJSONObject(LbData.LbInfo.INFO);
                        Suri suri = new Suri(jsInfo);
                        Sharing.Instance().getSuriViewModels().add(new SuriViewModel(suri));

                        //refresh current tab
                        ((MainActivity) context).refresh();

                        PopConfirm popConfirm = new PopConfirm()
                                .setTitle(LbPop.LbTitle.BOOKING_IS_CONFIRMED)
                                .setContent(LbPop.LbContent.BOOKING_IS_CONFIRMED);
                        Intent intent = new Intent(context, PopConfirmActivity.class);
                        intent.putExtra(LbPop.LbKey.POP_CONFIRM,popConfirm);
                        context.startActivity(intent);
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
            }
        });
    }
}
