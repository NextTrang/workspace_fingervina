package com.snstfinger.suri.network;

import androidx.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface StoreAPI {
    @GET("store/getAllStoreInfo")
    Call<CommonResponse> doGetAllStoreInfo(@Header("Cookie") String cookie, @Query(value="page", encoded = true) String pageNo);

    @FormUrlEncoded
    @POST("store/getAllSuriCase")
    Call<CommonResponse> doPostGetAllSuriCase(@Header("Cookie") String cookie, @Field("store_id") String storeId, @Field("page") String page);

    @FormUrlEncoded
    @POST("store/getStoreInfoByCoordinate")
    Call<CommonResponse> doPostGetStoreInfoByCoordinate(@Header("Cookie") String cookie, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("nKM") String nKM, @Nullable @Field("serviceKind") String serviceKind, @Nullable @Field("page") String page);

    @FormUrlEncoded
    @POST("store/getStoreInfoByName")
    Call<CommonResponse> doPostGetStoreInfoByName(@Header("Cookie") String cookie, @Field("name") String name, @Nullable @Field("page") String page, @Nullable @Field("orderByClause") String orderByClause);

    @FormUrlEncoded
    @POST("store/getAllRecentSuriCases")
    Call<CommonResponse> doPostGetAllRecentSuriCases(@Field("page") String page);

    @GET("brand/getBrandsByObjectId")
    Call<ResponseBody> getBrandsByObjectId(
            @Header("Cookie") String cookie,
            @Query(value="object_id", encoded = true) String object_id
    );

    @GET("model/getModelsByBrandId")
    Call<ResponseBody> getModelsByBrandId(
            @Header("Cookie") String cookie,
            @Query(value="brand_id", encoded = true) String brand_id
    );

    @FormUrlEncoded
    @POST("store/getAllRecentSuriCasesFilter")
    Call<CommonResponse> filterSuriCase(
            @Header("Cookie") String cookie,
            @Field("page") String page,
            @Field("object_id") String objectId,
            @Field("brand_name") String brandName,
            @Field("model_name") String modelName
    );
}
