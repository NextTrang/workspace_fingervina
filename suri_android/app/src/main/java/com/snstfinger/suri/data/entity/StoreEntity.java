package com.snstfinger.suri.data.entity;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.network.EmptyStringAsNullTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StoreEntity implements Serializable {
    @SerializedName("store_id")
    public int storeId;

    @SerializedName("store_name")
    public String storeName;

    @SerializedName("store_number")
    public String storeNumber;

    @SerializedName("store_manager_name")
    public String storeManagerName;

    @SerializedName("store_manager_id")
    public String storeManagerId;

    @SerializedName("store_image")
    public String storeImage;

    @SerializedName("store_description")
    public String storeDescription;

    @SerializedName("store_satisfaction")
    public String storeSatisfaction;

    @SerializedName("point_quality")
    public String pointQuality;

    @SerializedName("point_kindness")
    public String pointKindness;

    @SerializedName("point_accuracy")
    public String pointAccuracy;

    @SerializedName("point_score")
    public String pointScore;

    @SerializedName("review_total")
    public String reviewTotal;

    @SerializedName("suri_case_total")
    @JsonAdapter(EmptyStringAsNullTypeAdapter.class)
    public Integer suriCaseTotal;

    @SerializedName("city")
    public String city;

    @SerializedName("district")
    public String district;

    @SerializedName("address")
    public String address;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date registerDate;

    @SerializedName("service_phone")
    public String servicePhone;

    @SerializedName("service_pc")
    public String servicePc;

    @SerializedName("service_bike")
    public String serviceBike;

    @SerializedName("service_car")
    public String serviceCar;

    @SerializedName("store_url")
    public String storeUrl;

    @SerializedName("service_pickup")
    public String servicePickup;

    @SerializedName("service_engineer_visiting")
    public String serviceEngineerVisiting;

    @SerializedName("store_business_number")
    public String storeBusinessNumber;

    @SerializedName("store_business_number_image")
    public String storeBusinessNumberImage;

    @SerializedName("store_engineer")
    public String storeEngineer;

    @SerializedName("distance")
    public String distance;

    public String getRegisterDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return registerDate == null ? "" : formatter.format(registerDate);
    }

    public String getDistance() {
        DecimalFormat formatter = new DecimalFormat(Utils.FLOAT_FORMAT);
        return TextUtils.isEmpty(distance) ? "" : formatter.format(Double.parseDouble(distance));
    }

    public String getReviewTotal() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return TextUtils.isEmpty(reviewTotal) ? "0" : formatter.format(Double.parseDouble(reviewTotal));
    }

    public String getSuriCaseTotal() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return suriCaseTotal==null? "0" : formatter.format(suriCaseTotal);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Store Info::");
        sb.append(storeId);
        sb.append("::");
        sb.append(storeName);
        sb.append("::");
        sb.append(pointScore);

        return sb.toString();
    }

    public static ShopInfo convertToShopInfo(StoreEntity in) {
        ShopInfo out = new ShopInfo();
        out.storeId = in.storeId;
        out.storeImage = in.storeImage;
        out.storeName = in.storeName;
        out.pointScore = in.pointScore;
        out.pointQuality = in.pointQuality;
        out.pointKindness = in.pointKindness;
        out.pointAccuracy = in.pointAccuracy;
        out.storeEngineer = in.storeEngineer;
        out.distance = in.distance;
        out.storeDescription = in.storeDescription;
        out.storeNumber = in.storeNumber;
        out.address = in.address;
        out.latitude = in.latitude;
        out.longitude = in.longitude;
        out.storeManagerId = in.storeManagerId;

        return out;
    }

}
