package com.snstfinger.suri.ui.shop;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.snstfinger.suri.data.datasource.SuriCaseListDataSource;
import com.snstfinger.suri.data.datasource.SuriCaseListDataSourceFactory;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.data.repository.CaseRepository;

import java.util.List;

public class SuriCaseViewModel extends ViewModel {
    private CaseRepository repository = new CaseRepository();

    public LiveData<List<SuriCaseEntity>> getAllCase(String storeId) {

        return repository.getAllSuriCaseLiveData(storeId);
    }

    LiveData<PagedList<SuriCaseEntity>> suriCasePagedList = new MutableLiveData<>();

    public LiveData<PagedList<SuriCaseEntity>> getSuriCasePagedList(@Nullable String storeId,String objectId, String brandName, String modelName) {
        SuriCaseListDataSourceFactory factory = new SuriCaseListDataSourceFactory(storeId,objectId, brandName,modelName);

        PagedList.Config config = new PagedList.Config.Builder().setInitialLoadSizeHint(SuriCaseListDataSource.PAGE_SIZE).setPageSize(SuriCaseListDataSource.PAGE_SIZE).build();

        suriCasePagedList = new LivePagedListBuilder(factory, config).build();

        return suriCasePagedList;
    }
}
