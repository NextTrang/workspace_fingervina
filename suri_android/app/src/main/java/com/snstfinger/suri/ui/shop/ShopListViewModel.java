package com.snstfinger.suri.ui.shop;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.snstfinger.suri.data.datasource.ShopListDataSource;
import com.snstfinger.suri.data.datasource.ShopListDataSourceFactory;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.data.model.GeoCondition;

public class ShopListViewModel extends ViewModel {
    MutableLiveData<GeoCondition> geoCondition = new MutableLiveData<>();
    LiveData<PageKeyedDataSource<Integer, StoreEntity>> liveDataSource;
    public final LiveData<PagedList<StoreEntity>> shopPagedList = Transformations.switchMap(geoCondition, this::getShopPagedList);

//    public LiveData<PagedList<StoreEntity>> getShopPagedList(double latitude, double longitude, int range) {
//        ShopListDataSourceFactory factory = new ShopListDataSourceFactory(latitude, longitude, range);
//
//        liveDataSource = factory.getShopInfoLiveData();
//
//        PagedList.Config pagedListConfig = new PagedList.Config.Builder().setEnablePlaceholders(true).setInitialLoadSizeHint(ShopListDataSource.PAGE_SIZE).setPageSize(ShopListDataSource.PAGE_SIZE).build();
//
//        shopPagedList = new LivePagedListBuilder(factory,pagedListConfig).build();
//
//        return shopPagedList;
//    }

    public LiveData<PagedList<StoreEntity>> getShopPagedList(GeoCondition geoCondition) {
        ShopListDataSourceFactory factory = new ShopListDataSourceFactory(geoCondition);

        liveDataSource = factory.getShopInfoLiveData();

        PagedList.Config pagedListConfig = new PagedList.Config.Builder().setEnablePlaceholders(true).setInitialLoadSizeHint(ShopListDataSource.PAGE_SIZE).setPageSize(ShopListDataSource.PAGE_SIZE).build();

        return new LivePagedListBuilder(factory, pagedListConfig).build();
    }

    public void setGeoCondition(GeoCondition location) {
        geoCondition.setValue(location);
    }

    public LiveData<PagedList<StoreEntity>> getShopPagedList() {
        return shopPagedList;
    }

}
