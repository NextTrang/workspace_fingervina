package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseSuriCase {
    @SerializedName("pageMap")
    @Expose
    private PageMap pageMap;

    @SerializedName("Info")
    @Expose
    private List<SuriCaseEntity> suriCaseEntities = null;

    public PageMap getPageMap() {
        return pageMap;
    }

    public List<SuriCaseEntity> getSuriCaseEntities() {
        return suriCaseEntities;
    }
}
