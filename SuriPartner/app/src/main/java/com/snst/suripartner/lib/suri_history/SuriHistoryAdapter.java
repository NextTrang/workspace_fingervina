package com.snst.suripartner.lib.suri_history;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.databinding.SuriHistoryBinding;

import java.util.List;

public class SuriHistoryAdapter extends RecyclerView.Adapter<SuriHistoryAdapter.MyViewHolder> {

    private List<SuriHistoryViewModel> mData;
    private LayoutInflater inflater;

    public SuriHistoryAdapter(List<SuriHistoryViewModel> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        SuriHistoryBinding binding = SuriHistoryBinding.inflate(inflater, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private SuriHistoryBinding binding;

        public MyViewHolder(SuriHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(SuriHistoryViewModel suriHistoryViewModel) {
            binding.setSuriHistory(new SuriHistory(suriHistoryViewModel));
        }
    }
}
