package com.snstfinger.suri.ui.tools;

import android.content.Context;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.util.Utils;

public class ListVewDecorate extends RecyclerView.ItemDecoration {
    int mSpaceHorizon;
    int mSpaceVertical;

    public ListVewDecorate(Context context, int horizonDp, int verticalDp) {
        mSpaceHorizon = Utils.dp2Px(context, horizonDp);
        mSpaceVertical = Utils.dp2Px(context, verticalDp);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.top += mSpaceVertical;
        outRect.bottom += mSpaceVertical;
        outRect.left += mSpaceHorizon;
        outRect.right += mSpaceHorizon;
    }
}
