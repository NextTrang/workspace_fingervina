package com.snstfinger.suri.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.model.AccountType;
import com.snstfinger.suri.network.LoginProvider;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;
import com.snstfinger.suri.util.SettingSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, LoginProvider.OnResultLogoutListener {
    public static String TAG = SettingsActivity.class.getName();

    TextView mLoggedInState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        mLoggedInState = findViewById(R.id.btn_login);
        if (SuriLoginManager.getInstance(getApplicationContext()).isLoggedIn()) {
            mLoggedInState.setText(R.string.settings_logout);
        } else {
            mLoggedInState.setText(R.string.settings_login);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login: {
                if (SuriLoginManager.getInstance(getApplicationContext()).isLoggedIn()) {
                    SuriLoginManager.getInstance(getApplicationContext()).logout(this);
                    clearMyData();
                    finish();
                    Intent intent = new Intent(this,SnsAuthenticActivity.class);
                    startActivity(intent);
                } else {

                }
                break;
            }

            case R.id.btn_account:
                startActivity(new Intent(this, AccountSettingsActivity.class));

                break;

            case R.id.btn_payment:

                break;

            case R.id.btn_share:

                break;

            case R.id.btn_terms_condition:

                break;

            case R.id.btn_policy:

                break;

            case R.id.btn_delete_me:

                break;

        }
    }

    @Override
    public void onResult(int result) {
        Log.d(TAG, "result::" + result);
        if(result == SuriLoginManager.LOGOUT_SUCCESS)
            mLoggedInState.setText(R.string.settings_login);
    }

    private void clearMyData() {
        SettingSharedPreferences settings = SettingSharedPreferences.getInstance(getApplicationContext());
        settings.clear();
        MySuriInfoSharedPreferences mysuri = MySuriInfoSharedPreferences.getInstance(getApplicationContext());
        mysuri.clear();
        UserInfoSharedPreferences user = UserInfoSharedPreferences.getInstance(getApplicationContext());
        user.clear();
    }
}
