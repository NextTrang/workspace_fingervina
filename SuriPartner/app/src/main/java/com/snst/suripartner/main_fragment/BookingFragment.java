package com.snst.suripartner.main_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.R;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.booking.BookingAdapter;
import com.snst.suripartner.lib.booking.BookingViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookingFragment extends Fragment implements MyFragment {
    View view;
    private RecyclerView recyclerView;
    private List<BookingViewModel> mData = new ArrayList<>();

    public BookingFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.booking_fragment, container, false);
        recyclerView = view.findViewById(R.id.booking_recyclerview);
        BookingAdapter bookingAdapter = new BookingAdapter(mData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(bookingAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getBookingViewModels();
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getBookingViewModels();
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(BookingFragment.this)
                    .attach(BookingFragment.this)
                    .commit();
        }
    }
}
