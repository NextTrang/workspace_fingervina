package com.snst.suripartner.lib.payment;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.api.RequestResendPayment;
import com.snst.suripartner.lib.api.RequestSendPayment;
import com.snst.suripartner.lib.content_price.ContentPrice;
import com.snst.suripartner.pop_up.PopSendInvoiceActivity;

import java.util.List;

public class PaymentEvent {
    Context context;

    public PaymentEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onAddContentPrice(int code) {
        ((PopSendInvoiceActivity) context).addPrice(code);
    }

    public int getTotal(List<ContentPrice> contentPrices) {
        int result = 0;
        for (ContentPrice contentPrice : contentPrices) {
            if (!contentPrice.getPrice().isEmpty()) {
                result += Integer.parseInt(contentPrice.getPrice());
            }
        }
        return result;
    }

    public void onTransferData(Payment payment) {
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.PAYMENT, payment);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }

    public void onSendInvoice(Payment payment) {
        JsonObject jsInput = new JsonObject();

        jsInput.addProperty("prediction_estimate_id", payment.getPredictEstId());
        jsInput.addProperty("request_estimate_id", payment.getRequestEstId());

        addContentPriceToJson(jsInput, payment.getSuriPrices(), "fix_");
        addContentPriceToJson(jsInput, payment.getOptionPrices(), "option_");
        addContentPriceToJson(jsInput, payment.getPartPrices(), "part_");

        jsInput.addProperty("price_comment", "Send invoice");
        jsInput.addProperty("total_price", payment.getTotalPrice());

        new RequestSendPayment().execute(context, jsInput.toString());
    }

    public void onResendInvoice(Payment payment){
        JsonObject jsInput = new JsonObject();

        jsInput.addProperty("prediction_estimate_id", payment.getPredictEstId());
        jsInput.addProperty("request_estimate_id", payment.getRequestEstId());

        addContentPriceToJson(jsInput, payment.getSuriPrices(), "fix_");
        addContentPriceToJson(jsInput, payment.getOptionPrices(), "option_");
        addContentPriceToJson(jsInput, payment.getPartPrices(), "part_");

        jsInput.addProperty("price_comment", "Send invoice");
        jsInput.addProperty("total_price", payment.getTotalPrice());

        new RequestResendPayment().execute(context, jsInput.toString());
    }

    private void addContentPriceToJson(JsonObject jsInput, List<ContentPrice> contentPrices, String prefix) {
        int index = 0;
        for (ContentPrice contentPrice : contentPrices) {
            if (!contentPrice.getContent().isEmpty() && !contentPrice.getPrice().isEmpty()) {
                index++;
                jsInput.addProperty(prefix + index, contentPrice.getContent());
                jsInput.addProperty(prefix + index + "_price", contentPrice.getPrice());
            }
        }
    }

}
