package com.snst.suripartner;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class SignupActivity extends AppCompatActivity {

    private boolean checkedCustomer;
    private boolean checkedOtherCompany;
    private boolean checkedAdvertisment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.cbCustomer:
                checkedCustomer = checked;
                break;
            case R.id.cbOtherCompany:
                checkedOtherCompany = checked;
                break;
            case R.id.cbAdvertisement:
                checkedAdvertisment = checked;
                break;
        }
    }
}
