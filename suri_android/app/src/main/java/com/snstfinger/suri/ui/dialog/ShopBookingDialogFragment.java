package com.snstfinger.suri.ui.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.ui.shop.ShopBookingActivity;
import com.snstfinger.suri.ui.shop.ShopInfoActivity;

public class ShopBookingDialogFragment extends DialogFragment implements View.OnClickListener {
    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle options = getArguments();
        mRequestEstimate = (RequestNPredictionNStoreEntity) options.getSerializable("request_estimate");
        mShopInfo = ((ShopInfo) options.getSerializable("shop_info"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_booking, container, false);
        Button ok = root.findViewById(R.id.btn_ok);
        Button cancel = root.findViewById(R.id.btn_cancel);
        Button close = root.findViewById(R.id.btn_close);
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);
        close.setOnClickListener(this);

        return root;
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public void onStart() {
//        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
//        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);
//        getDialog().getWindow().setLayout(width, height);
        super.onStart();
    }

    public void onDismiss() {
        getDialog().dismiss();

    }

    public void onSubmit() {
        Intent bookingForm = new Intent(getContext(), ShopBookingActivity.class);
        bookingForm.putExtra("request_estimate", mRequestEstimate);
        bookingForm.putExtra("shop_info", mShopInfo);
        getActivity().startActivityForResult(bookingForm, ShopInfoActivity.REQ_BOOKING_CODE);

        getDialog().dismiss();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_ok) {
            onSubmit();

        } else {
            onDismiss();
        }
    }
}
