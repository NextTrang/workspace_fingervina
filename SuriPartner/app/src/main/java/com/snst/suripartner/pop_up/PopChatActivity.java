package com.snst.suripartner.pop_up;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.AppBarViewChatBinding;
import com.snst.suripartner.databinding.ChatRoomBinding;
import com.snst.suripartner.lib.chat.Chat;
import com.snst.suripartner.lib.chat.ChatEvent;

public class PopChatActivity extends AppCompatActivity {

    private Chat data;
    private ChatEvent handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChatRoomBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_chat);
        data = (Chat) getIntent().getSerializableExtra("Chat");
        binding.setChat(data);

        setUI();

        handler = new ChatEvent(this);

        adjustResizeOnGlobalLayout(R.id.popChat, binding.webChat);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }

            case R.id.action_close:
                handler.onClose(getWindow().getDecorView().getRootView());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        // add custom view
        actionBar.setDisplayShowCustomEnabled(true);
        AppBarViewChatBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.app_bar_view_chat, toolbar, false);
        actionBar.setCustomView(binding.getRoot());
        binding.setChat(data);

    }

    public void adjustResizeOnGlobalLayout(@IdRes final int viewGroupId, final WebView webView) {
        final View decorView = getWindow().getDecorView();
        final ViewGroup viewGroup = (ViewGroup) findViewById(viewGroupId);

        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onGlobalLayout() {
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                Rect rect = new Rect();
                decorView.getWindowVisibleDisplayFrame(rect);
                int paddingBottom = displayMetrics.heightPixels - rect.bottom;

                if (viewGroup.getPaddingBottom() != paddingBottom) {
                    // showing/hiding the soft keyboard
                    viewGroup.setPadding(viewGroup.getPaddingLeft(), viewGroup.getPaddingTop(), viewGroup.getPaddingRight(), paddingBottom);
                } else {
                    // soft keyboard shown/hidden and padding changed
                    if (paddingBottom != 0) {
                        // soft keyboard shown, scroll active element into view in case it is blocked by the soft keyboard
                        webView.evaluateJavascript("if (document.activeElement) { document.activeElement.scrollIntoView({behavior: \"smooth\", block: \"center\", inline: \"nearest\"}); }", null);
                    }
                }
            }
        });
    }
}

