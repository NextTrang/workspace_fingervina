package com.snstfinger.suri.data.model;

import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class RequestEstimate extends BaseObservable implements Serializable {
    public static final int TYPE_MOBILE = 1;
    public static final int TYPE_PC = 2;
    public static final int TYPE_BIKE = 3;
    public static final int TYPE_CAR = 4;

    @SerializedName("object_id")
    private Integer objectId = TYPE_MOBILE;
    @SerializedName("estimate_image1")
    private String estimateImg1;
    @SerializedName("estimate_image2")
    private String estimateImg2;
    @SerializedName("estimate_image3")
    private String estimateImg3;
    @SerializedName("estimate_image4")
    private String estimateImg4;

    @Nullable
    @SerializedName("vehicle_number")
    private String vehicleNumber;
    @Nullable
    @SerializedName("estimate_description")
    private String estimateDescription;

    @SerializedName("brand_name")
    private String brandName;
    private Integer brandId;

    @SerializedName("model_name")
    private String modelName;
    private Integer modelId;

    @SerializedName("estimateRepairTypeSelected")
    private String estimateRepairTypeSelected;

    private boolean isScreenRepair;
    private boolean isPowerRepair;
    private boolean isSubmergeRepair;
    private boolean isExteriorRepair;
    private boolean isTireRepair;
    private boolean isEtcRepair;

    @SerializedName("estimateRepairOptionSelected")
    private String estimateRepairOptionSelected;

    private boolean isPickupOpt;
    private boolean isInsuraceOpt;
    private boolean isEngCall;

    @SerializedName("estimate_address_id_1")
    private String estimateAddressId1;
    @SerializedName("estimate_address_id_2")
    private String estimateAddressId2;

    @Nullable
    @SerializedName("estimate_promotion_code")
    private String estimatePromotionCode;

    @SerializedName("estimate_phonenumber")
    private String estimatePhonenumber;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("desired_reservation_date")
    private String desiredReservationDate;

    public RequestEstimate(int type) {
        this.objectId = type;
    }

    public boolean isScreenRepair() {
        return isScreenRepair;
    }

    public void setScreenRepair(boolean screenRepair) {
        isScreenRepair = screenRepair;

        switch (objectId) {
            case 1: {
                setEstimateRepairTypeSelected("1", screenRepair);
                break;
            }

            case 2: {
                setEstimateRepairTypeSelected("5", screenRepair);
                break;
            }
        }
    }

    public boolean isPowerRepair() {
        return isPowerRepair;
    }

    public void setPowerRepair(boolean powerRepair) {
        isPowerRepair = powerRepair;

        switch (objectId) {
            case 1: {
                setEstimateRepairTypeSelected("2", powerRepair);
                break;
            }

            case 2: {
                setEstimateRepairTypeSelected("6", powerRepair);
                break;
            }
        }
    }

    public boolean isSubmergeRepair() {
        return isSubmergeRepair;
    }

    public void setSubmergeRepair(boolean submergeRepair) {
        isSubmergeRepair = submergeRepair;

        switch (objectId) {
            case 1: {
                setEstimateRepairTypeSelected("3", submergeRepair);
                break;
            }

            case 2: {
                setEstimateRepairTypeSelected("7", submergeRepair);
                break;
            }
        }
    }

    public boolean isExteriorRepair() {
        return isExteriorRepair;
    }

    public void setExteriorRepair(boolean exteriorRepair) {
        isExteriorRepair = exteriorRepair;

        switch (objectId) {
            case 3: {
                setEstimateRepairTypeSelected("9", exteriorRepair);
                break;
            }

            case 4: {
                setEstimateRepairTypeSelected("12", exteriorRepair);
                break;
            }
        }
    }

    public boolean isTireRepair() {
        return isTireRepair;
    }

    public void setTireRepair(boolean tireRepair) {
        isTireRepair = tireRepair;

        switch (objectId) {
            case 3: {
                setEstimateRepairTypeSelected("10", tireRepair);
                break;
            }

            case 4: {
                setEstimateRepairTypeSelected("13", tireRepair);
                break;
            }
        }
    }

    public boolean isEtcRepair() {
        return isEtcRepair;
    }

    public void setEtcRepair(boolean etcRepair) {
        isEtcRepair = etcRepair;

        switch (objectId) {
            case 1: {
                setEstimateRepairTypeSelected("4", etcRepair);
                break;
            }

            case 2: {
                setEstimateRepairTypeSelected("8", etcRepair);
                break;
            }

            case 3: {
                setEstimateRepairTypeSelected("11", etcRepair);
                break;
            }

            case 4: {
                setEstimateRepairTypeSelected("14", etcRepair);
                break;
            }
        }
    }

    public boolean isPickupOpt() {
        return isPickupOpt;
    }

    public void setPickupOpt(boolean pickupOpt) {
        isPickupOpt = pickupOpt;

        switch (objectId) {
            case 1: {
                setEstimateRepairOptionSelected("1", pickupOpt);
                break;
            }

            case 2: {
                setEstimateRepairOptionSelected("3", pickupOpt);
                break;
            }

            case 3: {
                setEstimateRepairOptionSelected("5", pickupOpt);
                break;
            }

            case 4: {
                setEstimateRepairOptionSelected("8", pickupOpt);
                break;
            }
        }
    }

    public boolean isInsuraceOpt() {
        return isInsuraceOpt;
    }

    public void setInsuraceOpt(boolean insuraceOpt) {
        isInsuraceOpt = insuraceOpt;

        switch (objectId) {
            case 1: {
                setEstimateRepairOptionSelected("2", insuraceOpt);
                break;
            }

            case 2: {
                setEstimateRepairOptionSelected("4", insuraceOpt);
                break;
            }

            case 3: {
                setEstimateRepairOptionSelected("6", insuraceOpt);
                break;
            }

            case 4: {
                setEstimateRepairOptionSelected("9", insuraceOpt);
                break;
            }
        }
    }

    public boolean isEngCall() {
        return isEngCall;
    }

    public void setEngCall(boolean engCall) {
        isEngCall = engCall;

        switch (objectId) {
            case 3: {
                setEstimateRepairOptionSelected("7", engCall);
                break;
            }

            case 4: {
                setEstimateRepairOptionSelected("10", engCall);
                break;
            }
        }
    }

    @Bindable
    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;

        notifyPropertyChanged(com.snstfinger.suri.BR.objectId);
    }

    public String getEstimateImg1() {
        return estimateImg1;
    }

    public void setEstimateImg1(String estimateImg1) {
        this.estimateImg1 = estimateImg1;
    }

    public String getEstimateImg2() {
        return estimateImg2;
    }

    public void setEstimateImg2(String estimateImg2) {
        this.estimateImg2 = estimateImg2;
    }

    public String getEstimateImg3() {
        return estimateImg3;
    }

    public void setEstimateImg3(String estimateImg3) {
        this.estimateImg3 = estimateImg3;
    }

    public String getEstimateImg4() {
        return estimateImg4;
    }

    public void setEstimateImg4(String estimateImg4) {
        this.estimateImg4 = estimateImg4;
    }

    @Nullable
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(@Nullable String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    @Nullable
    public String getEstimateDescription() {
        return estimateDescription;
    }

    public void setEstimateDescription(@Nullable String estimateDescription) {
        this.estimateDescription = estimateDescription;
    }

    @Bindable
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;

        notifyPropertyChanged(com.snstfinger.suri.BR.brandName);
    }

    @Bindable
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;

        notifyPropertyChanged(com.snstfinger.suri.BR.modelName);
    }

    public String getEstimateAddressId1() {
        return estimateAddressId1;
    }

    public void setEstimateAddressId1(String estimateAddressId1) {
        this.estimateAddressId1 = estimateAddressId1;
    }

    public String getEstimateAddressId2() {
        return estimateAddressId2;
    }

    public void setEstimateAddressId2(String estimateAddressId2) {
        this.estimateAddressId2 = estimateAddressId2;
    }

    @Nullable
    public String getEstimatePromotionCode() {
        return estimatePromotionCode;
    }

    public void setEstimatePromotionCode(@Nullable String estimatePromotionCode) {
        this.estimatePromotionCode = estimatePromotionCode;
    }

    public String getEstimatePhonenumber() {
        return estimatePhonenumber;
    }

    public void setEstimatePhonenumber(String estimatePhonenumber) {
        this.estimatePhonenumber = estimatePhonenumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getEstimateRepairTypeSelected() {
        return estimateRepairTypeSelected;
    }

    public void setEstimateRepairTypeSelected(String estimateRepairTypeSelected, boolean isChecked) {
        if (isChecked) {
            if (!TextUtils.isEmpty(this.estimateRepairTypeSelected)) {
                this.estimateRepairTypeSelected += "|" + estimateRepairTypeSelected;
            } else {
                this.estimateRepairTypeSelected = estimateRepairTypeSelected;
            }
        } else {
            if (!TextUtils.isEmpty(this.estimateRepairTypeSelected)) {
                ArrayList<String> types = new ArrayList<>(Arrays.asList(this.estimateRepairTypeSelected.split("\\|")));
                types.remove(estimateRepairTypeSelected);

                String newSelectedItem = "";
                for (String type : types) {
                    if (!TextUtils.isEmpty(newSelectedItem)) {
                        newSelectedItem += "|" + type;
                    } else {
                        newSelectedItem = type;
                    }
                }

                this.estimateRepairTypeSelected = newSelectedItem;
            }
        }
    }

    public String getEstimateRepairOptionSelected() {
        return estimateRepairOptionSelected;
    }

    public void setEstimateRepairOptionSelected(String estimateRepairOptionSelected, boolean isChecked) {
        if (isChecked) {
            if (!TextUtils.isEmpty(this.estimateRepairOptionSelected)) {
                this.estimateRepairOptionSelected += "|" + estimateRepairOptionSelected;
            } else {
                this.estimateRepairOptionSelected = estimateRepairOptionSelected;
            }
        } else {
            if (!TextUtils.isEmpty(this.estimateRepairOptionSelected)) {
                ArrayList<String> types = new ArrayList<>(Arrays.asList(this.estimateRepairOptionSelected.split("\\|")));
                types.remove(estimateRepairOptionSelected);

                String newSelectedItem = "";
                for (String type : types) {
                    if (!TextUtils.isEmpty(newSelectedItem)) {
                        newSelectedItem += "|" + type;
                    } else {
                        newSelectedItem = type;
                    }
                }
                this.estimateRepairOptionSelected = newSelectedItem;
            }
        }
    }

    public String getDesiredReservationDate() {
        return desiredReservationDate;
    }

    public void setDesiredReservationDate(String desiredReservationDate) {
        this.desiredReservationDate = Utils.localToUTC(desiredReservationDate);
    }
}
