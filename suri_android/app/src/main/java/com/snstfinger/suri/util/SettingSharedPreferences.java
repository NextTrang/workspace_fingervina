package com.snstfinger.suri.util;

import android.content.Context;

public class SettingSharedPreferences extends SharedPrefsManager {
    public final String KEY_COOKIE = "com.snstfinger.suri";
    public final String KEY_SNS_TYPE = "key_sns_type";

    private static SettingSharedPreferences instance;

    private SettingSharedPreferences(Context context, int mode) {
        super(context, "suri_settins", mode);
    }

    public static SettingSharedPreferences getInstance(Context context) {
        if(instance == null) {
            instance = new SettingSharedPreferences(context, Context.MODE_PRIVATE);
        }

        return instance;
    }

    public String getAccountType() {
        return get(KEY_SNS_TYPE,"");
    }

    // sns types
    public void putAccountType(String paramString) {
        put(KEY_SNS_TYPE, paramString);
    }
}
