package com.snstfinger.suri;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;

import com.snstfinger.suri.fix_patch.FixPatch;
import com.zing.zalo.zalosdk.oauth.ZaloSDKApplication;

public class SuriApplication extends Application implements ContextProvider{
    private static final String TAG = SuriApplication.class.getName();

    public static String APP_URL = "com.snstfinger.suri";
    public static final int REQ_PHOTO = 1000;
    public static final int REQ_SEARCH = 1001;

    public static final double DEFAULT_LATITUDE = 10.7808;
    public static final double DEFAULT_LONGITUDE = 106.6771;

    public static String mSuriToken;

    private Activity currentActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate.");

        CookieManager.getInstance().setAcceptCookie(true);
        ZaloSDKApplication.wrap(this);

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                SuriApplication.this.currentActivity = activity;
            }

            @Override
            public void onActivityStarted(Activity activity) {
                SuriApplication.this.currentActivity = activity;
            }

            @Override
            public void onActivityResumed(Activity activity) {
                SuriApplication.this.currentActivity = activity;

                if(FixPatch.Instance().getRefreshActivities().contains(activity.getClass().getSimpleName())){
                    ((MyFragment)activity).refresh();
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                SuriApplication.this.currentActivity = null;
            }

            @Override
            public void onActivityStopped(Activity activity) {
                // don't clear current activity because activity may get stopped after
                // the new activity is resumed
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                // don't clear current activity because activity may get destroyed after
                // the new activity is resumed
            }
        });
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public Context getActivityContext() {
        return currentActivity;
    }
}
