package com.snst.suripartner.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.os.Handler;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.PopComfirmBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.pop_confirm.PopConfirm;
import com.snst.suripartner.lib.pop_confirm.PopConfirmEvent;
import com.snst.suripartner.lib.suri.Suri;

public class PopConfirmActivity extends AppCompatActivity {

    private PopConfirm popConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopComfirmBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_pop_confirm);

        popConfirm = (PopConfirm) getIntent().getSerializableExtra(LbPop.LbKey.POP_CONFIRM);
        binding.setPopConfirm(popConfirm);
        binding.setHandler(new PopConfirmEvent(this));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 2000);
    }
}
