package com.snstfinger.suri.ui.shop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.ResponseReviews;
import com.snstfinger.suri.data.entity.SuriCaseEntity;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.databinding.FragmentShopInfoBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.ReviewAPI;
import com.snstfinger.suri.network.StoreAPI;
import com.snstfinger.suri.network.UserAPI;
import com.snstfinger.suri.ui.tools.HorizonListVewDecorate;
import com.snstfinger.suri.ui.tools.ListVewDecorate;
import com.snstfinger.suri.ui.tools.ReviewRecyclerViewAdapter;
import com.snstfinger.suri.ui.tools.SuriCaseRecyclerViewAdapter;
import com.snstfinger.suri.ui.tools.WebChatActivity;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 업체 정보를 보여줌
 */
public class ShopInfoActivityFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener, OnSuriCaseListInteractionListener {
    public static final String TAG = ShopInfoActivityFragment.class.getName();

    private RequestNPredictionNStoreEntity mRequestEstimate;
    private ShopInfo mShopInfo;
    private FragmentShopInfoBinding mBinding;

    private GoogleMap mMap;

    private SuriCaseRecyclerViewAdapter mCaseAdapter;
    private ReviewRecyclerViewAdapter mReviewAdapter;

    public ShopInfoActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        mRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
//        mQuotationShop = (StoreNPredictionEntity) estimateStatus.getSerializableExtra("shop_prediction_info");
        mShopInfo = (ShopInfo) estimateStatus.getSerializableExtra("shop_info");

        Log.d(TAG, "Request Estimate::" + mRequestEstimate);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_info, container, false);
        mBinding.setShopInfo(mShopInfo);
        mBinding.setHandler(this);
        mBinding.shopLocation.onCreate(savedInstanceState);
        mBinding.shopLocation.getMapAsync(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCaseAdapter = new SuriCaseRecyclerViewAdapter(this, true);
        mBinding.listviewCase.setAdapter(mCaseAdapter);
        mBinding.listviewCase.addItemDecoration(new HorizonListVewDecorate(getContext(), 19, 19));
        getAllSuriCaseByStoreId(String.valueOf(mShopInfo.storeId));

        mReviewAdapter = new ReviewRecyclerViewAdapter(null, false);
        mBinding.listviewReview.setAdapter(mReviewAdapter);
        mBinding.listviewReview.addItemDecoration(new ListVewDecorate(getContext(), 24, 12));
        getAllReviewByStoreId(String.valueOf(mShopInfo.storeId));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(this);

        if (!mShopInfo.latitude.isEmpty() && !mShopInfo.longitude.isEmpty()) {
            LatLng location = new LatLng(Double.valueOf(mShopInfo.latitude), Double.valueOf(mShopInfo.longitude));
            mMap.addMarker(new MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_normal_shop)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Intent shopMap = new Intent(getActivity(), ShopLocationActivity.class);
        shopMap.putExtra("name", mShopInfo.storeName);
        shopMap.putExtra("location", latLng);
        startActivity(shopMap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_chat: {
                makeChat();
                break;
            }

            case R.id.btn_call: {
                makeCall();
                break;
            }

            case R.id.btn_desc_all: {
                ((ShopInfoActivity) getActivity()).showShopProfile();
                break;
            }

            case R.id.btn_review_all: {
//                ((ShopInfoActivity)getActivity()).showAllReviews();
                Intent reviewIntent = new Intent(getActivity(), ShopInfoAllReviewActivity.class);
                reviewIntent.putExtra("request_estimate", mRequestEstimate);
                reviewIntent.putExtra("shop_info", mShopInfo);
                startActivity(reviewIntent);
                break;
            }

            case R.id.btn_case_all: {
//                ((ShopInfoActivity) getActivity()).showAllCase();
                Intent caseIntent = new Intent(getActivity(), ShopInfoAllCaseActivity.class);
                caseIntent.putExtra("request_estimate", mRequestEstimate);
                caseIntent.putExtra("shop_info", mShopInfo);
                startActivity(caseIntent);


                break;
            }
        }
    }

    @SuppressLint("NewApi")
    private void makeCall() {
        String tel = "tel:" + mShopInfo.storeNumber;
        if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        String user = UserInfoSharedPreferences.getInstance(getContext()).getUserId();
        String store = String.valueOf(mShopInfo.storeId);
        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(store)) {
            updateCountingForCall(user, store);
        }

        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(tel)));

    }

    private void makeChat() {
        Intent goChat = new Intent(getActivity(), WebChatActivity.class);
        goChat.putExtra("request_estimate", mRequestEstimate);
        goChat.putExtra("target", mShopInfo.storeManagerId);
        startActivity(goChat);
    }

    private void getAllSuriCaseByStoreId(String storeId) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetAllSuriCase(cookie, storeId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                        mBinding.titleCase.setText(String.format(getString(R.string.label_suri_case), String.valueOf(page.count)));
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<SuriCaseEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List itemList = gson.fromJson(asJsonArray, itemType);
                    if (!itemList.isEmpty()) {
                        Log.d(TAG, "SuriCaseEntity size::" + itemList.size());
                        mBinding.listviewCaseEmpty.setVisibility(View.GONE);
                        mBinding.listviewCase.setVisibility(View.VISIBLE);
                        mCaseAdapter.setItemList(itemList);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetAllSuriCase onFailure");
            }
        });
    }

    private void getAllReviewByStoreId(String storeId) {
        ReviewAPI reivewAPI = ApiManager.getProvider(ReviewAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = reivewAPI.doPostGetReviewByStoreId(cookie, storeId, null);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseReviews result = gson.fromJson(json, ResponseReviews.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                        mBinding.titleReviews.setText(String.format(getString(R.string.label_review), String.valueOf(page.count)));
                    }

                    List itemList = result.getReviewEntities();
                    if (!itemList.isEmpty()) {
                        Log.d(TAG, "ReviewEntity size::" + itemList.size());
                        mBinding.listviewReviewEmpty.setVisibility(View.GONE);
                        mBinding.listviewReview.setVisibility(View.VISIBLE);
                        mReviewAdapter.setItemList(itemList);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetReviewByStoreId onFailure");
            }
        });
    }

    // counting phoneCall, no responseBody
    private void updateCountingForCall(String userId, String storeId) {
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<Void> call = userAPI.doPhoneCall(cookie, userId, storeId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful()) {

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPhoneCall onFailure");
            }
        });
    }

    @Override
    public void onCaseClick(SuriCaseEntity item) {
        Intent caseDetail = new Intent(getActivity(), SuriCaseDetailsActivity.class);
        caseDetail.putExtra("case", item);
        startActivity(caseDetail);
    }
}
