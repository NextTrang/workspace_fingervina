package com.snstfinger.suri.data.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.snstfinger.suri.data.datasource.InboxDao;
import com.snstfinger.suri.data.datasource.SuriDatabase;
import com.snstfinger.suri.data.entity.InboxEntity;

import java.util.List;

public class InboxRepository {
    private static final String TAG = InboxRepository.class.getName();

    private Context mCtx;
    private SuriDatabase mDB;
    private InboxDao mDao;
    private LiveData<List<InboxEntity>> mAllNotice;

    public InboxRepository(Context context) {
        this.mCtx = context;
        this.mDB = SuriDatabase.getSuriDatabase(context);
        this.mDao = mDB.getInboxDao();
        this.mAllNotice = mDao.getAll();
    }

    public LiveData<List<InboxEntity>> getAll() {
        return mAllNotice;
    }

    public void insert(InboxEntity notice) {
        new InsertAsyncTask(mDao).execute(notice);
    }

    public void update(InboxEntity notice) {
        mDao.update(notice);
    }

    public void delete(InboxEntity... notice) {
        new DeleteAsyncTask(mDao).execute(notice);
    }

    public static class InsertAsyncTask extends AsyncTask<InboxEntity, Void, Void> {
        private InboxDao mDao;

        public InsertAsyncTask(InboxDao dao) {
            this.mDao = dao;
        }

        @Override
        protected Void doInBackground(InboxEntity... inboxEntities) {
            mDao.insert(inboxEntities[0]);

            return null;
        }
    }

    public static class DeleteAsyncTask extends AsyncTask<InboxEntity, Void, Void> {
        private InboxDao mDao;

        public DeleteAsyncTask(InboxDao dao) {
            this.mDao = dao;
        }

        @Override
        protected Void doInBackground(InboxEntity... inboxEntities) {
            for(InboxEntity item : inboxEntities) {
                mDao.delete(item);
            }

            return null;
        }
    }

//    public MutableLiveData<List<SuriCaseEntity>> getAllSuriCaseLiveData(String storeId) {
//        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);
//        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
//        Call<CommonResponse> call = storeAPI.doPostGetAllSuriCase(cookie, storeId, null);
//        call.enqueue(new Callback<CommonResponse>() {
//            @Override
//            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//
//                if (response.isSuccessful()) {
//                    CommonResponse res = response.body();
//                    Object data = res.getData();
//                    Log.d(TAG, "res::code::" + res.getStatus());
//
//                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//
//                        @Override
//                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
//                                throws JsonParseException {
//                            try {
//                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
//                                return m_ISO8601Local.parse(json.getAsString());
//                            } catch (ParseException e) {
//                                return null;
//                            }
//                        }
//                    }).create();
//                    Object paging = ((LinkedTreeMap) data).get("pageMap");
//                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
//                    PageMap page = gson.fromJson(json, PageMap.class);
//                    if (page != null) {
//                        Log.d(TAG, "PageInfo::" + json);
//                    }
//
//                    Object info = ((LinkedTreeMap) data).get("Info");
//                    Type itemType = new TypeToken<List<SuriCaseEntity>>() {
//                    }.getType();
//                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
//                    caseEntities = gson.fromJson(asJsonArray, itemType);
//                    if (!caseEntities.isEmpty()) {
//                        Log.d(TAG, "SuriCaseEntity size::" + caseEntities.size());
//                        mutableLiveData.setValue(caseEntities);
//                    }
//
//                } else {
//                    Log.e(TAG, response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommonResponse> call, Throwable t) {
//                call.cancel();
//                Log.e(TAG, "doPostGetAllSuriCase onFailure");
//            }
//        });
//
//        return mutableLiveData;
//    }
}
