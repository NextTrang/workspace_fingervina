package com.snstfinger.suri.ui.workingStatus;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.ui.shop.ShopInfoActivity;

/**
 * 수리완료된 견적에 대한 리뷰작성
 */
public class WriteReviewActivity extends AppCompatActivity implements QuotationShopListFragment.OnListFragmentInteractionListener {
    private static final String TAG = WriteReviewActivity.class.getName();

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;

    private FragmentManager mFragmentManager;

    private ReviewFormViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_review);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent estimateStatus = getIntent();
        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Completed RequestEstimate::" + mSelectedRequestEstimate);

        model = ViewModelProviders.of(this).get(ReviewFormViewModel.class);
        if (mSelectedRequestEstimate != null) {
            // enum('REQUEST','BOOKING','BOOKING_CONFIRM','SENDING_PAYMENT','REPAIR_STANDBY','REPAIR','SENDING_PAYMENT_UPDATE','COMPLETE','REVIEW')
            switch (mSelectedRequestEstimate.getStatusCode()) {
                default: {
                    actionBar.setTitle("Rate your SURI");
                    break;
                }
            }

            model.initReviewForm(mSelectedRequestEstimate);
        }

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new WriteReviewFragment()).commit();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(StoreNPredictionEntity item) {
        Log.d(TAG, "store clicked::" + item.storeId);
        Intent statusDetail = new Intent(this, ShopInfoActivity.class);
        statusDetail.putExtra("request_estimate", mSelectedRequestEstimate);
        statusDetail.putExtra("shop_info", item);
        startActivity(statusDetail);
    }

    public void showReviewComment() {
        mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new WriteReviewPhotoFragment()).addToBackStack(null).commit();
    }

}
