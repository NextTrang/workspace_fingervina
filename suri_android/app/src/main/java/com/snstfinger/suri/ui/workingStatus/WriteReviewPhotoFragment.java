package com.snstfinger.suri.ui.workingStatus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.model.ReviewForm;
import com.snstfinger.suri.databinding.FragmentWriteReviewPhotoBinding;
import com.snstfinger.suri.ui.tools.MyPicker;

import java.util.Arrays;
import java.util.List;

public class WriteReviewPhotoFragment extends Fragment implements View.OnClickListener {
    private final String TAG = WriteReviewPhotoFragment.class.getName();
    private final int PICKER_REQUEST_CODE = 1010;

//    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    private FragmentWriteReviewPhotoBinding mBinding;

    private ReviewFormViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_write_review_photo, container, false);
        mBinding.setHandler(this);
//        Intent estimateStatus = getActivity().getIntent();
//        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
//        Log.d(TAG, "Completed RequestEstimate::" + mSelectedRequestEstimate);
//
//        ReviewForm reviewForm = getReviewForm(mSelectedRequestEstimate);
//        mBinding.setReviewForm(reviewForm);


        model = ViewModelProviders.of(getActivity()).get(ReviewFormViewModel.class);
        model.getReviewForm().observe(this, new Observer<ReviewForm>() {
            @Override
            public void onChanged(ReviewForm reviewForm) {
                mBinding.setReviewForm(reviewForm);
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void takeMultiAlbum() {
        MyPicker picker = new MyPicker();
        picker.limit(2);
        picker.requestCode(PICKER_REQUEST_CODE).withFragment(this).show();
    }

    @Override
    public void onClick(View v) {
        if (v instanceof CheckedTextView) {
            ((CheckedTextView) v).toggle();
        } else {
            switch (v.getId()) {
                case R.id.icon_add_photo: {
//                Intent camera = new Intent(getActivity(), SuriCamera.class);
//                startActivity(camera);
                    takeMultiAlbum();

                    break;
                }

                case R.id.btn_confirm: {
//                    ReviewForm form = mBinding.getReviewForm();
//                    Log.d(TAG, "ReviewEntity::"+form.getReviewComment());

                    reviewComfirm();

                    break;
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case PICKER_REQUEST_CODE: {
                String pathsList[] = data.getExtras().getStringArray(MyPicker.IMAGES_RESULT); // return list of selected images paths.
                Log.d(TAG, "Number of selected Images: " + pathsList.length);
                List<String> items = Arrays.asList(pathsList);
                if (!items.isEmpty()) {
                    String imgPath = items.get(0);
                    if(!TextUtils.isEmpty(imgPath)) {
//                        Bitmap bitmap = BitmapFactory.decodeFile(imgPath);
//                        int exifDegree = Utils.getOrientationOfImage(imgPath);
//                        bitmap = Utils.getRotatedBitmap(bitmap, exifDegree);
//                        mBinding.reviewPhoto1.setImageBitmap(bitmap);

                        mBinding.getReviewForm().setReviewImage1(imgPath);
                    }

                    if(items.size() > 1) {
                        imgPath = items.get(1);
                        if(!TextUtils.isEmpty(imgPath)) {
//                            Bitmap bitmap = BitmapFactory.decodeFile(imgPath);
//                            int exifDegree = Utils.getOrientationOfImage(imgPath);
//                            bitmap = Utils.getRotatedBitmap(bitmap, exifDegree);
//                            mBinding.reviewPhoto2.setImageBitmap(bitmap);

                            mBinding.getReviewForm().setReviewImage2(imgPath);
                        }
                    }
                }

                break;
            }
        }
    }

    private int isValidateForm() {
        ReviewForm form = mBinding.getReviewForm();
        String comment = form.getReviewComment();
        String img1 = form.getReviewImage1();
        String img2 = form.getReviewImage2();

        if (TextUtils.isEmpty(comment)) {
            return R.string.error_no_comment;
        }

        if (TextUtils.isEmpty(img1) || TextUtils.isEmpty(img2)) {
            return R.string.error_no_image2;
        }

        return 0;
    }

    private void reviewComfirm() {
        int errorCode = isValidateForm();
        if (errorCode != 0) {
            Snackbar.make(mBinding.btnConfirm, errorCode, Snackbar.LENGTH_LONG).show();
            return;
        }

        getFragmentManager().popBackStack();
    }

}
