package com.snst.suripartner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.widget.GridView;
import android.widget.Toast;

import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Command;
import com.snst.suripartner.lib.grid.GridAdapter;
import com.snst.suripartner.lib.grid.GridModel;
import com.snst.suripartner.lib.gallery_cmd.CloseGallery;
import com.snst.suripartner.lib.gallery_cmd.Gallery;
import com.snst.suripartner.lib.gallery_cmd.GalleryInvoker;

import java.io.File;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    private static final String TAG = "GalleryActivity";
    ArrayList<GridModel> gridModelList = new ArrayList<>();
    GridView gridView;
    GridAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        gridView = findViewById(R.id.image_grid);

        gridModelList.add(new GridModel(null));
        gridModelList.addAll(imageReader(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Suri")));
        gridModelList.addAll(imageReader(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera")));
        gridAdapter = new GridAdapter(GalleryActivity.this, gridModelList);
        gridView.setAdapter(gridAdapter);
        Toast.makeText(this, "total picture: " + gridAdapter.getCount(), Toast.LENGTH_LONG).show();
    }

    private ArrayList<GridModel> imageReader(File externalStorageDirectory) {

        ArrayList<GridModel> result = new ArrayList<>();

        if (!externalStorageDirectory.exists()) {
            externalStorageDirectory.mkdir();
        }

        File[] files = externalStorageDirectory.listFiles();

        for (int i = 0; i < files.length; ++i) {
            if (files[i].isDirectory()) {
                result.addAll(imageReader(files[i]));
            } else if (files[i].getName().endsWith(".jpg")) {
                result.add(new GridModel(files[i]));
            }
        }

        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Gallery gallery = new Gallery()
                    .setRequestCode(requestCode)
                    .setData(data)
                    .setOnClickCamera(requestCode == LbPop.LbCode.CAMERA_REQUEST);
            Command close = new CloseGallery(GalleryActivity.this, gallery);
            GalleryInvoker invoker = new GalleryInvoker(null, close);
            invoker.doClose();
        }
    }
}
