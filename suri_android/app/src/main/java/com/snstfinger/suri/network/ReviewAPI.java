package com.snstfinger.suri.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ReviewAPI {
    @FormUrlEncoded
    @POST("review/getReviewByStoreId")
    Call<CommonResponse> doPostGetReviewByStoreId(@Header("Cookie") String cookie, @Field("store_id") String storeId, @Field("page") String page);

    @FormUrlEncoded
    @POST("review/getAllRecentReviews")
    Call<CommonResponse> doPostGetAllRecentReviews(@Field("page") String page);

}
