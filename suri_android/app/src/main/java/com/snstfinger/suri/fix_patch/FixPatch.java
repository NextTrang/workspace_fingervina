package com.snstfinger.suri.fix_patch;

import java.util.Arrays;
import java.util.List;

public class FixPatch {
    private static FixPatch _fixPatch = null;

    public static FixPatch Instance() {
        if (_fixPatch == null) {
            _fixPatch = new FixPatch();
        }

        return _fixPatch;
    }

    private String statusOfShop = "";
    private List<String> refreshActivities = Arrays.asList("QuotationGoingActivity");

    //Getter
    public String getStatusOfShop() {
        return statusOfShop;
    }

    public List<String> getRefreshActivities() {
        return refreshActivities;
    }

    //Setter
    public void setStatusOfShop(String statusOfShop) {
        this.statusOfShop = statusOfShop;
    }

    //Method
    public void reset(){
        statusOfShop = "";
    }
}
