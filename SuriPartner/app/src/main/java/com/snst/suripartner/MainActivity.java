package com.snst.suripartner;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.snst.suripartner.databinding.MainActivityBinding;
import com.snst.suripartner.databinding.NavMainBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.api.RequestData;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.ViewPagerAdapter;
import com.snst.suripartner.lib.booking.Booking;
import com.snst.suripartner.lib.booking.BookingEvent;
import com.snst.suripartner.lib.left_menu.LeftMenu;
import com.snst.suripartner.lib.payment.Payment;
import com.snst.suripartner.lib.payment.PaymentEvent;
import com.snst.suripartner.lib.quotation.Quotation;
import com.snst.suripartner.lib.quotation.QuotationDetailEvent;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriEvent;
import com.snst.suripartner.lib.suri_complete.SuriComplete;
import com.snst.suripartner.lib.suri_complete.SuriCompleteEvent;
import com.snst.suripartner.main_fragment.BookingFragment;
import com.snst.suripartner.main_fragment.ChatFragment;
import com.snst.suripartner.main_fragment.MyFragment;
import com.snst.suripartner.main_fragment.QuotationFragment;
import com.snst.suripartner.main_fragment.SuriFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private static int tabPosition = 0;
    private int requestedPage = 1;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setEffect(Sharing.Instance().getEffect());

        if(!Sharing.Instance().isLoadDataFinish()){
            new RequestData().execute(this, Sharing.Instance().getUser().getStoreId(), String.valueOf(requestedPage));
        }

        binding();
        SetupUI();
        setOnClickTabs();
        setPermissions();
    }

    private void binding() {
        tabLayout = findViewById(R.id.tablayout_id);
        viewPager = findViewById(R.id.viewpager_id);
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        NavMainBinding navMainBinding = DataBindingUtil.bind(navigationView.getHeaderView(0));
        LeftMenu leftMenu = new LeftMenu()
                .setImageUrl(Sharing.Instance().getUser().getManagerPicture())
                .setStoreBusinessNumber(Sharing.Instance().getStoreInfo().getStoreBusinessNumber())
                .setNeedToSuri(Sharing.Instance().getStoreInfo().getSuriTodo())
                .setSuriComplete(Sharing.Instance().getStoreInfo().getSuriComplete());
        navMainBinding.setLeftMenu(leftMenu);
    }

    private void SetupUI() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        //Adding Fragment
        adapter.AddFragment(new QuotationFragment(), "QUOTATION");
        adapter.AddFragment(new ChatFragment(), "CHAT");
        adapter.AddFragment(new BookingFragment(), "BOOKING");
        adapter.AddFragment(new SuriFragment(), "SURI");

        //adapter setup
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(tabPosition).select();

    }

    private void setOnClickTabs() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                refresh();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void refresh() {
        Fragment fragment = adapter.getItem(tabPosition);
        ((MyFragment) fragment).refresh();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case LbPop.LbCode.CANCEL_BOOKING:
                    int popCode = data.getIntExtra(LbPop.LbKey.POP_CODE, -1);
                    if (popCode >= 0) {
                        //show popup of cancel reason from popManagerActivity
                        startActivityForResult(data, popCode);
                    }
                    break;
                case LbPop.LbCode.CANCEL_REASON:
                    Booking booking = (Booking) data.getSerializableExtra(LbPop.LbKey.BOOKING);
                    String reasonTypeId = data.getStringExtra(LbPop.LbKey.REASON_TYPE_ID);
                    String reasonComment = data.getStringExtra(LbPop.LbKey.REASON_COMMENT);
                    new BookingEvent(this).onCancel(booking, reasonTypeId, reasonComment);
                    break;
                case LbPop.LbCode.QUOTATION_DETAIL:
                    QuotationViewModel viewModel = (QuotationViewModel) data.getSerializableExtra(LbPop.LbKey.QUOTATION);
                    new QuotationDetailEvent(this).onOffer(new Quotation(viewModel));
                    break;
                case LbPop.LbCode.SEND_INVOICE:
                    Payment payment = (Payment) data.getSerializableExtra(LbPop.LbKey.PAYMENT);
                    new PaymentEvent(this).onSendInvoice(payment);
                    break;
                case LbPop.LbCode.SURI_ONGOING_DETAIL:
                    Suri suri = (Suri) data.getSerializableExtra(LbPop.LbKey.SURI);
                    if (suri != null) {
                        new SuriEvent(this).onUpdateRepair(suri);
                        break;
                    }

                    Payment paymentResend = (Payment) data.getSerializableExtra(LbPop.LbKey.PAYMENT);
                    if (paymentResend != null) {
                        new PaymentEvent(this).onResendInvoice(paymentResend);
                        break;
                    }

                    SuriComplete suriComplete = (SuriComplete) data.getSerializableExtra(LbPop.LbKey.SURI_COMPLETE);
                    if (suriComplete != null) {
                        new SuriCompleteEvent(this).onCompleteRepair(suriComplete);
                    }
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setPermissions() {
        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    public void openDrawerLayout(View view) {
        drawerLayout.openDrawer(Gravity.LEFT);
    }
}
