package com.snstfinger.suri.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.app.Application;
import android.util.Log;
import android.util.Patterns;

import com.snstfinger.suri.data.repository.LoginRepository;
import com.snstfinger.suri.data.model.Result;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.R;
import com.snstfinger.suri.network.LoginProvider;
import com.snstfinger.suri.network.SuriLoginManager;

public class LoginViewModel extends AndroidViewModel implements LoginProvider.OnResultLoginListener {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private SuriLoginManager loginManager;

    LoginViewModel(@NonNull Application application) {
        super(application);
        this.loginManager = SuriLoginManager.getInstance(application.getApplicationContext());
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login() {
        // can be launched in a separate asynchronous job
        loginManager.loginWithSNS(this);
    }

    public void register() {
        loginManager.registerSuriAccount(this);
    }

    public void loginDataChanged(String userEmail, String userName, String phoneNumber) {
        if (!isEmailValid(userEmail)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isNotEmpty(userName)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_empty, null));
        } else if (!isNotEmpty(phoneNumber)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_empty));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder userEmail validation check
    private boolean isEmailValid(String userEmail) {
        if (userEmail == null) {
            return false;
        }
        if (userEmail.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(userEmail).matches();
        } else {
            return !userEmail.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    // empty check!
    private boolean isNotEmpty(String value) {
        return value != null && !value.isEmpty();
    }

    @Override
    public void onResult(int result, LoggedInUser user) {
        if (result == SuriLoginManager.LOGIN_SUCCESS)
            loginResult.setValue(new LoginResult(new LoggedInUserView(user.getDisplayName())));
        else if (result == SuriLoginManager.ACCOUNT_DUPLICATE) {
            loginResult.setValue(new LoginResult(R.string.account_duplicate));
        } else
            loginResult.setValue(new LoginResult(R.string.login_failed));
    }
}
