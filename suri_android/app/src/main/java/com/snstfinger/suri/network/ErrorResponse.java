package com.snstfinger.suri.network;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.Date;

public class ErrorResponse {
    @SerializedName("timestamp")
    private Date timestamp;
    @SerializedName("status")
    private Integer status;
    @SerializedName("message")
    private String message;

    public Date getData() {
        return timestamp;
    }

    public void setData(Date data) {
        this.timestamp = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String msg) {
        this.message = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append(" date:");
        result.append(timestamp);
        result.append("Code:");
        result.append(status);
        result.append(" msg:");
        result.append(message);
        return result.toString();
    }
}
