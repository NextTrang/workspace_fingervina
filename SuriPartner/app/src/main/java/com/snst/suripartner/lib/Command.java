package com.snst.suripartner.lib;

public interface Command {
    void execute();
}
