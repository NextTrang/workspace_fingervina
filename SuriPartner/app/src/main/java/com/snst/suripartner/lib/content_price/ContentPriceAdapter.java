package com.snst.suripartner.lib.content_price;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.databinding.ContentPriceBinding;
import com.snst.suripartner.lib.helper.EditTextHandler;

import java.util.List;

public class ContentPriceAdapter extends RecyclerView.Adapter<ContentPriceAdapter.MyViewHolder> {

    private List<ContentPrice> mData;
    private LayoutInflater inflater;

    public ContentPriceAdapter(List<ContentPrice> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        ContentPriceBinding binding = ContentPriceBinding.inflate(inflater, parent, false);

        return new ContentPriceAdapter.MyViewHolder(parent.getContext(), binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ContentPriceBinding binding;
        private Context context;
        private EditText txtPrice;

        public MyViewHolder(Context context, ContentPriceBinding binding) {
            super(binding.getRoot());
            this.context = context;
            this.binding = binding;
        }

        public void bind(ContentPrice contentPrice) {
            binding.setContentPrice(contentPrice);
            txtPrice = binding.txtPrice;
            setEvent();
        }

        private void setEvent(){
            txtPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    EditTextHandler.formatCurrency(txtPrice);
                }
            });
        }
    }
}
