package com.snst.suripartner.lib.content_price;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.BR;
import com.snst.suripartner.lib.helper.Conversion;

import java.io.Serializable;

public class ContentPrice extends BaseObservable implements Serializable {
    private String content = "";
    private String price = "";
    private String priceCurrency = "";

    //Getter
    @Bindable
    public String getContent() {
        return content;
    }

    @Bindable
    public String getPrice() {
        return price;
    }

    @Bindable
    public String getPriceCurrency() {
        return priceCurrency;
    }

    public ContentPrice setContent(String content) {
        this.content = content;
        notifyPropertyChanged(com.snst.suripartner.BR.content);
        return this;
    }

    public ContentPrice setPrice(String price) {
        this.price = price;
        this.priceCurrency = Conversion.number_2_currency(price);
        notifyPropertyChanged(BR.priceCurrency);
        return this;
    }

    public ContentPrice setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
        this.price = Conversion.currency_2_number(priceCurrency);
        notifyPropertyChanged(BR.priceCurrency);
        return this;
    }
}
