package com.snst.suripartner.lib.grid;

import android.widget.ImageView;

import java.io.File;

public class GridModel {
    private File imageFile;
    private ImageView imageView;

    public GridModel(File imageFile) {
        this.imageFile = imageFile;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
