package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbRequest;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.suri.SuriViewModel;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestUpdateBeforeCompleteRepair implements ApiRequest {
    private static final String TAG = "ReqBeforeCompleteRepair";

    @Override
    public void execute(final Context context, final String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());
        RetrofitClient.getInstance().getFlags().set(LbRequest.LbFlag.REQUEST_UPDATE_BEFORE_COMPLETE_REPAIR);

        //request update progress rate = 100
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateRepair(Sharing.Instance().getCookie(), inputs[0], "100");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String msg = jsonObject.getString(LbData.MSG);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (msg.equals("OK")) {
                        //update progress rate for UI
                        for (SuriViewModel viewModel : Sharing.Instance().getSuriViewModels()) {
                            if (viewModel.predictEstId.equals(inputs[0])) {
                                viewModel.progressRate = 100;
                                break;
                            }
                        }

                        new RequestCompleteRepair().execute(context, inputs);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "RequestUpdateBeforeCompleteRepair onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_UPDATE_BEFORE_COMPLETE_REPAIR)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_UPDATE_BEFORE_COMPLETE_REPAIR);
                }
            }
        });
    }
}
