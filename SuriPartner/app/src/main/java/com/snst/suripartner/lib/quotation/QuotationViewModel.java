package com.snst.suripartner.lib.quotation;

import android.view.View;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.BR;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.repair_type.RepairType;

import java.io.Serializable;
import java.util.List;

public class QuotationViewModel extends BaseObservable implements Serializable {

    public String imageUrl;
    public String name;
    public String timestamp;
    public String address;
    public String addressLatLng;
    public String id;
    public String state;
    public String damageDescription;
    public String brandName;
    public String modelName;
    @Bindable
    public long estCompletedDate;
    @Bindable
    public String estCompletedTime;
    public String estPrice;
    @Bindable
    public String estPriceCurrency;
    public String requestEstId;
    @Bindable
    public String predictionPriceComment;
    public String userId;
    public String desiredReservationDate;
    @Bindable
    public Boolean desiredReservationDateYn;

    public List<RepairType> estimateRepairTypes;

    private transient QuotationNavigator navigator;

    public int visible;
    public int visibleFadeCover;

    public QuotationViewModel(Quotation quotation) {
        this.imageUrl = quotation.getImageUrl();
        this.name = quotation.getName();
        this.timestamp = quotation.getTimestamp();
        this.address = quotation.getAddress();
        this.addressLatLng = quotation.getAddressLatLng();
        this.state = quotation.getState();
        this.id = quotation.getId();
        this.estimateRepairTypes = quotation.getEstimateRepairTypes();
        this.damageDescription = quotation.getDamageDescription();
        this.brandName = quotation.getBrandName();
        this.modelName = quotation.getModelName();
        this.estCompletedDate = quotation.getEstCompletedDate();
        this.estCompletedTime = quotation.getEstCompletedTime();
        setEstPrice(quotation.getEstPrice());
        this.requestEstId = quotation.getRequestEstId();
        this.predictionPriceComment = quotation.getPredictionPriceComment();
        this.userId = quotation.getUserId();
        this.desiredReservationDate = quotation.getDesiredReservationDate();
        this.desiredReservationDateYn = quotation.getDesiredReservationDateYn();

        if (quotation.getState().equals(LbState.OFFERED)) {
            setVisible(View.GONE);
            setVisibleFadeCover(View.VISIBLE);
        } else {
            setVisible(View.VISIBLE);
            setVisibleFadeCover(View.GONE);
        }
    }

    public void onItemClick(QuotationViewModel viewModel) {
        navigator.onItemClick(viewModel);
    }

    //Getter

    public String getImageUrl() {
        return imageUrl;
    }

    //Setter

    public void setNavigator(QuotationNavigator navigator) {
        this.navigator = navigator;
    }

    public void setVisible(int visible) {
        this.visible = visible;
        notifyChange();
    }

    public void setVisibleFadeCover(int visibleFadeCover) {
        this.visibleFadeCover = visibleFadeCover;
        notifyChange();
    }

    public void setEstPrice(String estPrice) {
        this.estPrice = estPrice;
        this.estPriceCurrency = Conversion.number_2_currency(estPrice);
        notifyPropertyChanged(BR.estPriceCurrency);
    }

    public void setEstPriceCurrency(String estPriceCurrency) {
        this.estPriceCurrency = estPriceCurrency;
        this.estPrice = Conversion.currency_2_number(estPriceCurrency);
        notifyPropertyChanged(BR.estPriceCurrency);
    }

    public void setPredictionPriceComment(String predictionPriceComment) {
        this.predictionPriceComment = predictionPriceComment;
        notifyPropertyChanged(BR.predictionPriceComment);
    }

    public void setDesiredReservationDateYn(Boolean desiredReservationDateYn) {
        this.desiredReservationDateYn = desiredReservationDateYn;
        notifyPropertyChanged(BR.desiredReservationDateYn);
    }

    public void setEstCompletedDate(long estCompletedDate) {
        this.estCompletedDate = estCompletedDate;
        notifyPropertyChanged(BR.estCompletedDate);
    }

    public void setEstCompletedTime(String estCompletedTime) {
        this.estCompletedTime = estCompletedTime;
        notifyPropertyChanged(BR.estCompletedTime);
    }

    //Method
    public void updateCost() {
        this.estPrice = Conversion.currency_2_number(estPriceCurrency);
    }
}
