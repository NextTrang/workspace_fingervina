package com.snstfinger.suri.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NotificationAPI {
    @FormUrlEncoded
    @POST("user/sendNotificationByUserId")
    Call<CommonResponse> doPostSendNotificationByUserId(@Header("Cookie") String cookie, @Field("user_id") String userId, @Field("title") String title, @Field("message") String message);
}
