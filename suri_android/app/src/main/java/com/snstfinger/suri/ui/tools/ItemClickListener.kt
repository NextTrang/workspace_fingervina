package com.snstfinger.suri.ui.tools

internal interface ItemClickListener {
    fun onItemClicked(position: Int)
}