package com.snstfinger.suri.ui.tools;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.snstfinger.suri.R;

public class SuriCamera extends AppCompatActivity implements View.OnClickListener {

    private int mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private SuriCameraPreview mSuriCameraPreview = null;

    private int RESULT_PERMISSIONS = 100;
    private FrameLayout mPreviewLayout;

    String[] REQUIRED_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suri_camera);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mPreviewLayout = findViewById(R.id.layout_camera_preview);

        Button btnPhoto = findViewById(R.id.btn_photo);
        btnPhoto.setOnClickListener(this);
        Button btnVideo = findViewById(R.id.btn_video);
        btnVideo.setOnClickListener(this);
        ImageButton btnGallery = findViewById(R.id.btn_gallery);
        btnGallery.setOnClickListener(this);
        ImageButton btnShot = findViewById(R.id.btn_shot);
        btnShot.setOnClickListener(this);
        ImageButton btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();

        requestPermissionCamera();
    }

    private void initPreview() {
        mSuriCameraPreview = new SuriCameraPreview(this, mCameraId);
        mPreviewLayout.addView(mSuriCameraPreview);
    }

    private void filpCamera() {
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

        } else {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }

        mPreviewLayout.removeAllViews();

        initPreview();
    }

    public boolean requestPermissionCamera() {

        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion >= Build.VERSION_CODES.M) {

            int permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permissionCamera == PackageManager.PERMISSION_GRANTED && permissionStorage == PackageManager.PERMISSION_GRANTED) {
                initPreview();
            } else {
                ActivityCompat.requestPermissions(SuriCamera.this, REQUIRED_PERMISSIONS, RESULT_PERMISSIONS);

            }
        } else {  // version 6 이하일때

            initPreview();
            return true;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (RESULT_PERMISSIONS == requestCode) {

            boolean check_result = true;

            for(int result : grantResults) {
                if(result != PackageManager.PERMISSION_GRANTED) {
                    check_result = false;

                    break;
                }
            }

            if (check_result) {
                // 권한 허가시
                initPreview();

            } else {
                // 권한 거부시
                finish();
            }
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();

                return true;
            }

            case R.id.action_filter: {

                return true;
            }

            case R.id.action_flash: {

                return true;
            }

            case R.id.action_filp: {
                filpCamera();

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_photo: {
                setResult(RESULT_OK);
                finish();

                break;
            }

            case R.id.btn_video: {

                break;
            }
            case R.id.btn_gallery: {
                setResult(RESULT_OK);
                finish();

                break;
            }

            case R.id.btn_shot: {
                mSuriCameraPreview.takePicture();

                break;
            }

            case R.id.btn_next: {
                setResult(RESULT_OK);
                finish();

                break;
            }
        }
    }
}
