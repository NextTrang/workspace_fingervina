package com.snstfinger.suri.data.datasource;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.snstfinger.suri.data.entity.InboxEntity;

import java.util.ArrayList;
import java.util.List;

@Database(entities = {InboxEntity.class}, version = 2)
public abstract class SuriDatabase extends RoomDatabase {
    public abstract InboxDao getInboxDao();

    private static SuriDatabase INSTANCE;

    public static SuriDatabase getSuriDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (SuriDatabase.class) {
                if (INSTANCE == null)
                    INSTANCE = Room.databaseBuilder(context, SuriDatabase.class, "suri-db")
                            .fallbackToDestructiveMigration()
                            .addCallback(dbCallback)
                            .build();
            }
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private static RoomDatabase.Callback dbCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            new PopulateData(INSTANCE).execute();
        }
    };

    // dummy for test
    private static class PopulateData extends AsyncTask<Void, Void, Void> {
        private final InboxDao mDao;
        List<InboxEntity> data = new ArrayList<>();

        PopulateData(SuriDatabase db) {
            data.add(new InboxEntity("test1", "test", false));
            data.add(new InboxEntity("test2", "testtestest", false));
            data.add(new InboxEntity("test3", "testtesadsfatest", true));

            mDao = db.getInboxDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d("DATABASE", "INSERT TEST DATA");

            if (mDao.getAnyData().length < 1) {
                for (InboxEntity item : data) {
                    mDao.insert(item);
                }
            }

            return null;
        }
    }
}
