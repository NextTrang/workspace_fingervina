package com.snstfinger.suri.data.datasource;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.snstfinger.suri.data.entity.InboxEntity;

import java.util.List;

@Dao
public interface InboxDao {
    @Query("select * from inbox LIMIT 1")
    InboxEntity[] getAnyData();

    @Query("select * from inbox order by id desc")
    LiveData<List<InboxEntity>> getAll();


    @Query("delete from inbox")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(InboxEntity inboxEntity);

    @Update
    void update(InboxEntity inboxEntity);

    @Delete
    void delete(InboxEntity inboxEntity);
}
