package com.snstfinger.suri.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.snstfinger.suri.data.model.HomeSuriStatus;

public class HomeStatusViewModel extends AndroidViewModel {
    private MutableLiveData<HomeSuriStatus> mSuriStatus = new MutableLiveData<>();

    public HomeStatusViewModel(@NonNull Application application) {
        super(application);
        mSuriStatus.setValue(new HomeSuriStatus());
    }

    public LiveData<HomeSuriStatus> getSuriStatus() {
        return mSuriStatus;
    }
}
