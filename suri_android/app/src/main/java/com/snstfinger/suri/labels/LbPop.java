package com.snstfinger.suri.labels;

public class LbPop {
    public static class LbKey {
        public static final String POP_CODE = "PopCode";
    }

    public static class LbCode {
        public final static int FILTER_SURI_CASE = 1;
    }
}
