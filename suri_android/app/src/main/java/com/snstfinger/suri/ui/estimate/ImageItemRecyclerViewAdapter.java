package com.snstfinger.suri.ui.estimate;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.snstfinger.suri.R;

import java.util.List;

public class ImageItemRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean mIsFilePath;
    private List<String> mValues;

    public ImageItemRecyclerViewAdapter(List items, boolean isFilePath) {
        mValues = items;
        mIsFilePath = isFilePath;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewpager_item, parent, false);

        return new ImageItemViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        String imgPath = mValues.get(position);
        if(mIsFilePath) {
            Bitmap bitmap = BitmapFactory.decodeFile(imgPath);
            ((ImageItemViewHoler) holder).itemImage.setImageBitmap(bitmap);
        } else {
            String BASE_URL = "http://101.99.34.53:1114/repairsystem";
            Glide.with(((ImageItemViewHoler) holder).itemImage).load(BASE_URL+imgPath).into(((ImageItemViewHoler) holder).itemImage);
        }

        ((ImageItemViewHoler) holder).indicator.setText((position+1) + " / " + mValues.size());

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ImageItemViewHoler extends RecyclerView.ViewHolder {
        private View mView;
        private ImageView itemImage;
        private TextView indicator;

        public ImageItemViewHoler(@NonNull View view) {
            super(view);
            mView = view;
            itemImage = view.findViewById(R.id.imageView);
            indicator = view.findViewById(R.id.indicator);
        }
    }
}
