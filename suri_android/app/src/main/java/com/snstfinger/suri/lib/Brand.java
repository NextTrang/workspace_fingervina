package com.snstfinger.suri.lib;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Brand {
    private static final String TAG = "Brand";

    private String id = "";
    private String name = "";
    private String objectId = "";

    public Brand(JSONObject jsInfo) {
        try {
            id = jsInfo.getString("brand_id");
            name = jsInfo.getString("brand_name");
            objectId = jsInfo.getString("object_id");
        } catch (JSONException e) {
            Log.e(TAG, "Brand: " + e.getMessage());
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObjectId() {
        return objectId;
    }
}
