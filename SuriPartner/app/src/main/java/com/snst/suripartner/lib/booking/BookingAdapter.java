package com.snst.suripartner.lib.booking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.databinding.BookingBinding;

import java.util.List;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {

    private List<BookingViewModel> mData;
    private LayoutInflater inflater;

    public BookingAdapter(List<BookingViewModel> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        BookingBinding bookingBinding = BookingBinding.inflate(inflater, parent, false);

        return new BookingAdapter.MyViewHolder(parent.getContext(), bookingBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }

        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private BookingBinding bookingBinding;
        private Context context;

        public MyViewHolder(Context context, BookingBinding bookingBinding) {
            super(bookingBinding.getRoot());
            this.context = context;
            this.bookingBinding = bookingBinding;
        }

        public void bind(BookingViewModel bookingViewModel) {
            bookingBinding.setBookingViewModel(bookingViewModel);
            bookingBinding.setBooking(new Booking(bookingViewModel));
            bookingBinding.setHandler(new BookingEvent(context));
        }
    }
}
