package com.snst.suripartner.lib;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.snst.suripartner.pop_up.PopQuotationDetailActivity;
import com.snst.suripartner.R;

public class CustomBinding {

    @BindingAdapter({"android:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        if (view.getContext().getClass() == PopQuotationDetailActivity.class) {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
        } else {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .thumbnail(0.1f)
                    .centerCrop()
                    .into(view);
        }
    }

    @BindingAdapter({"android:webChat"})
    public static void loadChatRoom(WebView view, String userId) {
        String toUserId = Sharing.Instance().getUser().getUserId();
        String url = "http://101.99.34.53:1114/repairsystem/chat/index?userId=" + userId + "&toUserId=" + toUserId + "&isStore=true";

        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }

    @BindingAdapter({"android:expandButton"})
    public static void loadExpandButton(ImageButton button, int state) {
        switch (state) {
            case View.VISIBLE:
                button.setBackgroundResource(R.drawable.arrow_up);
                break;
            case View.GONE:
                button.setBackgroundResource(R.drawable.arrow_down);
        }
    }

    @BindingAdapter({"android:expandBlackButton"})
    public static void loadExpandBlackButton(ImageButton button, int state) {
        switch (state) {
            case View.VISIBLE:
                button.setBackgroundResource(R.drawable.ic_arrow_up_black_24dp);
                break;
            case View.GONE:
                button.setBackgroundResource(R.drawable.ic_arrow_down_black_24dp);
            case View.INVISIBLE:
                button.setBackgroundResource(R.drawable.ic_arrow_down_black_24dp);
        }
    }

    @BindingAdapter({"android:onSingleClick"})
    public static void onSingleClick(View view, OnSingleClickListener clickListener) {
        view.setOnClickListener(clickListener);
    }
}
