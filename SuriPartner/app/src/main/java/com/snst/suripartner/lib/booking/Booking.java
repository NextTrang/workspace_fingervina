package com.snst.suripartner.lib.booking;

import android.util.Log;

import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.lib.helper.Getter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Booking implements Serializable {
    private static final String TAG = "Booking";
    private String imageUrl;
    private String deviceName;
    private String predictPrice;
    private String price;
    private String id;
    private String userName;
    private String phoneNumber;
    private String pickupAddress;
    private String predictionEstDate;
    private String bookingTime;

    //Using for send confirm
    private String predicEstId;

    public Booking(BookingViewModel viewModel) {
        this.imageUrl = viewModel.imageUrl;
        this.deviceName = viewModel.deviceName;
        this.price = viewModel.price;
        this.id = viewModel.id;
        this.userName = viewModel.userName;
        this.phoneNumber = viewModel.phoneNumber;
        this.pickupAddress = viewModel.pickupAddress;
        this.bookingTime = viewModel.bookingTime;
        this.predicEstId = viewModel.predicEstId;
        this.predictPrice = viewModel.predictPrice;
        this.predictionEstDate = viewModel.predictionEstDate;
    }

    public Booking(JSONObject jsInfo) {
        try {
            imageUrl = LbConst.BASE_LINK  + jsInfo.getString(LbData.LbInfo.ESTIMATE_IMAGE_1);
            deviceName = jsInfo.getString(LbData.LbInfo.BRAND_NAME);
            predictPrice = jsInfo.getString(LbData.LbInfo.PREDICTION_PRICE);
            price = jsInfo.getString(LbData.LbInfo.PRICE);
            id = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            userName = jsInfo.getString(LbData.LbInfo.USER_NAME);
            phoneNumber = jsInfo.getString(LbData.LbInfo.BOOKER_PHONE_NUMBER);
            pickupAddress = jsInfo.getString(LbData.LbInfo.ESTIMATE_ADDRESS_1);
            predictionEstDate = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_DATE));
            bookingTime = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.BOOKING_DATE));
            predicEstId = jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_ID);
        } catch (JSONException e) {
            Log.e(TAG, "Booking: " + e.getMessage());
        }
    }

    //Getter

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getPredictPrice() {
        return predictPrice;
    }

    public String getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public String getPredictionEstDate() {
        return predictionEstDate;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public String getPredicEstId() {
        return predicEstId;
    }
}
