package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class UploadImageEntity {
    @SerializedName("object_id")
    public int objectId;

    @SerializedName("success0")
    public String imagePath1;

    @SerializedName("success1")
    public String imagePath2;

    @SerializedName("success2")
    public String imagePath3;

    @SerializedName("success3")
    public String imagePath4;

}
