package com.snstfinger.suri.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;

import androidx.fragment.app.DialogFragment;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;

public class FilterQuotationShopDialogFragment extends DialogFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
        private RequestNPredictionNStoreEntity mMySuri;
        private String mSortingCondition;

    public static FilterQuotationShopDialogFragment newInstance() {
        FilterQuotationShopDialogFragment fragment = new FilterQuotationShopDialogFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
//        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
//        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);
//        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_filter_quotation, null);
        root.findViewById(R.id.btn_ok).setOnClickListener(this);
        RadioGroup rg = root.findViewById(R.id.content);
        rg.setOnCheckedChangeListener(this);

        builder.setView(root);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE); // compatible 4.4
//        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    public void dismissDialog() {
        this.dismiss();

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_ok) {
            OnClickDialogListener listener = (OnClickDialogListener) getTargetFragment();
            listener.onClickDialog(mSortingCondition);
        }

        dismissDialog();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch(checkedId) {
            case R.id.sort_price:
                mSortingCondition = "ISNULL(prediction_price) ASC, prediction_price ASC";

                break;
            case R.id.sort_distance:
                mSortingCondition = "ISNULL(prediction_price) ASC, distance ASC";

                break;

            case R.id.sort_rate:
                mSortingCondition = "ISNULL(prediction_price) ASC, point_score DESC";

                break;

            case R.id.sort_review:
                mSortingCondition = "ISNULL(prediction_price) ASC, total_review DESC";
                break;

            default:
                mSortingCondition = "ISNULL(prediction_price) ASC, point_score DESC";

                break;
        }
    }

    public interface OnClickDialogListener {
        void onClickDialog(String condition);
    }
}