package com.snst.suripartner.lib.map;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentActivity;

public class MyMapEvent {
    Context context;

    public MyMapEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((FragmentActivity) context).finish();
    }

    public void onMoveToMyLocation(MyMap myMap) {
        ((SuriMap) context).moveTo(myMap.getMyLocation().getLatLng());
    }

    public void onShowDirections(MyMap myMap){
        ((SuriMap) context).calculateDirections(myMap.getMyLocation().getLatLng(),myMap.getDestination().getLatLng());
    }
}
