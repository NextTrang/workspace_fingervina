package com.snstfinger.suri.ui.workingStatus;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.snstfinger.suri.MyFragment;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.SuriStoryEntity;
import com.snstfinger.suri.databinding.FragmentQuotationGoingBinding;
import com.snstfinger.suri.fix_patch.FixPatch;

import java.util.List;

/**
 * 예약한 상점의 작업진행 상세화면(인보이스상태부터 완료까지)
 */
public class QuotationGoingActivityFragment extends Fragment implements MyFragment {
    public static final String TAG = QuotationGoingActivityFragment.class.getName();

    private RequestNPredictionNStoreEntity mQuotationShop;
    private FragmentQuotationGoingBinding mBinding;

    public QuotationGoingActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent estimateStatus = getActivity().getIntent();
        mQuotationShop = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Request Estimate::" + mQuotationShop);

        SuriStoryViewModel storyViewModel = ViewModelProviders.of(this).get(SuriStoryViewModel.class);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_quotation_going, container, false);
        mBinding.setMyQuotation(mQuotationShop);
        SuriStoryRecyclerViewAdapter adapter = new SuriStoryRecyclerViewAdapter();
        mBinding.listviewSuriHistory.setAdapter(adapter);

        storyViewModel.getAllStories(mQuotationShop.requestEstimateId).observe(this, new Observer<List<SuriStoryEntity>>() {
            @Override
            public void onChanged(List<SuriStoryEntity> suriStoryEntities) {
                adapter.setItemList(suriStoryEntities);
            }
        });

        mBinding.workProgress.postDelayed(new Runnable() {
            @Override
            public void run() {
                int width = mBinding.workProgress.getWidth();
                int progress = mQuotationShop.getProgressRate();
                int left = (int) ((width / 2) * (progress / 100f));
//                Log.d(TAG,"left::"+left);
                mBinding.progressTag.setPadding(left, 0, 0, 0);
            }
        }, 500);


        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void refresh() {
        if(!FixPatch.Instance().getStatusOfShop().isEmpty()){
            mQuotationShop.setStatusCode(FixPatch.Instance().getStatusOfShop());
            FixPatch.Instance().reset();
        }

        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(QuotationGoingActivityFragment.this)
                    .attach(QuotationGoingActivityFragment.this)
                    .commit();
        }
    }
}
