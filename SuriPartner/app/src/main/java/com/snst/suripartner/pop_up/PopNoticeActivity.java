package com.snst.suripartner.pop_up;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.PopNoticeLeftMenuBinding;
import com.snst.suripartner.lib.left_menu.LeftMenuEvent;

public class PopNoticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PopNoticeLeftMenuBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_notice);

        //binding data
        binding.setHandler(new LeftMenuEvent(this));
    }
}
