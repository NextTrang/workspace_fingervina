package com.snstfinger.suri.ui.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.snstfinger.suri.R;

import java.util.ArrayList;

public class SuriIndicator extends LinearLayout {

    private Context mContext = null;
    private int mDefaultIndicator = R.drawable.indicator_off;
    private int mSelectIndicator = R.drawable.indicator_on;

    private ImageView[] indicators;

    private float temp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4.5f, getResources().getDisplayMetrics());

    public SuriIndicator(Context context) {
        super(context);
        mContext = context;
    }

    public SuriIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void createIndicatorPanel(int count, int defaultIndex, int selectIndex, int position) {

        mDefaultIndicator = defaultIndex;
        mSelectIndicator = selectIndex;

        indicators = new ImageView[count];

        for (int i = 0; i < count; i++) {
            indicators[i] = new ImageView(mContext);
            indicators[i].setPadding((int) temp, 0, (int) temp, 0);

            this.addView(indicators[i]);
        }

        selectIndicator(position);

    }

    public void selectIndicator(int position) {
        for (int i = 0; i < indicators.length; i++) {
            ImageView indicator = indicators[i];
            if (i == position) {
                indicator.setImageResource(mSelectIndicator);
            } else {
                indicator.setImageResource(mDefaultIndicator);
            }

//            indicator.postInvalidate();
        }
    }
}
