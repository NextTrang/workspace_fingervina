package com.snst.suripartner.main_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.snst.suripartner.R;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.quotation.QuotationAdapter;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.lib.Sharing;

import java.util.ArrayList;
import java.util.List;

public class QuotationFragment extends Fragment implements MyFragment {
    View view;
    private RecyclerView recyclerView;
    private List<QuotationViewModel> mData = new ArrayList<>();
    private TabLayout tabLayout;

    private static int tabPosition = 0;

    public QuotationFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.quotation_fragment, container, false);
        recyclerView = view.findViewById(R.id.quotation_recyclerview);

        if (Sharing.Instance().isLoadDataFinish()) {
            mData = filter(tabPosition);
            QuotationAdapter quotationAdapter = new QuotationAdapter(getContext(), mData);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(quotationAdapter);
        }

        tabLayout = view.findViewById(R.id.tablayout_quotation);
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        tabLayout.addTab(tabLayout.newTab().setText(LbState.ONGOING));
        tabLayout.addTab(tabLayout.newTab().setText(LbState.OFFERED));
        tabLayout.getTabAt(tabPosition).select();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getQuotationViewModels();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Sharing.Instance().isLoadDataFinish()) {
            setOnClickTabs();
        }
    }

    private void setOnClickTabs() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                refresh();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private List<QuotationViewModel> filter(int filterCode) {
        List<QuotationViewModel> result = new ArrayList<>();

        switch (filterCode) {
            case 1://filter Ongoing
                for (QuotationViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.ONGOING)) {
                        result.add(viewModel);
                    }
                }
                break;
            case 2://filter Offered
                for (QuotationViewModel viewModel : mData) {
                    if (viewModel.state.equals(LbState.OFFERED)) {
                        result.add(viewModel);
                    }
                }
                break;
            default:
                result = mData;
                break;
        }

        return result;
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getQuotationViewModels();
        mData = filter(tabPosition);
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(QuotationFragment.this)
                    .attach(QuotationFragment.this)
                    .commit();
        }
    }
}
