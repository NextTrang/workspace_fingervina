package com.snstfinger.suri.data.model;

import android.text.TextUtils;

import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShopInfo implements Serializable {
    public int storeId;
    public String storeImage;
    public String storeName;
    public String pointScore;
    public String pointQuality;
    public String pointKindness;
    public String pointAccuracy;
    public String storeEngineer;
    public String distance;
    public String storeDescription;
    public String storeNumber;
    public String address;
    public String latitude;
    public String longitude;
    public String storeManagerId;

    public String predictionEstimateId;
    public String predictionPrice;
    public Date predictionEstimateDate;
    public String desiredReservationDateYN;

    public boolean isDesiredDate() {
        if(!TextUtils.isEmpty(desiredReservationDateYN)) {
            if(desiredReservationDateYN.equalsIgnoreCase("y")) {
                return true;
            }
        }

        return false;
    }

//    private boolean containsQuotation = false;

    private boolean isPremium = false;

//    public boolean isContainsQuotation() {
//        return containsQuotation;
//    }
//
//    public void setContainsQuotation(boolean quotation) {
//        containsQuotation = quotation;
//    }


    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public String getDistance() {
        DecimalFormat formatter = new DecimalFormat(Utils.FLOAT_FORMAT);
        return TextUtils.isEmpty(distance) ? "" : formatter.format(Double.parseDouble(distance));
    }

    public String getPredictionPriceFormatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return predictionPrice == null ? "0" : formatter.format(Float.parseFloat(predictionPrice));
    }

    public String getPredictionEstimateDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionEstimateDate == null ? "" : formatter.format(predictionEstimateDate);
    }

}
