package com.snstfinger.suri.network;

import com.snstfinger.suri.data.model.LoggedInUser;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserAPI {

    @GET("user/deleteUser")
    Call<CommonResponse> doGetDeleteUser(@Header("Cookie") String cookie, @Query(value="user_id",encoded = true) String userId);

    @GET("user/getAllUserInfo")
    Call<CommonResponse> doGetAllUser(@Header("Cookie") String cookie, @Query(value="page", encoded = true) String pageNo);

    @GET("user/getUserById")
    Call<CommonResponse> doGetUserId(@Header("Cookie") String cookie, @Query(value="page", encoded = true) String pageNo, @Query(value="id", encoded = true) String userId);

    @GET("user/getUserByName")
    Call<CommonResponse> doGetUserName(@Header("Cookie") String cookie, @Query(value="page", encoded = true) String pageNo, @Query(value="name", encoded = true) String userName);

    @FormUrlEncoded
    @POST("user/login")
    Call<CommonResponse> doLogin(@Field("user_id") String userId, @Field("user_pw") String userPw, @Field("login_kind") String loginType, @Field("fcm_token") String fcmToken);

    @POST("user/saveUser")
    Call<CommonResponse> doCreateUser(@Body LoggedInUser user);

    @POST("user/updateUser")
    Call<CommonResponse> doUpdateUser(@Body LoggedInUser user);

    // update phoneCall counting
    @FormUrlEncoded
    @POST("user/phoneCall")
    Call<Void> doPhoneCall(@Header("Cookie") String cookie, @Field("user_id") String userId, @Field("store_id") String storeId);
}
