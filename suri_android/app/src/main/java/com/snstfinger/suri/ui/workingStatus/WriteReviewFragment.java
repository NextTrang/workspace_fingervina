package com.snstfinger.suri.ui.workingStatus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.CheckedTextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RepairOptionEntity;
import com.snstfinger.suri.data.entity.RepairTypeEntity;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.UploadImageEntity;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.data.model.ReviewForm;
import com.snstfinger.suri.databinding.FragmentWriteReviewBinding;
import com.snstfinger.suri.fix_patch.FixPatch;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RepairTypeOptionAPI;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.ui.estimate.ImageItemRecyclerViewAdapter;
import com.snstfinger.suri.ui.tools.MyPicker;

import java.io.File;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WriteReviewFragment extends Fragment implements View.OnClickListener {
    private final String TAG = WriteReviewFragment.class.getName();
    private final int PICKER_REQUEST_CODE = 1010;

    //    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    private FragmentWriteReviewBinding mBinding;

    private ReviewFormViewModel model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(ReviewFormViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_write_review, container, false);
        mBinding.setHandler(this);
//        Intent estimateStatus = getActivity().getIntent();
//        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
//        Log.d(TAG, "Completed RequestEstimate::" + mSelectedRequestEstimate);

        model.getReviewForm().observe(this, new Observer<ReviewForm>() {
            @Override
            public void onChanged(ReviewForm reviewForm) {
                mBinding.setReviewForm(reviewForm);
                mBinding.seekAccuracy.setEnabled(false);
            }
        });

//        ReviewForm reviewForm = getReviewForm(mSelectedRequestEstimate);
//        mBinding.setReviewForm(reviewForm);
//        mBinding.seekAccuracy.setEnabled(false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        if (v instanceof CheckedTextView) {
            ((CheckedTextView) v).toggle();
        } else {
            switch (v.getId()) {
                case R.id.input_review: {
//                    Intent goSearch = new Intent(getActivity(), SearchBrandModelActivity.class);
//                    goSearch.putExtra("request_form", mRequestForm);
//                    goSearch.putExtra("search_type", "model");
//                    startActivityForResult(goSearch, SuriApplication.REQ_SEARCH);

                    ((WriteReviewActivity) getActivity()).showReviewComment();
                    break;
                }

                case R.id.btn_write_review: {
                    ReviewForm form = mBinding.getReviewForm();
                    Log.d(TAG, "ReviewEntity::" + form.getTotalScore());
                    registerReview();
                    break;
                }
            }
        }
    }

    private int getObjectId(String name) {
        int objId = 1;

        switch (name.toLowerCase()) {
            case "phone":
                objId = 1;
                break;

            case "pc":
                objId = 2;
                break;

            case "bike":
                objId = 3;
                break;

            case "car":
                objId = 4;
                break;
        }

        return objId;
    }

    private String getObjectName(int id) {
        String name = getString(R.string.suggest_phone);

        switch (id) {
            case 1:
                name = getString(R.string.suggest_phone);
                break;

            case 2:
                name = getString(R.string.suggest_pc);
                break;

            case 3:
                name = getString(R.string.suggest_bike);
                break;

            case 4:
                name = getString(R.string.suggest_car);
                break;
        }

        return name;
    }

    private void getRepairType(RequestEstimate requestForm) {
        RepairTypeOptionAPI repairTypeOptionAPI = ApiManager.getProvider(RepairTypeOptionAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = repairTypeOptionAPI.doGetRepairTypeByObjectId(cookie, requestForm.getObjectId());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Object info = ((LinkedTreeMap) data).get("Info");

                    Gson gson = new Gson();
                    Type itemType = new TypeToken<List<RepairTypeEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    switch (requestForm.getObjectId()) {
                        case 1: {

                            break;
                        }

                        case 2: {

                            break;
                        }

                        case 3: {

                            break;
                        }

                        case 4: {

                            break;
                        }
                    }

                    Log.d(TAG, "res::code::" + res.getStatus());
                    Log.d(TAG, "res::data::" + data);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetRepairTypeByObjectId onFailure");
            }
        });
    }

    private void getRepairOption(int objectId) {
        RepairTypeOptionAPI repairTypeOptionAPI = ApiManager.getProvider(RepairTypeOptionAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = repairTypeOptionAPI.doGetRepairOptionByObjectId(cookie, objectId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Object info = ((LinkedTreeMap) data).get("Info");

                    Gson gson = new Gson();
                    Type itemType = new TypeToken<List<RepairOptionEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    Log.d(TAG, "res::code::" + res.getStatus());
                    Log.d(TAG, "res::data::" + data);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetRepairTypeByObjectId onFailure");
            }
        });

    }

    private void uploadingFile(List files) {
        {
            String filePath = (String) files.get(0);
            File file = new File(filePath);

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileBody);

            RequestEstimateAPI fileUploadAPI = ApiManager.getProvider(RequestEstimateAPI.class);
            Call<CommonResponse> call = fileUploadAPI.doPostUploadFile(part);
            call.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                    if (response.isSuccessful()) {
                        CommonResponse res = response.body();
                        Object data = res.getData();
                        String resString = data.toString();
                        Log.d(TAG, "res data::" + resString);
                        if (resString != null) {
                            String[] items = resString.split(",");
                            if (items.length > 1) {
                                String[] item = items[0].split(": ");
                                String object = item[1];
                                Log.d(TAG, "res data::object:: " + object);
//                                mRequestForm.setObjectId(getObjectId(object));
//                                showDetectDialog();
                            }
                        }

                        Log.d(TAG, "res::code::" + res.getStatus());
                        Log.d(TAG, "res::data::" + data);

                    } else {
                        Log.e(TAG, response.toString());
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    call.cancel();
                    Log.e(TAG, "doPostUploadFile onFailure");
                }
            });
        }

    }

    private void uploadingMultiFiles(List<String> files) {
        {
            MultipartBody.Part[] parts = new MultipartBody.Part[files.size()];
            if (!files.isEmpty()) {
                for (int idx = 0; idx < files.size(); idx++) {
                    File file = new File(files.get(idx));
                    if (file.exists()) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                        parts[idx] = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
                    }
                }
            }

            RequestEstimateAPI fileUploadAPI = ApiManager.getProvider(RequestEstimateAPI.class);
            Call<CommonResponse> call = fileUploadAPI.doPostUploadMultiFiles(parts);
            call.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                    if (response.isSuccessful()) {
                        CommonResponse res = response.body();
                        Object data = res.getData();
                        String resString = data.toString();
                        Log.d(TAG, "res data::" + resString);
                        if (resString != null) {
                            Gson gson = new Gson();
                            JsonObject json = gson.fromJson(resString, JsonObject.class);
//                            String objId = json.getAsJsonPrimitive("object_id").getAsString();
//                            Log.d(TAG, "res data::object:: " + objId);
                            UploadImageEntity uploadRes = gson.fromJson(json, UploadImageEntity.class);

//                            mRequestForm.setObjectId(uploadRes.objectId);
//                            mRequestForm.setEstimateImg1(uploadRes.imagePath1);
//                            mRequestForm.setEstimateImg2(uploadRes.imagePath2);
//                            mRequestForm.setEstimateImg3(uploadRes.imagePath3);
//                            mRequestForm.setEstimateImg4(uploadRes.imagePath4);
//                            showDetectDialog();
                        }

                        Log.d(TAG, "res::code::" + res.getStatus());
                        Log.d(TAG, "res::data::" + data);

                    } else {
                        Log.e(TAG, response.toString());
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    call.cancel();
                    Log.e(TAG, "postMultiFiles onFailure");
                }
            });
        }

    }

    private void registerReview() {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        HashMap<String, RequestBody> map = new HashMap<>();
        MultipartBody.Part img1 = null, img2 = null;

        ReviewForm formData = mBinding.getReviewForm();
        RequestBody requestId = MultipartBody.create(MediaType.parse("text/plain"), formData.getRequestEstimateId());
        RequestBody predictionId = MultipartBody.create(MediaType.parse("text/plain"), formData.getPredictionEstimateId());
        RequestBody userId = MultipartBody.create(MediaType.parse("text/plain"), formData.getUserId());
        RequestBody storeId = MultipartBody.create(MediaType.parse("text/plain"), formData.getStoreId());
        RequestBody comment = MultipartBody.create(MediaType.parse("text/plain"), formData.getReviewComment());
        RequestBody quality = MultipartBody.create(MediaType.parse("text/plain"), formData.getDisplayQuality());
        RequestBody kindness = MultipartBody.create(MediaType.parse("text/plain"), formData.getDisplayKindness());

        map.put("request_estimate_id", requestId);
        map.put("prediction_estimate_id", predictionId);
        map.put("user_id", userId);
        map.put("store_id", storeId);
        map.put("review_comment", comment);
        map.put("quality", quality);
        map.put("kindness", kindness);
        String filePath = formData.getReviewImage1();
        if (!TextUtils.isEmpty(filePath)) {
            File file = new File(filePath);
            if (file.exists()) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                img1 = MultipartBody.Part.createFormData("review_image1", file.getName(), requestFile);
            }
        }

        filePath = formData.getReviewImage2();
        if (!TextUtils.isEmpty(filePath)) {
            File file = new File(filePath);
            if (file.exists()) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                img2 = MultipartBody.Part.createFormData("review_image2", file.getName(), requestFile);
            }
        }

        Call<CommonResponse> call = requestEstimateAPI.doPostReviewRepairing(cookie, map, img1, img2);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    FixPatch.Instance().setStatusOfShop("REVIEW");
                    ((AppCompatActivity)getContext()).finish();
                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostReviewRepairing onFailure");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case PICKER_REQUEST_CODE: {
                String pathsList[] = data.getExtras().getStringArray(MyPicker.IMAGES_RESULT); // return list of selected images paths.
                Log.d(TAG, "Number of selected Images: " + pathsList.length);
                List<String> items = Arrays.asList(pathsList);
                if (!items.isEmpty()) {
                    ImageItemRecyclerViewAdapter adapter = new ImageItemRecyclerViewAdapter(items, true);

                } else {
//
                }

                break;
            }
        }
    }

}
