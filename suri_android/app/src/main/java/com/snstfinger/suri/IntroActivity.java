package com.snstfinger.suri;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.snstfinger.suri.data.datasource.SuriDatabase;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class IntroActivity extends AppCompatActivity {
    private static final String TAG = IntroActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_intro);

        SuriDatabase db = SuriDatabase.getSuriDatabase(this);

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()) {
                    Log.w(TAG, "FCM Log::getInstance failed."+task.getException());

                    finish();
                }

                SuriApplication.mSuriToken= task.getResult().getToken();
                Log.d(TAG,"FCM Log::Token."+SuriApplication.mSuriToken);

                startActivity(new Intent(getApplication(), MainActivity.class));
                finish();
            }

            // 인터넷안될 경우 java.io.IOException: SERVICE_NOT_AVAILABLE 죽음
        });
    }

    private void sendConnectionLog() {

        Map param = new HashMap<>();
//        param.put("uuid", BananaPreference.getInstance(this).getUUID());
        param.put("model", Build.MODEL);
        param.put("version", Build.VERSION.RELEASE);
        param.put("timezone", TimeZone.getDefault().getDisplayName());
        param.put("language", Locale.getDefault().getDisplayLanguage());
        param.put("serial", Build.SERIAL);

//        ApiManager.getInstance().connectLog(param, new ApiManager.CallbackListener() {
//
//            @Override
//            public void callback(String result) {
//                CommonResponse commonResponse = new Gson().fromJson(result, CommonResponse.class);
//                if (commonResponse == null)
//                    return;
//
//                if (commonResponse.getResult().equals("-1")) {
//                    SuriLoginManager.getInstance(IntroActivity.this).logout(new LoginProvider.OnResultLogoutListener() {
//                        @Override
//                        public void onLoadingResult(int result) {
//                            isConnectCheck = true;
////                            BananaPreference.getInstance(IntroActivity.this).saveUser(new User());
//                        }
//                    });
//                } else {
//                    isConnectCheck = true;
//                }
//            }
//
//            @Override
//            public void failed(String msg) {
//
//            }
//        });

    }
}
