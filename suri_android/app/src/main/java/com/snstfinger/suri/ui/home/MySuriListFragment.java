package com.snstfinger.suri.ui.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.snstfinger.suri.MainActivity;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.databinding.FragmentMysuriListBinding;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.ui.estimate.RequestEstimateActivity;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

import java.util.List;
import java.util.Objects;

/**
 * 메인 표시내역과 이용내역 리스트
 */
public class MySuriListFragment extends Fragment {
    private static final String TAG = MySuriListFragment.class.getName();

    private MySuriInfoSharedPreferences mPref;
    private OnMySuriListInteractionListener mListener;
    private MySuriViewModel mMySuriViewModel;
    private MySuriRecyclerViewAdapter mAdapter;

    public MySuriListFragment() {
    }

    @SuppressWarnings("unused")
    public static MySuriListFragment newInstance(int columnCount) {
        MySuriListFragment fragment = new MySuriListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = MySuriInfoSharedPreferences.getInstance(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setTitle(R.string.title_activity_quotation);

        Intent estimateStatus = getActivity().getIntent();
        RequestNPredictionNStoreEntity estimateInfo = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + estimateInfo);

        FragmentMysuriListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mysuri_list, container, false);
        binding.setRequestEstimate(estimateInfo);

        mMySuriViewModel = ViewModelProviders.of(getActivity()).get(MySuriViewModel.class);
        mMySuriViewModel.getMainSuri().observe(this, new Observer<RequestNPredictionNStoreEntity>() {
            @Override
            public void onChanged(RequestNPredictionNStoreEntity requestNPredictionNStoreEntity) {
                binding.setRequestEstimate(requestNPredictionNStoreEntity);

                if (SuriLoginManager.getInstance(getActivity()).isLoggedIn()) {
                    UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getActivity().getApplicationContext());
                    getMySuriList(profile.getUserId());
                }
            }
        });
        mAdapter = new MySuriRecyclerViewAdapter(mListener);
        binding.listRequestEstimate.setAdapter(mAdapter);

        if (SuriLoginManager.getInstance(getActivity()).isLoggedIn()) {
            UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getActivity().getApplicationContext());
            getMySuriList(profile.getUserId());
        }

        return binding.getRoot();

    }

    private void getMySuriList(String userId) {
        mMySuriViewModel.getAllRquestEstimate(userId).observe(this, new Observer<List<RequestNPredictionNStoreEntity>>() {
            @SuppressLint("NewApi")
            @Override
            public void onChanged(List<RequestNPredictionNStoreEntity> requestNPredictionNStoreEntities) {
                Log.d(TAG, "MySuriHistory Changed::"+requestNPredictionNStoreEntities.size());
                List<RequestNPredictionNStoreEntity> mySuriList = requestNPredictionNStoreEntities;
                mySuriList.removeIf(request -> request.requestEstimateId.equals(mPref.getReqeustId()));

                mAdapter.setItemList(mySuriList);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMySuriListInteractionListener) {
            mListener = (OnMySuriListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMySuriListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_close, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close: {
                getActivity().onBackPressed();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnMySuriListInteractionListener {

        void onItemClick(RequestNPredictionNStoreEntity item);

        void onItemLongClick(RequestNPredictionNStoreEntity item);
    }
}
