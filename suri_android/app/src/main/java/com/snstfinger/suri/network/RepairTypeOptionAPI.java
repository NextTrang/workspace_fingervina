package com.snstfinger.suri.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface RepairTypeOptionAPI {
    @GET("repairtype/getRepairTypeByObjectId")
    Call<CommonResponse> doGetRepairTypeByObjectId(@Header("Cookie") String cookie, @Query(value = "object_id") int objId);

    @GET("repairoption/getRepairOptionByObjectId")
    Call<CommonResponse> doGetRepairOptionByObjectId(@Header("Cookie") String cookie, @Query(value = "object_id") int objId);
}
