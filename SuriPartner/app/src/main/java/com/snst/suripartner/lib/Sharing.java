package com.snst.suripartner.lib;

import android.content.Context;
import android.view.View;

import com.snst.suripartner.LoginActivity;
import com.snst.suripartner.lib.booking.BookingViewModel;
import com.snst.suripartner.lib.chat.ChatViewModel;
import com.snst.suripartner.lib.effect.Effect;
import com.snst.suripartner.lib.left_menu.LeftMenu;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.lib.review.ReviewViewModel;
import com.snst.suripartner.lib.store_info.StoreInfo;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.lib.suri_history.SuriHistoryViewModel;

import java.util.ArrayList;
import java.util.List;

public class Sharing {
    private static Sharing _sharing = null;

    public static Sharing Instance() {
        if (_sharing == null) {
            _sharing = new Sharing();
        }

        return _sharing;
    }

    private List<QuotationViewModel> quotationViewModels = new ArrayList<>();
    private List<ChatViewModel> chatViewModels = new ArrayList<>();
    private List<BookingViewModel> bookingViewModels = new ArrayList<>();
    private List<SuriViewModel> suriViewModels = new ArrayList<>();
    private List<SuriHistoryViewModel> suriHistoryViewModels = new ArrayList<>();
    private List<ReviewViewModel> reviewViewModels = new ArrayList<>();
    private User user;
    private String cookie;
    private boolean loadDataFinish = false;
    private Effect effect = new Effect();
    private StoreInfo storeInfo = new StoreInfo();

    //Getter

    public List<QuotationViewModel> getQuotationViewModels() {
        return quotationViewModels;
    }

    public List<ChatViewModel> getChatViewModels() {
        return chatViewModels;
    }

    public List<BookingViewModel> getBookingViewModels() {
        return bookingViewModels;
    }

    public List<SuriViewModel> getSuriViewModels() {
        return suriViewModels;
    }

    public List<SuriHistoryViewModel> getSuriHistoryViewModels() {
        return suriHistoryViewModels;
    }

    public List<ReviewViewModel> getReviewViewModels() {
        return reviewViewModels;
    }

    public ReviewViewModel getReviewViewModel(String requestEstId) {
        for (ReviewViewModel viewModel : reviewViewModels) {
            if (viewModel.requestEstId.equals(requestEstId)) {
                return viewModel;
            }
        }

        return null;
    }

    public User getUser() {
        return user;
    }

    public String getCookie() {
        return cookie;
    }

    public boolean isLoadDataFinish() {
        return loadDataFinish;
    }

    public Effect getEffect() {
        return effect;
    }

    public StoreInfo getStoreInfo() {
        return storeInfo;
    }

    //Setter

    public void setUser(Context context, User user) {
        if (context.getClass() == LoginActivity.class) {
            this.user = user;
        }
    }

    public void setCookie(Context context, String cookie) {
        if (context.getClass() == LoginActivity.class) {
            this.cookie = cookie;
        }
    }

    public void setLoadDataFinish(boolean loadDataFinish) {
        this.loadDataFinish = loadDataFinish;
    }

    public void setEffect(Effect effect) {
        this.effect = effect;
    }

    public void setStoreInfo(StoreInfo storeInfo) {
        this.storeInfo = storeInfo;
    }

    //Method
    public void reset() {
        quotationViewModels.clear();
        chatViewModels.clear();
        bookingViewModels.clear();
        suriViewModels.clear();
        suriHistoryViewModels.clear();
        reviewViewModels.clear();
        user = null;
        loadDataFinish = false;
        effect = new Effect();
        storeInfo = new StoreInfo();
    }
}
