package com.snst.suripartner.lib.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.snst.suripartner.MainActivity;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbRequest;
import com.snst.suripartner.labels.LbTag;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.booking.Booking;
import com.snst.suripartner.lib.booking.BookingViewModel;
import com.snst.suripartner.lib.chat.Chat;
import com.snst.suripartner.lib.chat.ChatViewModel;
import com.snst.suripartner.lib.effect.EffectEvent;
import com.snst.suripartner.lib.quotation.Quotation;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.lib.review.Review;
import com.snst.suripartner.lib.review.ReviewViewModel;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestData implements ApiRequest{

    private static final String TAG = "RequestData";

    @Override
    public void execute(Context context, String... inputs) {
        if (context.getClass() != MainActivity.class) {
            Log.e(TAG, "execute: This request only should be called in MainActivity");
            return;
        }

        //Clear current data before requesting
        Sharing.Instance().getQuotationViewModels().clear();
        Sharing.Instance().getChatViewModels().clear();
        Sharing.Instance().getBookingViewModels().clear();
        Sharing.Instance().getSuriViewModels().clear();
        Sharing.Instance().getReviewViewModels().clear();

        requestQuotations(context,inputs);
    }

    private void requestQuotations(Context context, String... inputs) {

        new EffectEvent(context).visibleProgressBar(Sharing.Instance().getEffect());
        RetrofitClient.getInstance().getFlags().set(LbRequest.LbFlag.REQUEST_DATA);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getQuotations(inputs[0], inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsObject = new JSONObject(response.body().string());
                    JSONObject jsData = jsObject.getJSONObject(LbData.DATA);
                    JSONObject jsPageMap = jsData.getJSONObject(LbData.LbPageMap.PAGE_MAP);
                    JSONArray jsInfos = jsData.getJSONArray(LbData.LbInfo.INFO);

                    if(jsInfos.length() == 0){
                        requestBooking(context,inputs[0],"1");

                        if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                            new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                            RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                        }

                        return;
                    }

                    for (int i = 0; i < jsInfos.length(); ++i) {
                        JSONObject jsInfo = jsInfos.getJSONObject(i);

                        String state = jsInfo.getString(LbData.LbInfo.STATUS);

                        if (state.equals(LbData.LbInfo.REQUEST)) {
                            Quotation quotation = new Quotation(jsInfo);
                            Sharing.Instance().getQuotationViewModels().add(new QuotationViewModel(quotation));
                        }

                        Chat chat = new Chat(jsInfo);
                        Sharing.Instance().getChatViewModels().add(new ChatViewModel(chat));
                    }

                    //check next page and continue to request
                    int next_page = jsPageMap.getInt(LbData.LbPageMap.NEXT_PAGE);
                    int requestedPage = Integer.parseInt(inputs[1]);
                    if (requestedPage < next_page) {
                        requestedPage = next_page;
                        requestQuotations(context, inputs[0], String.valueOf(requestedPage));
                    } else {
                        requestBooking(context,inputs[0],"1");
                    }

                    if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                        new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                        RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                    }

                } catch (JSONException e) {
                    Log.e(LbTag.MAIN_ACTIVITY, "onPostExecute: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                }
            }
        });
    }

    private void requestBooking(Context context, String... inputs) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getBookings(inputs[0], inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsObject = new JSONObject(response.body().string());
                    JSONObject jsData = jsObject.getJSONObject(LbData.DATA);
                    JSONObject jsPageMap = jsData.getJSONObject(LbData.LbPageMap.PAGE_MAP);
                    JSONArray jsInfos = jsData.getJSONArray(LbData.LbInfo.INFO);

                    if(jsInfos.length() == 0){
                        requestSuri(context,inputs[0],"1");
                        return;
                    }

                    for (int i = 0; i < jsInfos.length(); ++i) {
                        JSONObject jsInfo = jsInfos.getJSONObject(i);

                        Chat chat = new Chat(jsInfo);
                        Sharing.Instance().getChatViewModels().add(new ChatViewModel(chat));

                        String write_yn = jsInfo.getString(LbData.LbInfo.WRITE_YN);
                        if(write_yn.equals("n")){
                            continue;
                        }

                        String state = jsInfo.getString(LbData.LbInfo.STATUS);
                        if (state.equals(LbData.LbInfo.BOOKING)) {
                            Booking booking = new Booking(jsInfo);
                            Sharing.Instance().getBookingViewModels().add(new BookingViewModel(booking));
                        }
                    }

                    //check next page and continue to request
                    int next_page = jsPageMap.getInt(LbData.LbPageMap.NEXT_PAGE);
                    int requestedPage = Integer.parseInt(inputs[1]);
                    if (requestedPage < next_page) {
                        requestedPage = next_page;
                        requestBooking(context, inputs[0], String.valueOf(requestedPage));
                    } else {
                        requestSuri(context,inputs[0],"1");
                    }

                } catch (JSONException e) {
                    Log.e(LbTag.MAIN_ACTIVITY, "onPostExecute: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                }
            }
        });

    }

    private void requestSuri(Context context, String... inputs) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSURI(inputs[0], inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsObject = new JSONObject(response.body().string());
                    JSONObject jsData = jsObject.getJSONObject(LbData.DATA);
                    JSONObject jsPageMap = jsData.getJSONObject(LbData.LbPageMap.PAGE_MAP);
                    JSONArray jsInfos = jsData.getJSONArray(LbData.LbInfo.INFO);

                    if(jsInfos.length() == 0){
                        requestReview(context,inputs[0],"1");
                        return;
                    }

                    for (int i = 0; i < jsInfos.length(); ++i) {
                        JSONObject jsInfo = jsInfos.getJSONObject(i);

                        Chat chat = new Chat(jsInfo);
                        Sharing.Instance().getChatViewModels().add(new ChatViewModel(chat));

                        String write_yn = jsInfo.getString(LbData.LbInfo.WRITE_YN);
                        if(write_yn.equals("n")){
                            continue;
                        }

                        String state = jsInfo.getString(LbData.LbInfo.STATUS);
                        if (!state.equals(LbData.LbInfo.REQUEST) && !state.equals(LbData.LbInfo.BOOKING)) {
                            Suri suri = new Suri(jsInfo);
                            Sharing.Instance().getSuriViewModels().add(new SuriViewModel(suri));
                        }
                    }

                    //check next page and continue to request
                    int next_page = jsPageMap.getInt(LbData.LbPageMap.NEXT_PAGE);
                    int requestedPage = Integer.parseInt(inputs[1]);
                    if (requestedPage < next_page) {
                        requestedPage = next_page;
                        requestSuri(context, inputs[0], String.valueOf(requestedPage));
                    } else {
                        requestReview(context,inputs[0],"1");
                    }

                } catch (JSONException e) {
                    Log.e(LbTag.MAIN_ACTIVITY, "onPostExecute: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                }
            }
        });

    }

    private void requestReview(Context context, String... inputs) {

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getReviews(Sharing.Instance().getCookie(), inputs[0], inputs[1]);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsObject = new JSONObject(response.body().string());
                    JSONObject jsData = jsObject.getJSONObject(LbData.DATA);
                    JSONObject jsPageMap = jsData.getJSONObject(LbData.LbPageMap.PAGE_MAP);
                    JSONArray jsInfos = jsData.getJSONArray(LbData.LbInfo.INFO);

                    if (jsInfos.length() == 0) {
                        Sharing.Instance().setLoadDataFinish(true);
                        //Refresh current tab
                        ((MainActivity) context).refresh();
                        return;
                    }

                    for (int i = 0; i < jsInfos.length(); ++i) {
                        JSONObject jsInfo = jsInfos.getJSONObject(i);

                        Review review = new Review(jsInfo);
                        Sharing.Instance().getReviewViewModels().add(new ReviewViewModel(review));
                    }

                    //check next page and continue to request
                    int next_page = jsPageMap.getInt(LbData.LbPageMap.NEXT_PAGE);
                    int requestedPage = Integer.parseInt(inputs[1]);
                    if (requestedPage < next_page) {
                        requestedPage = next_page;
                        requestReview(context,inputs[0], String.valueOf(requestedPage));
                    } else {
                        Sharing.Instance().setLoadDataFinish(true);
                        //Refresh current tab
                        ((MainActivity) context).refresh();
                    }

                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());

                } catch (JSONException e) {
                    Log.e(LbTag.MAIN_ACTIVITY, "onPostExecute: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                if(RetrofitClient.getInstance().getFlags().get(LbRequest.LbFlag.REQUEST_DATA)){
                    new EffectEvent(context).invisibleProgressBar(Sharing.Instance().getEffect());
                    RetrofitClient.getInstance().getFlags().clear(LbRequest.LbFlag.REQUEST_DATA);
                }
            }
        });
    }
}
