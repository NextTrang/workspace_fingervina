package com.snstfinger.suri;

import android.content.Context;

public interface ContextProvider {
    Context getActivityContext();
}
