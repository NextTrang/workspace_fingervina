package com.snst.suripartner.lib.suri_complete;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Command;
import com.snst.suripartner.lib.api.RequestCompleteRepair;
import com.snst.suripartner.lib.api.RequestUpdateBeforeCompleteRepair;
import com.snst.suripartner.lib.api.RequestUpdateRepair;
import com.snst.suripartner.lib.gallery_cmd.Gallery;
import com.snst.suripartner.lib.gallery_cmd.GalleryInvoker;
import com.snst.suripartner.lib.gallery_cmd.OpenGallery;
import com.snst.suripartner.lib.helper.Getter;

import java.io.File;

public class SuriCompleteEvent {

    Context context;

    public SuriCompleteEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onOpenGallery(View view) {
        Gallery gallery = new Gallery().setRequestCode(LbPop.LbCode.REQUEST_COMPLETE_SURI);
        Command open = new OpenGallery((AppCompatActivity) context, gallery);
        GalleryInvoker invoker = new GalleryInvoker(open, null);
        invoker.doOpen();
    }

    public void onAssignImage(SuriComplete suriComplete, File photoFile) {
        if (suriComplete.getImageUrl_first() == null) {
            suriComplete.setImageUrl_first(Getter.getRealPathFromURI(context, Uri.fromFile(photoFile)));
        } else if (suriComplete.getImageUrl_second() == null) {
            suriComplete.setImageUrl_second(Getter.getRealPathFromURI(context, Uri.fromFile(photoFile)));
        } else {
            suriComplete.setImageUrl_first(null).setImageUrl_second(null);
            suriComplete.setImageUrl_first(Getter.getRealPathFromURI(context, Uri.fromFile(photoFile)));
        }

    }

    public void onTransferData(SuriComplete suriComplete) {
        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.SURI_COMPLETE, suriComplete);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }

    public void onCompleteRepair(SuriComplete suriComplete) {
        String[] inputs = new String[5];
        inputs[0] = suriComplete.getPredictEstId();
        inputs[1] = suriComplete.getReqEstId();
        inputs[2] = suriComplete.getRepairComent();
        inputs[3] = suriComplete.getImageUrl_first();
        inputs[4] = suriComplete.getImageUrl_second();

        if(suriComplete.getProgressRate() < 100){
            new RequestUpdateBeforeCompleteRepair().execute(context,inputs);
        }
        else {
            new RequestCompleteRepair().execute(context, inputs);
        }
    }
}
