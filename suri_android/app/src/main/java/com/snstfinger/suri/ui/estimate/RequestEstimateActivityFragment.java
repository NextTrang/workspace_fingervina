package com.snstfinger.suri.ui.estimate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.CheckedTextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.snstfinger.suri.R;
import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.data.entity.RepairOptionEntity;
import com.snstfinger.suri.data.entity.RepairTypeEntity;
import com.snstfinger.suri.data.entity.UploadImageEntity;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.data.model.SelectSuriObject;
import com.snstfinger.suri.databinding.FragmentRequestEstimateBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RepairTypeOptionAPI;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.ui.tools.MyPicker;
import com.snstfinger.suri.ui.tools.TimeItemRecycleViewAdapter;
import com.snstfinger.suri.util.UserInfoSharedPreferences;
import com.snstfinger.suri.util.Utils;

import java.io.File;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestEstimateActivityFragment extends Fragment implements View.OnClickListener, CalendarView.OnCalendarSelectListener, TimeItemRecycleViewAdapter.OnSelectTimeInteractionListener {
    private final String TAG = RequestEstimateActivity.class.getName();
    private final int REQ_PICKER_CODE = 1010;
    private RequestEstimate mRequestForm;
    private FragmentRequestEstimateBinding mBinding;
    private java.util.Calendar mDesireDate;

    private ProgressDialog mUploadingDialog;
    private DialogFragment mRequestingDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_request_estimate, container, false);
        mBinding.setHandler(this);
        Intent request = getActivity().getIntent();
        mRequestForm = (RequestEstimate) request.getSerializableExtra("request_form");
        mBinding.setRequestForm(mRequestForm);

        if (SuriLoginManager.getInstance(getActivity()).isLoggedIn()) {
            UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(getActivity().getApplicationContext());
            mRequestForm.setUserId(profile.getUserId());
        }

//        mRequestForm.setDesiredReservationDate("2020-04-14 15:29:08");
        // calendar
        String desiredDate = mRequestForm.getDesiredReservationDate();
        if (TextUtils.isEmpty(desiredDate)) {
            // set current dateTime
            long selectedDate = mBinding.calendarView.getSelectedCalendar().getTimeInMillis();
            mDesireDate = java.util.Calendar.getInstance();
            mDesireDate.setTimeInMillis(selectedDate);

            Log.d(TAG, "today::" + mDesireDate);
            Log.d(TAG, "year::" + mDesireDate.get(java.util.Calendar.YEAR));
            Log.d(TAG, "month::" + mDesireDate.get(java.util.Calendar.MONTH));
            Log.d(TAG, "day::" + mDesireDate.get(java.util.Calendar.DATE));

        } else {
            // desired dateTime
            Date dateTime = new Date();
            try {
                dateTime = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER).parse(desiredDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mDesireDate = java.util.Calendar.getInstance();
            mDesireDate.setTime(dateTime);
            Log.d(TAG, "today::" + mDesireDate);
            Log.d(TAG, "year::" + mDesireDate.get(java.util.Calendar.YEAR));
            Log.d(TAG, "month::" + mDesireDate.get(java.util.Calendar.MONTH));
            Log.d(TAG, "day::" + mDesireDate.get(java.util.Calendar.DATE));

            mBinding.calendarView.scrollToCalendar(mDesireDate.get(java.util.Calendar.YEAR), mDesireDate.get(java.util.Calendar.MONTH) + 1, mDesireDate.get(java.util.Calendar.DATE));
        }

        mBinding.calendarView.setOnCalendarSelectListener(this);
        mBinding.titleYearMonth.setText(Utils.getMonthName(mDesireDate.get(java.util.Calendar.MONTH) + 1) + " " + mDesireDate.get(java.util.Calendar.YEAR));

        // timetable
        ArrayList<String> mItems = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_table)));
        TimeItemRecycleViewAdapter mAdapter = new TimeItemRecycleViewAdapter(getContext(), mItems, this);
        mBinding.listViewTimes.setAdapter(mAdapter);
        setTimeTable(mDesireDate);

//        TimeSliderDecoration decoration = new TimeSliderDecoration();
//        mBinding.listViewTimes.addItemDecoration(decoration);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        if (v instanceof CheckedTextView) {
            ((CheckedTextView) v).toggle();
        } else {
            switch (v.getId()) {
                case R.id.empty_pager:
                case R.id.btn_add_photo: {
//                Intent camera = new Intent(getActivity(), SuriCamera.class);
//                startActivity(camera);
                    takeMultiAlbum();

                    break;
                }

                case R.id.change_type: {
                    showDetectDialog();

                    break;
                }

                case R.id.input_brand: {
                    // 빠른 구현
                    // 프레그먼트로 변경해서 callback으로 데이터 받아오는 방법도 있음
                    // 새로운 액티비티라서 데이터바인딩이 풀리는건가?
                    // onResult에서 자동반영이 안되고 있음
                    Intent goSearch = new Intent(getActivity(), SearchBrandModelActivity.class);
                    goSearch.putExtra("request_form", mRequestForm);
                    goSearch.putExtra("search_type", "brand");

                    startActivityForResult(goSearch, SuriApplication.REQ_SEARCH);

                    break;
                }

                case R.id.input_model_name: {
                    Intent goSearch = new Intent(getActivity(), SearchBrandModelActivity.class);
                    goSearch.putExtra("request_form", mRequestForm);
                    goSearch.putExtra("search_type", "model");

                    startActivityForResult(goSearch, SuriApplication.REQ_SEARCH);

                    break;
                }

                case R.id.input_location_1:
                case R.id.input_location_2: {
                    Intent goSearch = new Intent(getActivity(), SearchLocationActivity.class);
                    goSearch.putExtra("request_form", mRequestForm);
                    goSearch.putExtra("location_id", v.getId());

                    startActivityForResult(goSearch, SuriApplication.REQ_SEARCH);

                    break;
                }

                case R.id.layout_item: {
                    String time = (String) v.getTag();
                    Log.d(TAG, "Selected::" + time);
                    v.setSelected(true);
                    break;
                }

                case R.id.btn_now: {
                    mBinding.calendarView.scrollToCurrent(true);
                    setTimeTable(java.util.Calendar.getInstance());

                    break;
                }

                case R.id.btn_ask_suri: {
                    askSuri();

                    break;
                }
            }
        }
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(requestCode == SuriApplication.REQ_PHOTO && resultCode == Activity.RESULT_OK) {
//            ArrayList imageList = new ArrayList<>();
//
//            // 멀티 선택을 지원하지 않는 기기에서는 getClipdata()가 없음 => getData()로 접근해야 함
//            if (data.getClipData() == null) {
//                Log.d(TAG, String.valueOf(data.getData()));
//                imageList.add(String.valueOf(data.getData()));
//            } else {
//
//                ClipData clipData = data.getClipData();
//                Log.d(TAG, String.valueOf(clipData.getItemCount()));
//                if (clipData.getItemCount() > 4){
//                    Toast.makeText(getContext(), "사진은 4개까지 선택가능 합니다.", Toast.LENGTH_SHORT).show();
//
//                    return;
//                }
//                // 멀티 선택에서 하나만 선택했을 경우
//                else if (clipData.getItemCount() == 1) {
//                    String dataStr = String.valueOf(clipData.getItemAt(0).getUri());
//                    Log.d("2. clipdata choice", String.valueOf(clipData.getItemAt(0).getUri()));
//                    Log.d("2. single choice", clipData.getItemAt(0).getUri().getPath());
//                    imageList.add(dataStr);
//
//                } else if (clipData.getItemCount() > 1 && clipData.getItemCount() < 10) {
//                    for (int i = 0; i < clipData.getItemCount(); i++) {
//                        Log.d("3. single choice", String.valueOf(clipData.getItemAt(i).getUri()));
//                        imageList.add(String.valueOf(clipData.getItemAt(i).getUri()));
//                    }
//                }
//            }
//
//            Log.d(TAG, "selectedPic::"+ imageList.size());
//        } else if(requestCode == SuriApplication.REQ_SEARCH && resultCode == Activity.RESULT_OK) {
//            mRequestForm = (RequestEstimate) data.getSerializableExtra("request_form");
//            Log.d(TAG, "Selected Search RESULT::"+ mRequestForm.getBrandName());
//            // 새로운 액티비티라서 바인딩범위가 아닌건지 자동반영안됨
////            mRequestForm.notifyPropertyChanged(com.snstfinger.suri.BR.brandName);
//            mBinding.setRequestForm(mRequestForm);
//        }
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQ_PICKER_CODE: {
                String pathsList[] = data.getExtras().getStringArray(MyPicker.IMAGES_RESULT); // return list of selected images paths.
                Log.d(TAG, "Number of selected Images: " + pathsList.length);
                List<String> items = Arrays.asList(pathsList);
                if (!items.isEmpty()) {
                    ImageItemRecyclerViewAdapter adapter = new ImageItemRecyclerViewAdapter(items, true);
                    mBinding.viewPager.setAdapter(adapter);
                    mBinding.viewPager.setVisibility(View.VISIBLE);
                    mBinding.btnAddPhoto.setVisibility(View.VISIBLE);
                    mBinding.emptyPager.setVisibility(View.GONE);

                    uploadingMultiFiles(items);

                } else {
                    mBinding.viewPager.setVisibility(View.GONE);
                    mBinding.btnAddPhoto.setVisibility(View.GONE);
                    mBinding.emptyPager.setVisibility(View.VISIBLE);
                }

                break;
            }

            case SuriApplication.REQ_SEARCH: {
                mRequestForm = (RequestEstimate) data.getSerializableExtra("request_form");
                Log.d(TAG, "Selected Search RESULT::" + mRequestForm.getBrandName());
                // 새로운 액티비티라서? 바인딩범위가 아닌건지 자동반영안됨
                // mRequestForm.notifyPropertyChanged(com.snstfinger.suri.BR.brandName);
                mBinding.setRequestForm(mRequestForm);
            }
        }
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mBinding.titleYearMonth.setText(Utils.getMonthName(calendar.getMonth()) + " " + calendar.getYear());

        if (isClick) {
            mDesireDate.set(java.util.Calendar.YEAR, calendar.getYear());
            mDesireDate.set(java.util.Calendar.MONTH, calendar.getMonth() - 1);
            mDesireDate.set(java.util.Calendar.DAY_OF_MONTH, calendar.getDay());

            String dateTime = Utils.getDateTime(mDesireDate.getTime());
//            String dateTimeUTC = Utils.getDateTime(Utils.dateToUTC(mDesireDate.getTime()));

            mRequestForm.setDesiredReservationDate(dateTime);
        }
    }

    @Override
    public void onSelectTimeInteraction(String item) {
        Log.d(TAG, "clicked::" + item);
        if (!TextUtils.isEmpty(item)) {
            String time[] = item.split(":");
            mDesireDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
            mDesireDate.set(java.util.Calendar.MINUTE, Integer.valueOf(time[1]));
            mDesireDate.set(java.util.Calendar.SECOND, 0);

            String dateTime = Utils.getDateTime(mDesireDate.getTime());
//            String dateTimeUTC = Utils.getDateTime(Utils.dateToUTC(mDesireDate.getTime()));

            mRequestForm.setDesiredReservationDate(dateTime);
        }
    }

    private void showLoadingDialog() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mUploadingDialog = new ProgressDialog(getActivity());
                mUploadingDialog.setIndeterminate(true);
                mUploadingDialog.setTitle("Analyzing Image...");
                mUploadingDialog.show();
            }
        });
    }

    private void endLoadingDialog() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (mUploadingDialog != null)
                    mUploadingDialog.dismiss();
            }
        });
    }

    private void takeMultiAlbum() {
//        Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        gallery.addCategory(Intent.CATEGORY_APP_GALLERY);
//        gallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
////        gallery.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(gallery, SuriApplication.REQ_PHOTO);
        MyPicker picker = new MyPicker();
        picker.limit(4);
        picker.requestCode(REQ_PICKER_CODE).withFragment(this).show();
    }

    private int getObjectId(String name) {
        int objId = 1;

        switch (name.toLowerCase()) {
            case "phone":
                objId = 1;
                break;

            case "pc":
                objId = 2;
                break;

            case "bike":
                objId = 3;
                break;

            case "car":
                objId = 4;
                break;
        }

        return objId;
    }

    private String getObjectName(int id) {
        String name = getString(R.string.suggest_unknown);

        switch (id) {
            case 1:
                name = getString(R.string.suggest_phone);
                break;

            case 2:
                name = getString(R.string.suggest_pc);
                break;

            case 3:
                name = getString(R.string.suggest_bike);
                break;

            case 4:
                name = getString(R.string.suggest_car);
                break;
        }

        return name;
    }

    private void getRepairType(RequestEstimate requestForm) {
        RepairTypeOptionAPI repairTypeOptionAPI = ApiManager.getProvider(RepairTypeOptionAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = repairTypeOptionAPI.doGetRepairTypeByObjectId(cookie, requestForm.getObjectId());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Object info = ((LinkedTreeMap) data).get("Info");

                    Gson gson = new Gson();
                    Type itemType = new TypeToken<List<RepairTypeEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    switch (requestForm.getObjectId()) {
                        case 1: {

                            break;
                        }

                        case 2: {

                            break;
                        }

                        case 3: {

                            break;
                        }

                        case 4: {

                            break;
                        }
                    }

                    Log.d(TAG, "res::code::" + res.getStatus());
                    Log.d(TAG, "res::data::" + data);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetRepairTypeByObjectId onFailure");
            }
        });
    }

    private void getRepairOption(int objectId) {
        RepairTypeOptionAPI repairTypeOptionAPI = ApiManager.getProvider(RepairTypeOptionAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = repairTypeOptionAPI.doGetRepairOptionByObjectId(cookie, objectId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Object info = ((LinkedTreeMap) data).get("Info");

                    Gson gson = new Gson();
                    Type itemType = new TypeToken<List<RepairOptionEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    Log.d(TAG, "res::code::" + res.getStatus());
                    Log.d(TAG, "res::data::" + data);

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetRepairTypeByObjectId onFailure");
            }
        });

    }

    private void showDetectDialog() {
        SelectSuriObject obj = new SelectSuriObject();
        obj.setContent(String.format(getString(R.string.msg_suggest_obj), getObjectName(mRequestForm.getObjectId())));
        obj.setRequestForm(mRequestForm);
        obj.setStep(1);

        ((RequestEstimateActivity) getActivity()).showDialogForObjectId(obj);
    }

    private void uploadingFile(List files) {
        {
            String filePath = (String) files.get(0);
            File file = new File(filePath);

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileBody);

            RequestEstimateAPI fileUploadAPI = ApiManager.getProvider(RequestEstimateAPI.class);
            Call<CommonResponse> call = fileUploadAPI.doPostUploadFile(part);
            call.enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                    if (response.isSuccessful()) {
                        CommonResponse res = response.body();
                        Object data = res.getData();
                        String resString = data.toString();
                        Log.d(TAG, "res data::" + resString);
                        if (resString != null) {
                            String[] items = resString.split(",");
                            if (items.length > 1) {
                                String[] item = items[0].split(": ");
                                String object = item[1];
                                Log.d(TAG, "res data::object:: " + object);
                                mRequestForm.setObjectId(getObjectId(object));
                                showDetectDialog();
                            }
                        }

                        Log.d(TAG, "res::code::" + res.getStatus());
                        Log.d(TAG, "res::data::" + data);

                    } else {
                        Log.e(TAG, response.toString());
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    call.cancel();
                    Log.e(TAG, "doPostUploadFile onFailure");
                }
            });
        }

    }

    private void uploadingMultiFiles(List<String> files) {
        MultipartBody.Part[] parts = new MultipartBody.Part[files.size()];
        if (!files.isEmpty()) {
            for (int idx = 0; idx < files.size(); idx++) {
                File file = new File(files.get(idx));
                if (file.exists()) {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                    parts[idx] = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
                }
            }
        }

        showLoadingDialog();

        RequestEstimateAPI fileUploadAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        Call<CommonResponse> call = fileUploadAPI.doPostUploadMultiFiles(parts);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    String resString = data.toString();
                    Log.d(TAG, "res data::" + resString);
                    if (resString != null) {
                        Gson gson = new Gson();
                        JsonObject json = gson.fromJson(resString, JsonObject.class);
//                            String objId = json.getAsJsonPrimitive("object_id").getAsString();
//                            Log.d(TAG, "res data::object:: " + objId);
                        UploadImageEntity uploadRes = gson.fromJson(json, UploadImageEntity.class);

                        mRequestForm.setObjectId(uploadRes.objectId);
                        mRequestForm.setEstimateImg1(uploadRes.imagePath1);
                        mRequestForm.setEstimateImg2(uploadRes.imagePath2);
                        mRequestForm.setEstimateImg3(uploadRes.imagePath3);
                        mRequestForm.setEstimateImg4(uploadRes.imagePath4);

                        showDetectDialog();
                    }

                    Log.d(TAG, "res::code::" + res.getStatus());
                    Log.d(TAG, "res::data::" + data);

                } else {
                    Log.e(TAG, response.toString());
                }

                endLoadingDialog();
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "postMultiFiles onFailure");
                endLoadingDialog();
            }
        });


    }

    private int isValidateForm() {
        String img = mRequestForm.getEstimateImg1();
        String brand = mRequestForm.getBrandName();
        String model = mRequestForm.getModelName();
        String repairType = mRequestForm.getEstimateRepairTypeSelected();
        String address = mRequestForm.getEstimateAddressId1();
        String dateTime = mRequestForm.getDesiredReservationDate();

        if (TextUtils.isEmpty(img)) {
            return R.string.error_no_model_image;
        }

        if (TextUtils.isEmpty(brand)) {
            return R.string.error_no_brand_name;
        }

        if (TextUtils.isEmpty(model)) {
            return R.string.error_no_model_name;
        }

        if (TextUtils.isEmpty(repairType)) {
            return R.string.error_no_repair_type;
        }

        if (TextUtils.isEmpty(address)) {
            return R.string.error_no_address;
        }

        if (TextUtils.isEmpty(dateTime)) {
            return R.string.error_no_desired_date;
        }

        return 0;
    }

    private void setTimeTable(java.util.Calendar dateTime) {
        TimeItemRecycleViewAdapter adapter = (TimeItemRecycleViewAdapter) mBinding.listViewTimes.getAdapter();
        int position = adapter.getPositionFromDate(dateTime);
        adapter.setSelectTime(position);
        mBinding.listViewTimes.scrollToPosition(position);
    }

    private void askSuri() {
        int errorCode = isValidateForm();
        if (errorCode != 0) {
            Snackbar.make(mBinding.btnAskSuri, errorCode, Snackbar.LENGTH_LONG).setAction(android.R.string.ok, v -> {
                // nothing
            }).show();

            return;
        }

        // delegate to loadingFragment
        ((RequestEstimateActivity) getActivity()).showDialogSending(mRequestForm);

//        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
//        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
//
//        RequestEstimate formData = mRequestForm;
//        // for test
//        // formData.setDesiredReservationDate(Utils.getCurrentTime());
//
//        HashMap<String, RequestBody> map = new HashMap<>();
//        RequestBody obj = MultipartBody.create(MediaType.parse("text/plain"), formData.getObjectId().toString());
//        RequestBody img1 = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateImg1());
//        RequestBody brand = MultipartBody.create(MediaType.parse("text/plain"), formData.getBrandName());
//        RequestBody model = MultipartBody.create(MediaType.parse("text/plain"), formData.getModelName());
//        RequestBody type = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateRepairTypeSelected());
//        RequestBody addr = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateAddressId1());
//        RequestBody id = MultipartBody.create(MediaType.parse("text/plain"), formData.getUserId());
//        RequestBody date = MultipartBody.create(MediaType.parse("text/plain"), formData.getDesiredReservationDate());
//        map.put("object_id", obj);
//        map.put("estimate_image1", img1);
//        map.put("brand_name", brand);
//        map.put("model_name", model);
//        map.put("estimate_repair_type_selected", type);
//        map.put("estimate_address_selected_1", addr);
//        map.put("user_id", id);
//        map.put("desired_reservation_date", date);
//
//        // region optional info
//        String path2 = formData.getEstimateImg2();
//        if (!TextUtils.isEmpty(path2)) {
//            RequestBody img2 = MultipartBody.create(MediaType.parse("text/plain"), path2);
//            map.put("estimate_image2", img2);
//
//            String path3 = formData.getEstimateImg3();
//            if (!TextUtils.isEmpty(path3)) {
//                RequestBody img3 = MultipartBody.create(MediaType.parse("text/plain"), path3);
//                map.put("estimate_image3", img3);
//
//                String path4 = formData.getEstimateImg4();
//                if (!TextUtils.isEmpty(path4)) {
//                    RequestBody img4 = MultipartBody.create(MediaType.parse("text/plain"), path4);
//                    map.put("estimate_image4", img4);
//                }
//            }
//        }
//
//        String vehiclePlate = formData.getVehicleNumber();
//        if (!TextUtils.isEmpty(vehiclePlate)) {
//            RequestBody vehicleNumber = MultipartBody.create(MediaType.parse("text/plain"), vehiclePlate);
//            map.put("vehicle_number", vehicleNumber);
//        }
//
//        String descript = formData.getEstimateDescription();
//        if (!TextUtils.isEmpty(descript)) {
//            RequestBody estDesc = MultipartBody.create(MediaType.parse("text/plain"), descript);
//            map.put("estimate_description", estDesc);
//        }
//
//        String repairOpt = formData.getEstimateRepairOptionSelected();
//        if (!TextUtils.isEmpty(repairOpt)) {
//            RequestBody option = MultipartBody.create(MediaType.parse("text/plain"), repairOpt);
//            map.put("estimate_repair_option_selected", option);
//        }
//
//        String address2 = formData.getEstimateAddressId2();
//        if (!TextUtils.isEmpty(address2)) {
//            RequestBody addr2 = MultipartBody.create(MediaType.parse("text/plain"), address2);
//            map.put("estimate_address_selected_2", addr2);
//        }
//        // endregion
//
//        Call<CommonResponse> call = requestEstimateAPI.doPostRequestEst(cookie, map);
//        call.enqueue(new Callback<CommonResponse>() {
//            @Override
//            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//
//                if (response.isSuccessful()) {
//                    CommonResponse res = response.body();
//                    Object data = res.getData();
//                    String requestId = ((LinkedTreeMap) data).get("request_estimate_id").toString();
//                    Log.d(TAG, res.toString() + "::" + requestId);
//
//                    getActivity().finish();
//                } else {
//                    Log.e(TAG, response.toString());
//
//                    Snackbar.make(mBinding.btnAskSuri, "SEND FAIL", BaseTransientBottomBar.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommonResponse> call, Throwable t) {
//                call.cancel();
//                Log.e(TAG, "doPostRequestEst onFailure");
//
//                Snackbar.make(mBinding.btnAskSuri, "SEND FAIL", BaseTransientBottomBar.LENGTH_LONG).show();
//            }
//        });
    }
}
