package com.snstfinger.suri.data.entity;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.network.DateTypeAdapter;
import com.snstfinger.suri.util.Utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StoreNPredictionEntity implements Serializable {
    @SerializedName("store_request_estimate_id")
    public String storeRequestEstimateId;

    @SerializedName("store_id")
    public int storeId;

    @SerializedName("request_estimate_id")
    public String requestEstimateId;

    @SerializedName("distance")
    public String distance;

    @SerializedName("write_yn")
    public String writeYN;

    @SerializedName("store_name")
    public String storeName;

    @SerializedName("store_number")
    public String storeNumber;

    @SerializedName("store_manager_name")
    public String storeManagerName;

    @SerializedName("store_manager_id")
    public String storeManagerId;

    @SerializedName("store_image")
    public String storeImage;

    @SerializedName("store_description")
    public String storeDescription;

    @SerializedName("store_satisfaction")
    public String storeSatisfaction;

    @SerializedName("point_quality")
    public String pointQuality;

    @SerializedName("point_kindness")
    public String pointKindness;

    @SerializedName("point_accuracy")
    public String pointAccuracy;

    @SerializedName("point_score")
    public String pointScore;

    @SerializedName("review_total")
    public String reviewTotal;

    @SerializedName("city")
    public String city;

    @SerializedName("district")
    public String district;

    @SerializedName("address")
    public String address;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

    @SerializedName("register_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date registerDate;

    @SerializedName("service_phone")
    public String servicePhone;

    @SerializedName("service_pc")
    public String servicePc;

    @SerializedName("service_bike")
    public String serviceBike;

    @SerializedName("service_car")
    public String serviceCar;

    @SerializedName("store_url")
    public String storeUrl;

    @SerializedName("service_pickup")
    public String servicePickup;

    @SerializedName("service_engineer_visiting")
    public String serviceEngineerVisiting;

    @SerializedName("store_business_number")
    public String storeBusinessNumber;

    @SerializedName("store_business_number_image")
    public String storeBusinessNumberImage;

    @SerializedName("store_engineer")
    public String storeEngineer;

    @SerializedName("prediction_estimate_id")
    private String predictionEstimateId;

    @SerializedName("prediction_price")
    public String predictionPrice;

    @SerializedName("prediction_price_comment")
    public String predictionPriceComment;

    @SerializedName("price")
    public String price;

    @SerializedName("prediction_estimate_time")
    public String predictionEstimateTime;

    @SerializedName("prediction_estimate_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionEstimateDate;

    @SerializedName("prediction_start_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionStartDate;

    @SerializedName("prediction_end_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date predictionEndDate;

    @SerializedName("progress_rate")
    public String progressRate;

    @SerializedName("realtime_streaming")
    public String realtimeStreaming;

    @SerializedName("booking_date")
    @JsonAdapter(DateTypeAdapter.class)
    public Date bookingDate;

    @SerializedName("booker_name")
    public String bookerName;

    @SerializedName("booker_phonenumber")
    public String bookerPhonenumber;

    @SerializedName("prediction_image1")
    public String predictionImage1;

    @SerializedName("prediction_image2")
    public String predictionImage2;

    @SerializedName("status")
    public String status;

    @SerializedName("engineer_id")
    public String engineerId;

    @SerializedName("desired_reservation_date_yn")
    public String desiredReservationDateYN;

    @SerializedName("repair_comment")
    public String repairComment;

    @SerializedName("client_read_yn")
    public String clientReadYN;

    public String getRegisterDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return registerDate == null ? "" : formatter.format(registerDate);
    }

    public String getPredictionEstimateDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionEstimateDate == null ? "" : formatter.format(predictionEstimateDate);
    }

    public String getPredictionStartDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return predictionStartDate == null ? "" : formatter.format(predictionStartDate);
    }

    public String getBookingDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        return bookingDate == null ? "" : formatter.format(bookingDate);
    }

    public String getPredictionPriceFormatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return predictionPrice == null ? "0" : formatter.format(Float.parseFloat(predictionPrice));
    }

    public String getPriceFormatted() {
        DecimalFormat formatter = new DecimalFormat(Utils.NUMBER_FORMAT);
        return price == null ? "0" : formatter.format(Float.parseFloat(price));
    }

    public String getPredictionEstimateId() {
        if(!TextUtils.isEmpty(predictionEstimateId)) {
            int dot = predictionEstimateId.indexOf(".");
            if(dot != -1)
                predictionEstimateId = predictionEstimateId.substring(0,dot);
        }

        return predictionEstimateId;
    }

    public void setPredictionEstimateId(String predictionEstimateId) {
        this.predictionEstimateId = predictionEstimateId;
    }

    @NonNull
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("StoreNPredictionEntity Info::");
        sb.append(requestEstimateId);
        sb.append("::");
        sb.append(predictionEstimateId);
        sb.append("::");
        sb.append(status);

        return sb.toString();
    }

    public static ShopInfo convertToShopInfo(StoreNPredictionEntity in) {
        ShopInfo out = new ShopInfo();
        out.storeId = in.storeId;
        out.storeImage = in.storeImage;
        out.storeName = in.storeName;
        out.pointScore = in.pointScore;
        out.pointQuality = in.pointQuality;
        out.pointKindness = in.pointKindness;
        out.pointAccuracy = in.pointAccuracy;
        out.storeEngineer = in.storeEngineer;
        out.distance = in.distance;
        out.storeDescription = in.storeDescription;
        out.storeNumber = in.storeNumber;
        out.address = in.address;
        out.latitude = in.latitude;
        out.longitude = in.longitude;

        out.storeManagerId = in.storeManagerId;

        out.predictionEstimateId = in.getPredictionEstimateId();
        out.predictionPrice = in.predictionPrice;
        out.predictionEstimateDate = in.predictionEstimateDate;
        out.desiredReservationDateYN = in.desiredReservationDateYN;

        return out;
    }
}
