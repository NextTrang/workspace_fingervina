package com.snst.suripartner.lib.suri_complete;

public class SuriCompleteViewModel {
    public String predictEstId;
    public String reqEstId;
    public String repairComent;
    public String imageUrl_first;
    public String imageUrl_second;

    public SuriCompleteViewModel(SuriComplete suriComplete) {
        predictEstId = suriComplete.getPredictEstId();
        reqEstId = suriComplete.getReqEstId();
        repairComent = suriComplete.getRepairComent();
        imageUrl_first = suriComplete.getImageUrl_first();
        imageUrl_second = suriComplete.getImageUrl_second();
    }
}
