package com.snstfinger.suri.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.data.model.ShopInfo;
import com.snstfinger.suri.databinding.FragmentReviewListBinding;
import com.snstfinger.suri.ui.shop.PagedReviewAdapter;
import com.snstfinger.suri.ui.shop.OnReviewListInteractionListener;
import com.snstfinger.suri.ui.shop.ReviewViewModel;
import com.snstfinger.suri.ui.tools.ListVewDecorate;

import java.util.List;

public class AllReviewListFragment extends Fragment {
    private static final String ARG_SHOP_INFO = "shop_info";
    private OnReviewListInteractionListener mListener;

    private ShopInfo mShopInfo;
    private ReviewViewModel mReivewViewModel;
    private FragmentReviewListBinding mBinding;
    private ObservableArrayList<ReviewEntity> mItems;
    private PagedReviewAdapter mAdapter;

    public AllReviewListFragment() {
    }

    public static AllReviewListFragment newInstance(ShopInfo shopInfo) {
        AllReviewListFragment fragment = new AllReviewListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOP_INFO, shopInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mShopInfo = (ShopInfo) getArguments().getSerializable(ARG_SHOP_INFO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mReivewViewModel = ViewModelProviders.of(this).get(ReviewViewModel.class);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_review_list, container, false);
        mAdapter = new PagedReviewAdapter(mListener, true);
        mBinding.listviewReview.addItemDecoration(new ListVewDecorate(getContext(), 24, 12));
        mBinding.listviewReview.setAdapter(mAdapter);

        if(mShopInfo == null) {
            getAllReviewByStoreId(null);
        } else {
            getAllReviewByStoreId(String.valueOf(mShopInfo.storeId));
        }

        return mBinding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReviewListInteractionListener) {
            mListener = (OnReviewListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReviewListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getAllReviewByStoreId(String storeId) {
        mReivewViewModel.getReviewPagedList(storeId).observe(this, new Observer<PagedList<ReviewEntity>>() {
            @Override
            public void onChanged(PagedList<ReviewEntity> reviewEntities) {
                List<ReviewEntity> list = reviewEntities.snapshot();

                mAdapter.submitList(reviewEntities);
                // 리스트 크기가 항상 0 나옴...???
//                if(mAdapter.getItemCount() > 0) {
                    mBinding.listviewReview.setVisibility(View.VISIBLE);
                    mBinding.listviewReviewEmpty.setVisibility(View.GONE);
//                }
            }
        });

    }
}
