package com.snst.suripartner.lib.left_menu;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.pop_up.PopManagerActivity;

public class LeftMenuEvent {
    Context context;

    public LeftMenuEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onPopNotice(View view){
        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.NOTICE_LEFT_MENU);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.NOTICE_LEFT_MENU);
    }


}
