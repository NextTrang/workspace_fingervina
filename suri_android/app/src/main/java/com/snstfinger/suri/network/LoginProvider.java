package com.snstfinger.suri.network;

import com.snstfinger.suri.data.model.LoggedInUser;

public abstract class LoginProvider {
    public abstract void requestLogin(OnResultLoginListener onResultLoginListener);

    public abstract void requestUser(OnReceiveUserListener onReceiveUserListener);

    public abstract boolean isLoggedIn();

    public abstract void logout(OnResultLogoutListener onResultLogoutListener);

    public interface OnResultLoginListener {
        void onResult(int result, LoggedInUser user);
    }

    public interface OnResultLogoutListener {
        void onResult(int result);
    }

    public interface OnReceiveUserListener {
        void onReceive(LoggedInUser user);
    }
}
