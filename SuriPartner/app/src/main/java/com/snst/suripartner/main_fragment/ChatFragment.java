package com.snst.suripartner.main_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.snst.suripartner.R;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.chat.ChatAdapter;
import com.snst.suripartner.lib.chat.ChatViewModel;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment implements MyFragment {
    View view;
    private RecyclerView recyclerView;
    private List<ChatViewModel> mData = new ArrayList<>();

    public ChatFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.chat_fragment, container, false);
        recyclerView = view.findViewById(R.id.chat_recyclerview);
        ChatAdapter chatAdapter = new ChatAdapter(getContext(), mData);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(chatAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = Sharing.Instance().getChatViewModels();
    }

    @Override
    public void refresh() {
        mData = Sharing.Instance().getChatViewModels();
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .detach(ChatFragment.this)
                    .attach(ChatFragment.this)
                    .commit();
        }
    }
}
