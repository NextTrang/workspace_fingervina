package com.snstfinger.suri.ui.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.InvoiceFormEntity;
import com.snstfinger.suri.databinding.DialogInvoiceBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = InvoiceDialogFragment.class.getName();

    private String mRequestEstimateId;
    private boolean mTakeAction;
    private DialogInvoiceBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle options = getArguments();
        mRequestEstimateId = options.getString("request_estimate_id");
        mTakeAction = options.getBoolean("take_action");

        getInvoice(mRequestEstimateId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_invoice, container, false);
        binding.btnClose.setOnClickListener(this);
        binding.btnOk.setOnClickListener(this);
        binding.btnSubmit.setOnClickListener(this);

        binding.oldTotalPrice.setPaintFlags(binding.oldTotalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (mTakeAction) {
            binding.btnOk.setVisibility(View.GONE);
            binding.btnSubmit.setVisibility(View.VISIBLE);
        } else {
            binding.btnOk.setVisibility(View.VISIBLE);
            binding.btnSubmit.setVisibility(View.GONE);
        }

        return binding.getRoot();
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public void onStart() {
//        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
//        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height_long);
//        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        super.onStart();
    }

    public void onDismiss() {
        getDialog().dismiss();

    }

    public void onSubmit() {
        sendConfirmPayment(mRequestEstimateId);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_submit) {
            onSubmit();

        } else {
            onDismiss();
        }
    }

    private void getInvoice(String requestEstimateId) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");

        Call<CommonResponse> call = requestEstimateAPI.doPostGetPaymentStatement(cookie, requestEstimateId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, data.toString());

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("Info");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    InvoiceFormEntity invoice = gson.fromJson(json, InvoiceFormEntity.class);
                    binding.setInvoice(invoice);

                    if (invoice != null) {
                        Log.d(TAG, "invoice::" + json);
                    }

                } else {
                    Log.e(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostGetPaymentStatement onFailure");
            }
        });
    }

    private void sendConfirmPayment(String requestEstimateId) {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");

        Call<CommonResponse> call = requestEstimateAPI.doPostConfirmPaymentStatement(cookie, requestEstimateId);

        if (binding.getInvoice() != null) {
            if (binding.getInvoice().count > 1) {
                call = requestEstimateAPI.doPostReConfirmPaymentStatement(cookie, requestEstimateId);
            }
        }

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();

                    Log.d(TAG, data.toString());
                    //   Log.d(TAG, null.toString());

                } else {
                    Log.e(TAG, response.toString());

                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetConfirmPaymentStatement onFailure");

            }
        });

        dismiss();
    }
}
