package com.snst.suripartner.lib.store_info;

import com.snst.suripartner.labels.LbData;

import org.json.JSONException;
import org.json.JSONObject;

public class StoreInfo {
    private String storeBusinessNumber="";
    private String suriTodo="";
    private String suriComplete="";

    public StoreInfo() {
    }

    public StoreInfo(JSONObject jsInfo) {
        try {
            storeBusinessNumber = jsInfo.getString(LbData.LbInfo.STORE_BUSINESS_NUMBER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getter

    public String getStoreBusinessNumber() {
        return storeBusinessNumber;
    }

    public String getSuriTodo() {
        return suriTodo;
    }

    public String getSuriComplete() {
        return suriComplete;
    }

    //Setter
    public StoreInfo setSuriTodo(String suriTodo) {
        this.suriTodo = suriTodo;
        return this;
    }

    public StoreInfo setSuriComplete(String suriComplete) {
        this.suriComplete = suriComplete;
        return this;
    }
}
