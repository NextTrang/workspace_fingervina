package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class RepairOptionEntity {
    @SerializedName("repair_option_id")
    public int optionId;

    @SerializedName("repair_option_name")
    public String optionName;

    @SerializedName("object_id")
    public int objectId;
}
