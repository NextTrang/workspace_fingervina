package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseReviews {
    @SerializedName("pageMap")
    @Expose
    private PageMap pageMap;

    @SerializedName("Info")
    @Expose
    private List<ReviewEntity> reviewEntities = null;

    public PageMap getPageMap() {
        return pageMap;
    }

    public List<ReviewEntity> getReviewEntities() {
        return reviewEntities;
    }
}
