package com.snstfinger.suri.data.model;

public class GeoCondition {
    public double latitude;
    public double longitude;
    public int range;
    public String searchName;
    public String orderBy = "point_score desc";

    public GeoCondition(double latitude, double longitude, int range, String searchName, String orderBy){
        this.latitude = latitude;
        this.longitude = longitude;
        this.range = range;
        this.searchName = searchName;
        this.orderBy = orderBy;
    }

}
