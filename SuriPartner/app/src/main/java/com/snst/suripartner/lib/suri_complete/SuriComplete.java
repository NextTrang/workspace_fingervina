package com.snst.suripartner.lib.suri_complete;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.BR;

import java.io.Serializable;

public class SuriComplete extends BaseObservable implements Serializable {
    private String predictEstId;
    private String reqEstId;
    private String repairComent;
    private String imageUrl_first;
    private String imageUrl_second;
    private int progressRate;

    //Getter
    public String getPredictEstId() {
        return predictEstId;
    }

    public String getReqEstId() {
        return reqEstId;
    }

    public String getRepairComent() {
        return repairComent;
    }

    @Bindable
    public String getImageUrl_first() {
        return imageUrl_first;
    }

    @Bindable
    public String getImageUrl_second() {
        return imageUrl_second;
    }

    public int getProgressRate() {
        return progressRate;
    }

    //Setter
    public void setRepairComent(String repairComent) {
        this.repairComent = repairComent;
    }

    public SuriComplete setPredictEstId(String predictEstId) {
        this.predictEstId = predictEstId;
        return this;
    }

    public SuriComplete setReqEstId(String reqEstId) {
        this.reqEstId = reqEstId;
        return this;
    }

    public SuriComplete setImageUrl_first(String imageUrl_first) {
        this.imageUrl_first = imageUrl_first;
        notifyPropertyChanged(com.snst.suripartner.BR.imageUrl_first);
        return this;
    }

    public SuriComplete setImageUrl_second(String imageUrl_second) {
        this.imageUrl_second = imageUrl_second;
        notifyPropertyChanged(com.snst.suripartner.BR.imageUrl_second);
        return this;
    }

    public SuriComplete setProgressRate(int progressRate) {
        this.progressRate = progressRate;
        return this;
    }
}
