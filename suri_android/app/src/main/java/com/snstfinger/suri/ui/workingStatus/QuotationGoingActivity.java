package com.snstfinger.suri.ui.workingStatus;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.snstfinger.suri.MyFragment;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.ui.dialog.InvoiceDialogFragment;
import com.snstfinger.suri.ui.shop.ShopInfoActivity;
import com.snstfinger.suri.ui.shop.ShopInfoActivityFragment;
import com.snstfinger.suri.ui.tools.WarrantyActivity;
import com.snstfinger.suri.ui.tools.WebChatActivity;

/**
 * 견적제안한 상점의 부킹상세화면,
 * 예약한 상점의 작업진행 상세화면(인보이스상태 포함)
 */
public class QuotationGoingActivity extends AppCompatActivity implements View.OnClickListener, MyFragment {
    private static final String TAG = QuotationGoingActivity.class.getName();

    private RequestNPredictionNStoreEntity mSelectedRequestEstimate;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation_going);
        View container = findViewById(R.id.layout_main_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mFragmentManager = getSupportFragmentManager();


        ImageButton warranty = findViewById(R.id.icon_warranty);
        warranty.setOnClickListener(this);
        Intent estimateStatus = getIntent();
        mSelectedRequestEstimate = (RequestNPredictionNStoreEntity) estimateStatus.getSerializableExtra("request_estimate");
        Log.d(TAG, "Selected Request Estimate::" + mSelectedRequestEstimate);

        if (mSelectedRequestEstimate != null) {
            // enum('REQUEST','BOOKING','BOOKING_CONFIRM','SENDING_PAYMENT','REPAIR_STANDBY','REPAIR','SENDING_PAYMENT_UPDATE','COMPLETE','REVIEW')
            switch (mSelectedRequestEstimate.getStatusCode()) {
                case RequestNPredictionNStoreEntity.TYPE_REQUEST: {
                    actionBar.setTitle(R.string.title_activity_quotation_waiting);
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_BOOKING:
                case RequestNPredictionNStoreEntity.TYPE_BOOKING_CONFIRM: {
                    actionBar.setTitle(R.string.title_activity_quotation_booking);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationBookingActivityFragment()).commit();
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT:
                case RequestNPredictionNStoreEntity.TYPE_SENDING_PAYMENT_UPDATE: {
                    actionBar.setTitle(R.string.title_activity_quotation_invoice);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }

                case RequestNPredictionNStoreEntity.TYPE_REVIEW: {
                    actionBar.setTitle(R.string.title_activity_quotation_complete);
                    container.setBackgroundResource(R.color.light_blue);
                    warranty.setVisibility(View.VISIBLE);
                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }

                default: {
                    actionBar.setTitle(R.string.title_activity_quotation_ongoing);
                    container.setBackgroundResource(R.color.light_blue);
                    // 테스트중으로 여기서도 보이게함.
                    // 리뷰이후에만 보이는게 정상
                    // warranty.setVisibility(View.VISIBLE);

                    mFragmentManager.beginTransaction().replace(R.id.content_fragment_layout, new QuotationGoingActivityFragment()).commit();
                    break;
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_content: {
                Intent detail = new Intent(this, EstimateDetailsActivity.class);
                detail.putExtra("request_estimate", mSelectedRequestEstimate);
                startActivity(detail);

                break;
            }

            case R.id.btn_chat: {
                makeChat();
                break;
            }

            case R.id.btn_call: {
                makeCall();
                break;
            }

            case R.id.btn_desc_all: {

                break;
            }

            case R.id.btn_take_invoice: {
                showDialogByRequestEstimateId(true);
                break;
            }

            case R.id.btn_suri_complete: {
                Intent review = new Intent(this, WriteReviewActivity.class);
                review.putExtra("request_estimate", mSelectedRequestEstimate);
//                review.putExtra("shop_info", item);
                startActivity(review);
                break;
            }

            case R.id.icon_warranty: {
                Intent warranty = new Intent(this, WarrantyActivity.class);
                startActivity(warranty);
                break;
            }

            default:
                Log.d(TAG, "View Clicked::" + v);
        }
    }

    void showDialogByRequestEstimateId(boolean takAction) {
        InvoiceDialogFragment dialog = new InvoiceDialogFragment();
        Bundle args = new Bundle();
        args.putString("request_estimate_id", mSelectedRequestEstimate.requestEstimateId);
        args.putBoolean("take_action", takAction);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "invoice");
    }

    @SuppressLint("NewApi")
    private void makeCall() {
        String tel = "tel:" + mSelectedRequestEstimate.storeNumber;
        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(tel)));
    }

    private void makeChat() {
        Intent goChat = new Intent(this, WebChatActivity.class);
        goChat.putExtra("request_estimate", mSelectedRequestEstimate);
        goChat.putExtra("target", mSelectedRequestEstimate.storeManagerId);
        startActivity(goChat);
    }

    @Override
    public void refresh() {
        Fragment fragment = mFragmentManager.getFragments().get(0);
        if(fragment.getClass() == QuotationGoingActivityFragment.class){
            ((MyFragment) fragment).refresh();
        }
    }
}
