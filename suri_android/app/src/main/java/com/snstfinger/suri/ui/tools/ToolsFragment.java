package com.snstfinger.suri.ui.tools;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.snstfinger.suri.R;
import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.data.model.AccountType;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.network.SuriLoginManager;
import com.snstfinger.suri.network.UserAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ToolsFragment extends Fragment implements View.OnClickListener, SuriLoginManager.OnResultLogoutListener {
    private static final String TAG = ToolsFragment.class.getName();

    private ToolsViewModel toolsViewModel;
    TextInputEditText id;
    TextInputEditText email;
    TextInputEditText name;
    TextInputEditText phone;
    CheckBox isSns;

//    private CompositeDisposable disposable;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
        toolsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        id = root.findViewById(R.id.user_id);
        email = root.findViewById(R.id.user_email);
        name = root.findViewById(R.id.title_section);
        phone = root.findViewById(R.id.user_phone);
        isSns = root.findViewById(R.id.is_sns);

        Button login = root.findViewById(R.id.login);
        login.setOnClickListener(this);
        Button save = root.findViewById(R.id.save_user);
        save.setOnClickListener(this);
        Button getUser = root.findViewById(R.id.get_user);
        getUser.setOnClickListener(this);
        Button delUser = root.findViewById(R.id.del_user);
        delUser.setOnClickListener(this);
        Button updateUser = root.findViewById(R.id.update_user);
        updateUser.setOnClickListener(this);
        Button logout = root.findViewById(R.id.logout);
        logout.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
        RequestEstimateAPI fileAPI = ApiManager.getProvider(RequestEstimateAPI.class);

        switch (vId) {
            case R.id.login: {
                LoggedInUser user = new LoggedInUser(id.getText().toString(), email.getText().toString(), email.getText().toString(), name.getText().toString(), phone.getText().toString(), isSns.isChecked() ? "SNS" : "ACCOUNT");
                Call<CommonResponse> call = userAPI.doLogin(user.getUserId(), user.getUserPw(), user.getLoginType(), SuriApplication.mSuriToken);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            Log.d(TAG, res.toString());

                            List<String> cookies = response.raw().headers("Set-Cookie");
                            if (cookies != null) {
                                for (String cookie : cookies) {
                                    Log.d(TAG, "cookies::" + cookie);
                                    String sessionid = cookie.split(";\\s*")[0];
                                    if (sessionid.startsWith("starrkCookie")) {
                                        CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
                                    }
                                }

                                String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                                Log.d(TAG, "doLogin SessionId=" + cook);
                            }

                        } else {
                            Log.e(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        call.cancel();
                        Log.e(TAG, "doLogin onFailure");
                    }
                });
//                LoggedInUser user = new LoggedInUser(id.getText().toString(), email.getText().toString(), email.getText().toString(), name.getText().toString(), phone.getText().toString(), isSns.isChecked() ? "SNS" : "ACCOUNT");
//                disposable = new CompositeDisposable();
//                disposable.add(userAPI.doLogin(user.getUserId(), user.getUserPw(), user.getLoginType())
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribeWith( new DisposableSingleObserver<CommonResponse>() {
//                                @Override
//                                public void onSuccess(CommonResponse commonResponse) {
//                                    Log.d(TAG, "res::"+commonResponse.getQuotationPrice());
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    Log.e(TAG, "res::"+e.getMessage());
//                                }
//                            }));
            }

            break;

            case R.id.save_user: {
                // api test
                LoggedInUser user = new LoggedInUser(id.getText().toString(), email.getText().toString(), email.getText().toString(), name.getText().toString(), phone.getText().toString(), isSns.isChecked() ? "SNS" : "ACCOUNT");
                Call<CommonResponse> call = userAPI.doCreateUser(user);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            Log.d(TAG, res.toString());

                            List<String> cookies = response.raw().headers("Set-Cookie");
                            if (cookies != null) {
                                for (String cookie : cookies) {
                                    Log.d(TAG, "cookies::" + cookie);
                                    String sessionid = cookie.split(";\\s*")[0];
                                    if (sessionid.startsWith("starrkCookie")) {
                                        CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
                                    }
                                }

                                String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                                Log.d(TAG, "doCreateUser SessionId=" + cook);
                            }

                        } else {
                            Log.e(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        call.cancel();
                        Log.e(TAG, "doCreateUser onFailure");
                    }
                });
            }

            break;

            case R.id.update_user: {
                // api test
                LoggedInUser user = new LoggedInUser(id.getText().toString(), email.getText().toString(), email.getText().toString(), name.getText().toString(), phone.getText().toString(), isSns.isChecked() ? "SNS" : "ACCOUNT");
                Call<CommonResponse> call = userAPI.doUpdateUser(user);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            Log.d(TAG, res.toString());

                            List<String> cookies = response.raw().headers("Set-Cookie");
                            if (cookies != null) {
                                for (String cookie : cookies) {
                                    Log.d(TAG, "cookies::" + cookie);
                                    String sessionid = cookie.split(";\\s*")[0];
                                    if (sessionid.startsWith("starrkCookie")) {
                                        CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
                                    }
                                }

                                String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                                Log.d(TAG, "doUpdateUser SessionId=" + cook);
                            }

                        } else {
                            Log.e(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        call.cancel();
                        Log.e(TAG, "doUpdateUser onFailure");
                    }
                });
            }

            break;

            case R.id.get_user: {
                String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                Call<CommonResponse> call = userAPI.doGetUserId(cookie, "1", id.getText().toString());
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            Object data = res.getData();
                            Object info = ((LinkedTreeMap) data).get("Info");

                            Gson gson = new Gson();
                            JsonObject json = gson.toJsonTree(info).getAsJsonObject();
                            LoggedInUser user = gson.fromJson(json, LoggedInUser.class);

                            Log.d(TAG, "res::code::" + res.getStatus());
                            Log.d(TAG, "res::data::" + data);
//                                String jsonString = res.getData().toString();

                            if (info instanceof PageMap) {
                                Log.d(TAG, "data::pageMap::" + data);

                            } else if (info instanceof LoggedInUser) {
                                Log.d(TAG, "res::data::Info::" + data);

                            }

                        } else {
                            Log.e(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        call.cancel();
                        Log.e(TAG, "doGetUserId onFailure");
                    }
                });

//                LoggedInUser user = new LoggedInUser(id.getText().toString(), email.getText().toString(), email.getText().toString(), name.getText().toString(), phone.getText().toString(), isSns.isChecked() ? "SNS" : "ACCOUNT");
//                String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
//                disposable = new CompositeDisposable();
//                disposable.add(userAPI.doGetUserId(cookie, "1", id.getText().toString())
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribeWith( new DisposableSingleObserver<CommonResponse>() {
//                            @Override
//                            public void onSuccess(CommonResponse commonResponse) {
//                                commonResponse.
//                                Log.d(TAG, "res::code::"+commonResponse.getQuotationPrice());
//                                Log.d(TAG, "res::data::"+commonResponse.getData());
//                                String jsonString = commonResponse.getData().toString();
//                                if(jsonString != null) {
//                                    Gson g = new GsonBuilder().serializeNulls().create();
//                                    JsonObject json = g.fromJson(jsonString, JsonObject.class);
//                                    JsonObject userInfo = (JsonObject) json.get("Info");
//                                    LoggedInUser user = g.fromJson(userInfo, LoggedInUser.class);
//                                    Log.d(TAG, "profile::"+ jsonString);
//                                }
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                Log.e(TAG, "res::"+e.getMessage());
//                            }
//                        }));

            }
            break;

            case R.id.del_user: {
                String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
                Call<CommonResponse> call = userAPI.doGetDeleteUser(cookie, id.getText().toString());
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            Log.d(TAG, res.toString());

                        } else {
                            Log.e(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        call.cancel();
                        Log.e(TAG, "API onFailure");
                    }
                });
            }

            break;

            case R.id.logout: {
                AccountType accountType = SuriLoginManager.getInstance(getActivity()).getAccountType();
                Log.e(TAG, "NOT supported yet::" + accountType == null ? "null" : accountType.name());
                SuriLoginManager.getInstance(getActivity()).logout(this);

            }
            break;

            case R.id.upload: {
                String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
//                File file = new File(imgPath);
//                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//                MultipartBody.Part uploadFile = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
//                Log.i(TAG, "insertPromote: "+ file.getName());
//                Log.i(TAG, "insertPromote: "+ requestFile.contentType());
//                Log.i(TAG, "insertPromote: "+ uploadFile.body());
//                Call<CommonResponse> call = fileAPI.doPostUploadFile(uploadFile);
//                call.enqueue(new Callback<CommonResponse>() {
//                    @Override
//                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//
//                        if (response.isSuccessful()) {
//                            CommonResponse res = response.body();
//                            Log.d(TAG, res.toString());
//
//                        } else {
//                            Log.e(TAG, response.toString());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<CommonResponse> call, Throwable t) {
//                        call.cancel();
//                        Log.e(TAG, "API onFailure");
//                    }
//                });
            }
            break;
        }

    }

    @Override
    public void onResult(int result) {
        Log.d(TAG, "result::" + result);

    }
}