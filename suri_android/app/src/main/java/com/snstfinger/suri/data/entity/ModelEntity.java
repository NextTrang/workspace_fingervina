package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class ModelEntity {
    @SerializedName("model_id")
    public int modelId;

    @SerializedName("model_name")
    public String modelName;

    @SerializedName("brand_id")
    public int brandId;
}
