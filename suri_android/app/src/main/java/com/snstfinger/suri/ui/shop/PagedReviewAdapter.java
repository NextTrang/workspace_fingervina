package com.snstfinger.suri.ui.shop;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.databinding.ListItemReviewBinding;
import com.snstfinger.suri.databinding.ListItemSimpleReviewBinding;

public class PagedReviewAdapter extends PagedListAdapter<ReviewEntity, RecyclerView.ViewHolder> {

    private final OnReviewListInteractionListener mListener;
    private boolean mIsSimple;

    public PagedReviewAdapter(OnReviewListInteractionListener listener, boolean isSimple) {
        super(DIFF_CALLBACK);
        mListener = listener;
        mIsSimple = isSimple;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mIsSimple) {
            ListItemSimpleReviewBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_simple_review, parent, false);
            return new SimpleReviewViewHolder(binding);

        } else {
            ListItemReviewBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_review, parent, false);
            return new ReviewViewHolder(binding);

        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ReviewEntity item = getItem(position);

        if (mIsSimple)
            ((SimpleReviewViewHolder) holder).bind(item);
        else
            ((ReviewViewHolder) holder).bind(item);

    }

    private static DiffUtil.ItemCallback<ReviewEntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<ReviewEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull ReviewEntity oldItem, @NonNull ReviewEntity newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull ReviewEntity oldItem, @NonNull ReviewEntity newItem) {
            return oldItem.getReviewId().equals(newItem.getReviewId());
        }
    };

    public class ReviewViewHolder extends RecyclerView.ViewHolder {
        ListItemReviewBinding binding;

        public ReviewViewHolder(ListItemReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ReviewEntity item) {
            binding.setReviewItem(item);
        }
    }

    public class SimpleReviewViewHolder extends RecyclerView.ViewHolder {
        ListItemSimpleReviewBinding binding;

        public SimpleReviewViewHolder(ListItemSimpleReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ReviewEntity item) {
            binding.setReviewItem(item);
        }
    }
}
