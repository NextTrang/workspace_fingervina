package com.snstfinger.suri.ui.workingStatus;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.SuriStoryEntity;
import com.snstfinger.suri.databinding.ListItemSuriHistoryBinding;

import java.util.ArrayList;
import java.util.List;

public class SuriStoryRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<SuriStoryEntity> mValues;

    public SuriStoryRecyclerViewAdapter() {
        mValues = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemSuriHistoryBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_suri_history, parent, false);
        return new StoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SuriStoryEntity item = mValues.get(position);
        ((StoryViewHolder) holder).bind(item);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setItemList(List<SuriStoryEntity> item) {
        if (item != null) {
            this.mValues = item;
            notifyDataSetChanged();
        }
    }

    public class StoryViewHolder extends RecyclerView.ViewHolder {
        ListItemSuriHistoryBinding binding;

        public StoryViewHolder(ListItemSuriHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(SuriStoryEntity item) {
            binding.setStoryItem(item);
        }
    }
}
