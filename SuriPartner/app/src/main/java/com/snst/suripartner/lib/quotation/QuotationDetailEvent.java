package com.snst.suripartner.lib.quotation;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.api.RequestOffer;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.map.MyLatLng;
import com.snst.suripartner.lib.map.MyMap;
import com.snst.suripartner.pop_up.PopManagerActivity;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Map;

public class QuotationDetailEvent {

    Context context;

    public QuotationDetailEvent(Context context) {
        this.context = context;
    }

    public void onClose(View view) {
        ((AppCompatActivity) context).finish();
    }

    public void onOffer(Quotation quotation) {
        String[] inputs = new String[7];
        inputs[0] = quotation.getRequestEstId();
        inputs[1] = Sharing.Instance().getUser().getStoreId();
        inputs[2] = quotation.getEstPrice();
        inputs[3] = quotation.getPredictionPriceComment();
        String estCompleteDateTime = quotation.getEstCompletedTime() + ":00 " + Conversion.date_2_string(quotation.getEstCompletedDate());
        inputs[4] = Conversion.string_2_serverDate(estCompleteDateTime);
        inputs[5] = quotation.getUserId();
        inputs[6] = quotation.getDesiredReservationDateYn() == true ? "y" : "n";

        new RequestOffer().execute(context, inputs);
    }

    public void onTransferData(QuotationViewModel viewModel) {
        viewModel.updateCost();

        Intent i = new Intent();
        i.putExtra(LbPop.LbKey.QUOTATION, viewModel);
        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setResult(activity.RESULT_OK, i);
        activity.finish();
    }

    public void onPopMap(QuotationViewModel viewModel){
        String[] strLatLng = viewModel.addressLatLng.split("[|]");
        MyMap map = new MyMap()
                .setDestination(new MyLatLng(Double.parseDouble(strLatLng[0]),Double.parseDouble(strLatLng[1])))
                .setDesAddress(viewModel.address);

        Intent intent = new Intent(context, PopManagerActivity.class);
        intent.putExtra(LbPop.LbKey.POP_CODE, LbPop.LbCode.SEE_MAP);
        intent.putExtra(LbPop.LbKey.MY_MAP, map);
        ((AppCompatActivity) context).startActivityForResult(intent, LbPop.LbCode.SEE_MAP);
    }
}
