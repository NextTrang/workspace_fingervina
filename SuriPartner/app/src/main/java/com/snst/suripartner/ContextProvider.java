package com.snst.suripartner;

import android.content.Context;

public interface  ContextProvider {
    Context getActivityContext();
}
