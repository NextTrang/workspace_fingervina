package com.snstfinger.suri.ui.tools

internal enum class ImageSource(val source: Int) {
    GALLERY(1),
    CAMERA(2),
    DUM(3)
}