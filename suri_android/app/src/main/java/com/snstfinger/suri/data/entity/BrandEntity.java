package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.SerializedName;

public class BrandEntity {
    @SerializedName("brand_id")
    public int brandId;

    @SerializedName("brand_name")
    public String brandName;

    @SerializedName("object_id")
    public int objectId;
}
