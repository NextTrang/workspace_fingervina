package com.snst.suripartner.pop_up;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.haibin.calendarview.CalendarView;
import com.snst.suripartner.R;
import com.snst.suripartner.databinding.QuotationDetailBinding;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.TimeItemRecycleViewAdapter;
import com.snst.suripartner.lib.helper.EditTextHandler;
import com.snst.suripartner.lib.helper.SimpleWeekView;
import com.snst.suripartner.lib.quotation.QuotationViewModel;
import com.snst.suripartner.lib.repair_type.RepairType;
import com.snst.suripartner.lib.repair_type.RepairTypeAdapter;
import com.snst.suripartner.lib.repair_type.RepairTypeViewModel;
import com.snst.suripartner.lib.quotation.Quotation;
import com.snst.suripartner.lib.quotation.QuotationDetailEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class PopQuotationDetailActivity extends AppCompatActivity implements CalendarView.OnCalendarSelectListener, TimeItemRecycleViewAdapter.OnSelectTimeInteractionListener {

    private QuotationViewModel data;
    private QuotationDetailEvent handler;

    private RepairTypeAdapter repairTypeAdapter;

    private com.haibin.calendarview.CalendarView mCalendarView;
    private TextView mTitleYearMonth;
    private RecyclerView mListViewTimes;
    private EditText txtEstPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QuotationDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_quotation_detail);

        //binding data
        data = (QuotationViewModel) getIntent().getSerializableExtra("Quotation");
        binding.setQuotationViewModel(data);
        handler = new QuotationDetailEvent(this);
        binding.setHandler(handler);
        txtEstPrice = binding.txtEstPrice;

        setUI();
        setEvent();

        binding.repairTypeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.repairTypeRecyclerView.setHasFixedSize(true);
        repairTypeAdapter = new RepairTypeAdapter(this, getRepairTypeViewModel());
        binding.repairTypeRecyclerView.setAdapter(repairTypeAdapter);

        mCalendarView = binding.customCalendarView;
        mTitleYearMonth = binding.titleYearMonth;
        mListViewTimes = binding.listViewTimes;

        Calendar mDesireDate = java.util.Calendar.getInstance();
        setupCalendarView(mDesireDate);
        setupListTimeView(mDesireDate);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                handler.onClose(getWindow().getDecorView().getRootView());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(data.id);
    }

    private void setEvent(){
        txtEstPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditTextHandler.formatCurrency(txtEstPrice);
            }
        });
    }

    private void setupCalendarView(Calendar mDesireDate) {
        // set estimated complete time to calendar
        long estCompletedDate = data.estCompletedDate;
        if (estCompletedDate > 0) {
            mDesireDate.setTimeInMillis(estCompletedDate);
        }

        mCalendarView.scrollToCalendar(mDesireDate.get(java.util.Calendar.YEAR), mDesireDate.get(java.util.Calendar.MONTH) + 1, mDesireDate.get(java.util.Calendar.DATE));
        mCalendarView.setOnCalendarSelectListener(this);
        SimpleWeekView.enable = !data.state.equals(LbState.OFFERED);
        mTitleYearMonth.setText(SimpleWeekView.getMonthName(mDesireDate.get(java.util.Calendar.MONTH) + 1) + " " + mDesireDate.get(java.util.Calendar.YEAR));

        data.setEstCompletedDate(mDesireDate.getTimeInMillis());
    }

    private void setupListTimeView(Calendar mDesireDate) {
        // timetable
        String estCompletedTime = data.estCompletedTime;
        if (!TextUtils.isEmpty(estCompletedTime)) {
            String time[] = estCompletedTime.split(":");
            mDesireDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
            mDesireDate.set(java.util.Calendar.MINUTE, Integer.valueOf(time[1]));
            mDesireDate.set(java.util.Calendar.SECOND, 0);
        }

        ArrayList<String> mItems = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.time_table)));
        TimeItemRecycleViewAdapter mAdapter = new TimeItemRecycleViewAdapter(this, mItems, this, !data.state.equals(LbState.OFFERED));
        mListViewTimes.setAdapter(mAdapter);
        setTimeTable(mDesireDate);

        data.setEstCompletedTime(mItems.get(mAdapter.getPositionFromDate(mDesireDate)));
    }

    private List<RepairTypeViewModel> getRepairTypeViewModel() {
        List<RepairTypeViewModel> viewModels = new ArrayList<>();

        for (RepairType type : data.estimateRepairTypes) {
            viewModels.add(new RepairTypeViewModel(type));
        }
        return viewModels;
    }

    private void setTimeTable(java.util.Calendar dateTime) {
        TimeItemRecycleViewAdapter adapter = (TimeItemRecycleViewAdapter) mListViewTimes.getAdapter();
        int position = adapter.getPositionFromDate(dateTime);
        adapter.setSelectTime(position);
        mListViewTimes.scrollToPosition(position);
    }

    @Override
    public void onCalendarOutOfRange(com.haibin.calendarview.Calendar calendar) {

    }

    @Override
    public void onCalendarSelect(com.haibin.calendarview.Calendar calendar, boolean isClick) {
        mTitleYearMonth.setText(SimpleWeekView.getMonthName(calendar.getMonth()) + " " + calendar.getYear());

        if (isClick) {
            data.setEstCompletedDate(calendar.getTimeInMillis());
        }
    }

    @Override
    public void onSelectTimeInteraction(String item) {
        if (!TextUtils.isEmpty(item)) {
            data.setEstCompletedTime(item);
        }
    }
}
