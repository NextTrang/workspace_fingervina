package com.snst.suripartner.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.WriteSuriHistoryBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.helper.Devicer;
import com.snst.suripartner.lib.helper.Getter;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri_history.SuriHistory;
import com.snst.suripartner.lib.suri_history.SuriHistoryEvent;

import java.io.File;

public class PopWriteSuriHistoryActivity extends AppCompatActivity {

    private Suri suri;
    private SuriHistory suriHistory = new SuriHistory();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WriteSuriHistoryBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_write_suri_history);

        suri = (Suri) getIntent().getSerializableExtra("Suri");

        //binding data
        binding.setHandler(new SuriHistoryEvent(this));
        binding.setSuriHistory(suriHistory.setId(suri.getId()).setPredictEstId(suri.getPredictEstId()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            //get file returned from Gallery
            File photoFile = (File) data.getSerializableExtra(LbPop.LbKey.PHOTO_FILE);
            boolean openCamera = data.getBooleanExtra(LbPop.LbKey.OPEN_CAMERA, false);

            if (openCamera) {
                Devicer.openCamera(this);
            } else if (photoFile != null) {
                suriHistory.setImageUrl(Getter.getRealPathFromURI(this, Uri.fromFile(photoFile)));
            }
        } else if (requestCode == LbPop.LbCode.CAMERA_REQUEST) {
            suriHistory.setImageUrl(Getter.getRealPathFromURI(this, Uri.fromFile(Devicer.photoFile)));
            Devicer.photoFile = null;
        }
    }
}
