package com.snstfinger.suri.network;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {
    @SerializedName("data")
    private Object data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private Integer status;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("Code:");
        result.append(status);
        result.append(" msg:");
        result.append(msg);
        result.append(" data:");
        result.append(data);
        return result.toString();
    }
}
