package com.snst.suripartner.lib.popCancelReason;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class PopCancelReason extends BaseObservable {
    private String title;
    private String reason;
    private int reasonId;

    //Getter
    public String getTitle() {
        return title;
    }

    @Bindable
    public String getReason() {
        return reason;
    }

    @Bindable
    public int getReasonId() {
        return reasonId;
    }

    //Setter
    public PopCancelReason setTitle(String title) {
        this.title = title;
        return this;
    }

    public PopCancelReason setReason(String reason) {
        this.reason = reason;
        notifyPropertyChanged(com.snst.suripartner.BR.reason);
        return this;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
        notifyPropertyChanged(com.snst.suripartner.BR.reasonId);
    }
}
