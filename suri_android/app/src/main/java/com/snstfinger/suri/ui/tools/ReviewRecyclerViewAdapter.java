package com.snstfinger.suri.ui.tools;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.ReviewEntity;
import com.snstfinger.suri.databinding.ListItemReviewBinding;
import com.snstfinger.suri.databinding.ListItemSimpleReviewBinding;
import com.snstfinger.suri.ui.shop.OnReviewListInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class ReviewRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ReviewEntity> mValues;
    private final OnReviewListInteractionListener mListener;
    private boolean mIsSimple;

    public ReviewRecyclerViewAdapter(OnReviewListInteractionListener listener, boolean isSimple) {
        mValues = new ArrayList<>();
        mListener = listener;
        mIsSimple = isSimple;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mIsSimple) {
            ListItemSimpleReviewBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_simple_review, parent, false);
            return new SimpleReviewViewHolder(binding);

        } else {
            ListItemReviewBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.list_item_review, parent, false);
            return new ReviewViewHolder(binding);

        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ReviewEntity item = mValues.get(position);
        if (mIsSimple)
            ((SimpleReviewViewHolder) holder).bind(item);
        else
            ((ReviewViewHolder) holder).bind(item);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setItemList(List<ReviewEntity> item) {
        if (item != null) {
            this.mValues = item;
            notifyDataSetChanged();
        }
    }

    public class ReviewViewHolder extends RecyclerView.ViewHolder {
        ListItemReviewBinding binding;

        public ReviewViewHolder(ListItemReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ReviewEntity item) {
            binding.setReviewItem(item);
        }
    }

    public class SimpleReviewViewHolder extends RecyclerView.ViewHolder {
        ListItemSimpleReviewBinding binding;

        public SimpleReviewViewHolder(ListItemSimpleReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ReviewEntity item) {
            binding.setReviewItem(item);
        }
    }
}
