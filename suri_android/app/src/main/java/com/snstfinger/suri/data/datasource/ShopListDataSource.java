package com.snstfinger.suri.data.datasource;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.snstfinger.suri.data.entity.ResponseStores;
import com.snstfinger.suri.data.entity.StoreEntity;
import com.snstfinger.suri.data.entity.PageMap;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.StoreAPI;
import com.snstfinger.suri.data.model.GeoCondition;
import com.snstfinger.suri.util.Utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopListDataSource extends PageKeyedDataSource<Integer, StoreEntity> {
    public static final String TAG = ShopListDataSource.class.getName();

    public static final int PAGE_SIZE = 100;
    private static final int FIRST_PAGE = 1;

    private double latitude, longitude;
    private int range;
    private String searchName;
    private String orderBy;

    public ShopListDataSource(GeoCondition condition) {
        this.latitude = condition.latitude;
        this.longitude = condition.longitude;
        this.range = condition.range;
        this.searchName = condition.searchName;
        this.orderBy = condition.orderBy;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {
        if (TextUtils.isEmpty(searchName)) {
            initGetAllStoreByLocation(callback);
        } else {
            initGetAllStoreByName(callback);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if (TextUtils.isEmpty(searchName)) {
            loadPrePageAllStoreByLocation(params, callback);
        } else {
            loadPrePageGetAllStoreByName(params, callback);
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams params, @NonNull LoadCallback callback) {
        if (TextUtils.isEmpty(searchName)) {
            loadNextPageAllStoreByLocation(params, callback);
        } else {
            loadNextPageGetAllStoreByName(params, callback);
        }
    }

    // region doGetAllStoreInfo
    private void initGetAllStore(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doGetAllStoreInfo(cookie, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

                        @Override
                        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                throws JsonParseException {
                            try {
                                DateFormat m_ISO8601Local = new SimpleDateFormat(Utils.DATE_FORMAT_SERVER);
                                return m_ISO8601Local.parse(json.getAsString());
//                                return new SimpleDateFormat().parse(json.getAsString());
                            } catch (ParseException e) {
                                return null;
                            }
                        }
                    }).create();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<StoreEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    callback.onResult(item, FIRST_PAGE, page.count, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetAllStoreInfo onFailure");
            }
        });
    }

    private void loadPrePageAllStore(LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doGetAllStoreInfo(cookie, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<StoreEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetAllStoreInfo onFailure");
            }
        });
    }

    private void loadNextPageAllStore(LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doGetAllStoreInfo(cookie, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    Object paging = ((LinkedTreeMap) data).get("pageMap");
                    JsonObject json = gson.toJsonTree(paging).getAsJsonObject();
                    PageMap page = gson.fromJson(json, PageMap.class);
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + json);
                    }

                    Object info = ((LinkedTreeMap) data).get("Info");
                    Type itemType = new TypeToken<List<StoreEntity>>() {
                    }.getType();
                    JsonArray asJsonArray = gson.toJsonTree(info).getAsJsonArray();
                    List item = gson.fromJson(asJsonArray, itemType);

                    callback.onResult(item, page.nextPage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doGetAllStoreInfo onFailure");
            }
        });
    }
    // endregion

    // region doPostGetStoreInfoByCoordinate
    private void initGetAllStoreByLocation(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByCoordinate(cookie, String.valueOf(latitude), String.valueOf(longitude), String.valueOf(range), null, String.valueOf(FIRST_PAGE));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "initGetAllStoreByLocation onFailure");
            }
        });
    }

    private void loadPrePageAllStoreByLocation(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByCoordinate(cookie, String.valueOf(latitude), String.valueOf(longitude), String.valueOf(range), null, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "loadPrePageAllStoreByLocation onFailure");
            }
        });
    }

    private void loadNextPageAllStoreByLocation(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByCoordinate(cookie, String.valueOf(latitude), String.valueOf(longitude), String.valueOf(range), null, String.valueOf(params.key));
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "loadNextPageAllStoreByLocation onFailure");
            }
        });
    }
    // endregion

    // region doPostGetStoreInfoByName
    private void initGetAllStoreByName(LoadInitialCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByName(cookie, searchName, String.valueOf(FIRST_PAGE), orderBy);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    callback.onResult(item, null, FIRST_PAGE + 1);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "initGetAllStoreByName onFailure");
            }
        });
    }

    private void loadPrePageGetAllStoreByName(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByName(cookie, searchName, String.valueOf(params.key), orderBy);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    callback.onResult(item, page.prePage);

                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "loadPrePageGetAllStoreByName onFailure");
            }
        });
    }

    private void loadNextPageGetAllStoreByName(LoadParams params, LoadCallback callback) {
        StoreAPI storeAPI = ApiManager.getProvider(StoreAPI.class);

        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");
        Call<CommonResponse> call = storeAPI.doPostGetStoreInfoByName(cookie, searchName, String.valueOf(params.key), orderBy);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    Log.d(TAG, "res::code::" + res.getStatus());
//                    Log.d(TAG, "res::data::" + data);

                    Gson gson = new Gson();
                    JsonObject json = gson.toJsonTree(data).getAsJsonObject();
                    ResponseStores result = gson.fromJson(json, ResponseStores.class);

                    PageMap page = result.getPageMap();
                    if (page != null) {
                        Log.d(TAG, "PageInfo::" + page.count);
                    }

                    List item = result.getStoreEntities();

                    if (page.count > ((Integer) params.key) * PAGE_SIZE)
                        callback.onResult(item, page.page + 1);
                    else {
                        callback.onResult(item, null);
                    }
                } else {
                    Log.e(TAG, response.toString());
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "loadNextPageGetAllStoreByName onFailure");
            }
        });
    }
    // endregion
}
