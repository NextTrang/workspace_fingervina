package com.snstfinger.suri.ui.tools

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
internal data class ImageItem(var imagePath: String, var source: ImageSource, var selected: Int) : Parcelable