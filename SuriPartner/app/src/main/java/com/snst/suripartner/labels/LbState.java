package com.snst.suripartner.labels;

public class LbState {
    public static final String INVOICE = "Invoice";
    public static final String ONGOING = "Ongoing";
    public static final String WAITING = "Waiting";
    public static final String COMPLETED = "Completed";
    public static final String OFFERED = "Offered";

    public static class LbMask{
        public static final String START_SURI = "Start suri";
        public static final String REPAIR = "Repair";
        public static final String UPDATE_PAYMENT = "Updating payment";
        public static final String WAITING_CONFIRM = "Waiting confirm";
        public static final String SEND_INVOICE = "Send invoice";
    }
}
