package com.snstfinger.suri.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseStoreNPrediction {
    @SerializedName("pageMap")
    @Expose
    private PageMap pageMap;

    @SerializedName("Info")
    @Expose
    private List<StoreNPredictionEntity> storeNPredictionEntities = null;

    public PageMap getPageMap() {
        return pageMap;
    }

    public List<StoreNPredictionEntity> getStoreNPredictionEntities() {
        return storeNPredictionEntities;
    }
}
