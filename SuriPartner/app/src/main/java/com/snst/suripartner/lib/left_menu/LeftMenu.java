package com.snst.suripartner.lib.left_menu;

public class LeftMenu {
    private String imageUrl;
    private String storeBusinessNumber;
    private String needToSuri;
    private String suriComplete;

    //Getter
    public String getImageUrl() {
        return imageUrl;
    }

    public String getStoreBusinessNumber() {
        return storeBusinessNumber;
    }

    public String getNeedToSuri() {
        return needToSuri;
    }

    public String getSuriComplete() {
        return suriComplete;
    }

    //Setter

    public LeftMenu setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public LeftMenu setStoreBusinessNumber(String storeBusinessNumber) {
        this.storeBusinessNumber = storeBusinessNumber;
        return this;
    }

    public LeftMenu setNeedToSuri(String needToSuri) {
        this.needToSuri = needToSuri;
        return this;
    }

    public LeftMenu setSuriComplete(String suriComplete) {
        this.suriComplete = suriComplete;
        return this;
    }
}
