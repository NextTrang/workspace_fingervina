package com.snstfinger.suri.ui.workingStatus;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedList;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.StoreNPredictionEntity;
import com.snstfinger.suri.databinding.ListItemQuotationBinding;

public class QuotationShopListAdapter extends PagedListAdapter<StoreNPredictionEntity, QuotationShopListAdapter.ItemViewHolder> {
    private static final String TAG = QuotationShopListAdapter.class.getName();

    private Context mContext;
    private QuotationShopListFragment.OnListFragmentInteractionListener mListener;
    private PagedList<StoreNPredictionEntity> mCurrentList;

    QuotationShopListAdapter(Context context, QuotationShopListFragment.OnListFragmentInteractionListener listener) {
        super(DIFF_CALLBACK);
        this.mContext = context;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemQuotationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_quotation, parent, false);

        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        StoreNPredictionEntity item = getItem(position);

        if(item != null) {
            holder.binding.setShopInfo(item);
            holder.itemView.setOnClickListener(v -> mListener.onListFragmentInteraction(item));

        } else {
            Log.d(TAG, "item is null");
//            holder.clear();
        }
    }


    private static DiffUtil.ItemCallback<StoreNPredictionEntity> DIFF_CALLBACK = new DiffUtil.ItemCallback<StoreNPredictionEntity>() {
        @Override
        public boolean areItemsTheSame(@NonNull StoreNPredictionEntity oldItem, @NonNull StoreNPredictionEntity newItem) {
            return oldItem.requestEstimateId == newItem.requestEstimateId;
        }

        @Override
        public boolean areContentsTheSame(@NonNull StoreNPredictionEntity oldItem, @NonNull StoreNPredictionEntity newItem) {
            return oldItem.requestEstimateId.equals(newItem.requestEstimateId);
        }
    };

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ListItemQuotationBinding binding;

        public ItemViewHolder(ListItemQuotationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
