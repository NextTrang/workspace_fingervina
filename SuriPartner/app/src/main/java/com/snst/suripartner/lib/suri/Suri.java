package com.snst.suripartner.lib.suri;

import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.snst.suripartner.BR;
import com.snst.suripartner.labels.LbConst;
import com.snst.suripartner.labels.LbData;
import com.snst.suripartner.labels.LbState;
import com.snst.suripartner.lib.helper.Checker;
import com.snst.suripartner.lib.helper.Conversion;
import com.snst.suripartner.lib.helper.Getter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;

public class Suri extends BaseObservable implements Serializable {
    private static final String TAG = "Suri";
    private String imageUrl;
    private String deviceName;
    private String predictPrice;
    private String price;
    private String id;
    private String userName;
    private String phoneNumber;
    private String pickupAddress;
    private String predictionEstDate;
    private long estCompletedDate;
    private String estCompletedTime;
    private String bookingTime;
    private String state;
    private String timestamp;

    //Using for send Invoice
    private String predictEstId;
    private String stateMask = "";

    //Using for Suri Ongoing Detail
    private int progressRate;
    private String bookerName;

    //Using in Suri Complete tab
    private String score;
    private String predictionEndDate;

    //Using in Suri Waiting & Complete Detail
    private String predictImageUrl_1;
    private String predictImageUrl_2;
    private String repairComment;

    public Suri(SuriViewModel viewModel) {
        this.imageUrl = viewModel.imageUrl;
        this.deviceName = viewModel.deviceName;
        this.predictPrice = viewModel.predictPrice;
        this.price = viewModel.price;
        this.id = viewModel.id;
        this.userName = viewModel.userName;
        this.phoneNumber = viewModel.phoneNumber;
        this.pickupAddress = viewModel.pickupAddress;
        this.estCompletedDate = viewModel.estCompletedDate;
        this.estCompletedTime = viewModel.estCompletedTime;
        this.bookingTime = viewModel.bookingTime;
        this.predictEstId = viewModel.predictEstId;
        this.stateMask = viewModel.stateMask;
        this.timestamp = viewModel.timestamp;
        this.progressRate = viewModel.progressRate;
        this.bookerName = viewModel.bookerName;
        this.score = viewModel.score;
        this.predictionEstDate = viewModel.predictionEstDate;
        this.predictImageUrl_1 = viewModel.predictImageUrl_1;
        this.predictImageUrl_2 = viewModel.predictImageUrl_2;
        this.repairComment = viewModel.repairComment;
        this.predictionEndDate = viewModel.predictionEndDate;
    }

    public Suri(JSONObject jsInfo) {
        try {
            state = jsInfo.getString(LbData.LbInfo.STATUS);
            if (state.equals(LbData.LbInfo.BOOKING_CONFIRM)) {
                state = LbState.INVOICE;
                stateMask = LbState.LbMask.SEND_INVOICE;
            } else if (state.equals(LbData.LbInfo.SENDING_PAYMENT)) {
                state = LbState.INVOICE;
                stateMask = LbState.LbMask.WAITING_CONFIRM;
            } else if (state.equals(LbData.LbInfo.REPAIR_STANDBY)) {
                state = LbState.ONGOING;
                stateMask = LbState.LbMask.START_SURI;
            } else if (state.equals(LbData.LbInfo.REPAIR)) {
                state = LbState.ONGOING;
                stateMask = LbState.LbMask.REPAIR;
            }else if(state.equals(LbData.LbInfo.SENDING_PAYMENT_UPDATE)) {
                state = LbState.ONGOING;
                stateMask = LbState.LbMask.WAITING_CONFIRM;
            }
            else if (state.equals(LbData.LbInfo.COMPLETE)) {
                state = stateMask = LbState.WAITING;
            } else {
                state = stateMask = LbState.COMPLETED;
            }

            imageUrl = LbConst.BASE_LINK + jsInfo.getString(LbData.LbInfo.ESTIMATE_IMAGE_1);
            deviceName = jsInfo.getString(LbData.LbInfo.BRAND_NAME);
            predictPrice = jsInfo.getString(LbData.LbInfo.PREDICTION_PRICE);
            price = jsInfo.getString(LbData.LbInfo.PRICE);
            id = jsInfo.getString(LbData.LbInfo.REQUEST_ESTIMATE_ID);
            userName = jsInfo.getString(LbData.LbInfo.USER_NAME);
            phoneNumber = jsInfo.getString(LbData.LbInfo.BOOKER_PHONE_NUMBER);
            pickupAddress = jsInfo.getString(LbData.LbInfo.ESTIMATE_ADDRESS_1);

            predictionEstDate = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_DATE));
            if (!predictionEstDate.isEmpty()) {
                String[] estDate = predictionEstDate.split(" ");
                estCompletedDate = Conversion.string_2_date(estDate[Getter.posLocalDate]);

                String[] hr_min_sec = estDate[Getter.posLocalTime].split(":");
                estCompletedTime = hr_min_sec[0] + ":" + hr_min_sec[1];
            }

            bookingTime = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.BOOKING_DATE));

            //Using for send Invoice
            predictEstId = jsInfo.getString(LbData.LbInfo.PREDICTION_ESTIMATE_ID);

            //Using for Suri Ongoing Detail
            timestamp = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.REGISTER_DATE));

            String strProgress = jsInfo.getString(LbData.LbInfo.PROGRESS_RATE);
            if (strProgress.isEmpty()) {
                progressRate = 0;
            } else {
                progressRate = Integer.parseInt(strProgress);
            }

            bookerName = jsInfo.getString(LbData.LbInfo.BOOKER_NAME);
            score = jsInfo.getString(LbData.LbInfo.SCORE);

            //Using for Suri Waiting & Complete Detail
            predictImageUrl_1 =LbConst.BASE_LINK + jsInfo.getString(LbData.LbInfo.PREDICTION_IMAGE_1);
            predictImageUrl_2 =LbConst.BASE_LINK + jsInfo.getString(LbData.LbInfo.PREDICTION_IMAGE_2);
            repairComment = jsInfo.getString(LbData.LbInfo.REPAIR_COMMENT);
            predictionEndDate = Getter.getDateTimeFromServer(jsInfo.getString(LbData.LbInfo.PREDICTION_END_DATE));

        } catch (JSONException e) {
            Log.e(TAG, "Suri: " + e.getMessage());
        } catch (ParseException e) {
            Log.e(TAG, "Suri: " + e.getMessage());
        }
    }

    //Getter

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getPredictPrice() {
        return predictPrice;
    }

    public String getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public String getPredictionEstDate() {
        return predictionEstDate;
    }

    @Bindable
    public long getEstCompletedDate() {
        return estCompletedDate;
    }

    @Bindable
    public String getEstCompletedTime() {
        return estCompletedTime;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public String getState() {
        return state;
    }

    public String getPredictEstId() {
        return predictEstId;
    }

    @Bindable
    public String getStateMask() {
        return stateMask;
    }

    public String getTimestamp() {
        return timestamp;
    }

    @Bindable
    public int getProgressRate() {
        return progressRate;
    }

    public String getBookerName() {
        return bookerName;
    }

    public String getScore() {
        return score;
    }

    public String getPredictImageUrl_1() {
        return predictImageUrl_1;
    }

    public String getPredictImageUrl_2() {
        return predictImageUrl_2;
    }

    public String getRepairComment() {
        return repairComment;
    }

    public String getPredictionEndDate() {
        return predictionEndDate;
    }

    //Setter

    public void setEstCompletedDate(long estCompletedDate) {
        this.estCompletedDate = estCompletedDate;
        notifyPropertyChanged(com.snst.suripartner.BR.estCompletedDate);
    }

    public void setEstCompletedTime(String estCompletedTime) {
        this.estCompletedTime = estCompletedTime;
        notifyPropertyChanged(com.snst.suripartner.BR.estCompletedTime);
    }

    public void setStateMask(String stateMask) {
        this.stateMask = stateMask;
        notifyPropertyChanged(com.snst.suripartner.BR.stateMask);
    }

    public void setProgressRate(int progressRate) {
        this.progressRate = progressRate;
        notifyPropertyChanged(com.snst.suripartner.BR.progressRate);
    }
}
