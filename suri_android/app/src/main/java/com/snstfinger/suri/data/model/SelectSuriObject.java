package com.snstfinger.suri.data.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.io.Serializable;

public class SelectSuriObject extends BaseObservable implements Serializable {
    private RequestEstimate requestForm;
    private int step = 1;
    private String content;

    @Bindable
    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;

        notifyPropertyChanged(com.snstfinger.suri.BR.step);
    }

    @Bindable
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;

        notifyPropertyChanged(com.snstfinger.suri.BR.content);
    }

    public RequestEstimate getRequestForm() {
        return requestForm;
    }

    public void setRequestForm(RequestEstimate requestForm) {
        this.requestForm = requestForm;
    }
}
