package com.snstfinger.suri.network;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface RequestEstimateAPI {
    @FormUrlEncoded
    @POST("requestestimate/getAllRequestEstimateNPredictionNStoreByUserIdWithPage")
    Call<CommonResponse> doPostGetAllRequestEstimateNPredictionNStore(@Header("Cookie") String cookie, @Field("user_id") String userId, @Field("page") String page);

    @FormUrlEncoded
    @POST("requestestimate/getRequestEstimateNPredictionNStoreByRequestEstimateId")
    Call<CommonResponse> doPostGetRequestEstimateNPredictionNStore(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId);

    @FormUrlEncoded
    @POST("requestestimate/getAllStoreNPredictionByRequestEstimateIdWithPage")
    Call<CommonResponse> doPostGetAllStoreNPrediction(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId, @Field("page") String page, @Field("orderByClause") String orderByClause);

    @Multipart
    @POST("requestestimate/registerRequestEstimate")
    Call<CommonResponse> doPostRequestEst(@Header("Cookie") String cookie, @PartMap Map<String, RequestBody> requestForm);

    @Multipart
    @POST("requestestimate/uploadImage")
    Call<CommonResponse> doPostUploadFile(@Part MultipartBody.Part file);

    @Multipart
    @POST("requestestimate/uploadImages")
    Call<CommonResponse> doPostUploadMultiFiles(@Part MultipartBody.Part... files);

    @FormUrlEncoded
    @POST("requestestimate/bookingRequestEstimate")
    Call<CommonResponse> doPostBookingRequestEstimate(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId, @Field("prediction_estimate_id") String predictionEstimateId, @Field("booking_date") String bookingDate, @Field("booker_name") String bookerName, @Field("booker_phonenumber") String bookerPhonenumber);

    @FormUrlEncoded
    @POST("requestestimate/getPaymentStatementByRequestEstimateId")
    Call<CommonResponse> doPostGetPaymentStatement(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId);

    @FormUrlEncoded
    @POST("requestestimate/confirmPaymentStatement")
    Call<CommonResponse> doPostConfirmPaymentStatement(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId);

    @FormUrlEncoded
    @POST("requestestimate/reconfirmPaymentStatement")
    Call<CommonResponse> doPostReConfirmPaymentStatement(@Header("Cookie") String cookie, @Field("request_estimate_id") String requestEstimateId);

    @Multipart
    @POST("requestestimate/reviewRepairing")
    Call<CommonResponse> doPostReviewRepairing(@Header("Cookie") String cookie, @PartMap Map<String, RequestBody> reviewForm, @Part MultipartBody.Part file1, @Part MultipartBody.Part file2);

}
