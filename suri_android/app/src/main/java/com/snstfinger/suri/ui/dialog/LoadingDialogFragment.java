package com.snstfinger.suri.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.google.gson.internal.LinkedTreeMap;
import com.snstfinger.suri.R;
import com.snstfinger.suri.data.model.RequestEstimate;
import com.snstfinger.suri.databinding.DialogLoadingBinding;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.RequestEstimateAPI;
import com.snstfinger.suri.util.MySuriInfoSharedPreferences;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadingDialogFragment extends DialogFragment {
    public static final String TAG = LoadingDialogFragment.class.getName();

    public interface OnLoadingResultListener {
        void onLoadingResult(boolean isSuccess);
    }

    private RequestEstimate mRequestForm;
    private DialogLoadingBinding binding;

    OnLoadingResultListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnLoadingResultListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnLoadingResultListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle options = getArguments();
        if (options != null) {
            mRequestForm = (RequestEstimate) options.getSerializable("request_form");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_loading, container, false);
        AnimationDrawable frameAnimate = (AnimationDrawable) binding.loading.getBackground();
        frameAnimate.start();

        askSuri();

        return binding.getRoot();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private void askSuri() {
        RequestEstimateAPI requestEstimateAPI = ApiManager.getProvider(RequestEstimateAPI.class);
        String cookie = CookieManager.getInstance().getCookie("com.snstfinger.suri");

        RequestEstimate formData = mRequestForm;
        // for test
        // formData.setDesiredReservationDate(Utils.getCurrentTime());

        HashMap<String, RequestBody> map = new HashMap<>();
        RequestBody obj = MultipartBody.create(MediaType.parse("text/plain"), formData.getObjectId().toString());
        RequestBody img1 = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateImg1());
        RequestBody brand = MultipartBody.create(MediaType.parse("text/plain"), formData.getBrandName());
        RequestBody model = MultipartBody.create(MediaType.parse("text/plain"), formData.getModelName());
        RequestBody type = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateRepairTypeSelected());
        RequestBody addr = MultipartBody.create(MediaType.parse("text/plain"), formData.getEstimateAddressId1());
        RequestBody id = MultipartBody.create(MediaType.parse("text/plain"), formData.getUserId());
        RequestBody date = MultipartBody.create(MediaType.parse("text/plain"), formData.getDesiredReservationDate());
        map.put("object_id", obj);
        map.put("estimate_image1", img1);
        map.put("brand_name", brand);
        map.put("model_name", model);
        map.put("estimate_repair_type_selected", type);
        map.put("estimate_address_selected_1", addr);
        map.put("user_id", id);
        map.put("desired_reservation_date", date);

        // region optional info
        String path2 = formData.getEstimateImg2();
        if (!TextUtils.isEmpty(path2)) {
            RequestBody img2 = MultipartBody.create(MediaType.parse("text/plain"), path2);
            map.put("estimate_image2", img2);

            String path3 = formData.getEstimateImg3();
            if (!TextUtils.isEmpty(path3)) {
                RequestBody img3 = MultipartBody.create(MediaType.parse("text/plain"), path3);
                map.put("estimate_image3", img3);

                String path4 = formData.getEstimateImg4();
                if (!TextUtils.isEmpty(path4)) {
                    RequestBody img4 = MultipartBody.create(MediaType.parse("text/plain"), path4);
                    map.put("estimate_image4", img4);
                }
            }
        }

        String vehiclePlate = formData.getVehicleNumber();
        if (!TextUtils.isEmpty(vehiclePlate)) {
            RequestBody vehicleNumber = MultipartBody.create(MediaType.parse("text/plain"), vehiclePlate);
            map.put("vehicle_number", vehicleNumber);
        }

        String descript = formData.getEstimateDescription();
        if (!TextUtils.isEmpty(descript)) {
            RequestBody estDesc = MultipartBody.create(MediaType.parse("text/plain"), descript);
            map.put("estimate_description", estDesc);
        }

        String repairOpt = formData.getEstimateRepairOptionSelected();
        if (!TextUtils.isEmpty(repairOpt)) {
            RequestBody option = MultipartBody.create(MediaType.parse("text/plain"), repairOpt);
            map.put("estimate_repair_option_selected", option);
        }

        String address2 = formData.getEstimateAddressId2();
        if (!TextUtils.isEmpty(address2)) {
            RequestBody addr2 = MultipartBody.create(MediaType.parse("text/plain"), address2);
            map.put("estimate_address_selected_2", addr2);
        }
        // endregion

        Call<CommonResponse> call = requestEstimateAPI.doPostRequestEst(cookie, map);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (response.isSuccessful()) {
                    CommonResponse res = response.body();
                    Object data = res.getData();
                    String requestId = ((LinkedTreeMap) data).get("request_estimate_id").toString();
                    Log.d(TAG, res.toString() + "::" + requestId);

                    MySuriInfoSharedPreferences pref = MySuriInfoSharedPreferences.getInstance(getActivity());
                    pref.putReqeustId(requestId);

                    binding.content.setText(R.string.msg_loading_2);
                    binding.content2.setText(R.string.msg_loading_2_sub);

                    try {
                        Thread.sleep(700);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    listener.onLoadingResult(true);
                } else {
                    Log.e(TAG, response.toString());
                    listener.onLoadingResult(false);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "doPostRequestEst onFailure");
                listener.onLoadingResult(false);
            }
        });
    }


    private void testSuri() {
        binding.content.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.content.setText(R.string.msg_loading_2);
                binding.content2.setText(R.string.msg_loading_2_sub);

                listener.onLoadingResult(true);
            }
        }, 2000);
    }
}