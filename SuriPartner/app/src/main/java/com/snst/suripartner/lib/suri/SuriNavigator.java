package com.snst.suripartner.lib.suri;

public interface SuriNavigator {
    void onItemClick(Suri suri);
}
