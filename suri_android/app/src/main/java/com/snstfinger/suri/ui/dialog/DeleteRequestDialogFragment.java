package com.snstfinger.suri.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.fragment.app.DialogFragment;

import com.snstfinger.suri.R;
import com.snstfinger.suri.data.entity.RequestNPredictionNStoreEntity;

public class DeleteRequestDialogFragment extends DialogFragment implements View.OnClickListener {
        private  RequestNPredictionNStoreEntity mMySuri;

    public static DeleteRequestDialogFragment newInstance(RequestNPredictionNStoreEntity item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("my_suri",item);

        DeleteRequestDialogFragment fragment = new DeleteRequestDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle options = getArguments();
        if (options != null) {
            mMySuri = (RequestNPredictionNStoreEntity) options.getSerializable("my_suri");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
//        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);
//        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_delete_request, null);
        root.findViewById(R.id.btn_close).setOnClickListener(this);
        root.findViewById(R.id.btn_cancel).setOnClickListener(this);
        root.findViewById(R.id.btn_yes).setOnClickListener(this);

        builder.setView(root);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE); // compatible 4.4
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    public void dismissDialog() {
        this.dismiss();

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_yes) {

        } else {

        }

        dismissDialog();
    }
}