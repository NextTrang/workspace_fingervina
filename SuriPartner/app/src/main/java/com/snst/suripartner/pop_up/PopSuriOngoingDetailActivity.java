package com.snst.suripartner.pop_up;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.snst.suripartner.R;
import com.snst.suripartner.databinding.SuriOngoingDetailBinding;
import com.snst.suripartner.labels.LbPop;
import com.snst.suripartner.lib.Sharing;
import com.snst.suripartner.lib.suri.Suri;
import com.snst.suripartner.lib.suri.SuriEvent;
import com.snst.suripartner.lib.suri.SuriViewModel;
import com.snst.suripartner.lib.suri_complete.SuriComplete;
import com.snst.suripartner.lib.suri_complete.SuriCompleteEvent;
import com.snst.suripartner.lib.suri_history.SuriHistory;
import com.snst.suripartner.lib.suri_history.SuriHistoryAdapter;
import com.snst.suripartner.lib.suri_history.SuriHistoryEvent;
import com.snst.suripartner.lib.suri_history.SuriHistoryViewModel;

import java.util.ArrayList;
import java.util.List;

public class PopSuriOngoingDetailActivity extends AppCompatActivity {

    private Suri data;

    private RecyclerView rwSuriHistory;
    private List<SuriHistoryViewModel> suriHistories = new ArrayList<>();
    private SuriHistoryAdapter suriHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SuriOngoingDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pop_suri_ongoing_detail);

        data = (Suri) getIntent().getSerializableExtra(LbPop.LbKey.SURI);

        //binding data
        binding.setSuriViewModel(new SuriViewModel(data));
        binding.setSuri(data);
        binding.setHandler(new SuriEvent(this));
        binding.setUser(Sharing.Instance().getUser());
        binding.setEffect(Sharing.Instance().getEffect());

        suriHistories = Sharing.Instance().getSuriHistoryViewModels();
        setUI();

    }

    private void setUI() {
        rwSuriHistory = findViewById(R.id.suriHistory_recyclerview);
        suriHistoryAdapter = new SuriHistoryAdapter(suriHistories);
        rwSuriHistory.setLayoutManager(new LinearLayoutManager(this));
        rwSuriHistory.setAdapter(suriHistoryAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case LbPop.LbCode.WRITE_SURI_HISTORY:
                    SuriHistory suriHistory = (SuriHistory) data.getSerializableExtra(LbPop.LbKey.SURI_HISTORY);
                    new SuriHistoryEvent(this).onWriteSuriHistory(suriHistory);
                    break;
                default:
                    setResult(RESULT_OK, data);
                    finish();
            }
        }
    }

    public void refresh() {
        suriHistoryAdapter.notifyDataSetChanged();
    }
}
