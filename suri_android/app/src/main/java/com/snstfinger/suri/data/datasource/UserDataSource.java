package com.snstfinger.suri.data.datasource;

import android.util.Log;
import android.webkit.CookieManager;

import com.snstfinger.suri.SuriApplication;
import com.snstfinger.suri.data.model.LoggedInUser;
import com.snstfinger.suri.data.model.Result;
import com.snstfinger.suri.network.ApiManager;
import com.snstfinger.suri.network.CommonResponse;
import com.snstfinger.suri.network.LoginProvider;
import com.snstfinger.suri.network.UserAPI;
import com.snstfinger.suri.util.UserInfoSharedPreferences;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class UserDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
//            LoggedInUser fakeUser =
//                    new LoggedInUser(
//                            java.util.UUID.randomUUID().toString(),
//                            "Jane Doe");
            LoggedInUser fakeUser = null;

            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }

    // region suri login
//    private void loginWithSNS(final LoginProvider.OnResultLoginListener onResultLoginListener) {
//        Log.d(TAG, "loginWithSNS::Token::"+ SuriApplication.mSuriToken);
//
//        UserInfoSharedPreferences profile = UserInfoSharedPreferences.getInstance(mContext);
//        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
//        final LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), profile.getUserName(), profile.getUserPhone(), "SNS");
//
//        Call<CommonResponse> call = userAPI.doLogin(user.getUserId(), user.getUserPw(), user.getLoginType(), SuriApplication.mSuriToken);
//        call.enqueue(new Callback<CommonResponse>() {
//            @Override
//            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//
//                if (response.isSuccessful()) {
//                    CommonResponse res = response.body();
//                    Log.d(TAG, res.toString());
//
//                    List<String> cookies = response.raw().headers("Set-Cookie");
//                    if (cookies != null) {
//                        for (String cookie : cookies) {
//                            Log.d(TAG, "cookies::" + cookie);
//                            String sessionid = cookie.split(";\\s*")[0];
//                            if (sessionid.startsWith("starrkCookie")) {
//                                CookieManager.getInstance().setCookie(SuriApplication.APP_URL, sessionid);
//                            }
//                        }
//
//                        String cook = CookieManager.getInstance().getCookie(SuriApplication.APP_URL);
//                        Log.d(TAG, "check SessionId=" + cook);
//                        onResultLoginListener.onResult(LOGIN_SUCCESS, user);
//                    }
//
//                } else {
//                    Log.e(TAG, response.toString());
//                    onResultLoginListener.onResult(LOGIN_FAILED, null);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommonResponse> call, Throwable t) {
//                call.cancel();
//                Log.e(TAG, "API onFailure");
//                onResultLoginListener.onResult(LOGIN_FAILED, null);
//            }
//        });
//
//    }

    // endregion

    // region create SURI account
//    private void registerSuriAccount() {
//        UserAPI userAPI = ApiManager.getProvider(UserAPI.class);
//        LoggedInUser user = new LoggedInUser(profile.getUserId(), profile.getUserEmail(), profile.getUserPw(), profile.getUserName(), profile.getUserPhone(), "SNS");
//        Call<CommonResponse> call = userAPI.doCreateUser(user);
//        call.enqueue(new Callback<CommonResponse>() {
//            @Override
//            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//
//                if (response.isSuccessful()) {
//                    CommonResponse res = response.body();
//                    Log.d(TAG, res.toString());
//
//                    List<String> cookies = response.raw().headers("Set-Cookie");
//                    if (cookies != null) {
//                        for (String cookie : cookies) {
//                            Log.d(TAG, "cookies::" + cookie);
//                            String sessionid = cookie.split(";\\s*")[0];
//                            if (sessionid.startsWith("starrkCookie")) {
//                                CookieManager.getInstance().setCookie("com.snstfinger.suri", sessionid);
//                            }
//                        }
//
//                        String cook = CookieManager.getInstance().getCookie("com.snstfinger.suri");
//                        Log.d(TAG, "check SessionId=" + cook);
//                    }
//
//                } else {
//                    Log.e(TAG, response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommonResponse> call, Throwable t) {
//                call.cancel();
//                Log.e(TAG, "API onFailure");
//            }
//        });
//
//    }
    // endregion
}
