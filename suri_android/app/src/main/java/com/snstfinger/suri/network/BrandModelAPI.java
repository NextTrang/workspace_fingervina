package com.snstfinger.suri.network;

import com.snstfinger.suri.data.model.LoggedInUser;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BrandModelAPI {
    @GET("brand/getBrandByName")
    Call<CommonResponse> doGetBrandByName(@Header("Cookie") String cookie, @Query(value="name",encoded = true) String name, @Query(value = "object_id") String objId);

    @GET("brand/getBrandByNameWithPage")
    Call<CommonResponse> doGetBrandByNameWithPage(@Header("Cookie") String cookie, @Query(value = "page") String page, @Query(value="name",encoded = true) String name, @Query(value = "object_id") String objId);

    @GET("model/getModelByName")
    Call<CommonResponse> doGetModelByName(@Header("Cookie") String cookie, @Query(value="name",encoded = true) String name, @Query(value = "brand_id") String brandId);

    @GET("model/getModelByNameWithPage")
    Call<CommonResponse> doGetModelByNameWithPage(@Header("Cookie") String cookie, @Query(value = "page") String page, @Query(value="name",encoded = true) String name, @Query(value = "brand_id") String brandId);

}
